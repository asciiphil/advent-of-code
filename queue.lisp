(in-package :queue)

;;;; From Okasaki

(defstruct queue
  (size 0)
  (front nil)
  (back nil))

(defun checkf (queue)
  (with-slots (front back) queue
    (if (and (endp front)
             (not (endp back)))
        (make-queue :size (queue-size queue)
                    :front (reverse back)
                    :back nil)
        queue)))

(defun size (queue)
  (queue-size queue))

(defun empty-p (queue)
  (endp (queue-front queue)))

(defun snoc (queue value)
  (checkf (make-queue :size (1+ (queue-size queue))
                      :front (queue-front queue)
                      :back (cons value (queue-back queue)))))

(defun head (queue)
  (car (queue-front queue)))

(defun tail (queue)
  (checkf (make-queue :size (1- (queue-size queue))
                      :front (cdr (queue-front queue))
                      :back (queue-back queue))))
