(in-package :aoc-2023-05)

(aoc:define-day 600279879 20191102)


;;; Data Structures

(defstruct almanac
  seeds
  maps)


;;; Parsing

;;; Input ranges are in a tuple: destination start, source start, length.
;;; I think it'll be easier to process if I use a different tuple instead:
;;; source start (inclusive), source end (exclusive), destination offset.
;;;
;;; Maps will be a tuple, too: source type, dest type, and a list of
;;; ranges, in increasing order by source start.

(parseq:defrule almanac ()
    (and seeds #\Newline (+ map))
  (:choose 0 2)
  (:lambda (seeds maps)
    (make-almanac :seeds seeds :maps maps)))

(parseq:defrule seeds ()
    (and "seeds:" (+ (and " " aoc:integer-string)) (? #\Newline))
  (:choose 1)
  (:lambda (&rest seeds)
    (mapcar #'second seeds)))

(parseq:defrule map ()
    (and (+ (char "a-z")) "-to-" (+ (char "a-z")) " map:" #\Newline (+ range) (? #\Newline))
  (:choose 0 2 5)
  (:lambda (source-name dest-name ranges)
    (list (intern (string-upcase (coerce source-name 'string)))
          (intern (string-upcase (coerce dest-name 'string)))
          (sort ranges #'< :key #'first))))

(parseq:defrule range ()
    (and aoc:integer-string " " aoc:integer-string " " aoc:integer-string (? #\Newline))
  (:choose 0 2 4)
  (:lambda (dst0 src0 len)
    (list src0 (+ src0 len) (- dst0 src0))))


;;; Input

(defparameter *almanac* (aoc:input :parse 'almanac))
(defparameter *example* (aoc:input :parse 'almanac :file "examples/05.txt"))


;;; Part 1

(defun find-category (maps category)
  (find category maps :key #'first))

(defun map-range (ranges id)
  (if (endp ranges)
      id
      (destructuring-bind (start end offset) (car ranges)
        (cond
          ((< id start)
           id)
          ((< id end)
           (+ id offset))
          (t
           (map-range (cdr ranges) id))))))

(defun category-to-next (maps category id)
  "Returns two values: the name of the next category and the id in that category."
  (destructuring-bind (source dest ranges)
      (find-category maps category)
    (declare (ignore source))
    (values dest
            (map-range ranges id))))

(defun category-to-destination (maps dest category id)
  (if (eq category dest)
      id
      (multiple-value-call (alexandria:curry #'category-to-destination maps dest)
        (category-to-next maps category id))))

(defun seed-to-location (maps seed)
  (category-to-destination maps 'location 'seed seed))

(defun get-answer-1 (&optional (almanac *almanac*))
  (with-slots (seeds maps) almanac
    (fset:reduce #'min
                 (fset:image (alexandria:curry #'seed-to-location maps)
                             seeds))))


;;; Part 2

(defun seed-ranges (raw-seeds &optional accumulated-ranges)
  (if (endp raw-seeds)
      (sort accumulated-ranges #'< :key #'first)
      (destructuring-bind (start length &rest remaining-seeds) raw-seeds
          (seed-ranges remaining-seeds
                       (cons (list start (+ start length))
                             accumulated-ranges)))))

(defun merge-ranges (category-ranges)
  (if (endp (cdr category-ranges))
      category-ranges
      (destructuring-bind ((start1 end1) (start2 end2) &rest other-ranges) category-ranges
        (if (<= start2 end1)
            (merge-ranges (cons (list start1 (max end1 end2)) other-ranges))
            (cons (car category-ranges)
                  (merge-ranges (cdr category-ranges)))))))

(defun map-range-on-range (ranges category-ranges &optional accumulated-ranges)
  (if (or (endp ranges)
          (endp category-ranges))
      (merge-ranges (sort (append category-ranges accumulated-ranges) #'< :key #'first))
      (destructuring-bind (range-start range-end range-offset) (car ranges)
        (destructuring-bind (category-start category-end) (car category-ranges)
          (cond
            ((<= range-end category-start)
             ;; No overlap with current range; go to the next one
             (map-range-on-range (cdr ranges)
                                 category-ranges
                                 accumulated-ranges))
            ((<= category-end range-start)
             ;; No overlap with current category; go to the next one
             (map-range-on-range ranges
                                 (cdr category-ranges)
                                 (cons (car category-ranges) accumulated-ranges)))
            ((<= range-start category-start category-end range-end)
             ;; Category fully within range; map and continue with next one
             (map-range-on-range ranges
                                 (cdr category-ranges)
                                 (cons (list (+ category-start range-offset) (+ category-end range-offset))
                                       accumulated-ranges)))
            ((< category-start range-start)
             ;; Some of category is before range; keep that bit and map the remainder
             (map-range-on-range ranges
                                 (cons (list range-start category-end)
                                       (cdr category-ranges))
                                 (cons (list category-start range-start)
                                       accumulated-ranges)))
            ((< range-end category-end)
             ;; Some of category is after range; map the in-range stuff and process the rest next time.
             (map-range-on-range (cdr ranges)
                                 (cons (list range-end category-end)
                                       (cdr category-ranges))
                                 (cons (list (+ category-start range-offset) (+ range-end range-offset))
                                       accumulated-ranges)))
            (t
             (assert nil nil "Missing test case")))))))

(defun maps-in-order-p (maps)
  (and (eq 'seed (first (first maps)))
       (eq 'location (second (fset:last maps)))
       (iter (for (_0 from _1) in maps)
             (for (to _2 _3) in (cdr maps))
             (always (eq from to)))))

(defun map-all-ranges (maps seed-ranges)
  (assert (maps-in-order-p maps))
  (reduce (lambda (category-ranges ranges)
            (map-range-on-range ranges category-ranges))
          (mapcar #'third maps)
          :initial-value seed-ranges))

(defun get-answer-2 (&optional (almanac *almanac*))
  (first (first (map-all-ranges (almanac-maps almanac) (seed-ranges (almanac-seeds almanac))))))
