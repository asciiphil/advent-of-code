(in-package :aoc-2023-09)

(aoc:define-day 1930746032 1154)


;;; Input

(defparameter *sequences* (mapcar #'aoc:extract-ints (aoc:input)))
(defparameter *example*
  '((0 3 6 9 12 15)
    (1 3 6 10 15 21)
    (10 13 16 21 30 45)))


;;; Part 1

(defun constant-sequence-p (sequence)
  (or (endp (cdr sequence))
      (and (= (first sequence) (second sequence))
           (constant-sequence-p (cdr sequence)))))

(defun sequence-differences (sequence)
  (mapcar #'- (cdr sequence) sequence))

(defun extract-coefficients (sequence)
  (cons (car sequence)
        (if (constant-sequence-p sequence)
            nil
            (extract-coefficients (sequence-differences sequence)))))

(defun factorial (n)
  (case n
    (0 0)
    (1 1)
    (t (* n (factorial (1- n))))))

(defun rising-factorial (x n)
  (case n
    (0 0)
    (1 x)
    (t (* x (rising-factorial (1+ x) (1- n))))))

(defun simplex-number (dimension n)
  (if (zerop dimension)
      1
      (/ (rising-factorial n dimension)
         (factorial dimension))))

(defun sequence-value (coefficients n)
  (assert (<= (length coefficients) n))
  (reduce #'+
          (mapcar (lambda (coefficient dimension)
                    (* coefficient (simplex-number dimension (- n dimension))))
                  coefficients
                  (alexandria:iota (length coefficients)))))

(defun sequence-next-value (sequence)
  (sequence-value (extract-coefficients sequence)
                  (1+ (length sequence))))

(defun get-answer-1 (&optional (sequences *sequences*))
  (reduce #'+ (mapcar #'sequence-next-value sequences)))

(aoc:given 1
  (= 114 (get-answer-1 *example*)))


;;; Part 2

(defun sequence-previous-value (sequence)
  (reduce #'- (extract-coefficients sequence) :from-end t))

(defun get-answer-2 (&optional (sequences *sequences*))
  (reduce #'+ (mapcar #'sequence-previous-value sequences)))

(aoc:given 2
  (= 2 (get-answer-2 *example*)))
