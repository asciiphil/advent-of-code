(in-package :aoc-2023-06)

(aoc:define-day 316800 45647654)


;;; Parsing

(defun parse-races (lines)
  "Returns a list of (time distance) pairs."
  (apply (alexandria:curry #'mapcar #'list)
         (mapcar #'aoc:extract-ints lines)))


;;; Input

(defparameter *races* (parse-races (aoc:input)))
(defparameter *example* '((7 9) (15 40) (30 200)))


;;; Part 1

;; Basically the quadratic forumula.  SQRT needs to return a double in
;; order for the answer to have enough precision for part 2.
(defun winning-time-bounds (time distance)
  "For the given time and distance, returns the lower and upper
  button-holding times (as two values) that yield winning distances."
  (let ((t1 (/ (+ (- time) (sqrt (- (expt time 2) (* 4d0 distance))))
               -2))
        (t2 (/ (- (- time) (sqrt (- (expt time 2) (* 4d0 distance))))
               -2)))
    (values (1+ (floor (min t1 t2)))
            (1- (ceiling (max t1 t2))))))

(defun number-of-winning-times (race)
  (destructuring-bind (time distance) race
    (multiple-value-bind (min-time max-time)
        (winning-time-bounds time distance)
      (1+ (- max-time min-time)))))

(defun get-answer-1 (&optional (races *races*))
  (reduce #'* (mapcar #'number-of-winning-times races)))

(aoc:given 1
  (= 288 (get-answer-1 *example*)))


;;; Part 2

(defun combine-numbers (numbers)
  (if (endp (cdr numbers))
      (car numbers)
      (let ((other-number (combine-numbers (cdr numbers))))
        (+ (* (car numbers) (expt 10 (1+ (floor (log other-number 10)))))
           other-number))))

(defun get-answer-2 (&optional (races *races*))
  (let ((time (combine-numbers (mapcar #'first races)))
        (distance (combine-numbers (mapcar #'second races))))
    (number-of-winning-times (list time distance))))

(aoc:given 2
  (= 71503 (get-answer-2 *example*)))
