(in-package :aoc-2023-17)

(aoc:define-day 686 801)


;;; Parsing

(defun char-to-int (char)
  (parse-integer (string char)))

(defun parse-lines (lines)
  (aoc:parse-grid-to-array lines :key #'char-to-int))


;;; Input

(defparameter *map* (parse-lines (aoc:input)))
(defparameter *example*
  (parse-lines '("2413432311323"
                 "3215453535623"
                 "3255245654254"
                 "3446585845452"
                 "4546657867536"
                 "1438598798454"
                 "4457876987766"
                 "3637877979653"
                 "4654967986887"
                 "4564679986453"
                 "1224686865563"
                 "2546548887735"
                 "4322674655533")))


;;; Part 1

(defstruct state
  position
  vector
  straight-blocks)

(defmethod fset:compare ((s1 state) (s2 state))
  (fset:compare-slots s1 s2 'position 'vector 'straight-blocks))

(defun make-next-state-fn (map min-straight max-straight)
  (lambda (state)
    (with-slots (position vector straight-blocks) state
      (let ((result))
        (flet ((add-if-in-bounds (new-vector)
                 (let ((new-position (if (point:= vector new-vector)
                                         (point:+ position new-vector)
                                         (point:+ position (point:* new-vector min-straight)))))
                   (when (and (< -1 (point:x new-position) (array-dimension map 1))
                              (< -1 (point:y new-position) (array-dimension map 0)))
                     (let ((cost (if (point:= vector new-vector)
                                     (point:aref map new-position)
                                     (reduce #'+ (mapcar (lambda (offset)
                                                           (point:aref map (point:+ position (point:* new-vector offset))))
                                                         (alexandria:iota min-straight :start 1))))))
                       (push (list cost
                                   (make-state :position new-position
                                               :vector new-vector
                                               :straight-blocks (if (point:= vector new-vector)
                                                                    (1+ straight-blocks)
                                                                    min-straight)))
                             result))))))
          (add-if-in-bounds (point:turn vector :left))
          (add-if-in-bounds (point:turn vector :right))
          (when (< straight-blocks max-straight)
            (add-if-in-bounds vector)))
        result))))

(defun make-finished-fn (map)
  (let ((end-pos (point:make-point (1- (array-dimension map 1))
                                   (1- (array-dimension map 0)))))
    (lambda (state)
      (point:= end-pos (state-position state)))))

(defun find-crucible-path (map min-straight max-straight)
  (aoc:shortest-path (make-state :position #<0 0>
                                 :vector #<1 0>
                                 :straight-blocks 0)
                     (make-next-state-fn map min-straight max-straight)
                     :finishedp (make-finished-fn map)))

(defun get-answer-1 (&optional (map *map*))
  (nth-value 1 (find-crucible-path map 1 3)))

(aoc:given 1
  (= 102 (get-answer-1 *example*)))


;;; Part 2

(defun get-answer-2 (&optional (map *map*))
  (nth-value 1 (find-crucible-path map 4 10)))

(aoc:given 2
  (= 94 (get-answer-2 *example*)))
