(in-package :aoc-2023-10)

(aoc:define-day 6701 303)


;;; Parsing

(defparameter +char-to-symbol-map+
  (fset:map (#\| :ns)
            (#\- :ew)
            (#\L :ne)
            (#\J :nw)
            (#\7 :sw)
            (#\F :se)
            (#\S :start)))

(defun parse-char (string col)
  (fset:lookup +char-to-symbol-map+ (schar string col)))

(defun parse-line (line row)
  (reduce (lambda (result col)
            (let ((symbol (parse-char line col)))
              (if symbol
                  (fset:with result (point:make-point col row) symbol)
                  result)))
          (alexandria:iota (length line))
          :initial-value (fset:empty-map)))

(defun parse-lines (lines)
  (reduce #'fset:map-union
          (mapcar #'parse-line
                  lines
                  (alexandria:iota (length lines)))))


;;; Input

(defparameter *map* (parse-lines (aoc:input)))
(defparameter *example-1*
  (parse-lines '("-L|F7"
                 "7S-7|"
                 "L|7||"
                 "-L-J|"
                 "L|-JF")))
(defparameter *example-2*
  (parse-lines '("7-F7-"
                 ".FJ|7"
                 "SJLL7"
                 "|F--J"
                 "LJ.LJ")))


;;; Part 1

(defparameter +neighbor-vectors+
  (let ((north #< 0 -1>)
        (south #< 0  1>)
        (east  #< 1  0>)
        (west  #<-1  0>))
    (fset:map (:ns (list north south (fset:set east) (fset:set west)))
              (:ew (list west east (fset:set north) (fset:set south)))
              (:ne (list north east (fset:empty-set) (fset:set south west)))
              (:nw (list north west (fset:set east south) (fset:empty-set)))
              (:sw (list south west (fset:empty-set) (fset:set north east)))
              (:se (list south east (fset:set north west) (fset:empty-set)))))
  "List with elements: start (point), end (point), left (point set), right (point set)")
(defparameter +neighbor-symbols+
  (fset:reduce (lambda (result symbol vectors)
                 (fset:with result
                            (fset:set (first vectors) (second vectors))
                            symbol))
               +neighbor-vectors+
               :initial-value (fset:empty-map)))

(defun tile-neighbors (map pos)
  "Returns three values: a list (start end), a set of left neighbors, and a
  set of right neighbors."
  (let ((vectors (fset:lookup +neighbor-vectors+ (fset:lookup map pos))))
    (if (null vectors)
        (values nil (fset:empty-set) (fset:empty-set))
        (destructuring-bind (start end left right) vectors
          (values (fset:image (alexandria:curry #'point:+ pos)
                              (list start end))
                  (fset:image (alexandria:curry #'point:+ pos)
                              left)
                  (fset:image (alexandria:curry #'point:+ pos)
                              right))))))

(defun find-start (map)
  (fset:do-map (pos symbol map)
    (when (eq symbol :start)
      (return-from find-start pos))))

(defun fix-start (map)
  (let* ((start-pos (find-start map))
         (neighbors (fset:filter (lambda (neighbor)
                                   (and (tile-neighbors map neighbor)
                                        (fset:lookup (fset:convert 'fset:set (tile-neighbors map neighbor))
                                                     start-pos)))
                                 (point:neighbor-set start-pos)))
         (neighbor-vectors (fset:image (alexandria:rcurry #'point:- start-pos)
                                       neighbors))
         (correct-symbol (fset:lookup +neighbor-symbols+ neighbor-vectors)))
    (fset:with map start-pos correct-symbol)))

(defun next-tile (map pos last-pos)
  "Returns three values: The next position, the tiles on the left, and the tiles on the right."
  (multiple-value-bind (endpoints left right)
      (tile-neighbors map pos)
    (destructuring-bind (start end) endpoints
      (assert (or (point:= last-pos start)
                  (point:= last-pos end))
              nil
              "Coming from ~A to ~A, but the latter's endpoints are ~A"
              last-pos pos endpoints)
      (if (point:= last-pos start)
          (values end left right)
          (values start right left)))))

(defun map-loop (map)
  "Returns three values, all sets: the points that are part of the loop, the
  points immediately to the left of the loop, and the points immediately
  to the right of the loop.

  Note that 'left' and 'right' are arbitrary, depending on the direction
  of the loop traversal."
  (let ((start-pos (find-start map)))
    (multiple-value-bind (start-neighbors start-left start-right)
        (tile-neighbors (fix-start map) start-pos)
      (labels ((follow-loop (pos last-pos loop-tiles left-tiles right-tiles)
                 (if (point:= pos start-pos)
                     (values loop-tiles
                             (fset:set-difference left-tiles loop-tiles)
                             (fset:set-difference right-tiles loop-tiles))
                     (multiple-value-bind (next-pos left right)
                         (next-tile map pos last-pos)
                       (follow-loop next-pos
                                    pos
                                    (fset:with loop-tiles pos)
                                    (fset:union left left-tiles)
                                    (fset:union right right-tiles))))))
        (follow-loop (second start-neighbors)
                     start-pos
                     (fset:set start-pos)
                     start-left
                     start-right)))))

(defun get-answer-1 (&optional (map *map*))
  (/ (fset:size (map-loop map)) 2))

(aoc:given 1
  (= 4 (get-answer-1 *example-1*))
  (= 8 (get-answer-1 *example-2*)))


;;; Part 2

(defun tiles-inside-loop-p (loop-tiles tiles)
  (multiple-value-bind (loop-min loop-max) (point:bbox loop-tiles)
    (multiple-value-bind (tile-min tile-max) (point:bbox tiles)
      (point:<= loop-min tile-min tile-max loop-max))))

(defun buffer-tile-set (loop-tiles tiles)
  "Expands TILES by one tile in each direction, bounded by LOOP-TILES."
  (fset:set-difference (fset:reduce (lambda (result tile)
                                      (fset:union result (point:neighbor-set tile)))
                                    tiles
                                    :initial-value tiles)
                       loop-tiles))

(defun select-inner-tiles (loop-tiles tiles1 tiles2)
  (cond
    ((not (tiles-inside-loop-p loop-tiles tiles1))
     tiles2)
    ((not (tiles-inside-loop-p loop-tiles tiles2))
     tiles1)
    (t
     (select-inner-tiles loop-tiles
                         (buffer-tile-set loop-tiles tiles1)
                         (buffer-tile-set loop-tiles tiles2)))))

(defun buffer-all-inner-tiles (loop-tiles inner-tiles)
  (let ((new-tiles (buffer-tile-set loop-tiles inner-tiles)))
    (if (= (fset:size inner-tiles) (fset:size new-tiles))
        inner-tiles
        (buffer-all-inner-tiles loop-tiles new-tiles))))

(defun find-inner-tiles (map)
  (multiple-value-bind (loop-tiles left-tiles right-tiles)
      (map-loop map)
    (buffer-all-inner-tiles loop-tiles (select-inner-tiles loop-tiles left-tiles right-tiles))))

(defun get-answer-2 (&optional (map *map*))
  (fset:size (find-inner-tiles map)))


;;; Visualization

(defun draw-map (map)
  (aoc:draw-map-svg-grid
   map
   :key (lambda (symbol)
          (case symbol
            (:ew '(:shape :line))
            (:ns '(:shape :line :rotation 90))
            (:ne '(:shape :corner))
            (:se '(:shape :corner :rotation 90))
            (:sw '(:shape :corner :rotation 180))
            (:nw '(:shape :corner :rotation 270))
            (:start '(:shape :circle :color :green))
            (t symbol)))))
