(in-package :aoc-2023-15)

(aoc:define-day 516657 210906)


;;; Parsing

(defun split-instructions (instruction-string)
  (ppcre:split "," instruction-string))


;;; Input

(defparameter *instructions* (split-instructions (aoc:input)))
(defparameter *example*
  (split-instructions "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"))


;;; Part 1

(defun hash (string)
  (labels ((hash-r (index current-value)
             (if (<= (length string) index)
                 current-value
                 (hash-r (1+ index)
                         (mod (* 17 (+ current-value (char-code (schar string index))))
                              256)))))
    (hash-r 0 0)))

(aoc:deftest hash
  (5am:is (= 52 (hash "HASH"))))

(defun get-answer-1 (&optional (instructions *instructions*))
  (reduce #'+ (mapcar #'hash instructions)))


;;; Part 2

(defun parse-instruction (string)
  "Returns a list of three elements:  box number, label, and focal length.
  Focal length may be NIL.

  If focal length is non-NIL, the label and focal length should be added
  to the box.  If focal length is NIL, the label should be removed from
  the box."
  (let ((parts (ppcre:split "=" string)))
    (if (= 1 (length parts))
        (let ((label (subseq string 0 (1- (length string)))))
          (assert (char= #\- (schar string (1- (length string)))))
          (list (hash label) (intern (string-upcase label)) nil))
        (destructuring-bind (label focal-length-str) parts
          (list (hash label) (intern (string-upcase label)) (parse-integer focal-length-str))))))

(defun add-lens (boxes box label lens)
  (let* ((lenses (fset:lookup boxes box))
         (lens-position (fset:position label lenses :key #'first)))
    (fset:with boxes box
               (if lens-position
                   (fset:with lenses lens-position (list label lens))
                   (fset:with-last lenses (list label lens))))))

(defun remove-lens (boxes box label)
  (let* ((lenses (fset:lookup boxes box))
         (lens-position (fset:position label lenses :key #'first)))
    (if lens-position
        (fset:with boxes box
                   (fset:less lenses lens-position))
        boxes)))

(defun apply-instruction (boxes instruction)
  (destructuring-bind (box label lens) instruction
    (if lens
        (add-lens boxes box label lens)
        (remove-lens boxes box label))))

(defun apply-instruction-strings (instruction-strings)
  (reduce #'apply-instruction
          (mapcar #'parse-instruction instruction-strings)
          :initial-value (fset:empty-map (fset:empty-seq))))

(defun focusing-power (boxes)
  (fset:reduce (lambda (sum box lenses)
                 (+ sum
                    (gmap:gmap :sum
                               (lambda (index lens-tuple)
                                 (* (1+ box) index (second lens-tuple)))
                               (:index 1)
                               (:seq lenses))))
               boxes
               :initial-value 0))

(defun get-answer-2 (&optional (instructions *instructions*))
  (focusing-power (apply-instruction-strings instructions)))
