(in-package :aoc-2023-08)

(aoc:define-day 19951 16342438708751)


;;; Parsing

(parseq:defrule path ()
    (+ direction))

(parseq:defrule direction ()
    (char "LR")
  (:string)
  (:function #'intern))

(parseq:defrule full-node ()
    (and node " = (" node ", " node ")" (? #\Newline))
  (:choose 0 2 4)
  (:lambda (node left right)
    (fset:map (node (list left right)))))

(parseq:defrule node ()
    (+ (char "A-Z1-9"))
  (:string)
  (:function #'intern))

(defun parse-path (input)
  (parseq:parseq 'path (car input)))

(defun parse-network (input)
  (reduce #'fset:map-union
          (mapcar (alexandria:curry #'parseq:parseq 'full-node)
                  (nthcdr 2 input))))

;;; Input

(defparameter *path* (parse-path (aoc:input)))
(defparameter *network* (parse-network (aoc:input)))
(defparameter *example-path-1* (parse-path (aoc:input :file "examples/08-1.txt")))
(defparameter *example-network-1* (parse-network (aoc:input :file "examples/08-1.txt")))
(defparameter *example-path-2* (parse-path (aoc:input :file "examples/08-2.txt")))
(defparameter *example-network-2* (parse-network (aoc:input :file "examples/08-2.txt")))
(defparameter *example-path-3* (parse-path (aoc:input :file "examples/08-3.txt")))
(defparameter *example-network-3* (parse-network (aoc:input :file "examples/08-3.txt")))


;;; Part 1

(defparameter *end-rules* nil
  "Set to either 1 or 2 to select the rules used to determine path termination.")

(defun next-node (network direction node)
  (nth (ecase direction
         (l 0)
         (r 1))
       (fset:lookup network node)))

(defun node-end-p (node)
  (case *end-rules*
    (1 (eq 'zzz node))
    (2 (let ((name-string (string node)))
         (char= #\Z (schar name-string (1- (length name-string))))))
    (t (assert nil nil "*END-RULES* was not bound"))))

(defun follow-path (network full-path current-node &optional current-path (steps 0))
  (cond
    ((node-end-p current-node)
     steps)
    ((endp current-path)
     (follow-path network full-path current-node full-path steps))
    (t
     (follow-path network
                  full-path
                  (next-node network (car current-path) current-node)
                  (cdr current-path) 
                  (1+ steps)))))

(defun get-answer-1 (&optional (network *network*) (path *path*))
  (let ((*end-rules* 1))
    (follow-path network path 'aaa)))

(aoc:given 1
  (= 2 (get-answer-1 *example-network-1* *example-path-1*))
  (= 6 (get-answer-1 *example-network-2* *example-path-2*)))


;;; Part 2

(defun node-start-p (node)
  (let ((name-string (string node)))
    (char= #\A (schar name-string (1- (length name-string))))))

(defun get-answer-2 (&optional (network *network*) (path *path*))
  (let ((*end-rules* 2))
    (fset:reduce #'lcm
                 (fset:image (alexandria:curry #'follow-path network path)
                             (fset:filter #'node-start-p (fset:domain network))))))

(aoc:given 2
  (= 6 (get-answer-2 *example-network-3* *example-path-3*)))
