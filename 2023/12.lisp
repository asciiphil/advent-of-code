(in-package :aoc-2023-12)

(aoc:define-day 7350 200097286528151)


;;; Parsing

(parseq:defrule record ()
    (and gears " " blocks)
  (:choose 0 2))

(parseq:defrule gears ()
    (+ gear))

(parseq:defrule gear ()
    (char "#.?")
  (:lambda (c)
    (ecase c
      (#\# :damaged)
      (#\. :undamaged)
      (#\? :unknown))))

(parseq:defrule blocks ()
    (aoc:comma-list aoc:integer-string))


;;; Input

(defparameter *records* (aoc:input :parse-line 'record))
(defparameter *example*
  (mapcar (alexandria:curry #'parseq:parseq 'record)
          '("???.### 1,1,3"
            ".??..??...?##. 1,1,3"
            "?#?#?#?#?#?#?#? 1,3,1,6"
            "????.#...#... 4,1,1"
            "????.######..#####. 1,6,5"
            "?###???????? 3,2,1")))


;;; Part 1

(defun min-spaces-remaining (blocks)
  (if (endp blocks)
      0
      (+ (reduce #'+ blocks)
         (1- (length blocks)))))

;; BLOCKS is a list of block of *damaged* springs.
(aoc:defmemo count-variants (gears blocks current-block)
  (cond
    ((< (length gears) (min-spaces-remaining (if current-block
                                                 (cons current-block blocks)
                                                 blocks)))
     0)                     ; Not enough gears left to satisfy block list.
    ((endp gears)
     (if (and (endp blocks)
              (or (null current-block)
                  (zerop current-block)))
         1                   ; All lists ended together; no conflicts
         0))                 ; More blocks, but no more gears for them
    ((and (endp blocks)      ; End of blocks; only undamaged gears allowed
          (null current-block))
     (if (eq :damaged (car gears))
         0
         (count-variants (cdr gears)
                         blocks
                         nil)))
    (t
     (ecase (car gears)
       (:damaged                        ; Can we handle a damaged gear?
        (if (or (and (numberp current-block) ; No; current block just ended
                     (zerop current-block))
                (and (null current-block) ; No; no current block and no more blocks
                     (endp blocks)))
            0
            (if (null current-block)
                (count-variants (cdr gears) ; Yes; start a new block
                                (cdr blocks)
                                (1- (car blocks)))
                (count-variants (cdr gears) ; Yes; follow the current block
                                blocks
                                (1- current-block)))))
       (:undamaged                      ; Can we handle an undamaged gear?
        (if (and (numberp current-block) ; No; currently in the middle of an damaged block
                 (plusp current-block))
            0
            (count-variants (cdr gears) ; Yes; look at next gear
                            blocks
                            nil)))
       (:unknown                    ; What do we do with an unknown block?
        (if (numberp current-block)
            (if (plusp current-block)       ; Currently in a block...
                (count-variants (cdr gears) ; ...in the middle; must be damaged
                                blocks
                                (1- current-block))
                (count-variants (cdr gears) ; ...at the end; must be undamaged
                                blocks
                                nil))
             (+ (count-variants (cdr gears) ; Not in a block; try both options
                                (cdr blocks)
                                (1- (car blocks)))
                (count-variants (cdr gears)
                                blocks
                                nil))))))))

(defun get-answer-1 (&optional (records *records*))
  (reduce #'+
          (lparallel:pmapcar (lambda (record)
                               (destructuring-bind (gears blocks) record
                                 (count-variants gears blocks nil)))
                             records)))


;;; Part 2

(defun unfold-gears (gears)
  (append gears '(:unknown) gears '(:unknown) gears '(:unknown) gears '(:unknown) gears))

(defun unfold-blocks (blocks)
  (append blocks blocks blocks blocks blocks))

(defun unfold-record (record)
  (destructuring-bind (gears blocks) record
    (list (unfold-gears gears)
          (unfold-blocks blocks))))

(defun get-answer-2 (&optional (records *records*))
  (get-answer-1 (mapcar #'unfold-record records)))
