(in-package :aoc-2023-16)

;; Part two is 7521, but it takes about four minutes on my laptop.
(aoc:define-day 7236 nil)


;;; Parsing

(defun layout-char (char)
  (ecase char
    (#\| :vertical)
    (#\- :horizontal)
    (#\\ :nw-se)
    (#\/ :ne-sw)
    (#\. nil)))

(defun parse-lines (lines)
  (aoc:parse-grid-to-map lines :key #'layout-char))


;;; Input

(defparameter *contraption* (parse-lines (aoc:input)))
(defparameter *example*
  (parse-lines '(".|...\\...."
                 "|.-.\\....."
                 ".....|-..."
                 "........|."
                 ".........."
                 ".........\\"
                 "..../.\\\\.."
                 ".-.-/..|.."
                 ".|....-|.\\"
                 "..//.|....")))


;;; Part 1

(defun out-of-bounds (points point)
  (multiple-value-bind (min-point max-point) (point:bbox points)
    (or (< (point:x point) (point:x min-point))
        (< (point:y point) (point:y min-point))
        (< (point:x max-point) (point:x point))
        (< (point:y max-point) (point:y point)))))

(defun next-states (contraption position vector)
  (case (fset:lookup contraption position)
    (:vertical
     (if (zerop (point:x vector))
         (fset:set (cons (point:+ position vector) vector))
         (fset:set (cons (point:+ position #<0  1>) #<0  1>)
                   (cons (point:+ position #<0 -1>) #<0 -1>))))
    (:horizontal
     (if (zerop (point:y vector))
         (fset:set (cons (point:+ position vector) vector))
         (fset:set (cons (point:+ position #< 1 0>) #< 1 0>)
                   (cons (point:+ position #<-1 0>) #<-1 0>))))
    (:nw-se
     (let ((new-vector (point:make-point (point:y vector) (point:x vector))))
       (fset:set (cons (point:+ position new-vector) new-vector))))
    (:ne-sw
     (let ((new-vector (point:make-point (- (point:y vector)) (- (point:x vector)))))
       (fset:set (cons (point:+ position new-vector) new-vector))))
    (t
     (assert (null (fset:lookup contraption position)))
     (fset:set (cons (point:+ position vector) vector)))))

(defun follow-beam (contraption &optional (state (cons #<0 0> #<1 0>)) (visited-states (fset:empty-set)))
  (destructuring-bind (position . vector) state
    (if (or (fset:lookup visited-states state)
            (out-of-bounds (fset:domain contraption) position))
        visited-states
        (fset:reduce (lambda (new-visited new-state)
                       (follow-beam contraption new-state new-visited))
                     (next-states contraption position vector)
                     :initial-value (fset:with visited-states state)))))

(defun get-answer-1 (&optional (contraption *contraption*))
  (fset:size (fset:image #'car (follow-beam contraption))))

(aoc:given 1
  (= 46 (get-answer-1 *example*)))


;;; Part 2

(defun start-states (contraption)
  (let* ((max-point (nth-value 1 (point:bbox (fset:domain contraption))))
         (max-x (point:x max-point))
         (max-y (point:y max-point)))
    (reduce #'fset:union
            (mapcar #'fset:union
                    (mapcar (lambda (x)
                              (fset:set (cons (point:make-point x 0) #<0 1>)
                                        (cons (point:make-point x max-y) #<0 -1>)))
                            (alexandria:iota (1+ max-x)))
                    (mapcar (lambda (y)
                              (fset:set (cons (point:make-point 0 y) #<1 0>)
                                        (cons (point:make-point max-x y) #<-1 0>)))
                            (alexandria:iota (1+ max-y)))))))

(defun find-best-beam (contraption)
  (let ((best-state)
        (best-tiles (fset:empty-set)))
    (fset:do-set (state (start-states contraption))
      (let ((tiles (fset:image #'car (follow-beam contraption state))))
        (when (< (fset:size best-tiles) (fset:size tiles))
          (setf best-state state
                best-tiles tiles))))
    (values best-tiles best-state)))

(defun find-best-beam-size (contraption)
  "Faster than `find-best-beam' because it checks the starting points in parallel."
  (reduce #'max
          (lparallel:pmapcar (lambda (state) (fset:image #'car (follow-beam contraption state)))
                             (fset:convert 'list (start-states contraption)))
          :key #'fset:size))

(defun get-answer-2 (&optional (contraption *contraption*))
  (find-best-beam-size contraption))

(aoc:given 2
  (= 51 (get-answer-2 *example*)))
