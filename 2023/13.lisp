(in-package :aoc-2023-13)

(aoc:define-day 30158 36474)


;;; Parsing

(defun parse-line (line row)
  (reduce (lambda (result col)
            (if (char= #\# (schar line col))
                (fset:with result (point:make-point col row))
                result))
          (alexandria:iota (length line))
          :initial-value (fset:empty-set)))

(defun parse-patterns (lines &optional (current-row 0) (current-points (fset:empty-set)))
  (cond
    ((endp lines)
     (list current-points))
    ((string= "" (car lines))
     (cons current-points (parse-patterns (cdr lines))))
    (t
     (parse-patterns (cdr lines)
                     (1+ current-row)
                     (fset:union current-points (parse-line (car lines) current-row))))))


;;; Input

(defparameter *patterns* (parse-patterns (aoc:input)))
(defparameter *example* (parse-patterns (aoc:input :file "examples/13.txt")))


;;; Part 1

(defun row-symmetric-p (pattern dimension row cell)
  (multiple-value-bind (min-point max-point) (point:bbox pattern)
    (do ((l cell (1- l))
         (r (1+ cell) (1+ r)))
        ((or (< l (point:elt min-point dimension))
             (< (point:elt max-point dimension) r))
         t)
      (let ((point-l (if (zerop dimension)
                         (point:make-point l row)
                         (point:make-point row l)))
            (point-r (if (zerop dimension)
                         (point:make-point r row)
                         (point:make-point row r))))
        (unless (eq (fset:lookup pattern point-l)
                    (fset:lookup pattern point-r))
          (return-from row-symmetric-p nil))))))

(defun axis-symmetric-p (pattern dimension cell)
  (multiple-value-bind (min-point max-point) (point:bbox pattern)
    (do ((row (point:elt min-point (mod (1+ dimension) 2))
              (1+ row)))
        ((< (point:elt max-point (mod (1+ dimension) 2)) row)
         t)
      (unless (row-symmetric-p pattern dimension row cell)
        (return-from axis-symmetric-p nil)))))

(defun find-reflection-column (pattern dimension)
  (multiple-value-bind (min-point max-point) (point:bbox pattern)
    (let ((result (fset:empty-set)))
      (do ((col (point:elt min-point dimension)
                (1+ col)))
          ((<= (point:elt max-point dimension) col)
           nil)
        (when (axis-symmetric-p pattern dimension col)
          (fset:adjoinf result col)))
      result)))

(defun find-reflection-point (pattern)
  (let ((results (fset:union (fset:image #'1+ (find-reflection-column pattern 0))
                             (fset:image (lambda (r) (* 100 (1+ r)))
                                         (find-reflection-column pattern 1)))))
    (cond
      ((fset:empty? results)
       nil)
      ((= 1 (fset:size results))
       (fset:arb results))
      (t
       results))))

(defun get-answer-1 (&optional (patterns *patterns*))
  (reduce #'+ (mapcar #'find-reflection-point patterns)))

(aoc:given 1
  (= 405 (get-answer-1 *example*)))


;;; Part 2

(defun fix-smudge (pattern)
  (let ((max-point (nth-value 1 (point:bbox pattern)))
        (original-point (find-reflection-point pattern)))
    (dotimes (x (1+ (point:x max-point)))
      (dotimes (y (1+ (point:y max-point)))
        (let* ((point (point:make-point x y))
               (new-pattern (if (fset:lookup pattern point)
                                (fset:less pattern point)
                                (fset:with pattern point)))
               (reflection-point (find-reflection-point new-pattern)))
          (when (and reflection-point
                     (not (eql reflection-point original-point)))
            (return-from fix-smudge (if (fset:set? reflection-point)
                                        (fset:arb (fset:less reflection-point original-point))
                                        reflection-point))))))
    (aoc:draw-set-svg-grid pattern)
    (:printv max-point original-point)
    (assert nil)))

(defun get-answer-2 (&optional (patterns *patterns*))
  (reduce #'+ (mapcar #'fix-smudge patterns)))

(aoc:given 2
  (= 400 (get-answer-2 *example*)))
