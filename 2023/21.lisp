(in-package :aoc-2023-21)

(aoc:define-day 3847 nil)


;;; Parsing

(defun garden-key-fn (char)
  (ecase char
    (#\# nil)  ; Rock
    (#\S :start) 
    (#\. :plot)))


;;; Input

(defparameter *garden* (aoc:parse-grid-to-map (aoc:input) :key #'garden-key-fn))
(defparameter *example*
  (aoc:parse-grid-to-map
   '("..........."
     ".....###.#."
     ".###.##..#."
     "..#.#...#.."
     "....#.#...."
     ".##..S####."
     ".##..#...#."
     ".......##.."
     ".##.#.####."
     ".##..##.##."
     "...........")
   :key #'garden-key-fn))


;;; Part 1

(defun start-pos (garden)
  (fset:lookup (aoc:invert-map garden) :start))

(defun valid-steps-from-point (plots position)
  (fset:intersection (point:neighbor-set position)
                     plots))

(defun next-steps (plots positions)
  (fset:reduce (lambda (result position)
                 (fset:union result (valid-steps-from-point plots position)))
               positions
               :initial-value (fset:empty-set)))

(defun take-steps (plots positions count)
  (if (zerop count)
      positions
      (take-steps plots (next-steps plots positions) (1- count))))

(defun garden-take-steps (garden steps)
  (take-steps (fset:domain garden) (fset:set (start-pos garden)) steps))

(defun get-answer-1 (&optional (garden *garden*) (steps 64))
  (fset:size (garden-take-steps garden steps)))

(aoc:given 1
  (= 16 (get-answer-1 *example* 6)))


;;; Part 2

(defparameter *steps* 26501365)

(defun rockp (plots point)
  (multiple-value-bind (min-point max-point) (point:bbox plots)
    (assert (point:= #<0 0> min-point))
    (let ((clamped-point (point:make-point (mod (point:x point) (1+ (point:x max-point)))
                                           (mod (point:y point) (1+ (point:y max-point))))))
      (not (fset:lookup plots clamped-point)))))

(defun odd-steps-p (start target)
  (plusp (mod (point:manhattan-distance start target) 2)))

(defun count-odd-step-plots (garden)
  (fset:size (fset:filter (alexandria:curry #'odd-steps-p (start-pos garden))
                          (fset:domain garden))))

(defun count-garden-areas (garden steps)
  (multiple-value-bind (min-point max-point) (point:bbox (fset:domain garden))
    (assert (point:= #<0 0> min-point))
    (assert (= (point:x max-point) (point:y max-point)))
    (let* ((garden-width (1+ (point:x max-point)))
           (half-garden-width (/ (1- garden-width) 2))
           (half-area-gardens (/ (- steps half-garden-width) garden-width))
           (area-garden-width (1+ (* 2 half-area-gardens))))
      (assert (integerp area-garden-width))
      (- (expt area-garden-width 2)
         (* 4 (/ (* half-area-gardens (1+ half-area-gardens))
                 2))))))

;;; Visualization

(defun select-point (fn p1 p2)
  (if (funcall fn p1 p2)
      p1
      (progn
        (assert (funcall fn p2 p1))
        p2)))

(defun draw-steps (garden steps)
  (let ((reachable (garden-take-steps garden steps))
        (plots (fset:domain garden)))
    (multiple-value-bind (garden-min-point garden-max-point) (point:bbox (fset:domain garden))
      (multiple-value-bind (reachable-min-point reachable-max-point) (point:bbox reachable)
        (let ((min-point (select-point #'point:<= garden-min-point reachable-min-point))
              (max-point (if (point:<= garden-max-point reachable-max-point)
                             reachable-max-point
                             (progn
                               (assert (point:< reachable-max-point garden-max-point))
                               garden-max-point))))
          (aoc:draw-svg-grid (1+ (- (point:x max-point) (point:x min-point)))
                             (1+ (- (point:y max-point) (point:y min-point)))
                             (lambda (point)
                               (cond
                                 ((fset:lookup reachable (point:+ min-point point))
                                  '(:shape :square :color :green))
                                 ((rockp plots (point:+ min-point point))
                                  '(:shape :circle :color :dark-secondary))
                                 (t nil)))
                             :cell-size 6))))))
