(in-package :aoc-2023-18)

(aoc:define-day 76387 250022188522074)


;;; Parsing

(parseq:defrule edge ()
    (and direction " " aoc:integer-string " (" color ")")
  (:choose 0 2 4))

(parseq:defrule direction ()
    (char "UDLR")
  (:lambda (c)
    (ecase c
      (#\U :up)
      (#\D :down)
      (#\L :left)
      (#\R :right))))

(parseq:defrule color ()
    (and "#" (+ (char "0-9a-f")))
  (:choose 1)
  (:string))


;;; Input

(defparameter *edges* (aoc:input :parse-line 'edge))
(defparameter *example*
  (mapcar (alexandria:curry #'parseq:parseq 'edge)
          '("R 6 (#70c710)"
            "D 5 (#0dc571)"
            "L 2 (#5713f0)"
            "D 2 (#d2c081)"
            "R 2 (#59c680)"
            "D 2 (#411b91)"
            "L 5 (#8ceee2)"
            "U 2 (#caa173)"
            "L 1 (#1b58a2)"
            "U 2 (#caa171)"
            "R 2 (#7807d2)"
            "U 3 (#a77fa3)"
            "L 2 (#015232)"
            "U 2 (#7a21e3)")))


;;; Part 1

(defun follow-edge (edge position)
  (destructuring-bind (direction distance &optional color) edge
    (declare (ignore color))
    (let ((vector (ecase direction
                    (:up    #< 0 -1>)
                    (:down  #< 0  1>)
                    (:left  #<-1  0>)
                    (:right #< 1  0>))))
      (point:+ position (point:* vector distance)))))

(defun edges-to-path (edges)
  (labels ((build-path (remaining-edges position)
             (if (endp remaining-edges)
                 (list position)
                 (cons position (build-path (cdr remaining-edges)
                                            (follow-edge (car remaining-edges) position))))))
    (build-path edges #<0 0>)))

;; Okay, so this is kind of a hack.  The second `reduce' clause is the
;; classic "area enclosed by a polygon" algorithm.  But we also have to
;; take the width of the edge into account, so we add that in, too
;; (divided in half, because the inside half of the line is accounted for
;; already).  Finally, there's an extra 1 because the curvature of the
;; line adds an extra unit square of area.
(defun path-area (path)
  (1+ (/ (+ (reduce #'+
                    (mapcar #'point:manhattan-distance
                            path
                            (cdr path)))
            (reduce #'+
                    (mapcar (lambda (v1 v2)
                              (- (* (point:x v1) (point:y v2))
                                 (* (point:y v1) (point:x v2))))
                            path
                            (cdr path))))
         2)))

(defun get-answer-1 (&optional (edges *edges*))
  (path-area (edges-to-path edges)))

(aoc:given 1
  (= 62 (get-answer-1 *example*)))


;;; Part 2

(defun hex-to-edge (hex-string)
  (list (ecase (schar hex-string 5)
          (#\0 :right)
          (#\1 :down)
          (#\2 :left)
          (#\3 :up))
        (parse-integer (subseq hex-string 0 5)
                       :radix 16)))

(defun get-answer-2 (&optional (edges *edges*))
  (path-area
   (edges-to-path
    (mapcar (lambda (edge) (hex-to-edge (third edge)))
            edges))))

(aoc:given 2
  (= 952408144115 (get-answer-2 *example*)))
