(in-package :aoc-2018-08)

(aoc:define-day 45868 19724)


;;; Tree

(defstruct node
  (data nil)
  (children nil))

(defun add-child (node child)
  (make-node :data (node-data node)
             :children (cons child (node-children node))))


;;; Input

(defparameter *input* (aoc:extract-ints (aoc:input)))
(defparameter *test-input*
  '(2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2))

(defun build-tree (input)
  (if (endp input)
      (values nil nil)
      (destructuring-bind ((child-count data-count) input-without-header)
          (aoc:split-at 2 input)
        (destructuring-bind (children input-without-children)
            (if (<= child-count 0)
                (list nil input-without-header)
                (iter (repeat child-count)
                      (for (values new-child remaining-input)
                           first (build-tree input-without-header)
                           then (build-tree remaining-input))
                      (collecting new-child into children)
                      (finally (return (list children remaining-input)))))
          (destructuring-bind (data input-without-node)
              (aoc:split-at data-count input-without-children)
            (values (make-node :data data
                               :children children)
                    input-without-node))))))


;;; Part 1

(defun sum-tree-data (tree)
  (if (null tree)
      0
      (+ (apply #'+ (node-data tree))
         (apply #'+ (mapcar #'sum-tree-data (node-children tree))))))

(defun get-answer-1 (&optional (input *input*))
  (sum-tree-data (build-tree input)))

(aoc:given 1
  (= 138 (get-answer-1 *test-input*)))


;;; Part 2

(defun node-value (tree)
  (cond
    ((null tree)
     0)
    ((endp (node-children tree))
     (apply #'+ (node-data tree)))
    (t
     (let ((child-sums (mapcar #'node-value (node-children tree))))
       (apply #'+ (mapcar (lambda (v)
                            (or (nth (1- v) child-sums) 0))
                          (node-data tree)))))))

(defun get-answer-2 (&optional (input *input*))
  (node-value (build-tree input)))

(aoc:given 2
  (= 66 (get-answer-2 *test-input*)))


;;; Visualization

(defun label-tree (tree &optional (label "0"))
  (let ((new-children (iter (for child in (node-children tree))
                            (for i from 0)
                            (collecting (label-tree child
                                                    (format nil "~A~A" label i))))))
    (make-node :data (cons label (node-data tree))
               :children new-children)))

(defun write-node (tree dotfile)
  (format dotfile "    node~A [label=\"~{~A~^ ~}\"]~%"
          (first (node-data tree))
          (rest (node-data tree)))
  (iter (for child in (node-children tree))
        (format dotfile "    node~A -> node~A~%"
                (first (node-data tree))
                (first (node-data child)))
        (write-node child dotfile)))

(defun write-dot (&optional (input *input*))
  (with-open-file (dotfile "2018-08.dot"
                           :direction :output
                           :if-exists :supersede)
    (format dotfile "digraph aoc_2018_08 {~%")
    (format dotfile "    rankdir=\"LR\"~%")
    (format dotfile "    node [shape=\"box\"]~%")
    (write-node (label-tree (build-tree input)) dotfile)
    (format dotfile "}~%")))
