(in-package :aoc-2018-16)

(aoc:define-day 642 481)


;;;; CPU Stuff

(defvar *registers*)
(defvar *instructions* nil)

(defmacro with-registers (initial-values &body body)
  (let ((init (gensym "INIT")))
    `(let* ((,init ,initial-values)
            (*registers* (make-array (length ,init) :initial-contents ,init)))
         ,@body)))

(defmacro register (n)
  `(svref *registers* ,n))

(defun registers ()
  (copy-seq *registers*))

(defmacro instruction (name (a b) &body body)
  (let* ((c (gensym "C"))
         (has-declare (and (listp (car body))
                           (eq 'declare (caar body))))
         (declare-form (if has-declare
                           (car body)
                           nil))
         (body-form (if has-declare
                       (cdr body)
                       body)))
    `(progn
       (declaim (inline ,name))
       (when (not (member (quote ,name) *instructions*))
         (push (quote ,name) *instructions*))
       (defun ,name (,a ,b ,c)
         (declare (type (integer 0) ,a ,b ,c))
         ,(when declare-form declare-form)
         (setf (register ,c)
               (progn ,@body-form))))))

(defmacro instruction-func (name function &key (immediate nil))
  (let ((a (gensym "A"))
        (b (gensym "B")))
    `(instruction ,name (,a ,b)
       (,function (register ,a)
                  ,(if immediate
                       b
                       `(register ,b))))))

(defmacro instruction-compare (name function &key (first-immediate nil) (second-immediate nil))
  (let ((a (gensym "A"))
        (b (gensym "B")))
    `(instruction ,name (,a ,b)
       (if (,function ,(if first-immediate
                           a
                           `(register ,a))
                      ,(if second-immediate
                           b
                           `(register ,b)))
           1
           0))))

(instruction-func addr +)
(instruction-func addi + :immediate t)

(instruction-func mulr *)
(instruction-func muli * :immediate t)

(instruction-func banr logand)
(instruction-func bani logand :immediate t)

(instruction-func borr logior)
(instruction-func bori logior :immediate t)

(instruction setr (a b)
  (declare (ignore b))
  (register a))
(instruction seti (a b)
  (declare (ignore b))
  a)

(instruction-compare gtir > :first-immediate t)
(instruction-compare gtri > :second-immediate t)
(instruction-compare gtrr >)

(instruction-compare eqir = :first-immediate t)
(instruction-compare eqri = :second-immediate t)
(instruction-compare eqrr =)


;;;; Other structures

(defstruct instruction-test
  before
  instruction
  after)


;;;; Input

(defun parse-instruction-testing (input)
  (iter (generating line in input)
        (for before-str = (next line))
        (while (not (string= before-str "")))
        (for instruction-str = (next line))
        (for after-str = (next line))
        (collecting (make-instruction-test
                     :before (make-array 4 :initial-contents (aoc:extract-ints before-str))
                     :instruction (aoc:extract-ints instruction-str)
                     :after (make-array 4 :initial-contents (aoc:extract-ints after-str))))
        (next line)))

(defun parse-program (input)
  (iter (with in-program = nil)
        (with consecutive-blanks = 0)
        (for line in input)
        (when in-program
          (collecting (aoc:extract-ints line)))
        (if (string= line "")
            (incf consecutive-blanks)
            (setf consecutive-blanks 0))
        (when (and (not in-program)
                   (= 3 consecutive-blanks))
          (setf in-program t))))

(defparameter *sample-instruction-test*
  (first (parse-instruction-testing '("Before: [3, 2, 1, 1]"
                                 "9 2 1 2"
                                 "After:  [3, 2, 2, 1]"))))

(defparameter *instruction-tests* (parse-instruction-testing (aoc:input)))
(defparameter *program* (parse-program (aoc:input)))

;;;; Part 1

(defun instruction-test-matches-p (mnemonic instruction-test)
  (with-slots (before instruction after) instruction-test
   (with-registers before
     (apply mnemonic (cdr instruction))
     (equalp *registers* after))))

(defun find-matching-instructions (instruction-test)
  (iter (for instruction in *instructions*)
        (when (instruction-test-matches-p instruction instruction-test)
          (collecting instruction))))

(aoc:given 1
  (equal nil (set-difference '(mulr addi seti) (find-matching-instructions *sample-instruction-test*))))

(defun get-answer-1 ()
  (iter (for instruction-test in *instruction-tests*)
        (counting (<= 3 (length (find-matching-instructions instruction-test))))))


;;;; Part 2

(defparameter *opcode-map* (make-array (length *instructions*) :initial-element nil))

(defun fill-opcode-map! ()
  (iter (for instruction-test in *instruction-tests*)
        (for opcode = (first (instruction-test-instruction instruction-test)))
        (when (svref *opcode-map* opcode)
          (next-iteration))
        (for candidates = (remove-if (lambda (i) (position i *opcode-map*))
                                     (find-matching-instructions instruction-test)))
        (when (= 1 (length candidates))
          (setf (svref *opcode-map* opcode) (first candidates))))
  (when (position nil *opcode-map*)
    (fill-opcode-map!)))

(defun run-program (program)
  (with-registers '(0 0 0 0)
    (iter (for (i a b c) in program)
          (funcall (svref *opcode-map* i) a b c))
    *registers*))

(defun get-answer-2 ()
  (fill-opcode-map!)
  (svref (run-program *program*) 0))
