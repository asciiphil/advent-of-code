(in-package :aoc-2018-05)

(aoc:define-day 11310 6020)

(defparameter *polymer* (aoc:input))

(defun reactive-pair-p (c1 c2)
  (and (char-equal c1 c2)
       (char/= c1 c2)))

(defun react (polymer)
  (labels ((react-r (head tail)
             (cond
               ((endp tail)
                (reverse head))
               ((endp head)
                (react-r (cons (car tail) head) (cdr tail)))
               ((reactive-pair-p (car head) (car tail))
                (react-r (cdr head) (cdr tail)))
               (t
                (react-r (cons (car tail) head) (cdr tail))))))
    (coerce (react-r nil (coerce polymer 'list))
            'string)))

(defun reacted-length (polymer)
  (length (react polymer)))

(aoc:given 1
  (string= "" (react "aA"))
  (string= "" (react "abBA"))
  (string= "abAB" (react "abAB"))
  (string= "aabAAB" (react "aabAAB"))
  (string= "dabCBAcaDA" (react "dabAcCaCBAcCcaDA")))

(defun get-answer-1 ()
  (reacted-length *polymer*))

(defun reduce-and-react (polymer)
  (iter (for char-to-remove in-string (remove-duplicates (string-upcase polymer)))
        (for reduced-polymer = (remove (char-downcase char-to-remove)
                                       (remove char-to-remove polymer)))
        (finding reduced-polymer minimizing (reacted-length reduced-polymer))))

(aoc:given 2
  (string= "dabAaBAaDA" (reduce-and-react "dabAcCaCBAcCcaDA")))

(defun get-answer-2 ()
  (reacted-length (reduce-and-react (react *polymer*))))
