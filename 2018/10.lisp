(in-package :aoc-2018-10)

(aoc:define-day nil 10007)


;;; Point

(defstruct moving-point
  position
  velocity)

(defstruct bbox
  left
  bottom
  top
  right)

(defun find-bbox (points)
  "POINTS should be a list of complex numbers."
  (iter (for point in points)
        (minimizing (realpart point) into left)
        (minimizing (imagpart point) into bottom)
        (maximizing (realpart point) into right)
        (maximizing (imagpart point) into top)
        (finally (return (make-bbox :left left :right right
                                    :top top :bottom bottom)))))

(defun bbox-area (bbox)
  (* (- (bbox-right bbox) (bbox-left bbox))
     (- (bbox-top bbox) (bbox-bottom bbox))))


;;; Input

(defun parse-input (input)
  (mapcar (lambda (i)
            (let ((ints (aoc:extract-ints i)))
              (make-moving-point :position (complex (first ints) (second ints))
                          :velocity (complex (third ints) (fourth ints)))))
          input))

(defparameter *test-input*
  '("position=< 9,  1> velocity=< 0,  2>"
    "position=< 7,  0> velocity=<-1,  0>"
    "position=< 3, -2> velocity=<-1,  1>"
    "position=< 6, 10> velocity=<-2, -1>"
    "position=< 2, -4> velocity=< 2,  2>"
    "position=<-6, 10> velocity=< 2, -2>"
    "position=< 1,  8> velocity=< 1, -1>"
    "position=< 1,  7> velocity=< 1,  0>"
    "position=<-3, 11> velocity=< 1, -2>"
    "position=< 7,  6> velocity=<-1, -1>"
    "position=<-2,  3> velocity=< 1,  0>"
    "position=<-4,  3> velocity=< 2,  0>"
    "position=<10, -3> velocity=<-1,  1>"
    "position=< 5, 11> velocity=< 1, -2>"
    "position=< 4,  7> velocity=< 0, -1>"
    "position=< 8, -2> velocity=< 0,  1>"
    "position=<15,  0> velocity=<-2,  0>"
    "position=< 1,  6> velocity=< 1,  0>"
    "position=< 8,  9> velocity=< 0, -1>"
    "position=< 3,  3> velocity=<-1,  1>"
    "position=< 0,  5> velocity=< 0, -1>"
    "position=<-2,  2> velocity=< 2,  0>"
    "position=< 5, -2> velocity=< 1,  2>"
    "position=< 1,  4> velocity=< 2,  1>"
    "position=<-2,  7> velocity=< 2, -2>"
    "position=< 3,  6> velocity=<-1, -1>"
    "position=< 5,  0> velocity=< 1,  0>"
    "position=<-6,  0> velocity=< 2,  0>"
    "position=< 5,  9> velocity=< 1, -2>"
    "position=<14,  7> velocity=<-2,  0>"
    "position=<-3,  6> velocity=< 2, -1>"))

(defparameter *test-points* (parse-input *test-input*))
(defparameter *points* (parse-input (aoc:input)))


;;; Part 1

;; First we need to figure out what time it is when the points align.  We
;; can assume that they're initially all moving towards each other,
;; they'll align at some point, and then they'll diverge.  We can look at
;; each pair of points, calculate the time that their paths will cross,
;; then use all of the intersection times together to determine a likely
;; alignment time.

;; Two equations:
;;   <y> = m1(<x> - X1) + Y1
;;   <y> = m2(<x> - X2) + Y2
;; Substitute <y>:
;;   m1(<x> - X1) + Y1 = m2(<x> - X2) + Y2
;; Solve for <x>:
;;   m1(<x> - X1) - m2(<x> - X2) = Y2 - Y1
;;                  (m1 - m2)<x> = m1 * X1 - m2 * X2 + Y2 - Y1
;;                           <x> = (m1 * X1 - m2 * X2 + Y2 - Y1) / (m1 - m2)
(defun intersection-time (p1 p2)
  (declare (type moving-point p1 p2))
  (if (or (zerop (realpart (moving-point-velocity p1)))
          (zerop (realpart (moving-point-velocity p2))))
      nil
      (let ((m1 (/ (imagpart (moving-point-velocity p1))
                   (realpart (moving-point-velocity p1))))
            (x1 (realpart (moving-point-position p1)))
            (y1 (imagpart (moving-point-position p1)))
            (m2 (/ (imagpart (moving-point-velocity p2))
                   (realpart (moving-point-velocity p2))))
            (x2 (realpart (moving-point-position p2)))
            (y2 (imagpart (moving-point-position p2))))
        (if (= m1 m2)
            nil
            (let* ((x-intersection (/ (+ (* m1 x1)
                                         (- (* m2 x2))
                                         y2
                                         (- y1))
                                      (- m1 m2)))
                   (time (/ (- x-intersection x1)
                            (realpart (moving-point-velocity p1)))))
              time)))))

(defun intersection-times (moving-points)
  (iter outer
        (for points on moving-points)
        (for p1 = (car points))
        (iter (for p2 in (cdr points))
              (in outer
                  (collecting (intersection-time p1 p2))))))

;; Median of all intersection times.
(defun most-likely-intersection (moving-points)
  (let ((intersections (remove nil (intersection-times moving-points))))
    (round (nth (floor (length intersections)
                       2)
                (sort (copy-list intersections) #'<)))))


(defun sky-at-time (moving-points seconds)
  (iter (for moving-point in moving-points)
        (collecting (+ (moving-point-position moving-point)
                       (* seconds (moving-point-velocity moving-point))))))


;; If the points are aligned, they'll combine to form lines that make
;; letters.  That means that we should be able to cluster the points into
;; distinct groups based on which points are near which other points.
;; We'll consider "near" to be "within one grid cell", which translates
;; into "distance less than sqrt(2)".
;;
;; The algorithm for clustering is O(n^2), which works well enough here.
;; 1. Grab the first point in the list.
;; 2. Find all points near that one.
;; 3. For each adjacent point, find all of the points near that one, etc.

(defparameter *adjacency-threshold* (sqrt 2))

(defun find-adjacent-points (p points)
  (iter (for candidate in points)
        (when (<= (abs (- candidate p))
                  *adjacency-threshold*)
          (collecting candidate))))

(defun find-grouped-points (seed points)
  (let ((candidates (find-adjacent-points seed points)))
    (if (endp candidates)
        (values (list seed) points)
        (values-list
         (iter (for candidate in candidates)
               (for (values subgroup remaining)
                    first (find-grouped-points candidate (remove candidate points))
                    then (find-grouped-points candidate (remove candidate remaining)))
               (appending subgroup into result)
               (finally (return (list (cons seed result) remaining))))))))

(defun group-all-points (points)
  (multiple-value-bind (group1 remaining)
      (find-grouped-points (car points) (cdr points))
    (if (endp remaining)
        (list group1)
        (cons group1 (group-all-points remaining)))))

;; Assume that a set of points is aligned if there are no singleton
;; groups.  This isn't necessarily correct all the time, but it serves as
;; a heuristic for eliminating unaligned constellations.  `find-alignment'
;; below also tries to minimize bounding box area to find the best
;; alignment.
(defun aligned-p (points)
  (iter (for group in (group-all-points points))
        (always (< 1 (length group)))))


(defun find-alignment (moving-points)
  (values-list
   (iter (with start-time = (most-likely-intersection moving-points))
         (for delta first 0
              then (if (plusp delta)
                       (- delta)
                       (1+ (- delta))))
         (for time = (+ start-time delta))
         (for sky = (sky-at-time moving-points time))
         (counting (not (aligned-p sky)) into unaligned-count)
         ; Somewhat arbitrary limit, but it gives us three unaligned
         ; constellations on each side of the aligned area.
         (while (< unaligned-count 6))
         (finding (list sky time) minimizing (bbox-area (find-bbox sky))))))


(defun plot-sky (sky)
  (let ((bbox (find-bbox sky)))
    (iter (for y from (1- (bbox-bottom bbox)) to (1+ (bbox-top bbox)))
          (iter (for x from (1- (bbox-left bbox)) to (1+ (bbox-right bbox)))
                (format t (if (member (complex x y) sky)
                              "#"
                              ".")))
          (format t "~%"))))

;; To see the answer, run:
;;   (plot-sky (find-alignment *points*))

;;; Part 2

(defun get-answer-2 (&optional (moving-points *points*))
  (nth-value 1 (find-alignment moving-points)))

(aoc:given 2
  (= 3 (get-answer-2 *test-points*)))
