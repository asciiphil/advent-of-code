(in-package :aoc-2018-13)

(aoc:define-day #C(136 36) #C(53 111))


;;; Cart and Track

(defstruct (track
             (:constructor make-track (segments carts &optional wrecks)))
  segments
  carts
  (wrecks nil))

(defclass track-segment () ())

(defclass straight-track-segment (track-segment) ())
(defclass vertical-track-segment (straight-track-segment) ())
(defclass horizontal-track-segment (straight-track-segment) ())

(defclass curve (track-segment) ())
(defclass forward-curve (curve) ())
(defclass bottom-right-curve (forward-curve) ())
(defclass top-left-curve (forward-curve) ())
(defclass reverse-curve (curve) ())
(defclass bottom-left-curve (reverse-curve) ())
(defclass top-right-curve (reverse-curve) ())

(defclass track-intersection (track-segment) ())

(defgeneric segment-char (segment))
(defmethod segment-char ((segment null))                     #\ )
(defmethod segment-char ((segment track-segment))            #\?)
(defmethod segment-char ((segment horizontal-track-segment)) #.(code-char #x2550))
(defmethod segment-char ((segment vertical-track-segment))   #.(code-char #x2551))
(defmethod segment-char ((segment forward-curve))            #\\)
(defmethod segment-char ((segment bottom-right-curve))       #.(code-char #x2554))
(defmethod segment-char ((segment top-left-curve))           #.(code-char #x255D))
(defmethod segment-char ((segment reverse-curve))            #\/)
(defmethod segment-char ((segment bottom-left-curve))        #.(code-char #x2557))
(defmethod segment-char ((segment top-right-curve))          #.(code-char #x255A))
(defmethod segment-char ((segment track-intersection))       #.(code-char #x256C))

(defstruct (cart
             (:constructor make-cart (position direction &optional next-turn)))
  position
  direction
  (next-turn (clist:make-circular-list '(#C(0 -1) 1 #C(0 1)))))

(defmethod print-object ((cart cart) stream)
  (with-slots (position) cart
    (format stream "#<CART ~A ~A >" position (cart-char cart))))

(defun cart< (cart-1 cart-2)
  (with-slots ((position-1 position)) cart-1
    (with-slots ((position-2 position)) cart-2
      (or (< (imagpart position-1) (imagpart position-2))
          (and (= (imagpart position-1) (imagpart position-2))
               (< (realpart position-1) (realpart position-2)))))))

(defun cart-char (cart)
  (ecase (cart-direction cart)
    (#C( 0  1) #.(code-char #x25BC))
    (#C( 0 -1) #.(code-char #x25B2))
    (#C( 1  0) #.(code-char #x25B6))
    (#C(-1  0) #.(code-char #x25C0))))


(defgeneric move-cart (cart next-track-segment))

(defmethod move-cart (cart (next-track-segment straight-track-segment))
  (with-slots (position direction next-turn) cart
    (make-cart (+ position direction)
               direction
               next-turn)))

(defmethod move-cart (cart (next-track-segment forward-curve))
  (with-slots (position direction next-turn) cart
    (make-cart (+ position direction)
               (- (complex (imagpart direction) (realpart direction)))
               next-turn)))

(defmethod move-cart (cart (next-track-segment reverse-curve))
  (with-slots (position direction next-turn) cart
    (make-cart (+ position direction)
               (complex (imagpart direction) (realpart direction))
               next-turn)))

(defmethod move-cart (cart (next-track-segment track-intersection))
  (with-slots (position direction next-turn) cart
    (make-cart (+ position direction)
               (* direction (clist:focused next-turn))
               (clist:rotate next-turn 1))))


(defun map-track-segment (char)
  (ecase char
    (#\| 'vertical-track-segment)
    (#\- 'horizontal-track-segment)
    (#\/ 'reverse-curve)
    (#\\ 'forward-curve)
    (#\+ 'track-intersection)
    (#\> 'horizontal-track-segment)
    (#\< 'horizontal-track-segment)
    (#\^ 'vertical-track-segment)
    (#\v 'vertical-track-segment)
    (#\V 'vertical-track-segment)
    (#\  nil)))

(defun map-cart-direction (char)
  (case char
    (#\> #C( 1  0))
    (#\< #C(-1  0))
    (#\v #C( 0  1))
    (#\V #C( 0  1))
    (#\^ #C( 0 -1))
    (otherwise nil)))

(defun track-curve-type (general-shape coordinates segments)
  (let ((x (realpart coordinates))
        (y (imagpart coordinates)))
    (if (eq general-shape 'forward)
        (cond
          ((and (plusp y)
                (member (type-of (aref segments (1- y) x))
                        '(vertical-track-segment track-intersection
                          bottom-right-curve bottom-left-curve)))
           'top-right-curve)
          ((and (plusp x)
                (member (type-of (aref segments y (1- x)))
                        '(horizontal-track-segment track-intersection
                          bottom-right-curve top-right-curve)))
           'bottom-left-curve)
          ((and (< y (array-dimension segments 0))
                (member (type-of (aref segments (1+ y) x))
                        '(vertical-track-segment track-intersection
                          top-right-curve top-left-curve)))
           'bottom-left-curve)
          ((and (< x (array-dimension segments 1))
                (member (type-of (aref segments y (1+ x)))
                        '(horizontal-track-segment track-intersection
                          bottom-left-curve top-left-curve)))
           'top-right-curve)
          (t
           (error "Couldn't determine ~A curve type for ~A." general-shape coordinates)))
        (cond
          ((and (plusp y)
                (member (type-of (aref segments (1- y) x))
                        '(vertical-track-segment track-intersection
                          bottom-right-curve bottom-left-curve)))
           'top-left-curve)
          ((and (plusp x)
                (member (type-of (aref segments y (1- x)))
                        '(horizontal-track-segment track-intersection
                          bottom-right-curve top-right-curve)))
           'top-left-curve)
          ((and (< y (array-dimension segments 0))
                (member (type-of (aref segments (1+ y) x))
                        '(vertical-track-segment track-intersection
                          top-right-curve top-left-curve)))
           'bottom-right-curve)
          ((and (< x (array-dimension segments 1))
                (member (type-of (aref segments y (1+ x)))
                        '(horizontal-track-segment track-intersection
                          bottom-left-curve top-left-curve)))
           'bottom-right-curve)
          (t
           (error "Couldn't determine ~A curve type for ~A." general-shape coordinates))))))

(defun parse-track (string-list)
  (let ((track-segments
         (make-array (list (length string-list) (length (first string-list)))
                     :element-type '(or null track-segment)
                     :initial-element nil)))
    (iter outer
          (for string in string-list)
          (for y from 0)
          (iter (for c in-string string)
                (for cp previous c)
                (for x from 0)
                (for segment-type = (map-track-segment c))
                (when segment-type
                  (case segment-type
                    (forward-curve
                     (in outer (collecting (complex x y) into forward-curve-coordinates)))
                    (reverse-curve
                     (in outer(collecting (complex x y) into reverse-curve-coordinates)))
                    (otherwise
                     (setf (aref track-segments y x) (make-instance segment-type)))))
                (for cart-direction = (map-cart-direction c))
                (when cart-direction
                  (in outer
                      (collecting (make-cart (complex x y) cart-direction) into carts))))
          (finally
           (mapc (lambda (coords)
                   (setf (aref track-segments (imagpart coords) (realpart coords))
                         (make-instance (track-curve-type 'forward coords track-segments))))
                 forward-curve-coordinates)
           (mapc (lambda (coords)
                   (setf (aref track-segments (imagpart coords) (realpart coords))
                         (make-instance (track-curve-type 'reverse coords track-segments))))
                 reverse-curve-coordinates)
           (return-from outer (make-track
                                       track-segments
                                       (sort carts #'cart<)))))))

(defun print-track (track)
  (with-slots (segments wrecks) track
    (labels ((print-rows (y carts)
               (when (< y (array-dimension segments 0))
                 (let ((remaining-carts (print-cells 0 y carts)))
                   (format t "~%")
                   (print-rows (1+ y) remaining-carts))))
             (print-cells (x y carts)
               (if (< x (array-dimension segments 1))
                   (cond
                     ((and (not (endp carts))
                           (= (complex x y) (cart-position (first carts))))
                      (format t "~A" (cart-char (first carts)))
                      (print-cells (1+ x) y (rest carts)))
                     ((member (complex x y) wrecks)
                      (format t "~A" #.(code-char #x2591))
                      (print-cells (1+ x) y carts))
                     (t
                      (format t "~A" (segment-char (aref segments y x)))
                      (print-cells (1+ x) y carts)))
                   carts)))
      (print-rows 0 (track-carts track)))))


;;; Input

(defparameter *test-track-1*
  (parse-track '("|"
                 "v"
                 "|"
                 "|"
                 "|"
                 "^"
                 "|")))
(defparameter *test-track-2*
  (parse-track '("/->-\\        "
                 "|   |  /----\\"
                 "| /-+--+-\\  |"
                 "| | |  | v  |"
                 "\\-+-/  \\-+--/"
                 "  \\------/   ")))
(defparameter *test-track-3*
  (parse-track '("/>-<\\  "
                 "|   |  "
                 "| /<+-\\"
                 "| | | v"
                 "\\>+</ |"
                 "  |   ^"
                 "  \\<->/")))
(defparameter *track* (parse-track (aoc:input)))


;;; Part 1

(defun step-time (track)
  (let ((wrecks (track-wrecks track)))
    (labels ((move-carts (unmoved-carts moved-carts)
               (if (endp unmoved-carts)
                   (sort moved-carts #'cart<)
                   (let* ((cart (car unmoved-carts))
                          (new-pos (+ (cart-position cart) (cart-direction cart)))
                          (segment (aref (track-segments track)
                                         (imagpart new-pos)
                                         (realpart new-pos)))
                          (moved-cart (move-cart cart segment)))
                     (cond
                       ((member (cart-position moved-cart) moved-carts :key #'cart-position)
                        (push (cart-position moved-cart) wrecks)
                        (move-carts (cdr unmoved-carts)
                                    (remove (cart-position moved-cart) moved-carts
                                            :key #'cart-position)))
                       ((member (cart-position moved-cart) unmoved-carts :key #'cart-position)
                        (push (cart-position moved-cart) wrecks)
                        (move-carts (remove (cart-position moved-cart) (cdr unmoved-carts)
                                            :key #'cart-position)
                                    moved-carts))
                       (t
                        (move-carts (cdr unmoved-carts) (cons moved-cart moved-carts))))))))
      (let ((new-carts (move-carts (track-carts track) nil)))
        (make-track (track-segments track) new-carts wrecks)))))

(defun run-until-crash (track &optional (iterations 0))
  (if (not (endp (track-wrecks track)))
      (values track iterations)
      (run-until-crash (step-time track) (1+ iterations))))

(defun first-crash-location (track)
  (car (track-wrecks (run-until-crash track))))

(aoc:given 1
  (= #C(7 3) (first-crash-location *test-track-2*)))

(defun get-answer-1 ()
  (first-crash-location *track*))


;;; Part 2

(defun run-until-one-cart (track &optional (iterations 0))
  (if (endp (cdr (track-carts track)))
      (values track iterations)
      (run-until-one-cart (step-time track) (1+ iterations))))

(defun last-cart-location (track)
  (cart-position (first (track-carts (run-until-one-cart track)))))

(aoc:given 2
  (= #C(6 4) (last-cart-location *test-track-3*)))

(defun get-answer-2 ()
  (last-cart-location *track*))
