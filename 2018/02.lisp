(in-package :aoc-2018-02)

(aoc:define-day 5976 "xretqmmonskvzupalfiwhcfdb")

(defparameter *input* (aoc:input))
(defparameter *test-input-1*
  '("abcdef"
    "bababc"
    "abbcde"
    "abcccd"
    "aabcdd"
    "abcdee"
    "ababab"))
(defparameter *test-input-2*
  '("abcde"
    "fghij"
    "klmno"
    "pqrst"
    "fguij"
    "axcye"
    "wvxyz"))

(defun letter-count-p (string target-count)
  (let ((counts (make-hash-table)))
    (iter (for c in-string string)
          (incf (gethash c counts 0)))
    (iter (for (char count) in-hashtable counts)
          (thereis (= count target-count)))))

(defun checksum (ids)
  (iter (for id in ids)
        (counting (letter-count-p id 2) into twos)
        (counting (letter-count-p id 3) into threes)
        (finally (return (* twos threes)))))

(aoc:given 1
  (= 12 (checksum *test-input-1*)))

(defun get-answer-1 ()
  (checksum *input*))

(defun ids-match-p (id1 id2)
  (assert (= (length id1) (length id2))
          (id1 id2)
          "ID lengths don't match: \"~A\", \"~A\"" id1 id2)
  (iter (for c1 in-string id1)
        (for c2 in-string id2)
        (counting (char= c1 c2) into match-count)
        (finally (return (= match-count (1- (length id1)))))))

(defun find-matching-id (id other-ids)
  (iter (for other-id in other-ids)
        (finding (list id other-id)
                 such-that (ids-match-p id other-id))))

(defun find-prototype-ids (ids)
  (iter (for id in ids)
        (for other-ids on (cdr ids))
        (thereis (find-matching-id id other-ids))))

(defun common-prototype-id-characters (ids)
  (destructuring-bind (id1 id2) (find-prototype-ids ids)
    (assert (and id1 id2)
            (id1 id2)
            "No IDs provided.")
    (iter (for c1 in-string id1)
          (for c2 in-string id2)
          (when (char= c1 c2)
            (collecting c1 result-type 'string)))))

(aoc:given 2
  (string= "fgij" (common-prototype-id-characters *test-input-2*)))

(defun get-answer-2 ()
  (common-prototype-id-characters *input*))
