(in-package :aoc-2018-18)

(aoc:define-day 543312 199064)


;;; Data

(defparameter *char-alist*
  '((#\. . :open)
    (#\| . :trees)
    (#\# . :lumberyard)))

(defun char-to-keyword (char)
  (cdr (assoc char *char-alist*)))

(defun keyword-to-char (keyword)
  (car (rassoc keyword *char-alist*)))

(defun print-area (area)
  (iter (for y below (array-dimension area 0))
        (iter (for x below (array-dimension area 1))
              (format t "~A" (keyword-to-char (aref area y x))))
        (format t "~%")))

;;; Input

(defun parse-input (input)
  (let ((area (make-array (list (length input) (length (first input)))
                          :element-type 'keyword
                          :initial-element :open)))
    (iter (for line in input)
          (for y from 0)
          (iter (for char in-string line with-index x)
                (setf (aref area y x) (char-to-keyword char))))
    area))

(defparameter *test-area*
  (parse-input '(".#.#...|#."
                 ".....#|##|"
                 ".|..|...#."
                 "..|#.....#"
                 "#.#|||#|#|"
                 "...#.||..."
                 ".|....|..."
                 "||...#|.#|"
                 "|.||||..|."
                 "...#.|..|.")))
(defparameter *area* (parse-input (aoc:input)))


;;;; Part 1

(defun neighbors (area x y)
  (iter outer
        (for yp from (max (1- y) 0) to (min (1+ y) (1- (array-dimension area 0))))
        (iter (for xp from (max (1- x) 0) to (min (1+ x) (1- (array-dimension area 1))))
              (when (or (/= x xp) (/= y yp))
                (in outer
                    (collecting (aref area yp xp)))))))

(defun new-value (area x y)
  (let ((current (aref area y x))
        (neighbors (neighbors area x y)))
    (cond
      ((and (eq current :open)
            (<= 3 (count :trees neighbors)))
       :trees)
      ((and (eq current :trees)
            (<= 3 (count :lumberyard neighbors)))
       :lumberyard)
      ((and (eq current :lumberyard)
            (<= 1 (count :lumberyard neighbors))
            (<= 1 (count :trees neighbors)))
       :lumberyard)
      ((eq current :lumberyard)
       :open)
      (t
       current))))

(defun effect-change (area)
  (let ((new-area (make-array (array-dimensions area)
                              :element-type 'keyword
                              :initial-element :open)))
    (iter (for y below (array-dimension area 0))
          (iter (for x below (array-dimension area 1))
                (setf (aref new-area y x) (new-value area x y))))
    new-area))

(defun effect-changes (area count)
  (iter (with states-seen = (make-hash-table :test 'equalp))
        (for i from 0 to count)
        (for a first area then (effect-change a))
        (when (gethash a states-seen)
          (let* ((base (gethash a states-seen))
                 (offset (mod (- count base) (- i base))))
            (leave (effect-changes a offset))))
        (setf (gethash a states-seen) i)
        (finally (return a))))

(defun resource-value (area)
  (* (count :trees (aoc:flatten-array area))
     (count :lumberyard (aoc:flatten-array area))))

(defun get-answer-1 (&optional (area *area*))
  (resource-value (effect-changes area 10)))

(aoc:given 1
  (= 1147 (get-answer-1 *test-area*)))


;;; Part 2

(defun get-answer-2 (&optional (area *area*))
  (resource-value (effect-changes area 1000000000))  )

