(in-package :aoc-2018-09)

(aoc:define-day 386151 3211264152)

(defparameter *debug* nil)


;;; Input

(defparameter *input* (aoc:extract-ints (aoc:input)))


;;; Part 1

(defun init-board ()
  (clist:make-circular-list 0))

(defun place-marble (board marble-number)
  "Returns two values: the state of the board after placing the marble;
  and the score received by the current player after their move."
  (if (/= 0 (mod marble-number 23))
      (values (clist:insert (clist:rotate board 1)
                            marble-number
                            :right)
              0)
      (let ((new-board (clist:rotate board -7)))
        (values (clist:remove-focused new-board)
                (+ marble-number (clist:focused new-board))))))

(defun play-game (players last-marble)
  (iter (with initial-board = (init-board))
        (with scores = (make-array players))
        (for marble from 1 to last-marble)
        (for player first 0
                    then (mod (1+ player) players))
        (for (values board score)
             first (place-marble initial-board marble)
             then (place-marble board marble))
        (incf (svref scores player) score)
        (when *debug*
          (format t "[~A:~A] ~A~%" player score board))
        (finally (return scores))))

(defun find-winning-score (players last-marble)
  (reduce #'max (play-game players last-marble)))

(aoc:given 1
  (= 32 (find-winning-score 9 25))
  (= 8317 (find-winning-score 10 1618))
  (= 146373 (find-winning-score 13 7999))
  (= 2764 (find-winning-score 17 1104))
  (= 54718 (find-winning-score 21 6111))
  (= 37305 (find-winning-score 30 5807)))

(defun get-answer-1 ()
  (apply #'find-winning-score *input*))


;;; Part 2

(defun get-answer-2 ()
  (find-winning-score (first *input*) (* 100 (second *input*))))
