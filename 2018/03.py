#!/usr/bin/env python3

import re

import shapely.geometry

def parse_input(line):
    m = re.search(r'^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$', line)
    assert m, 'Unexpected input: {}'.format(line)
    cl_id, x, y, w, h = [int(g) for g in m.groups()]
    return (cl_id, shapely.geometry.box(x, y, x + w, y + h))
    
claims = []
with open('input.03') as input:
    for line in input:
        claims.append(parse_input(line))

claim_intersection = shapely.geometry.Point()
claim_union = shapely.geometry.Point()
uncontested_claims = []
for cl_id, box in claims:
    uncontested_claims = [(i, c) for (i, c) in uncontested_claims if not c.intersects(box)]
    if not claim_union.intersects(box):
        uncontested_claims.append((cl_id, box))
    claim_intersection = claim_intersection.union(claim_union.intersection(box))
    claim_union = claim_union.union(box)
    
assert len(uncontested_claims) == 1
print('3a: {}'.format(int(claim_intersection.area)))
print('3b: {}'.format(uncontested_claims[0][0]))
