#!/usr/bin/env python3

import argparse
import math
import re

import cairo
import numpy as np
from tqdm import tqdm

CONJUNCTION_TIME = 10007  # Precalculated
CONJUNCTION_CENTER = np.array([(117 + 178) / 2, (156 + 165) / 2])  # Precalculated
IMAGE_WIDTH = 1000
POINT_RADIUS = 0.2
START_TIME = CONJUNCTION_TIME - 30
FPS = 30
FRAME_OFFSET = 10 * FPS

def draw_sky(positions, frameno, scale):
    surface = cairo.ImageSurface(cairo.FORMAT_RGB24, IMAGE_WIDTH, IMAGE_WIDTH)
    ctx = cairo.Context(surface)
    
    ctx.set_source_rgb(0, 0, 0)
    ctx.rectangle(0, 0, IMAGE_WIDTH, IMAGE_WIDTH)
    ctx.fill()
    
    # Adjust transform so that our absolute coordinates will be placed appropriately.
    ctx.set_matrix(cairo.Matrix(
        xx=scale,
        yy=scale,
        x0=IMAGE_WIDTH / 2 - CONJUNCTION_CENTER[0] * scale,
        y0=IMAGE_WIDTH / 2 - CONJUNCTION_CENTER[1] * scale,
    ))
    ctx.set_source_rgb(1, 1, 1)
    
    for x, y in positions:
        ctx.arc(x, y, POINT_RADIUS * scale, 0, 2*math.pi)
        ctx.fill()

    surface.write_to_png('2018-10-{:05d}.png'.format(frameno))
    
    
### Input

with open('/home/user/.cache/aoc/input.2018-10') as ipt:
    raw_input = np.array([
        [int(i) for i in re.findall(r'[+-]?\d+', line)]
        for line in ipt])
positions = raw_input[:, :2]
velocities = raw_input[:, 2:]


current_pos = positions + START_TIME * velocities
min_pos = np.amin(current_pos, 0)
max_pos = np.amax(current_pos, 0)
scale = IMAGE_WIDTH / max(max_pos - min_pos)
for frame in tqdm(range(FPS * 2 * (CONJUNCTION_TIME - START_TIME))):
    current_pos = positions + \
                  (START_TIME + frame / FPS) * velocities
    draw_sky(current_pos, frame + FRAME_OFFSET, scale)
    
