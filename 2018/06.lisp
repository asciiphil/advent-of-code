(in-package :aoc-2018-06)

(aoc:define-day 4011 46054)


;;;; Input

(defparameter *targets* (mapcar (lambda (s) (apply #'complex (aoc:extract-ints s)))
                                (aoc:input)))
(defparameter *test-targets*
  '(#C(1 1)
    #C(1 6)
    #C(8 3)
    #C(3 4)
    #C(5 5)
    #C(8 9)))

;; Just shorthand so I have to type less.
;; Also, WARNING: SBCL-specific
(defparameter *infinity* sb-ext:double-float-positive-infinity)


;;;; Part 1

(defun bounding-box (targets)
  (iter (for target in targets)
        (minimizing (realpart target) into minx)
        (minimizing (imagpart target) into miny)
        (maximizing (realpart target) into maxx)
        (maximizing (imagpart target) into maxy)
        (finally (return (list minx miny maxx maxy)))))

(defun manhattan-distance (c1 c2)
  (+ (abs (- (realpart c1) (realpart c2)))
     (abs (- (imagpart c1) (imagpart c2)))))

(defun closest-target (c targets)
  (iter (for target in targets)
        (finding target minimizing (manhattan-distance c target))))

(defun largest-finite-area (targets)
  (destructuring-bind (minx miny maxx maxy)
      (bounding-box targets)
    (let ((areas (make-hash-table)))
      (iter (for target in targets)
            (setf (gethash target areas) 0))
      (iter (for x from minx to maxx)
            (iter (for y from miny to maxy)
                  (for target = (closest-target (complex x y) targets))
                  (if (or (= x minx)
                          (= x maxx)
                          (= y miny)
                          (= y maxy))
                      (setf (gethash target areas) *infinity*)
                      (incf (gethash target areas)))))
      (values-list (iter (for (target area) in-hashtable areas)
                         (when (/= area *infinity*)
                           (finding (list area target) maximizing area)))))))

(aoc:given 1
  (= 17 (largest-finite-area *test-targets*)))

(defun get-answer-1 ()
  (largest-finite-area *targets*))


;;;; Part 2

(defun total-distance (c targets)
  (iter (for target in targets)
        (summing (manhattan-distance c target))))

(defun count-central-coordinates (threshold targets)
  (destructuring-bind (minx miny maxx maxy)
      (bounding-box targets)
    (iter outer
          (for x from minx to maxx)
          (iter (for y from miny to maxy)
                (in outer
                    (counting (< (total-distance (complex x y) targets)
                                 threshold)))))))

(aoc:given 2
  (= 16 (count-central-coordinates 32 *test-targets*)))

(defun get-answer-2 ()
  (count-central-coordinates 10000 *targets*))
