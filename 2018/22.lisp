(in-package :aoc-2018-22)

;; Part 2 is 1051, but it's really slow
(aoc:define-day 9899 nil)


;;;; Input

(defparameter *depth* (aoc:extract-ints (first (aoc:input))))
(defparameter *target* (apply #'complex (aoc:extract-ints (second (aoc:input)))))

(defconstant +geologic-index-base-x+ 16807)
(defconstant +geologic-index-base-y+ 48271)
(defconstant +erosion-level-modulo+ 20183)

;;; Part 1

(defvar *erosion-levels* nil)
(defmacro with-erosion-levels (&body body)
  `(let ((*erosion-levels* (make-hash-table)))
     ,@body))

(defun geologic-level (coordinates)
  (cond
    ((= coordinates #C(0 0))
     0)
    ((= coordinates *target*)
     0)
    ((zerop (realpart coordinates))
     (mod (* (imagpart coordinates) +geologic-index-base-y+)
          +erosion-level-modulo+))
    ((zerop (imagpart coordinates))
     (mod (* (realpart coordinates) +geologic-index-base-x+)
          +erosion-level-modulo+))
    (t
     (mod (* (erosion-level (+ coordinates #C(-1 0)))
             (erosion-level (+ coordinates #C(0 -1))))
          +erosion-level-modulo+))))

(defun erosion-level (coordinates)
  (or (gethash coordinates *erosion-levels*)
      (setf (gethash coordinates *erosion-levels*)
            (mod (+ (geologic-level coordinates)
                    *depth*)
                 +erosion-level-modulo+))))

(defun print-erosion-levels (width height)
  (with-erosion-levels
    (iter (for y from 0 below height)
          (iter (for x from 0 below width)
                (format t "~A" (cond
                                 ((= 0 x y)
                                  #\M)
                                 ((= (complex x y) *target*)
                                  #\T)
                                 (t
                                  (svref #(#\. #\= #\|) (mod (erosion-level (complex x y)) 3))))))
          (format t "~%"))))

(defun risk-level ()
  (with-erosion-levels
    (iter outer
          (for y from 0 to (imagpart *target*))
          (iter (for x from 0 to (realpart *target*))
                (in outer
                    (summing (mod (erosion-level (complex x y)) 3)))))))

(aoc:deftest given-1
  (let ((*depth* 510)
        (*target* #C(10 10)))
    (5am:is (= 114 (risk-level)))))

(defun get-answer-1 ()
  (risk-level))


;;;; Part 2

(defstruct (state
             (:constructor make-state (position tool)))
  position
  tool)

(defmethod print-object ((state state) stream)
  (with-slots (position tool) state
    (format stream "#<STATE ~A ~A>" position tool)))

(defun sxhash-state (state)
  (sxhash (list (state-position state) (state-tool state))))

(defun state-equal (state1 state2)
  (and (= (state-position state1) (state-position state2))
       (eq (state-tool state1) (state-tool state2))))

(sb-ext:define-hash-table-test state-equal sxhash-state)

(defun tools-for-region (coordinates)
  (ecase (mod (erosion-level coordinates) 3)
    (0 '(:climbing-gear :torch))
    (1 '(:climbing-gear :neither))
    (2 '(:torch :neither))))

(defun adjacent-regions (coordinates)
  (remove-if (lambda (c) (or (< (realpart c) 0)
                             (< (imagpart c) 0)))
             (mapcar (lambda (v) (+ v coordinates)) '(#C(0 -1) #C(-1 0) #C(1 0) #C(0 1)))))

(defun other-tool (state)
  "Given a state, returns the other tool that can be used at the state's
  location."
  (with-slots (position tool) state
    (first (remove tool (tools-for-region position)))))

(defun possible-moves (state)
  "Returns a list of possible moves from the current state.  Each item
  is (cost new-state)."
  (with-slots (position tool) state
    (cons (list 7 (make-state position (other-tool state)))
          (remove nil (mapcar (lambda (r)
                                (when (member tool (tools-for-region r))
                                  (list 1 (make-state r tool))))
                              (adjacent-regions position))))))

(defun find-path (initial-state target-state)
  (aoc:shortest-path initial-state
                     #'possible-moves
                     :end target-state
                     :test #'state-equal
                     :heuristic (lambda (state)
                                  (+ (abs (realpart (- (state-position state)
                                                       (state-position target-state))))
                                     (abs (imagpart (- (state-position state)
                                                       (state-position target-state))))
                                     (if (eq (state-tool state) (state-tool target-state))
                                         0
                                         7)))))

(defun get-answer-2 ()
  (with-erosion-levels
    (nth-value 1 (find-path (make-state #C(0 0) :torch) (make-state *target* :torch)))))

