(in-package :aoc-2018-23)

; Part 2 is 123356173, but takes too long to run.
(aoc:define-day 737 nil)


;;;; Nanobots

(defstruct nanobot
  position
  radius)

(defun in-range-p (nanobot1 nanobot2)
  "Returns true if NANOBOT2 is in range of NANOBOT1.  Note: not
  necessarily commutative."
  (<= (point:manhattan-distance (nanobot-position nanobot1) (nanobot-position nanobot2))
      (nanobot-radius nanobot1)))

;;;; Input

(defun parse-line (line)
  (let ((ints (aoc:extract-ints line)))
    (make-nanobot :position (apply #'point:make-point (subseq ints 0 3))
                  :radius (fourth ints))))

(defparameter *test-nanobots-range*
  (mapcar #'parse-line '("pos=<0,0,0>, r=4"
                         "pos=<1,0,0>, r=1"
                         "pos=<4,0,0>, r=3"
                         "pos=<0,2,0>, r=1"
                         "pos=<0,5,0>, r=3"
                         "pos=<0,0,3>, r=1"
                         "pos=<1,1,1>, r=1"
                         "pos=<1,1,2>, r=1"
                         "pos=<1,3,1>, r=1")))
(defparameter *test-nanobots-position*
  (mapcar #'parse-line '("pos=<10,12,12>, r=2"
                         "pos=<12,14,12>, r=2"
                         "pos=<16,12,12>, r=4"
                         "pos=<14,14,14>, r=6"
                         "pos=<50,50,50>, r=200"
                         "pos=<10,10,10>, r=5")))
(defparameter *nanobots* (mapcar #'parse-line (aoc:input)))


;;;; Part 1

(defun strongest-nanobot (nanobots)
  (labels ((strongest-r (best rest)
             (cond
               ((endp rest)
                best)
               ((< (nanobot-radius best) (nanobot-radius (car rest)))
                (strongest-r (car rest) (cdr rest)))
               (t
                (strongest-r best (cdr rest))))))
    (strongest-r (car nanobots) (cdr nanobots))))

(defun get-answer-1 (&optional (nanobots *nanobots*))
  (let ((strongest (strongest-nanobot nanobots)))
    (count-if (lambda (n) (in-range-p strongest n)) nanobots)))

(aoc:given 1
  (= 7 (get-answer-1 *test-nanobots-range*)))


;;;; Part 2

;;; The general case solution here is quite difficult, but all of the
;;; various inputs for this problem seem to have most of the areas
;;; overlapping.  That gives us a simpler (but still slow) approach:  Pick
;;; a point somewhere in the overlapping region and look around it for
;;; areas of increasing overlap.  Continue looking until you don't find
;;; anything else.
;;;
;;; This only works if the local maximum we find is the global maximum.
;;; Fortunately, that tends to be the case for most of this problem's
;;; inputs.
;;;
;;; We'll start with the mean of all of the nanobot locations.  From
;;; there, we'll make large steps in all siz cardinal directions to look
;;; for areas of greater overlap.  Once an area is exhausted (we've moved
;;; entirely into area of lesser overlap), we'll take the best discovered
;;; location as a starting point for the next round, with shorter steps.
;;; This ends up searching and re-searching some large areas multiple
;;; times, but it does eventually get an answer.
;;;
;;; We'll start the steps at a moderate power of two, minus one.  From
;;; there, they'll decrease by a power of two each time.  This
;;; approximates a binary search.  Using powers of two minus one means
;;; that the steps are mostly prime numbers, which should reduce the
;;; number of times that steps of different sizes end up retesting the
;;; same locations.

(defun nanobots-in-range (point nanobots)
  (labels
      ((nanobots-in-range-r (remaining count)
         (if (endp remaining)
             count
             (nanobots-in-range-r
              (cdr remaining)
              (if (<= (point:manhattan-distance point (nanobot-position (car remaining)))
                      (nanobot-radius (car remaining)))
                  (1+ count)
                  count)))))
    (nanobots-in-range-r nanobots 0)))

(defun maximum-nearby-intersection (point nanobots &optional (delta 100000))
  (labels
      ((search-r (candidates seen max-point max-value)
         (cond
           ((endp candidates)
            max-point)
           ((gethash (car candidates) seen)
            (search-r (cdr candidates) seen max-point max-value))
           (t
            (let ((new-value (nanobots-in-range (car candidates) nanobots))
                  (adjacents (mapcar (lambda (v) (point:+ (car candidates) v))
                                     (list (point:make-point (list delta 0 0)) (point:make-point (list (- delta) 0 0))
                                           (point:make-point (list 0 delta 0)) (point:make-point (list 0 (- delta) 0))
                                           (point:make-point (list 0 0 delta)) (point:make-point (list 0 0 (- delta)))))))
              (setf (gethash (car candidates) seen) t)
              (if (< max-value new-value)
                  (format t "new max: ~A ~A~%" (car candidates) new-value)
                  (when (and (= max-value new-value)
                             (< (point:manhattan-distance (point:make-point (list 0 0 0)) (car candidates))
                                (point:manhattan-distance (point:make-point (list 0 0 0)) max-point)))
                    (format t "closer: ~A ~A~%" (car candidates) new-value)))
              (if (< new-value max-value)
                  (search-r (cdr candidates) seen max-point max-value)
                  (search-r (union (cdr candidates) adjacents)
                            seen
                            (if (or (< max-value new-value)
                                    (and (= max-value new-value)
                                         (< (point:manhattan-distance (point:make-point (list 0 0 0)) (car candidates))
                                            (point:manhattan-distance (point:make-point (list 0 0 0)) max-point))))
                                (car candidates)
                                max-point)
                            (if (< max-value new-value)
                                new-value
                                max-value))))))))
    (search-r (list point) (make-hash-table :test #'point:=) point (nanobots-in-range point nanobots))))

(defun mean-position (nanobots)
  (point:make-point (mapcar (lambda (c) (truncate c (length nanobots)))
                            (point::list-coordinates (apply #'point:+
                                                            (mapcar #'nanobot-position nanobots))))))

(defun guess-nanobot-intersection (nanobots)
  (iter (for power from 21 downto 1)
        (for point first (mean-position nanobots)
             then (maximum-nearby-intersection point nanobots (1- (expt 2 power))))
        (format t "2^~A: ~A ~A~%" power point (nanobots-in-range point nanobots))
        (finally (return point))))

;;; To get the answer for part 2, run `guess-nanobot-intersection'.  It
;;; will take several hours, though.
;;;
;;; For my data, the best point was #[44516373 27600734 51239066] with 930
;;; adjacent nanobots (out of 1000).  It took about six hours to find on
;;; my moderately slow laptop and about two hours on my faster desktop.
