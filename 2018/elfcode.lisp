(in-package :elfcode)


;;;; Instructions

(defvar *instructions* nil)

(defmacro instruction (name (a b) &body body)
  "Defines an instruction.  A and B are either symbols or the
  form (register X) where X is a symbol.  If they're symbols, they're
  immediate, otherwise they represent register lookups.  If an instruction
  is defined with, e.g. `(register a)` then `a` in the body will
  automatically refer to the value of register `a`.  If the symbol is `_`,
  it will be ignored."
  (flet ((appropriate-symbol (x)
           (if (symbolp x)
               (if (eq x '_)
                   (gensym "IGNORE")
                   x)
               (gensym (symbol-name (second x)))))
         (appropriate-place (x symbol registers)
           (if (symbolp x)
               nil
               `(svref ,registers ,symbol))))
    (let* ((registers (gensym "REGISTERS"))
           (a-symbol (appropriate-symbol a))
           (a-place (appropriate-place a a-symbol registers))
           (b-symbol (appropriate-symbol b))
           (b-place (appropriate-place b b-symbol registers))
           (c (gensym "C"))
           (macrolets (remove nil
                              (list (when a-place `(,(second a) ,a-place))
                                    (when b-place `(,(second b) ,b-place)))))
           (declarations (remove nil
                                 (list `(type (integer 0) ,a-symbol ,b-symbol ,c)
                                       (when (eq a '_) `(ignore ,a-symbol))
                                       (when (eq b '_) `(ignore ,b-symbol))))))
      `(progn
         (declaim (inline ,name))
         (when (not (member (quote ,name) *instructions*))
           (push (quote ,name) *instructions*))
         (defun ,name (,registers ,a-symbol ,b-symbol ,c)
           (declare ,@declarations)
           (setf (svref ,registers ,c)
                 ,(if (endp macrolets)
                      `(progn ,@body)
                      `(symbol-macrolet (,@macrolets)
                         ,@body))))))))

(defmacro instruction-func (name function &key (immediate nil))
  "Defines an elfcode instruction that has a direct mapping to a Common
  Lisp function.  FUNCTION is the unquoted name of the appropriate
  function.  Set :IMMEDIATE to true if the second parameter to the
  instruction is immediate."
  (let ((a (gensym "A"))
        (b (gensym "B")))
    `(instruction ,name ((register ,a) ,(if immediate b `(register ,b)))
       (,function ,a ,b))))

(defmacro instruction-compare (name function &key (first-immediate nil) (second-immediate nil))
  "Defines an elfcode instruction that has a direct mapping to a Common
  Lisp comparative function.  FUNCTION is the unquoted name of the
  appropriate function.  Return value is 1 for true and 0 for false."
  (let ((a (gensym "A"))
        (b (gensym "B")))
    `(instruction ,name
         (,(if first-immediate a `(register ,a))
          ,(if second-immediate b `(register ,b)))
       (if (,function ,a ,b)
           1
           0))))

(instruction-func addr +)
(instruction-func addi + :immediate t)

(instruction-func mulr *)
(instruction-func muli * :immediate t)

(instruction-func banr logand)
(instruction-func bani logand :immediate t)

(instruction-func borr logior)
(instruction-func bori logior :immediate t)

(instruction setr ((register a) _)
  a)
(instruction seti (a _)
  a)

(instruction-compare gtir > :first-immediate t)
(instruction-compare gtri > :second-immediate t)
(instruction-compare gtrr >)

(instruction-compare eqir = :first-immediate t)
(instruction-compare eqri = :second-immediate t)
(instruction-compare eqrr =)


;;;; "Compiler"

(defstruct program
  instructions
  ip)

(define-condition parse-error (error)
  ((text :initarg :text :reader text)))

(define-condition unknown-directive (parse-error) ())
(define-condition unknown-instruction (parse-error) ())

(defun directivep (string)
  (and (< 0 (length string))
       (char= #\# (schar string 0))))

(defun parse-directive (string)
  (cond
    ((string= "#ip " (subseq string 0 4))
     (let ((groups (nth-value 1 (ppcre:scan-to-strings "^#ip (\\d+)$" string))))
       (when (not groups)
         (error 'parse-error :text string))
       (cons :ip (parse-integer (svref groups 0)))))
    (t
     (error 'unknown-directive :text string))))

(defun parse-instruction (string)
  (let ((groups (nth-value 1 (ppcre:scan-to-strings "^([a-z]+) (\\d+) (\\d+) (\\d+)$" string))))
    (when (not groups)
      (error 'parse-error :text string))
    (let ((instruction-name (intern (string-upcase (svref groups 0))
                                    (find-package :elfcode))))
      (when (not (member instruction-name *instructions*))
        (error 'unknown-instruction :text string))
      (cons instruction-name (mapcar (lambda (i) (parse-integer (svref groups i)))
                                     '(1 2 3))))))

(defun parse-program (program-string)
  (if (endp program-string)
      (values nil nil)
      (let ((this-string (car program-string)))
        (multiple-value-bind (instructions directives)
            (parse-program (cdr program-string))
          (values
           (if (not (directivep this-string))
               (cons (parse-instruction this-string) instructions)
               instructions)
           (if (directivep this-string)
               (cons (parse-directive this-string) directives)
               directives))))))

(defun compile-instruction (instruction)
  (let* ((op (first instruction))
         (args (rest instruction))
         (registers (gensym "REGISTERS")))
    (eval
     `(lambda (,registers)
        ,(format nil "~A ~{~A~^ ~}" op args)
        (,op ,registers ,@args)))))

(defun compile (program-string)
  (if (stringp program-string)
      (compile (ppcre:split "\\n" program-string))
      (multiple-value-bind (instructions directives)
          (parse-program program-string)
        (make-program
         :instructions (make-array (length instructions)
                                   :initial-contents (mapcar #'compile-instruction instructions))
         :ip (if (assoc :ip directives)
                 (cdr (assoc :ip directives))
                 nil)))))


;;;; Execution

(define-condition breakpoint (serious-condition)
  ((ip :initarg :ip :reader breakpoint-ip)
   (registers :initarg :registers :reader breakpoint-registers)
   (instruction :initarg :instruction :reader breakpoint-instruction)))

;; Fun fact: I tried doing this with iterate.  The following tail-call
;; recursive function was three times faster.
(defun execute-instruction (instructions ip-register registers ip instruction-count breakpoints)
  (if (<= (length instructions) ip)
      (values registers instruction-count)
      (progn
        (setf (svref registers ip-register) ip)
        (when (member ip breakpoints)
          (restart-case
              (error 'breakpoint :ip ip :registers registers :instruction (svref instructions ip))
            (:continue ()
              :report "Continue program execution."
              nil)
            (:halt ()
              :report "Halt program execution, as if the instruction pointer had gone out of bounds."
              (return-from execute-instruction
                (values registers instruction-count)))))
        (funcall (svref instructions ip) registers)
        (execute-instruction instructions ip-register registers
                             (1+ (svref registers ip-register))
                             (1+ instruction-count)
                             breakpoints))))

(defun execute (program &key (initial-registers '(0 0 0 0 0 0)) (initial-ip 0) (breakpoints nil))
  (let ((registers (make-array (length initial-registers) :initial-contents initial-registers)))
    (with-slots (instructions (ip-register ip)) program
      (execute-instruction instructions ip-register registers initial-ip 0 breakpoints))))
