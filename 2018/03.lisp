(in-package :aoc-2018-03)

(aoc:define-day 100261 251)


;;;; Input

(defparameter *input* (aoc:input))
(defparameter *test-input*
  '("#1 @ 1,3: 4x4"
    "#2 @ 3,1: 4x4"
    "#3 @ 5,5: 2x2"))

(defstruct claim
  id
  ul
  lr)

(defun parse-claim (claim-str)
  (let ((groups (nth-value 1 (ppcre:scan-to-strings
                              "^#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)$"
                              claim-str))))
    (let ((id (parse-integer (svref groups 0)))
          (ul (complex
               (parse-integer (svref groups 1))
               (parse-integer (svref groups 2))))
          (dims (complex
                 (parse-integer (svref groups 3))
                 (parse-integer (svref groups 4)))))
      (make-claim :id id :ul ul :lr (+ ul dims)))))


;;;; Part 1

(defun apply-claim (claim fabric)
  (iter (for x from (realpart (claim-ul claim))
               below (realpart (claim-lr claim)))
        (iter (for y from (imagpart (claim-ul claim))
                     below (imagpart (claim-lr claim)))
              (setf (aref fabric y x) (cons (claim-id claim) (aref fabric y x))))))

(defun apply-claims (claims)
  (destructuring-bind (max-x max-y)
      (iter (for claim in claims)
            (maximizing (realpart (claim-lr claim)) into max-x)
            (maximizing (imagpart (claim-lr claim)) into max-y)
            (finally (return (list max-x max-y))))
    (let ((fabric (make-array (list (1+ max-x) (1+ max-y)) :initial-element nil)))
      (iter (for claim in claims)
            (apply-claim claim fabric))
      fabric)))

(defun count-overlaps (claim-strs)
  (let ((fabric (apply-claims (mapcar #'parse-claim claim-strs))))
    (count-if (lambda (c) (< 1 (length c)))
              (make-array (array-total-size fabric)
                          :displaced-to fabric))))

(aoc:given 1
  (= 4 (count-overlaps *test-input*)))

(defun get-answer-1 ()
  (count-overlaps *input*))


;;;; Part 2

(defun find-non-overlapping-claim (claim-strs)
  (let ((fabric (apply-claims (mapcar #'parse-claim claim-strs)))
        (claim-counts (make-hash-table)))
    (iter (for x from 0 below (array-dimension fabric 1))
          (iter (for y from 0 below (array-dimension fabric 0))
                (iter (for id in (aref fabric y x))
                      (setf (gethash id claim-counts)
                            (max (gethash id claim-counts 0)
                                 (length (aref fabric y x)))))))
    (iter (for (id count) in-hashtable claim-counts)
          (finding id such-that (= 1 count)))))

(aoc:given 2
  (= 3 (find-non-overlapping-claim *test-input*)))

(defun get-answer-2 ()
  (find-non-overlapping-claim *input*))


;;;; Visualization

(defun draw-claim (claim)
  (with-slots (ul lr) claim
    (cairo:rectangle (realpart ul) (imagpart ul)
                     (realpart (- lr ul)) (imagpart (- lr ul)))))

(defun draw-claims (claim-strs filename)
  (let* ((claims (mapcar #'parse-claim claim-strs))
         (fabric (apply-claims claims))
         (winning-claim (find (find-non-overlapping-claim claim-strs)
                              claims
                              :key #'claim-id)))
    (cairo:with-png-file (filename
                          :rgb24
                          (aops:dim fabric 1)
                          (aops:dim fabric 0))
      (viz:set-color :light-background)
      (cairo:paint)
      (viz:set-color :light-primary)
      (iter (for claim in claims)
            (draw-claim claim))
      (cairo:fill-path)
      (viz:set-color :light-secondary)
      (iter (for y below (aops:dim fabric 0))
            (iter (for x below (aops:dim fabric 1))
                  (when (< 1 (length (aref fabric y x)))
                    (cairo:rectangle x y 1 1))))
      (cairo:fill-path)
      (viz:set-color :red)
      (draw-claim winning-claim)
      (cairo:fill-path))))
