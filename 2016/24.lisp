(in-package :aoc-2016-24)

(aoc:define-day 490 744)


;;;; Parsing

(defun parse-map (lines)
  (let ((map (make-array (list (length lines) (length (first lines)))
                         :element-type 'bit
                         :initial-element 0)))
    (iter
      (for line in lines)
      (for row from 0)
      (iter
        (for char in-string line)
        (for col from 0)
        (when (char= #\# char)
          (setf (aref map row col) 1))))
    map))

(defun parse-targets (lines)
  (let ((targets (fset:empty-map)))
    (iter
      (for line in lines)
      (for row from 0)
      (iter
        (for char in-string line)
        (for col from 0)
        (when (and (not (char= #\# char))
                   (not (char= #\. char)))
          (fset:adjoinf targets
                        (- (char-code char) (char-code #\0))
                        (point:make-point col row)))))
    targets))


;;;; Input

(defparameter *example-map* (parse-map (aoc:input :file "examples/24.txt")))
(defparameter *example-targets* (parse-targets (aoc:input :file "examples/24.txt")))
(defparameter *map* (parse-map (aoc:input)))
(defparameter *targets* (parse-targets (aoc:input)))


;;;; Part 1

(defparameter +travel-vectors+
  (list #<-1 0> #<0 -1> #<0 1> #<1 0>))

(defun make-travel-option-fn (map)
  (let ((min-point #<-1 -1>)
        (max-point (point:make-point (array-dimension map 1) (array-dimension map 0))))
    (lambda (point)
      (remove nil
              (mapcar (lambda (vector)
                        (let ((new-point (point:+ point vector)))
                          (when (and (point:< min-point new-point max-point)
                                     (zerop (point:aref map new-point)))
                            (list 1 new-point))))
                      +travel-vectors+)))))

(defun travel-cost (map start end)
  (nth-value 1
             (aoc:shortest-path start
                                (make-travel-option-fn map)
                                :end end
                                :heuristic (alexandria:curry #'point:manhattan-distance end))))

(defun get-travel-costs (map targets)
  (let ((costs (fset:empty-map)))
    (aoc:visit-subsets (lambda (points)
                         (destructuring-bind (p1 p2) (fset:convert 'list points)
                           (let ((cost (travel-cost map
                                                    (fset:lookup targets p1)
                                                    (fset:lookup targets p2))))
                             (fset:adjoinf costs (cons p1 p2) cost)
                             (fset:adjoinf costs (cons p2 p1) cost))))
                       (fset:domain targets)
                       2)
    costs))


(defun make-route-option-fn (travel-costs)
  (lambda (state)
    (destructuring-bind (location . remaining) state
      (fset:reduce (lambda (result new-location)
                     (cons (list (fset:lookup travel-costs (cons location new-location))
                                 (cons new-location (fset:less remaining new-location)))
                           result))
                   remaining
                   :initial-value nil))))

(defun route-state-equal-p (s1 s2)
  (and (= (car s1) (car s2))
       (fset:equal? (cdr s1) (cdr s2))))

(defun find-optimal-route (map targets)
  (let ((travel-costs (get-travel-costs map targets)))
    (aoc:shortest-path (cons 0 (fset:less (fset:domain targets) 0))
                       (make-route-option-fn travel-costs)
                       :finishedp (lambda (state) (fset:empty? (cdr state))))))

(defun get-answer-1 (&optional (map *map*) (targets *targets*))
  (nth-value 1 (find-optimal-route map targets)))


;;;; Part 2

(defun make-circular-option-fn (travel-costs)
  (lambda (state)
    (destructuring-bind (location . remaining) state
      (if (fset:empty? remaining)
          (list (list (fset:lookup travel-costs (cons location 0))
                      (cons 0 remaining)))
          (fset:reduce (lambda (result new-location)
                         (cons (list (fset:lookup travel-costs (cons location new-location))
                                     (cons new-location (fset:less remaining new-location)))
                               result))
                       remaining
                       :initial-value nil)))))

(defun find-circular-route (map targets)
  (let ((travel-costs (get-travel-costs map targets)))
    (aoc:shortest-path (cons 0 (fset:less (fset:domain targets) 0))
                       (make-circular-option-fn travel-costs)
                       :finishedp (lambda (state) (and (zerop (car state)) (fset:empty? (cdr state)))))))

(defun get-answer-2 (&optional (map *map*) (targets *targets*))
  (nth-value 1 (find-circular-route map targets)))


;;;; Visualization

(defun draw-map (map targets)
  (let ((target-points (aoc:invert-map targets)))
    (aoc:draw-svg-grid (array-dimension map 1)
                       (array-dimension map 0)
                       (lambda (point)
                         (cond
                           ((plusp (point:aref map point))
                            t)
                           ((fset:lookup target-points point)
                            (list :shape :circle
                                  :color :dark-highlight
                                  :character (fset:lookup target-points point))))))))
