(in-package :aoc-2016-11)

;; Part 1 is 47, but it's too slow
;; Part 2's answer is 71, but it takes a couple of minutes to run so I'm
;; not including it in the automatic tests.
(aoc:define-day nil nil)


;;;; Input

(defparameter *example*
  '("The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip."
    "The second floor contains a hydrogen generator."
    "The third floor contains a lithium generator."
    "The fourth floor contains nothing relevant."))
(defparameter *input* (aoc:input))


;;;; Data Structures

;;; Structs

(defstruct (state
             (:print-object print-state))
  (elevator 1 :type (integer 1 4))
  (floors (fset:empty-map (make-items)) :type fset:map))

(defstruct items
  (pairs 0 :type (integer 0))
  (microchips (fset:empty-set) :type (or fset:set fset:bag))
  (generators (fset:empty-set) :type (or fset:set fset:bag)))

;;; Struct printing

(defun item-abbreviation (item)
  "Abbreviates the standard item names."
  (case item
    (hydrogen "H")
    (lithium "Li")
    (cobalt "Co")
    (ruthenium "Ru")
    (polonium "Po")
    (promethium "Pm")
    (thulium "Tm")
    (t item)))

(defun summarize-items (items)
  (flet ((initials (set)
           (mapcar #'item-abbreviation (fset:convert 'list set))))
    (format nil "~[~:;~:*~AP ~]~@[~{~AM ~}~]~@[~{~AG ~}~]"
            (items-pairs items)
            (initials (items-microchips items))
            (initials (items-generators items)))))

(defun floor-summary-string (state floor)
  (format nil "F~A ~A ~A"
          floor
          (if (= floor (current-floor state))
              "E"
              ".")
          (summarize-items (floor-items state floor))))

(defun print-state (state stream)
  (format stream "#<STATE~%~{  ~A~%~}>"
          (mapcar (alexandria:curry #'floor-summary-string state)
                  '(4 3 2 1))))

;;; Struct access

(defun all-item-symbols (state)
  (fset:reduce (lambda (result floor items)
                 (declare (ignore floor))
                 (fset:union result (items-microchips items)))
               (state-floors state)
               :initial-value (fset:empty-set)))

(defun next-item-symbol (state &key start)
  (iter (with symbols = (all-item-symbols state))
        (for i from (1+ (or start 0)))
        (finding i such-that (not (fset:member? i symbols)))))

(defun count-items (items)
  (+ (* 2 (items-pairs items))
     (fset:size (items-microchips items))
     (fset:size (items-generators items))))

(defun current-floor (state)
  (state-elevator state))

(defun floor-items (state &optional floor)
  "Returns the items on the given floor.  Defaults to the current floor."
  (fset:lookup (state-floors state) (or floor (current-floor state))))

(defun symbols-for-floor-remap (floors)
  (labels ((remap-r (floor n)
             (if (< 4 floor)
                 (fset:empty-map)
                 (let ((this-map (fset:empty-map))
                       (this-n n))
                   (fset:do-set (s (items-microchips (fset:lookup floors floor)))
                     (fset:adjoinf this-map s (incf this-n)))
                   (fset:map-union (remap-r (1+ floor) this-n)
                                   this-map)))))
    (remap-r 1 0)))

(defun remap-symbol (symbol-remap symbol)
  (fset:lookup symbol-remap symbol))

(defun remap-items (symbol-remap items)
  (make-items :pairs (items-pairs items)
              :microchips (fset:image (alexandria:curry #'remap-symbol symbol-remap)
                                      (items-microchips items))
              :generators (fset:image (alexandria:curry #'remap-symbol symbol-remap)
                                      (items-generators items))))

(defun remap-floors (symbol-remap floors)
  (fset:image (lambda (floor items)
                (values floor (remap-items symbol-remap items)))
              floors))

;; (make-items :pairs 1)
;; (make-items :generators (fset:set 'lithium t))
;; (make-items :microchips (fset:bag t t))
(defun remove-items-from-floor (state items &optional floor)
  "ITEMS is an items struct, but the microchips and generators may be bags
  instead of sets.  For each member of the microchips and generators
  bags/sets that is T (rather than a symbol), one pair will be split.

  FLOOR will default to the current floor if omitted.

  Returns two values:  The new state, with the requested items removed;
  and an items struct containing the removed items."
  (let* ((real-floor (or floor (current-floor state)))
         (old-items (floor-items state real-floor)))
    (iter (for fn in '(items-microchips items-generators))
          (for item-type in '(microchips generators))
          (assert (fset:empty?
                   (fset:set-difference (fset:less (fset:convert 'fset:set (funcall fn items))
                                                   t)
                                        (funcall fn old-items)))
                  ()
                  "Cannot remove ~A ~A from ~A"
                  item-type (funcall fn items) (funcall fn old-items)))
    (let* ((microchip-splits (fset:multiplicity (items-microchips items) t))
           (generator-splits (fset:multiplicity (items-generators items) t))
           (microchip-symbols (fset:convert 'fset:set
                                            (iter (repeat microchip-splits)
                                                  (for symbol
                                                       first (next-item-symbol state)
                                                       then (next-item-symbol state :start symbol))
                                                  (collecting symbol))))
           (generator-symbols (fset:convert 'fset:set
                                            (iter (repeat generator-splits)
                                                  (for symbol
                                                       first (next-item-symbol state :start (fset:greatest microchip-symbols))
                                                       then (next-item-symbol state :start symbol))
                                                  (collecting symbol))))
           (new-items (make-items
                       :pairs (- (items-pairs old-items)
                                 (items-pairs items)
                                 microchip-splits
                                 generator-splits)
                       :microchips
                       (fset:union (fset:set-difference (items-microchips old-items)
                                                        (fset:convert 'fset:set (items-microchips items)))
                                   generator-symbols)
                       :generators
                       (fset:union (fset:set-difference (items-generators old-items)
                                                        (fset:convert 'fset:set (items-generators items)))
                                   microchip-symbols)))
           (removed-items (make-items
                           :pairs (items-pairs items)
                           :microchips
                           (fset:union (fset:less (fset:convert 'fset:set (items-microchips items))
                                                  t)
                                       microchip-symbols)
                           :generators
                           (fset:union (fset:less (fset:convert 'fset:set (items-generators items))
                                                  t)
                                       generator-symbols))))
      (values (make-state :elevator (state-elevator state)
                          :floors (fset:with (state-floors state)
                                             real-floor new-items))
              removed-items))))

(defun add-items-to-floor (state items &optional floor)
  "Defaults to current floor."
  (let* ((real-floor (or floor (current-floor state)))
         (old-items (floor-items state real-floor))
         (new-microchips (fset:union (items-microchips items)
                                     (items-microchips old-items)))
         (new-generators (fset:union (items-generators items)
                                     (items-generators old-items)))
         (new-floors (fset:with (state-floors state)
                        real-floor
                        (make-items
                         :pairs (+ (items-pairs items)
                                   (items-pairs old-items)
                                   (fset:size (fset:intersection new-microchips new-generators)))
                         :microchips (fset:set-difference new-microchips new-generators)
                         :generators (fset:set-difference new-generators new-microchips))))
         (symbol-remap (symbols-for-floor-remap new-floors)))
    (make-state
     :elevator (state-elevator state)
     :floors (remap-floors symbol-remap new-floors))))

(defun move-elevator (state new-floor items)
  "See REMOVE-ITEMS-FROM-FLOOR for how to deal with pairs in ITEMS."
  (assert (= 1 (abs (- new-floor (current-floor state))))
          () "Cannot move from floor ~A to floor ~A" (current-floor state) new-floor)
  (assert (<= 1 (count-items items) 2)
          () "Incorrect number of items to move: ~A" items)
  (multiple-value-bind (removed-state removed-items)
      (remove-items-from-floor state items)
    (add-items-to-floor (make-state :elevator new-floor
                                    :floors (state-floors removed-state))
                        removed-items)))


;;;; Parsing

(defun parse-floors (floors)
  (make-state
   :floors (reduce #'fset:map-union
                   (mapcar (lambda (floor) (parseq:parseq 'floor floor)) floors))
   :elevator 1))

(parseq:defrule floor ()
  (or occupied-floor empty-floor))

(parseq:defrule empty-floor ()
  (and "The " nth " floor contains nothing relevant.")
  (:choose 1)
  (:lambda (floor) (fset:map (floor (make-items)))))

(parseq:defrule occupied-floor ()
  (and "The " nth " floor contains " (aoc:comma-list item) ".")
  (:choose 1 3)
  (:lambda (floor items)
    (fset:map (floor (segment-items items)))))

(defun segment-items (item-conses)
  (flet ((extract (item-type)
           (fset:image #'car (fset:filter (lambda (item) (eq (cdr item) item-type))
                                          (fset:convert 'fset:set item-conses)))))
    (let* ((microchips (extract 'microchip))
           (generators (extract 'generator))
           (pairs (fset:intersection microchips generators)))
      (multiple-value-bind (solo-microchips solo-generators)
          (fset:set-difference-2 microchips generators)
        (make-items
         :pairs (fset:size pairs)
         :microchips solo-microchips
         :generators solo-generators)))))


(let ((ordinals (fset:map ("first" 1)
                          ("second" 2)
                          ("third" 3)
                          ("fourth" 4))))
  (parseq:defrule nth ()
    (+ alpha)
    (:string)
    (:lambda (string) (fset:lookup ordinals string))
    (:test (&optional n) n)))

(parseq:defrule item ()
  (or generator microchip))

(parseq:defrule generator ()
  (and (or "a " "an ") element " generator")
  (:choose 1)
  (:string)
  (:function (lambda (e) (cons (intern (string-upcase e)) 'generator))))

(parseq:defrule microchip ()
  (and (or "a " "an ") element "-compatible microchip")
  (:choose 1)
  (:string)
  (:function (lambda (e) (cons (intern (string-upcase e)) 'microchip))))

(parseq:defrule element ()
  (+ alpha)
  (:string))


;;;; Part 1

(defun floor-safe-p (items)
  (or (fset:empty? (fset:set-difference (items-microchips items)
                                        (items-generators items)))
      (and (zerop (items-pairs items))
           (fset:empty? (items-generators items)))))

(defun floors-safe-p (floors)
  (fset:reduce (lambda (result floor-no items)
                 (declare (ignore floor-no))
                 (and result (floor-safe-p items)))
               floors
               :initial-value t))

(defun list-subsets (items subset-size)
  (labels ((list-r (remaining-items remaining-size)
             (cond
               ((= (fset:size remaining-items) remaining-size)
                (list remaining-items))
               ((< (fset:size remaining-items) remaining-size)
                nil)
               ((zerop remaining-size)
                (list (fset:empty-set)))
               (t
                (let ((item (fset:arb remaining-items)))
                  (nconc (mapcar (lambda (subset) (fset:with subset item))
                                 (list-r (fset:less remaining-items item)
                                         (1- remaining-size)))
                         (list-r (fset:less remaining-items item)
                                 remaining-size)))))))
    (cond
      ;; special-case some easy values
      ((zerop subset-size)
       nil)
      ((= 1 subset-size)
       (mapcar (lambda (item) (fset:set item)) (fset:convert 'list items)))
      ((= (fset:size items) subset-size)
       (list items))
      ((= (1- (fset:size items)) subset-size)
         (mapcar (lambda (item) (fset:less items item))
                 (fset:convert 'list items)))
      (t
       (list-r items subset-size)))))

(defun list-movement-candidates (items)
  (let ((result nil))
    (when (<= 2 (items-pairs items))
      (push (make-items :microchips (fset:bag t t)) result)
      (push (make-items :microchips (fset:set t) :generators (fset:set t)) result)
      (push (make-items :generators (fset:bag t t)) result))
    (when (<= 1 (items-pairs items))
      (push (make-items :pairs 1) result)
      (push (make-items :microchips (fset:set t)) result)
      (push (make-items :generators (fset:set t)) result)
      (fset:do-set (microchip (items-microchips items))
        (push (make-items :microchips (fset:set microchip t)) result)
        (push (make-items :microchips (fset:set microchip) :generators (fset:set t)) result))
      (fset:do-set (generator (items-generators items))
        (push (make-items :generators (fset:set generator t)) result)
        (push (make-items :microchips (fset:set t) :generators (fset:set generator)) result)))
    (dolist (microchips (list-subsets (items-microchips items) 2))
      (push (make-items :microchips microchips) result))
    (fset:do-set (microchip (items-microchips items))
      (push (make-items :microchips (fset:set microchip)) result)
      (fset:do-set (generator (items-generators items))
        (push (make-items :microchips (fset:set microchip) :generators (fset:set generator))
              result)))
    (dolist (generators (list-subsets (items-generators items) 2))
      (push (make-items :generators generators) result))
    (fset:do-set (generator (items-generators items))
      (push (make-items :generators (fset:set generator)) result))
    result))

(defun first-occupied-floor (state)
  (iter (for i from 1 to 4)
        (finding i such-that (plusp (count-items (floor-items state i))))))

(defun apply-moves (state new-floor item-list)
  (remove-if-not
   (lambda (state) (floors-safe-p (state-floors state)))
   (mapcar (lambda (items) (move-elevator state new-floor items))
           item-list)))

(defun list-next-moves (state)
  (let* ((next-candidates (list-movement-candidates (floor-items state)))
         (single-moves (remove-if-not (lambda (i) (= 1 (count-items i))) next-candidates))
         (double-moves (remove-if-not (lambda (i) (= 2 (count-items i))) next-candidates))
         (up-moves (and (<= (1+ (current-floor state)) 4)
                        (or (apply-moves state (1+ (current-floor state)) double-moves)
                            (apply-moves state (1+ (current-floor state)) single-moves))))
         (down-moves (and (<= (first-occupied-floor state) (1- (current-floor state)))
                          (or (apply-moves state (1- (current-floor state)) single-moves)
                              (apply-moves state (1- (current-floor state)) double-moves)))))
    (nconc up-moves down-moves)))

(defun fourth-floor-full-p (state)
  (fset:reduce (lambda (result floor items)
                 (and result
                      (or (= floor 4)
                          (zerop (count-items items)))))
               (state-floors state)
               :initial-value t))

(defun floor-steps-to-end (floor items)
  (+ (* (- 4 floor) (ceiling (count-items items) 2))
     (* (- 4 floor) (max 0 (- (ceiling (count-items items) 2)
                              2)))))

(defun min-steps-to-end (state)
  (fset:reduce (lambda (sum floor items)
                 (+ sum (floor-steps-to-end floor items)))
               (state-floors state)
               :initial-value 0))

(defun get-answer-1 (&optional (input *input*))
  (multiple-value-bind (path steps)
      (aoc:shortest-path (parse-floors input)
                         (lambda (state) (mapcar (lambda (s) (list 1 s)) (list-next-moves state)))
                         :finishedp #'fourth-floor-full-p
                         :test 'equalp
                         :heuristic #'min-steps-to-end)
    (values steps path)))

(aoc:given 1
  (= 11 (get-answer-1 *example*)))


;;;; Part 2

(defun get-answer-2 (&optional (input *input*))
  (let ((initial-state (add-items-to-floor (parse-floors input)
                                           (make-items :pairs 2)
                                           1)))
    (multiple-value-bind (path steps)
        (aoc:shortest-path initial-state
                           (lambda (state) (mapcar (lambda (s) (list 1 s)) (list-next-moves state)))
                           :finishedp #'fourth-floor-full-p
                           :test 'equalp
                           :heuristic #'min-steps-to-end)
      (values steps path))))
