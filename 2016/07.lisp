(in-package :aoc-2016-07)

(aoc:define-day 115 231)


;;;; Input

(defparameter *example-true-1* "abba[mnop]qrst")
(defparameter *example-false-1* "abcd[bddb]xyyx")
(defparameter *example-false-2* "aaaa[qwer]tyui")
(defparameter *example-true-2* "ioxxoj[asdfgh]zxcvbn")

(defparameter *ssl-true-1* "aba[bab]xyz")
(defparameter *ssl-false-1* "xyx[xyx]xyx")
(defparameter *ssl-true-2* "aaa[kek]eke")
(defparameter *ssl-true-3* "zazbz[bzb]cdb")

(defparameter *addresses* (aoc:input))


;;;; Part 1

(defun palindromep (string index length)
  (or (<= length 0)
      (and (or (= length 2)
               (char/= (schar string index) (schar string (1- index))))
           (char= (schar string index) (schar string (- index (1- length))))
           (palindromep string (1- index) (- length 2)))))

(defun list-palindromes (address length)
  (let ((supernet-sequences (fset:empty-set))
        (hypernet-sequences (fset:empty-set)))
    (iter (with in-hypernet = 0)
          (for c in-string address from (1- length) with-index i)
          (case c
            (#\[ (incf in-hypernet))
            (#\] (decf in-hypernet)))
          ;; Sanity check for nested brackets
          (when (not (<= 0 in-hypernet 1))
            (format t "~A ~A~%" in-hypernet address))
          (when (palindromep address i length)
            (let ((sequence (subseq address (1+ (- i length)) (1+ i))))
              (if (plusp in-hypernet)
                  (fset:adjoinf hypernet-sequences sequence)
                  (fset:adjoinf supernet-sequences sequence)))))
    (values supernet-sequences hypernet-sequences)))

(defun tlsp (address)
  (multiple-value-bind (supernet-sequences hypernet-sequences) (list-palindromes address 4)
    (and (plusp (fset:size supernet-sequences))
         (zerop (fset:size hypernet-sequences)))))

(aoc:deftest given-1
  (5am:is-true (tlsp *example-true-1*))
  (5am:is-false (tlsp *example-false-1*))
  (5am:is-false (tlsp *example-false-2*))
  (5am:is-true (tlsp *example-true-2*))
  (5am:is-false (tlsp "abba[xyyx]foobar")))

(defun get-answer-1 ()
  (length (remove-if-not #'tlsp *addresses*)))


;;;; Part 2

(defun invert-palindrome-triple (string)
  (format nil "~A~A~0@*~A" (schar string 1) (schar string 0)))

(defun sslp (address)
  (multiple-value-bind (supernet-sequences hypernet-sequences) (list-palindromes address 3)
    (plusp (fset:size (fset:intersection (fset:image #'invert-palindrome-triple supernet-sequences)
                                         hypernet-sequences)))))

(aoc:deftest given-2
  (5am:is-true (sslp *ssl-true-1*))
  (5am:is-true (sslp *ssl-true-2*))
  (5am:is-true (sslp *ssl-true-3*))
  (5am:is-false (sslp *ssl-false-1*)))

(defun get-answer-2 ()
  (length (remove-if-not #'sslp *addresses*)))
