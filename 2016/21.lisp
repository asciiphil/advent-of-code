(in-package :aoc-2016-21)

(aoc:define-day "aefgbcdh" "egcdahbf")


;;;; Parsing

(parseq:defrule operation ()
    (or swap-position swap-letter rotate rotate-letter reverse move))

(parseq:defrule swap-position ()
    (and "swap position " aoc:integer-string " with position " aoc:integer-string)
  (:choose 1 3)
  (:lambda (p1 p2) (list :swap-position p1 p2)))

(parseq:defrule swap-letter ()
    (and "swap letter " char " with letter " char)
  (:choose 1 3)
  (:lambda (c1 c2) (list :swap-letter c1 c2)))

(parseq:defrule rotate ()
    (and "rotate " (or "left" "right") " " aoc:integer-string " step" (? "s"))
  (:choose 1 3)
  (:lambda (dir steps) (list :rotate (* steps (if (string= dir "left") -1 1)))))

(parseq:defrule rotate-letter ()
    (and "rotate based on position of letter " char)
  (:choose 1)
  (:lambda (letter) (list :rotate-letter letter)))

(parseq:defrule reverse ()
    (and "reverse positions " aoc:integer-string " through " aoc:integer-string)
  (:choose 1 3)
  (:lambda (p1 p2) (list :reverse p1 p2)))

(parseq:defrule move ()
    (and "move position " aoc:integer-string " to position " aoc:integer-string)
  (:choose 1 3)
  (:lambda (p1 p2) (list :move p1 p2)))


;;;; Input

(defparameter *example* (aoc:input :parse-line 'operation :file "examples/21.txt"))
(defparameter *operations* (aoc:input :parse-line 'operation))


;;;; Part 1

(defun op-swap-position (seq p1 p2)
  (fset:with (fset:with seq
                        p1
                        (fset:lookup seq p2))
             p2
             (fset:lookup seq p1)))

(defun op-swap-letter (seq l1 l2)
  (let ((p1 (fset:position l1 seq))
        (p2 (fset:position l2 seq)))
    (op-swap-position seq p1 p2)))

(defun op-rotate (seq steps)
  (let ((left-steps (mod (- steps) (fset:size seq))))
    (fset:concat (fset:subseq seq left-steps) (fset:subseq seq 0 left-steps))))

(defun op-rotate-letter (seq l)
  (let ((index (fset:position l seq)))
    (op-rotate seq
               (+ 1 index (if (< index 4) 0 1)))))

(defun op-reverse (seq p1 p2)
  (fset:concat (fset:subseq seq 0 p1)
               ;; Work around https://github.com/slburson/fset/issues/40
               (fset:convert 'fset:seq
                             (fset:reverse (fset:convert 'string (fset:subseq seq p1 (1+ p2)))))
               (fset:subseq seq (1+ p2) (fset:size seq))))

(defun op-move (seq p1 p2)
  (fset:insert (fset:less seq p1)
               p2 (fset:lookup seq p1)))

(defun apply-operation (operation seq)
  (let ((fn (ecase (first operation)
              (:swap-position #'op-swap-position)
              (:swap-letter #'op-swap-letter)
              (:rotate #'op-rotate)
              (:rotate-letter #'op-rotate-letter)
              (:reverse #'op-reverse)
              (:move #'op-move))))
    (apply fn (cons seq (rest operation)))))

(defun apply-operations (operations seq)
  (if (endp operations)
      seq
      (apply-operations (cdr operations)
                        (apply-operation (car operations)
                                         seq))))

(defun get-answer-1 (&optional (operations *operations*) (start-string "abcdefgh"))
  (fset:convert 'string (apply-operations operations (fset:convert 'fset:seq start-string))))


;;;; Part 2

;;; Well, I *could* work on functions to reverse the effects of each
;;; scrambling step and then run the operations in reverse.  But some of
;;; those are annoying, like "rotate based on position of letter X".  So
;;; let's just try running through all of the permutations of the
;;; constituent letters and seeing which one gives the right result.
;;; (This takes about three seconds; the other approach would be faster,
;;; but this is fine.)

(defun get-answer-2 (&optional (operations *operations*) (target "fbgdceah"))
  (aoc:visit-permutations (lambda (seq)
                            (when (string= (get-answer-1 operations seq)
                                           target)
                              (return-from get-answer-2 seq)))
                          target
                          :copy t))
