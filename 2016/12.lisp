(in-package :aoc-2016-12)

(aoc:define-day 318007 9227661)


;;;; Input

(defparameter *program* (aoc:input))


;;;; Part 1

(defun get-answer-1 (&optional (program *program*))
  (fset:lookup (assembunny:run (assembunny:compile program))
               :a))


;;;; Part 2

(defun get-answer-2 (&optional (program *program*))
  (fset:lookup (assembunny:run (assembunny:compile program)
                               :initial-registers (fset:map (:c 1)))
               :a))
