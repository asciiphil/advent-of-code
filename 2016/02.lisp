(in-package :aoc-2016-02)

(aoc:define-day '(6 1 5 2 9) '(c 2 c 2 8))


;;;; Input

(defparameter *example*
  '("ULL"
    "RRDDD"
    "LURDL"
    "UUUUD"))
(defparameter *instructions* (aoc:input))

(defparameter *keypad*
  #2A((1 2 3)
      (4 5 6)
      (7 8 9)))
(defparameter *keypad2*
  #2A((nil nil 1 nil nil)
      (nil  2  3  4  nil)
      ( 5   6  7  8   9 )
      (nil  a  b  c  nil)
      (nil nil d nil nil)))


;;;; Part 1

(defun instruction-to-vector (instruction-char)
  (ecase instruction-char
    (#\U (point:make-point  0 -1))
    (#\D (point:make-point  0  1))
    (#\L (point:make-point -1  0))
    (#\R (point:make-point  1  0))))

(defun instructions-to-vectors (instruction-string)
  (map 'list #'instruction-to-vector instruction-string))

;; FIXME: This only works if VECTOR is a unit vector.
;; This works: (0, 0) + (-1, 0) -> (0, 0)
;; But this doesn't: (1, 1) + (-5, 0) -> (1, 1)
;; (The latter should return (0, 1).)
(defun vector-add-and-clamp (position vector array)
  (let ((new-position (point:+ position vector)))
    (if (and (point:< (point:make-point -1 -1) new-position (apply #'point:make-point (nreverse (array-dimensions array))))
             (point:aref array new-position))
        new-position
        position)))

(defun apply-vectors-and-clamp (position vectors array)
  (if (endp vectors)
      position
      (apply-vectors-and-clamp (vector-add-and-clamp position (car vectors) array)
                               (cdr vectors)
                               array)))

(defun list-buttons (instruction-strings keypad &key (starting-point (point:make-point 1 1)))
  (labels ((list-r (position remaining-strings)
             (if (endp remaining-strings)
                 nil
                 (let ((new-position (apply-vectors-and-clamp
                                      position
                                      (instructions-to-vectors (car remaining-strings))
                                      keypad)))
                   (cons (point:aref keypad new-position)
                         (list-r new-position (cdr remaining-strings)))))))
    (list-r starting-point instruction-strings)))

(defun get-answer-1 ()
  (list-buttons *instructions* *keypad*))


;;;; Part 2

(defun get-answer-2 ()
  (list-buttons *instructions* *keypad2*))
