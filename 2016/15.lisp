(in-package :aoc-2016-15)

(aoc:define-day 317371 2080951)


;;;; Data Structures

(defstruct disk
  size
  offset)


;;;; Parsing

(parseq:defrule disk ()
    (and "Disc #" aoc:integer-string " has " aoc:integer-string " positions; at time=0, it is at position " aoc:integer-string ".")
  (:choose 1 3 5)
  (:lambda (id size position)
    (make-disk :size size :offset (mod (- size position id) size))))


;;;; Input

(defparameter *disks* (aoc:input :parse-line 'disk))

;;;; Part 1

(defun get-synchronization-time (disk-a disk-b)
  (with-slots ((a-size size) (a-offset offset)) disk-a
    (with-slots ((b-size size) (b-offset offset)) disk-b
      (if (< a-size b-size)
          (get-synchronization-time disk-b disk-a)
          (labels ((test-time (time)
                     (if (= (mod time b-size) b-offset)
                         (let ((new-size (lcm a-size b-size)))
                           (make-disk :size new-size :offset (mod time new-size)))
                         (test-time (+ time a-size)))))
            (test-time a-offset))))))

(defun get-answer-1 ()
  (disk-offset (reduce #'get-synchronization-time *disks*)))


;;;; Part 2

(defparameter *extra-disk*
  (let ((new-size 11))
    (make-disk :size new-size
               :offset (mod (- new-size (1+ (length *disks*))) new-size))))

(defun get-answer-2 ()
  (disk-offset (reduce #'get-synchronization-time (cons *extra-disk* *disks*))))
