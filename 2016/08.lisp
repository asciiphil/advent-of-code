(in-package :aoc-2016-08)

;; Part 2 answer is "EFEYKFRFIJ", but it's visual, not computed.
(aoc:define-day 115 nil)


;;;; Input

(defparameter *example*
  '("rect 3x2"
    "rotate column x=1 by 1"
    "rotate row y=0 by 4"
    "rotate column x=1 by 1"))
(defparameter *instructions* (aoc:input))


;;;; Braille character display support

(defun print-screen-braille (screen)
  (aoc:print-array-braille screen :key #'plusp))


;;;; Quadrant character display support

(defun int-to-quadrant (int)
  (ecase int
    (0  #\ )
    (1  #\u2598)
    (2  #\u259d)
    (3  #\u2580)
    (4  #\u2596)
    (5  #\u258c)
    (6  #\u259e)
    (7  #\u259b)
    (8  #\u2597)
    (9  #\u259a)
    (10 #\u2590)
    (11 #\u259c)
    (12 #\u2584)
    (13 #\u2599)
    (14 #\u259f)
    (15 #\u2588)))

(defun quadrant-dot-offset (dot-number)
  (ecase dot-number
    (1 '(0 0))
    (2 '(0 1))
    (3 '(1 0))
    (4 '(1 1))))

(defun quadrant-int-at-pos (array row col)
  (destructuring-bind (row-limit col-limit) (array-dimensions array)
    (iter (with result = 0)
          (for dot-number from 4 downto 1)
          (for (dy dx) = (quadrant-dot-offset dot-number))
          (for y = (+ row dy))
          (for x = (+ col dx))
          (when (and (< -1 y row-limit)
                     (< -1 x col-limit)
                     (plusp (aref array y x)))
            (incf result))
          (setf result (* 2 result))
          (finally (return (truncate result 2))))))

(defun print-screen-quadrant (screen)
  (iter (for y from 0 below (array-dimension screen 0) by 2)
        (iter (for x from 0 below (array-dimension screen 1) by 2)
              (princ (int-to-quadrant (quadrant-int-at-pos screen y x))))
        (format t "~%")))


;;;; Basic Commands

(defun rect (width height screen)
  (iter (for y from 0 below height)
        (iter (for x from 0 below width)
              (setf (aref screen y x) 1)))
  screen)

(defun rotate (row-or-column index distance screen)
  (let* ((seq-length (array-dimension screen (ecase row-or-column
                                               (:row 1)
                                               (:column 0))))
         (new-seq (make-array seq-length :element-type (array-element-type screen))))
    (iter (for i from 0 below seq-length)
          (setf (aref new-seq (mod (+ i distance) seq-length))
                (ecase row-or-column
                  (:row (aref screen index i))
                  (:column (aref screen i index)))))
    (iter (for i from 0 below seq-length)
          (ecase row-or-column
            (:row (setf (aref screen index i) (aref new-seq i)))
            (:column (setf (aref screen i index) (aref new-seq i))))))
  screen)


;;;; Parsing

(parseq:defrule rect ()
    (and "rect " (+ digit) "x" (+ digit))
  (:choose 1 3)
  (:function (lambda (w-seq h-seq)
               (alexandria:curry #'rect
                                 (parse-integer (coerce w-seq 'string))
                                 (parse-integer (coerce h-seq 'string))))))

(parseq:defrule rotate-column ()
    "column x="
  (:constant :column))

(parseq:defrule rotate-row ()
    "row y="
  (:constant :row))

(parseq:defrule rotate ()
    (and "rotate " (or rotate-column rotate-row) (+ digit) " by " (+ digit))
  (:choose 1 2 4)
  (:function (lambda (row-or-column i-seq d-seq)
               (alexandria:curry #'rotate
                                 row-or-column
                                 (parse-integer (coerce i-seq 'string))
                                 (parse-integer (coerce d-seq 'string))))))

(parseq:defrule instruction ()
    (or rotate rect))

(defun parse-instructions (instructions)
  (mapcar (lambda (instruction-string)
            (multiple-value-bind (fn parsedp) (parseq:parseq 'instruction instruction-string)
              (assert parsedp (instruction-string) "Failed to parse string \"~A\"" instruction-string)
              fn))
          instructions))


;;;; Part 1

(defun make-screen (&optional (width 50) (height 6))
  (make-array (list height width) :element-type 'bit :initial-element 0))

(defun run-instructions (instructions &optional (width 50) (height 6))
  (reduce (lambda (screen fn) (funcall fn screen))
          (parse-instructions instructions)
          :initial-value (make-screen width height)))

(defun count-lit-pixels (instructions &optional (width 50) (height 6))
  (reduce #'+ (aoc:flatten-array (run-instructions instructions width height))))

(aoc:given 1
  (= 6 (count-lit-pixels *example* 7 3)))

(defun get-answer-1 ()
  (count-lit-pixels *instructions*))


;;;; Part 2

;;; Just run:
;;;
;;;     (print-screen-quadrant (run-instructions *instructions*))
