(in-package :aoc-2016-14)

;; The solution for part 2 is 22423, but it takes a couple of minutes to
;; complete.  (I'm not sure there's a way to speed it up without some sort
;; of parallelism.  It just has to do a lot of MD5 hashes and I don't
;; there's an algorithmic way to do that faster from my code.)
(aoc:define-day 16106 nil)


;;;; Input

(defparameter *salt* (aoc:input))


;;;; Part 1

(defstruct hashes
  (salt *salt* :type string)
  (reps 0 :type (integer 0))
  (next-index 0 :type (integer 0))
  (triplets (fset:empty-seq) :type fset:seq)
  (quintuplets (fset:empty-map (fset:empty-set))))

(defun hash-string (string repetitions)
  (let ((result (nstring-downcase (format nil "~{~2,'0x~}" (coerce (md5:md5sum-string string) 'list)))))
    (if (zerop repetitions)
        result
        (hash-string result (1- repetitions)))))

(defun hash-index (salt index repetitions)
  (hash-string (format nil "~A~A" salt index) repetitions))

(defun check-for-group (string end-pos max-size)
  "Returns two values: a character, if there's a group ending at END-POS;
  and the number of characters preceding END-POS that match it (up to
  MAX-SIZE)."
  (assert (<= (1- max-size) end-pos (1- (length string))))
  (let ((this-char (schar string end-pos)))
    (if (= 1 max-size)
        (values this-char max-size)
        (multiple-value-bind (smaller-char smaller-group) (check-for-group string end-pos (1- max-size))
          (if (and smaller-char
                   (char= this-char (schar string (1+ (- end-pos max-size)))))
              (values this-char max-size)
              (values nil smaller-group))))))

(defun list-grouped-chars (string group-size)
  (labels ((list-r (end-pos result)
             (if (<= (length string) end-pos)
                 result
                 (multiple-value-bind (group-char subgroup-size)
                     (check-for-group string end-pos group-size)
                   (list-r (+ end-pos
                              (if group-char
                                  group-size
                                  (- group-size subgroup-size)))
                           (if group-char
                               (cons group-char result)
                               result))))))
    (reverse (list-r (1- group-size) nil))))

(defun map-chars-to-n (chars n)
  (fset:convert 'fset:map (mapcar (lambda (c) (cons c (fset:set n))) chars)))

(defun union-with-nil (set1 set2)
  (if set1
      (if set2
          (fset:union set1 set2)
          set1)
      (or set2
          (fset:empty-set))))

(defun add-hash (hashes)
  (with-slots (salt reps next-index triplets quintuplets) hashes
    (let* ((hash (hash-index salt next-index reps))
           (triplet-chars (list-grouped-chars hash 3))
           (quintuplet-chars (list-grouped-chars hash 5)))
      (make-hashes :salt salt
                   :reps reps
                   :next-index (1+ next-index)
                   :triplets (if triplet-chars
                                 (fset:with-last triplets (cons next-index (first triplet-chars)))
                                 triplets)
                   :quintuplets (if quintuplet-chars
                                    (progn
                                      (fset:map-union (map-chars-to-n quintuplet-chars next-index)
                                                      quintuplets
                                                      #'union-with-nil))
                                    quintuplets)))))

(defun ensure-hashes (hashes min-index)
  (if (< min-index (hashes-next-index hashes))
      hashes
      (ensure-hashes (add-hash hashes) min-index)))

(defun find-next-triplet (hashes)
  (with-slots (triplets) hashes
    (labels ((find-r (current-hashes)
               (with-slots ((current-triplets triplets)) current-hashes
                 (if (eq triplets current-triplets)
                     (find-r (add-hash current-hashes))
                     current-hashes))))
      (find-r hashes))))

(defun has-bounded-member-p (set min-value max-value)
  "Returns T if SET has a number that is greater than or equal to
  MIN-VALUE and less than MAX-VALUE."
  (fset:do-set (n set)
    (when (and (<= min-value n)
               (< n max-value))
      (return n))))

(defun find-next-quintuplet (hashes &key char max-index)
  "Returns the next HASHES needed to add a quintuplet.  If CHAR is given,
  continues until a quintuplet is given with that character.  If MAX-INDEX
  is given, stops when it gets to that index (exclusive)."
  (with-slots (quintuplets) hashes
    (labels ((find-r (current-hashes)
               (with-slots ((current-quintuplets quintuplets) next-index) current-hashes
                 (if (or (and max-index
                              (<= max-index next-index))
                         (if char
                             (not (eq (fset:lookup quintuplets char)
                                      (fset:lookup current-quintuplets char)))
                             (not (eq quintuplets current-quintuplets))))
                     current-hashes
                     (find-r (add-hash current-hashes))))))
      (find-r hashes))))

(defun has-quintuplet-p (hashes char min-index max-index)
  "Returns two values: T if there is a CHAR quintuplet with an index
  between min-index (inclusive) and max-index (exclusive), NIL otherwise;
  and the HASHES object necessary make to that assessment."
  (cond
    ((has-bounded-member-p (fset:lookup (hashes-quintuplets hashes) char)
                           min-index max-index)
     ;; A member exists: return T with current values.
     (values t hashes))
    ((<= max-index (hashes-next-index hashes))
     ;; No member exists *and* we've checked all of the indices necessary:
     ;; return NIL with current values.
     (values nil hashes))
    (t
     ;; No member exists, but we haven't checked all possible values.
     ;; Look for a matching quintuplet and check again.
     (has-quintuplet-p (find-next-quintuplet hashes :char char :max-index max-index)
                       char min-index max-index))))

(defun triplet-char (hashes index)
  "If INDEX has a triplet, returns the character, otherwise returns NIL.
  The second valus is the HASHES object necessary to make the
  determination."
  (let ((hashes-with-index (ensure-hashes hashes index)))
    (fset:do-seq (entry (hashes-triplets hashes-with-index)
                        :value (values nil hashes-with-index))
      (cond
        ((= (car entry) index)
         (return (values (cdr entry) hashes-with-index)))
        ((< index (car entry))
         (return (values nil hashes-with-index)))))))

(defun keyp (hashes index)
  "Returns two values: T or NIL; and the HASHES necessary to make that determination."
  (multiple-value-bind (key-char hashes-with-key) (triplet-char hashes index)
    (if key-char
        (has-quintuplet-p hashes-with-key key-char (1+ index) (+ index 1001))
        (values nil hashes-with-key))))

(defun get-next-key (hashes triplet-pos)
  "TRIPLET-POS is the position at which to start checking.  Returns
  *three* values: the index of the next key; the position of the next
  triplet to check; and the final state of HASHES."
  (with-slots (triplets) hashes
    (if (<= (fset:size triplets) triplet-pos)
        (get-next-key (find-next-triplet hashes) triplet-pos)
        (destructuring-bind (index . char) (fset:lookup triplets triplet-pos)
          (multiple-value-bind (quintuplet hashes-with-quintuplet)
              (has-quintuplet-p hashes char (1+ index) (+ 1001 index))
            (if quintuplet
                (values index (1+ triplet-pos) hashes-with-quintuplet)
                (get-next-key hashes-with-quintuplet (1+ triplet-pos))))))))

(defun list-keys (salt num-keys reps)
  (iter (repeat num-keys)
        (for (values key-index triplet-pos hashes)
             first (get-next-key (make-hashes :salt salt :reps reps) 0)
             then (get-next-key hashes triplet-pos))
        (collecting key-index)))

(defun get-answer-1 (&key (salt *salt*) (reps 0) (nth-key 64))
  (car (last (list-keys salt nth-key reps))))

(aoc:given 1
  (= 22728 (get-answer-1 :salt "abc")))


;;;; Part 2

(defun get-answer-2 (&key (salt *salt*) (reps 2016))
  (get-answer-1 :salt salt :reps reps))

