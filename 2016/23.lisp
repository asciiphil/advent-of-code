(in-package :aoc-2016-23)

;; Part 2 is 479008074, but it takes about half an hour with the naive
;; implementation below.
(aoc:define-day 11514 nil)


;;;; Input

(defparameter *program* (aoc:input))


;;;; Part 1

(defun get-answer-1 (&optional (program *program*))
  (fset:lookup (assembunny:run (assembunny:compile program)
                               :initial-registers (fset:map (:a 7)))
               :a))


;;;; Part 2

(defun get-answer-2 (&optional (program *program*))
  (fset:lookup (assembunny:run (assembunny:compile program)
                               :initial-registers (fset:map (:a 12)))
               :a))
