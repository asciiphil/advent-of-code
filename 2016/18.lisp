(in-package :aoc-2016-18)

(aoc:define-day 1939 19999535)


;;; Parsing

(parseq:defrule tiles ()
    (+ (or trap safe))
  (:lambda (&rest elts)
    (make-array (length elts) :element-type 'bit :initial-contents elts)))

(parseq:defrule trap ()
    "^"
  (:constant 1))

(parseq:defrule safe ()
    "."
  (:constant 0))


;;; Input

(defparameter *first-row* (aoc:input :parse-line 'tiles))

(defparameter *example-1* (parseq:parseq 'tiles "..^^."))


;;; Part 1

(defun ghost-aref (array j i)
  "Returns the element at (i,j) in the array, or 0 if the indices are out
  of bounds."
  (if (and (< -1 j (array-dimension array 0))
           (< -1 i (array-dimension array 1)))
      (aref array j i)
      0))

(defun get-tile-state (tiles j i)
  (boole boole-xor
         (ghost-aref tiles (1- j) (1- i))
         (ghost-aref tiles (1- j) (1+ i))))

(defun expand-rows (first-row row-count)
  (let ((result (make-array (list row-count (length first-row)) :element-type 'bit)))
    (dotimes (i (length first-row))
      (setf (aref result 0 i) (sbit first-row i)))
    (dotimes (j (1- row-count))
      (dotimes (i (length first-row))
        (setf (aref result (1+ j) i)
              (get-tile-state result (1+ j) i))))
    result))

(defun get-answer-1 (&key (first-row *first-row*) (rows 40))
  (- (* rows (length first-row))
     (reduce #'+ (aoc:flatten-array (expand-rows first-row rows)))))


;;; Part 2

(defun get-answer-2 ()
  (get-answer-1 :rows 400000))
