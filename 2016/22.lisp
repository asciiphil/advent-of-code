(in-package :aoc-2016-22)

;; Part 1 is 1038.  See below for why there's no function to calculate it.
;; Part 2 is 252.  See below for why there's no function to calculate it.
(aoc:define-day nil nil)


;;;; Data Structures

(defstruct node
  position
  size
  used)

(defun node-avail (node)
  (- (node-size node) (node-used node)))

(defmethod print-object ((node node) stream)
  (with-slots (position size used) node
    (print-unreadable-object (node stream)
      (format stream "/dev/grid/node-x~A-y~A~5@AT~5@AT~6@AT~5@A%"
              (point:x position) (point:y position)
              size used
              (node-avail node)
              (round (* 100 (/ used size)))))))


;;;; Parsing

(parseq:defrule node ()
    (and "/dev/grid/node-x" aoc:integer-string "-y" aoc:integer-string
         (+ " ") aoc:integer-string "T"
         (+ " ") aoc:integer-string "T"
         (+ " ") aoc:integer-string "T"
         (+ " ") aoc:integer-string "%")
  (:choose 1 3 5 8 11 14)
  (:lambda (x y size used avail pct)
    (assert (= avail (- size used)))
    (assert (< (1- pct) (* 100 (/ used size)) (1+ pct)))
    (make-node :position (point:make-point x y)
               :size size
               :used used)))


;;;; Input

(defparameter *nodes* (mapcar (alexandria:curry #'parseq:parseq 'node)
                              (subseq (aoc:input) 2)))


;;;; Part 1

(defun viable-pair-p (node1 node2)
  (and (not (zerop (node-size node1)))
       (not (point:= (node-position node1) (node-position node2)))
       (<= (node-used node1) (node-avail node2))))


(defun count-viable-pairs (nodes)
  (let ((result 0))
    (aoc:visit-subsets (lambda (subset)
                         (destructuring-bind (n1 n2) subset
                           (when (viable-pair-p n1 n2) (incf result))
                           (when (viable-pair-p n2 n1) (incf result))))
                       nodes
                       2)
    result))

;;; Answer was calculated by hand.  There are 1064 nodes in the set.  25
;;; are very large.  One is empty.  1064 - 25 - 1 = 1038.


;;;; Part 2

;;; Answer was calculated by hand.  The empty node is at (35,24).  There's
;;; a line of immovable nodes from (13, 19) to (37, 19).  It takes 71
;;; moves to get the empty node just to the left of the target node.  From
;;; there, each move of the target node leftward takes five moves.  There
;;; are 36 such cycles needed, plus the final move into the destination.
;;; 71 + 5 * 36 + 1 = 252


;;;; Visualization

(defun nodes-to-map (nodes)
  (fset:reduce (lambda (result node)
                 (fset:with result (node-position node) node))
               nodes
               :initial-value (fset:empty-map)))

(defun max-size (nodes)
  (if (endp nodes)
      0
      (max (node-size (car nodes)) (max-size (cdr nodes)))))

(defun draw-nodes (nodes &key (cell-size 16))
  (let ((node-map (nodes-to-map nodes))
        (max-size (max-size nodes)))
    (multiple-value-bind (min-point max-point) (point:bbox (fset:domain node-map))
      (let* ((dims (point:+ #<1 1> (point:- max-point min-point)))
             (image-width (* (point:x dims) cell-size))
             (image-height (* (point:y dims) cell-size)))
        (aoc:show-svg
         (aoc:with-svg-surface (surface image-width image-height)
           (viz:clear-surface surface :dark-background)
           (cairo:with-context-from-surface (surface)
             (cairo:scale cell-size cell-size)
             (dotimes (y (point:y dims))
               (dotimes (x (point:x dims))
                 (let ((point (point:make-point x y)))
                   (cairo:save)
                   (cairo:translate (+ 0.5 x) (+ 0.5 y))
                   (draw-node max-size (fset:lookup node-map point))
                   (cairo:restore)))))))))))

(defun draw-node (max-size node)
  (cairo:save)
  (cairo:scale 7/8 7/8)
  (apply #'cairo:set-source-rgb
         (colorcet:interpolate colorcet:+L08+
                               (/ (node-size node) max-size)))
  (cairo:rectangle -1/2 -1/2 1 1)
  (cairo:fill-path)
  (apply #'cairo:set-source-rgb
         (colorcet:interpolate colorcet:+L08+
                               (/ (node-used node) max-size)))
  (let ((used-side (sqrt (/ (node-used node) (node-size node)))))
    (cairo:rectangle (- (/ used-side 2))
                     (- (/ used-side 2))
                     used-side
                     used-side))
  (cairo:fill-path)
  (cairo:restore))
