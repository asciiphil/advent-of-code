(in-package :aoc-2016-10)

(aoc:define-day 116 23903)


;;;; Input

(defparameter *example*
  '("value 5 goes to bot 2"
    "bot 2 gives low to bot 1 and high to bot 0"
    "value 3 goes to bot 1"
    "bot 1 gives low to output 1 and high to bot 0"
    "bot 0 gives low to output 2 and high to output 0"
    "value 2 goes to bot 2"))
(defparameter *input* (aoc:input))


;;;; Parsing

;;; The data structures will be twofold:
;;;
;;; First, a map from symbols to maps.  The inner maps are from integers
;;; to bags of integers.  The symbols are either 'bot or 'output.  The
;;; inner maps are the microchips held by each bot or output.
;;;
;;; Second, a map from integers to functions.  Each function takes two
;;; numbers and returns a map giving the new locations of the two
;;; microchips.

(defun destination-map (dest-type dest-index microchip)
  (fset:with-default (fset:map (dest-type (fset:with-default (fset:map (dest-index (fset:bag microchip)))
                                            (fset:empty-bag))))
    (fset:empty-map (fset:empty-bag))))

(defun destination-union (destination-1 destination-2)
  (fset:map-union destination-1 destination-2
                  (lambda (a b)
                    (and a b (fset:map-union a b #'fset:union)))))

(defun destination-lookup (destinations dest-type dest-index)
  (fset:lookup (fset:lookup destinations dest-type) dest-index))

(defun list-all-microchips (locations)
  (fset:reduce (lambda (result dest-type map)
                 (declare (ignore dest-type))
                 (fset:reduce (lambda (r b c) (declare (ignore b)) (fset:union r c))
                              map
                              :initial-value result))
               locations
               :initial-value (fset:empty-bag)))

(parseq:defrule init ()
    (and "value " aoc:integer-string " goes to bot " aoc:integer-string)
  (:choose 1 3)
  (:function (lambda (microchip bot)
               (destination-map 'bot bot microchip))))

(parseq:defrule bot-or-output ()
    (or "bot" "output")
  (:function #'string-upcase)
  (:function #'intern))

(parseq:defrule bot ()
    (and "bot " aoc:integer-string
         " gives low to " bot-or-output " " aoc:integer-string
         " and high to " bot-or-output " " aoc:integer-string)
  (:choose 1 3 5 7 9)
  (:function (lambda (bot-no low-type low-index high-type high-index)
               (fset:map (bot-no (lambda (a b)
                                   (let ((low (min a b))
                                         (high (max a b)))
                                     (destination-union
                                      (destination-map low-type low-index low)
                                      (destination-map high-type high-index high)))))))))

(defun parse-instructions (rule instructions)
  "Returns two values: a lits of parsed values and a list of unparsed
  instructions."
  (if (endp instructions)
      (values nil nil)
      (multiple-value-bind (parsed-list unparsed-list) (parse-instructions rule (cdr instructions))
        (multiple-value-bind (value parsedp) (parseq:parseq rule (car instructions))
          (if parsedp
              (values (cons value parsed-list) unparsed-list)
              (values parsed-list (cons (car instructions) unparsed-list)))))))

(defun parse-init-instructions (instructions)
  "Returns two values: the union of the initial locations and a list of
  unparsed instructions."
  (multiple-value-bind (parsed-list unparsed-list) (parse-instructions 'init instructions)
    (values (reduce #'destination-union parsed-list)
            unparsed-list)))

(defun parse-bot-instructions (instructions)
  "Returns two values: the union of the bot instructions and a list of
  unparsed instructions."
  (multiple-value-bind (parsed-list unparsed-list) (parse-instructions 'bot instructions)
    ;; The ASSERT is because we should never have two instructions for the same bot
    (values (reduce (lambda (a b)
                      (fset:map-union a b (lambda (a b) (and a b (assert nil)))))
                    parsed-list)
            unparsed-list)))

(defun parse-all-instructions (instructions)
  "Returns two values: a map of locations and a map of bot actions."
  (multiple-value-bind (locations bot-instructions) (parse-init-instructions instructions)
    (multiple-value-bind (bots unparsed-instructions) (parse-bot-instructions bot-instructions)
      (assert (endp unparsed-instructions) () "The following instructions were unparsed~{ \"~A\"~}" unparsed-instructions)
      (let ((microchips (list-all-microchips locations)))
        (assert (= (fset:size microchips)
                   (fset:size (fset:convert 'fset:set microchips)))
                ()
                "There are duplicate microchip numbers: ~A" microchips))
      (values locations bots))))


;;;; Part 1

(defun find-actionable-bots (locations)
  (fset:filter (lambda (bot microchips) (declare (ignore bot)) (< 1 (fset:size microchips)))
               (fset:lookup locations 'bot)))

(defun remove-bots (locations bots)
  (if (fset:empty? bots)
      locations
      (let ((bot (fset:arb bots)))
        (remove-bots (fset:with locations
                                'bot (fset:less (fset:lookup locations 'bot) bot))
                     (fset:less bots bot)))))

(defun bots-act (locations bot-actions)
  (let ((bots (find-actionable-bots locations)))
    (fset:reduce (lambda (new-locations bot-no microchips)
                   (destination-union
                    new-locations
                    (apply (fset:lookup bot-actions bot-no) (fset:convert 'list microchips))))
                 bots
                 :initial-value (remove-bots locations bots))))

(defun find-bot-with-microchips (locations microchips)
  (let ((results (fset:filter (lambda (bot-no bot-microchips)
                                (declare (ignore bot-no))
                                (fset:equal? microchips bot-microchips))
                              (fset:lookup locations 'bot))))
    (ecase (fset:size results)
      (0 nil)
      (1 (fset:arb results)))))

(defun get-answer-1 (&optional (instructions *input*) (microchips (fset:bag 17 61)))
  (multiple-value-bind (initial-locations bot-actions) (parse-all-instructions instructions)
    (iter (for locations first initial-locations
               then (bots-act locations bot-actions))
          (thereis (find-bot-with-microchips locations microchips)))))


;;;; Part 2

(defun run-to-completion (instructions)
  (multiple-value-bind (initial-locations bot-actions) (parse-all-instructions instructions)
    (iter (for locations first initial-locations
               then (bots-act locations bot-actions))
          (until (zerop (fset:size (fset:lookup locations 'bot))))
          (finally (return (fset:lookup locations 'output))))))

(defun get-answer-2 ()
  (let ((output (run-to-completion *input*)))
    (iter (for bin-index in '(0 1 2))
          (for bin = (fset:lookup output bin-index))
          (assert (= 1 (fset:size bin)) ()
                  "Output bin ~A has the wrong number of entries: ~A" bin-index bin)
          (multiplying (fset:arb bin)))))
