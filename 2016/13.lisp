(in-package :aoc-2016-13)

(aoc:define-day 92 124)


;;;; Input

(defparameter *favorite-number* (aoc:input :parse-line 'aoc:integer-string))


;;;; Part 1

(defparameter +cardinal-directions+
  (list (point:make-point  0 -1)
        (point:make-point  0  1)
        (point:make-point -1  0)
        (point:make-point  1  0)))

(defun wall-p (favorite-number x y)
  (oddp (logcount (+ (* x x) (* 3 x) (* 2 x y) y (* y y)
                     favorite-number))))

(defun adjacent-positions (pos)
  (remove-if-not (alexandria:curry #'point:< (point:make-point -1 -1))
                 (mapcar (alexandria:curry #'point:+ pos)
                         +cardinal-directions+)))

(defun filter-open (favorite-number positions)
  (remove-if (lambda (pos) (wall-p favorite-number (point:elt pos 0) (point:elt pos 1)))
             positions))

(defun adjacent-open (favorite-number pos)
  (filter-open favorite-number (adjacent-positions pos)))

(defun find-path (favorite-number start end)
  (aoc:shortest-path start
                     (alexandria:compose (alexandria:curry #'mapcar (alexandria:curry #'list 1))
                                         (alexandria:curry #'adjacent-open favorite-number))
                     :end end
                     :heuristic (alexandria:curry #'point:manhattan-distance end)))

(defun get-answer-1 (&key (favorite-number *favorite-number*) (start (point:make-point 1 1)) (end (point:make-point 31 39)))
  (nth-value 1 (find-path favorite-number start end)))


;;;; Part 2

(defun find-nearby-locations (favorite-number new-locations old-locations steps-remaining)
  (let ((combined-locations (fset:union new-locations old-locations)))
    (if (zerop steps-remaining)
        combined-locations
        (let ((discovered-locations (fset:reduce (lambda (result pos)
                                                   (fset:union (fset:convert 'fset:set (adjacent-open favorite-number pos))
                                                               result))
                                                 new-locations
                                                 :initial-value (fset:empty-set))))
          (find-nearby-locations favorite-number
                                 (fset:set-difference discovered-locations combined-locations)
                                 combined-locations
                                 (1- steps-remaining))))))

(defun get-answer-2 (&key (favorite-number *favorite-number*))
  (fset:size (find-nearby-locations favorite-number (fset:set (point:make-point 1 1)) (fset:empty-set) 50)))
