(in-package :assembunny)
(5am:def-suite :assembunny)
(5am:in-suite :assembunny)

;;;; Instructions

(parseq:defrule operation ()
    (and instruction (+ (and " " parameter)))
  (:lambda (instruction params)
    (cons instruction (mapcar #'second params))))

(parseq:defrule instruction ()
    (rep 3 char)
  (:string)
  (:function #'string-upcase)
  (:lambda (register) (intern register :assembunny)))

(parseq:defrule parameter ()
    (or param-integer param-register))

(parseq:defrule param-integer ()
  aoc:integer-string)

(parseq:defrule param-register ()
  alpha
  (:string)
  (:function #'string-upcase)
  (:lambda (register) (intern register :keyword)))

(defstruct vm
  instructions
  (ip 0 :type (integer 0))
  (registers (fset:empty-map 0)))

(defun update-vm (vm &key instructions ip registers)
  (make-vm :instructions (or instructions (vm-instructions vm))
           :ip (or ip (vm-ip vm))
           :registers (or registers (vm-registers vm))))

(defun param-value (vm param)
  (if (symbolp param)
      (fset:lookup (vm-registers vm) param)
      param))

;;; Each function takes a vm plus its parameters.  It returns a new vm
;;; state.  The VM's instruction pointer will be set to the *next*
;;; instruction; the function can change it in the returned VM state in
;;; order to change the control flow.

(defun cpy (vm x y)
  (if (symbolp y)
      (update-vm vm :registers (fset:with (vm-registers vm)
                                          y
                                          (param-value vm x)))
      vm))

(defun inc (vm x)
  (if (symbolp x)
      (update-vm vm :registers (fset:with (vm-registers vm)
                                          x
                                          (1+ (param-value vm x))))
      vm))

(defun dec (vm x)
  (if (symbolp x)
      (update-vm vm :registers (fset:with (vm-registers vm)
                                          x
                                          (1- (param-value vm x))))
      vm))

(defun jnz (vm x y)
  (if (zerop (param-value vm x))
      vm
      (update-vm vm :ip (+ (1- (vm-ip vm))
                           (param-value vm y)))))

(defun toggle-instruction (operation)
  (ecase (length operation)
    (2 (if (eq 'inc (car operation))
           (cons 'dec (cdr operation))
           (cons 'inc (cdr operation))))
    (3 (if (eq 'jnz (car operation))
           (cons 'cpy (cdr operation))
           (cons 'jnz (cdr operation))))))

(defun tgl (vm x)
  (with-slots (instructions ip) vm
    (let ((toggled-index (+ ip (param-value vm x) -1)))
      (if (<= (fset:size instructions) toggled-index)
          vm
          (update-vm vm :instructions (fset:with instructions
                                                 toggled-index
                                                 (toggle-instruction (fset:lookup instructions
                                                                                  toggled-index))))))))

(defun out (vm x)
  (format t "~A~%" (param-value vm x))
  vm)


;;;; Compilation

(defun compile (strings)
  "Takes a list of strings.  Each string should be a separate instruction.
  Returns a FSet sequence of lists."
  (fset:convert 'fset:seq (mapcar (alexandria:curry #'parseq:parseq 'operation) strings)))


;;;; Execution

(defun run-internal (vm)
  (with-slots (instructions ip registers) vm
    (if (<= (fset:size instructions) ip)
        registers
        (let ((operation (fset:lookup instructions ip)))
          (run-internal (apply (car operation)
                               (cons (update-vm vm :ip (1+ ip))
                                     (cdr operation))))))))

(defun run (program &key initial-registers)
  (let ((registers (fset:with-default (or initial-registers
                                          (fset:empty-map))
                                      0)))
    (run-internal (make-vm :instructions program
                           :registers registers))))
