(in-package :aoc-2016-06)

(aoc:define-day "umcvzsmw" "rwqoacfz")


;;;; Input

(defparameter *example*
  '("eedadn"
    "drvtee"
    "eandsr"
    "raavrd"
    "atevrs"
    "tsrnev"
    "sdttsa"
    "rasrtv"
    "nssdts"
    "ntnada"
    "svetve"
    "tesnvt"
    "vntsnd"
    "vrdear"
    "dvrsen"
    "enarar"))
(defparameter *message* (aoc:input))


;;;; Part 1

(defun base-collection (length)
  (make-list length :initial-element (fset:empty-map 0)))

(defun add-letter (map letter)
  (fset:with map letter (1+ (fset:lookup map letter))))

(defun add-letters (collection letters)
  (map 'list #'add-letter
       (or collection (base-collection (length letters)))
       letters))

(defun count-letters (string-list)
  (reduce #'add-letters string-list :initial-value nil))

(defun select-letter (selector old-cons letter count)
  (if (null old-cons)
      (list letter count)
      (destructuring-bind (old-letter old-count) old-cons
        (declare (ignore old-letter))
        (if (funcall selector count old-count)
            (list letter count)
            old-cons))))

(defun find-selected-letter (selector map)
  (first (fset:reduce (alexandria:curry #'select-letter selector)
                      map
                      :initial-value nil)))

(defun find-selected-letters (selector maps)
  (mapcar (alexandria:curry #'find-selected-letter selector)
          maps))

(defun get-answer-1 (&optional (message *message*) (selector #'>))
  (format nil "~{~A~}" (find-selected-letters selector (count-letters message))))

(aoc:given 1
  (string= "easter" (get-answer-1 *example*)))


;;;; Part 2

(defun get-answer-2 (&optional (message *message*))
  (get-answer-1 message #'<))

(aoc:given 2
  (string= "advent" (get-answer-2 *example*)))
