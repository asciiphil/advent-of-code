#!/usr/bin/env python3
"""Uses Shapely to solve the problem.  Unfortunately, the intersection
algorithm is O(n²), so it gets really slow with larger inputs.

Also, it gives wrong answers.  :("""


import math
import re
import sys

from shapely.geometry import LineString, GeometryCollection
from tqdm import tqdm


def parse_data(filename):
    lines = []
    with open(filename) as input_file:
        for line in input_file:
            coords = [int(s) for s in re.findall(r'[-+]?\d+', line)]
            lines.append(LineString([coords[0:2], coords[2:4]]))
    return lines

def intersect(lines):
    intersections = GeometryCollection()
    untouched = lines[0]
    for line in tqdm(lines[1:]):
        intersections = intersections.union(line.intersection(untouched))
        untouched = untouched.union(line)
    return intersections

def count_points(intersections):
    count = 0
    for geom in intersections.geoms:
        if sum([b - int(b) for b in geom.bounds]) > 0.4:
            continue
        (minx, miny, maxx, maxy) = geom.bounds
        count += max(int(maxx) - int(minx) + 1,
                     int(maxy) - int(miny) + 1)
    return count

        
if __name__ == '__main__':
    assert len(sys.argv) == 2, 'Need an input file'
    lines = parse_data(sys.argv[1])
    intersections = intersect(lines)
    print(count_points(intersections))
