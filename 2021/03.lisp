(in-package :aoc-2021-03)

(aoc:define-day 3549854 3765399)


;;;; Parsing

;; This is about 100x faster than using parseq.
(defun parse-bit-string (string)
  (let ((result (make-array (length string) :element-type 'bit :initial-element 0)))
    (iter (for c in-string string with-index i)
      (when (char= c #\1)
        (setf (sbit result i) 1)))
    result))


;;;; Input

(defparameter *numbers* (mapcar #'parse-bit-string (aoc:input)))


;;;; Part 1

(defun count-one-bits (numbers)
  (reduce (lambda (a b) (gmap:gmap :seq #'+ (:seq a) (:vector b)))
          numbers
          :initial-value (fset:insert (fset:empty-seq 0) (length (first numbers)) 0)))

(defun most-common-bits (numbers)
  (let ((one-bits (count-one-bits numbers))
        (number-count (length numbers)))
    (fset:image (lambda (n) (if (< n (/ number-count 2))
                                0
                                1))
                one-bits)))

(defun invert-bits (number)
  (fset:image (lambda (b) (mod (1+ b) 2)) number))

(defun bits-to-int (number)
  (fset:reduce (lambda (r b) (+ (* 2 r) b)) number))

(defun get-answer-1 (&optional (numbers *numbers*))
  (let* ((gamma-rate (most-common-bits numbers))
         (epsilon-rate (invert-bits gamma-rate)))
    (* (bits-to-int gamma-rate)
       (bits-to-int epsilon-rate))))


;;;; Part 2

(defun most-common-bit (numbers bit)
  (let ((number-of-ones (fset:reduce (lambda (s n) (+ s (sbit n bit))) numbers
                                     :initial-value 0)))
    (if (< number-of-ones (/ (fset:size numbers) 2))
        0
        1)))

(defun filter-bit (numbers bit most-common-p)
  (let* ((ref-bit (mod (+ (most-common-bit numbers bit)
                          (if most-common-p 0 1))
                       2)))
    (fset:filter (lambda (n) (= ref-bit (fset:lookup n bit)))
                 numbers)))

(defun filter-bits (numbers most-common-p)
  (labels ((filter-bits-r (bit remaining-numbers)
             (if (endp (cdr remaining-numbers))
                 (car remaining-numbers)
                 (filter-bits-r (1+ bit)
                                (filter-bit remaining-numbers bit most-common-p)))))
    (filter-bits-r 0 numbers)))

(defun get-answer-2 (&optional (numbers *numbers*))
  (let ((oxygen-generator-rating (filter-bits numbers t))
        (co2-scrubber-rating (filter-bits numbers nil)))
    (* (bits-to-int oxygen-generator-rating)
       (bits-to-int co2-scrubber-rating))))


;;;; Visualization

(defparameter *video-width* 1920)
(defparameter *video-height* 1080)
(defparameter *video-fps* 30)

(defparameter *vstretch* 12)

(defparameter *scan-seconds* 30)
(defparameter *slide-seconds* 1)

(defun make-video (filename numbers)
  (let ((state (mapcar (lambda (n) (cons 0 n)) numbers)))
    (viz:with-video (video (viz:create-video filename *video-width* *video-height* :fps *video-fps*))
      (let ((base-image (viz:create-video-surface video)))
        (draw-numbers base-image state)
        (dotimes (i (* *video-fps* 10))
          (viz:write-video-surface video base-image))))))

(defun draw-numbers (surface state)
  (viz:clear-surface surface :dark-background)
  (let* ((canvas-width (cairo:image-surface-get-width surface))
         (canvas-height (cairo:image-surface-get-height surface))
         (scale (/ canvas-width (length state))))
    (cairo:with-context-from-surface (surface)
      (cairo:translate 0 (/ canvas-height 2))
      (cairo:scale scale scale)
      (iter (for i from 0)
            (for (offset . number) in state)
            (cairo:save)
            (cairo:translate i (* offset (/ canvas-height 4 scale)))
            (draw-number number)
            (cairo:restore)))))

(defun draw-number (number)
  (cairo:save)
  (viz:set-color :dark-primary)
  (cairo:translate 0 (- (* *vstretch* (/ (length number) 2))))
  (iter (for b in-vector number with-index i)
        (when (plusp b)
          (cairo:rectangle 0 (* i *vstretch*) 1 *vstretch*)
          (cairo:fill-path)))
  (cairo:restore))
