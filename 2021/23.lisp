(in-package :aoc-2021-23)

;;; Part 2 answer is 47232, but it takes ~45 seconds.
(aoc:define-day 19160 nil)


;;;; Data Structures

;;; Amphipods are just symbols

;;; The current state will be a map from symbols ('a, 'b, 'c, and 'd) to a
;;; set of points (two points each).


;;;; Input

;;; Handcoded, because it's easier than parsing.
(defparameter *example*
  (fset:map ('a (fset:set (point:make-point 3 3)
                          (point:make-point 9 3)))
            ('b (fset:set (point:make-point 3 2)
                          (point:make-point 7 2)))
            ('c (fset:set (point:make-point 5 2)
                          (point:make-point 7 3)))
            ('d (fset:set (point:make-point 5 3)
                          (point:make-point 9 2)))))
(defparameter *diagram*
  (fset:map ('a (fset:set (point:make-point 7 2)
                          (point:make-point 9 3)))
            ('b (fset:set (point:make-point 7 3)
                          (point:make-point 9 2)))
            ('c (fset:set (point:make-point 5 2)
                          (point:make-point 5 3)))
            ('d (fset:set (point:make-point 3 2)
                          (point:make-point 3 3)))))
(defparameter *example2*
  (fset:map ('a (fset:set (point:make-point 3 5)
                          (point:make-point 9 5)
                          (point:make-point 7 4)
                          (point:make-point 9 3)))
            ('b (fset:set (point:make-point 3 2)
                          (point:make-point 7 2)
                          (point:make-point 5 4)
                          (point:make-point 7 3)))
            ('c (fset:set (point:make-point 5 2)
                          (point:make-point 7 5)
                          (point:make-point 5 3)
                          (point:make-point 9 4)))
            ('d (fset:set (point:make-point 5 5)
                          (point:make-point 9 2)
                          (point:make-point 3 3)
                          (point:make-point 3 4)))))
(defparameter *diagram2*
  (fset:map ('a (fset:set (point:make-point 7 2)
                          (point:make-point 9 5)
                          (point:make-point 7 4)
                          (point:make-point 9 3)))
            ('b (fset:set (point:make-point 7 5)
                          (point:make-point 9 2)
                          (point:make-point 5 4)
                          (point:make-point 7 3)))
            ('c (fset:set (point:make-point 5 2)
                          (point:make-point 5 5)
                          (point:make-point 5 3)
                          (point:make-point 9 4)))
            ('d (fset:set (point:make-point 3 2)
                          (point:make-point 3 5)
                          (point:make-point 3 3)
                          (point:make-point 3 4)))))


;;;; Part 1

;;;               1
;;;   0           2
;;; 0 #############
;;;   #...........#
;;;   ###.#.#.#.###
;;;     #.#.#.#.#
;;; 4   #########

(defparameter *adjacency*
  (let ((result (fset:empty-map)))
    (setf (fset:lookup result (point:make-point 1 1))
          (fset:set (point:make-point 2 1))
          (fset:lookup result (point:make-point 11 1))
          (fset:set (point:make-point 10 1)))
    (dotimes (i 9)
      (setf (fset:lookup result (point:make-point (+ 2 i) 1))
            (fset:set (point:make-point (+ 1 i) 1)
                      (point:make-point (+ 3 i) 1))))
    (dotimes (i 4)
      (let ((x (+ 3 (* 2 i))))
        (fset:adjoinf (fset:lookup result (point:make-point x 1))
                      (point:make-point x 2))
        (setf (fset:lookup result (point:make-point x 2))
              (fset:set (point:make-point x 1)
                        (point:make-point x 3))
              (fset:lookup result (point:make-point x 3))
              (fset:set (point:make-point x 2)))))
    result)
  "Map from points to sets of points.")
(defparameter +hallway+
  (fset:image (lambda (x) (point:make-point x 1))
              (fset:set 1 2 4 6 8 10 11))
  "Set of points.")
(defparameter *rooms*
  (fset:map ('a (fset:seq (point:make-point 3 2)
                          (point:make-point 3 3)))
            ('b (fset:seq (point:make-point 5 2)
                          (point:make-point 5 3)))
            ('c (fset:seq (point:make-point 7 2)
                          (point:make-point 7 3)))
            ('d (fset:seq (point:make-point 9 2)
                          (point:make-point 9 3))))
  "Map from symbols to seqs of points.")

(defun invert-state (state)
  (let ((result (fset:empty-map)))
    (fset:do-map (amphipod points state)
      (fset:do-set (point points)
        (setf (fset:lookup result point) amphipod)))
    result))

(defun print-state (state)
  (let ((pos-to-amphipod (invert-state state)))
    (flet ((print-point (point)
             (cond
               ((fset:lookup pos-to-amphipod point)
                (princ (fset:lookup pos-to-amphipod point)))
               ((fset:lookup *adjacency* point)
                (princ #\.))
               (t
                (princ #\#)))))
      (dotimes (y 3)
        (dotimes (x 13)
          (print-point (point:make-point x y)))
        (princ #\Newline))
      (dotimes (y (fset:size (nth-value 1 (fset:arb *rooms*))))
        (princ "  ")
        (dotimes (x 9)
          (print-point (point:make-point (+ 2 x) (+ 3 y))))
        (princ #\Newline))))
  (values))

(defun step-cost (amphipod)
  (ecase amphipod
    (a 1)
    (b 10)
    (c 100)
    (d 1000)))

(defun amphipod-target-position (state amphipod)
  (let ((pos-to-amphipod (invert-state state))
        (target-room (fset:lookup *rooms* amphipod)))
    (fset:find-if (lambda (pos)
                    (not (eq amphipod (fset:lookup pos-to-amphipod pos))))
                  target-room
                  :from-end t)))

;;; The amphipod has a target position, which is the furthest point in its
;;; destination room not occupied by another amphipod of the same type.
;;;
;;; If an amphipod starts in a room, it can either move to a hallway
;;; location or to its target position.  If the amphipod starts in the
;;; hallway, it can only move to its target location.
(defun list-amphipod-available-movements (state point)
  "Returns a set of possible moves for the amphipod at POINT.  Each move
  is a two-element list consisting, in order, of the number of steps taken
  and the ending point."
  (let* ((occupied (fset:reduce #'fset:union (fset:range state)))
         (amphipod (fset:lookup (invert-state state) point))
         (hallway-allowed (not (fset:contains? +hallway+ point)))
         (target (amphipod-target-position state amphipod)))
    (labels ((list-r (position visited result)
               (fset:reduce (lambda (new-result new-position)
                              (cond
                                ((fset:contains? occupied new-position)
                                 ;; Occupied; no go.
                                 new-result)
                                ((point:= target new-position)
                                 ;; Target position.  Take it; don't try
                                 ;; to move from here.
                                 (cons (list (fset:size visited)
                                             new-position)
                                       new-result))
                                ((and hallway-allowed
                                      (fset:contains? +hallway+ new-position))
                                 ;; We're in the hallway and allowed to
                                 ;; stop here.  Try this option, but also
                                 ;; keep looking for other options.
                                 (list-r new-position
                                         (fset:with visited new-position)
                                         (cons (list (fset:size visited)
                                                     new-position)
                                               new-result)))
                                (t
                                 ;; Default: we can be here, but we can't
                                 ;; stop here.  Keep moving.
                                 (list-r new-position
                                         (fset:with visited new-position)
                                         new-result))))
                            (fset:set-difference (fset:lookup *adjacency* position)
                                                 visited)
                            :initial-value result)))
      ;; If TARGET is null, the room is full and this amphipod doesn't
      ;; need to move.  If the amphipod's position is in the same column
      ;; as the target but at a higher y value, then it's already been
      ;; placed and also doesn't need to move.
      (if (or (null target)
              (and (= (point:x target) (point:x point))
                   (< (point:y target) (point:y point))))
          nil
          (list-r point (fset:set point) nil)))))

(defun state-remove-amphipod (state amphipod position)
  (fset:with state amphipod
             (fset:less (fset:lookup state amphipod)
                        position)))

(defun state-add-amphipod (state amphipod position)
  (fset:with state amphipod
             (fset:with (fset:lookup state amphipod)
                        position)))

(defun add-moves-for-amphipod (state amphipod position other-moves)
  "Adds all moves for this amphipod to OTHER-MOVES.  A move is as accepted
  by AOC:SHORTEST-PATH; a list of the move cost and the new state."
  (let ((base-state (state-remove-amphipod state amphipod position)))
    (fset:reduce (lambda (result move)
                   (destructuring-bind (steps end-position) move
                     (cons (list (* steps (step-cost amphipod))
                                 (state-add-amphipod base-state amphipod end-position))
                           result)))
                 (list-amphipod-available-movements state position)
                 :initial-value other-moves)))

(defun next-moves (state)
  (fset:reduce (lambda (moves amphipod positions)
                 (fset:reduce (lambda (ap-moves position)
                                (add-moves-for-amphipod state amphipod position ap-moves))
                              positions
                              :initial-value moves))
               state
               :initial-value nil))

;;; This gets called a *lot*, so we want to make the memoization as
;;; efficient as possible.
(let ((memos (make-array '(4 7 14) :initial-element nil)))
  (defun steps-to-room (amphipod position)
    (or (aref memos (ecase amphipod
                      (a 0)
                      (b 1)
                      (c 2)
                      (d 3))
              (point:y position)
              (point:x position))
        (setf (aref memos (ecase amphipod
                            (a 0)
                            (b 1)
                            (c 2)
                            (d 3))
                    (point:y position)
                    (point:x position))
              (let ((room (fset:lookup *rooms* amphipod)))
                (if (fset:position position room)
                    0
                    (let ((target (fset:first room)))
                      (+ (abs (- (point:x position) (point:x target)))
                         (abs (- (point:y position) (point:y target)))
                         (if (fset:contains? +hallway+ position)
                             0
                             2)))))))))

;;; I'd prefer to use REDUCE, but this was a little faster in empirical
;;; testing.
(defun minimum-cost-to-end (state)
  (let ((result 0))
    (fset:do-map (amphipod positions state)
      (fset:do-set (position positions)
        (incf result (* (step-cost amphipod)
                        (steps-to-room amphipod position)))))
    result))

(defun get-answer-1 (&optional (diagram *diagram*))
  (nth-value 1 (aoc:shortest-path diagram #'next-moves
                                  :end (fset:image (lambda (ap rs)
                                                     (values ap (fset:convert 'fset:set rs)))
                                                   *rooms*)
                                  :heuristic #'minimum-cost-to-end)))


;;;; Part 2

(defparameter *adjacency2*
  (let ((result (fset:empty-map)))
    (setf (fset:lookup result (point:make-point 1 1))
          (fset:set (point:make-point 2 1))
          (fset:lookup result (point:make-point 11 1))
          (fset:set (point:make-point 10 1)))
    (dotimes (i 9)
      (setf (fset:lookup result (point:make-point (+ 2 i) 1))
            (fset:set (point:make-point (+ 1 i) 1)
                      (point:make-point (+ 3 i) 1))))
    (dotimes (i 4)
      (let ((x (+ 3 (* 2 i))))
        (fset:adjoinf (fset:lookup result (point:make-point x 1))
                      (point:make-point x 2))
        (setf (fset:lookup result (point:make-point x 2))
              (fset:set (point:make-point x 1)
                        (point:make-point x 3))
              (fset:lookup result (point:make-point x 3))
              (fset:set (point:make-point x 2)
                        (point:make-point x 4))
              (fset:lookup result (point:make-point x 4))
              (fset:set (point:make-point x 3)
                        (point:make-point x 5))
              (fset:lookup result (point:make-point x 5))
              (fset:set (point:make-point x 4)))))
    result)
  "Map from points to sets of points.")
(defparameter *rooms2*
  (labels ((make-seq (x)
             (fset:image (lambda (y) (point:make-point x y))
                         (fset:seq 2 3 4 5))))
    (fset:map ('a (make-seq 3))
              ('b (make-seq 5))
              ('c (make-seq 7))
              ('d (make-seq 9))))
  "Map from symbols to sets of points.")

(defun get-answer-2 (&optional (diagram *diagram2*))
  (let ((*adjacency* *adjacency2*)
        (*rooms* *rooms2*))
    (nth-value 1 (aoc:shortest-path diagram #'next-moves
                                    :end (fset:image (lambda (ap rs)
                                                       (values ap (fset:convert 'fset:set rs)))
                                                     *rooms*)
                                    :heuristic #'minimum-cost-to-end))))
