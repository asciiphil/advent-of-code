(in-package :aoc-2021-11)

(aoc:define-day 1546 471)


;;;; Input

(defparameter *example* (aoc-2021-09::parse-dem (aoc:input :file "examples/11.txt")))
(defparameter *grid* (aoc-2021-09::parse-dem (aoc:input)))


;;;; Part 1

(defun visit-neighbors (grid x y fn)
  (dolist (dy '(-1 0 1))
    (dolist (dx '(-1 0 1))
      (unless (= 0 dy dx)
        (let ((xn (+ x dx))
              (yn (+ y dy)))
          (when (and (< -1 yn (array-dimension grid 0))
                     (< -1 xn (array-dimension grid 1)))
            (funcall fn xn yn)))))))

(defun add-cell-energy! (grid x y)
  (unless (< 9 (aref grid y x))
    (when (= 10 (incf (aref grid y x)))
      (visit-neighbors grid x y
                       (alexandria:curry #'add-cell-energy! grid)))))

(defun add-grid-energy! (grid)
  (dotimes (y (array-dimension grid 0))
    (dotimes (x (array-dimension grid 1))
      (add-cell-energy! grid x y))))

(defun cap-grid-energy! (grid)
  "Returns a count of modified cells."
  (let ((result 0))
    (dotimes (y (array-dimension grid 0))
      (dotimes (x (array-dimension grid 1))
        (when (< 9 (aref grid y x))
          (setf (aref grid y x) 0)
          (incf result))))
    result))

(defun step-grid! (grid &optional (count 1) (flash-count 0))
  (if (zerop count)
      (values grid flash-count)
      (progn
        (add-grid-energy! grid)
        (step-grid! grid
                    (1- count)
                    (+ flash-count (cap-grid-energy! grid))))))

(defun get-answer-1 (&optional (grid *grid*) (steps 100))
  (nth-value 1 (step-grid! (alexandria:copy-array grid) steps)))


;;;; Part 2

(defun find-simultaneous-flash (grid &optional (step-count 0))
  (multiple-value-bind (new-grid flashes) (step-grid grid)
    (if (= flashes (array-total-size new-grid))
        (1+ step-count)
        (find-simultaneous-flash new-grid (1+ step-count)))))

(defun get-answer-2 (&optional (grid *grid*))
  (find-simultaneous-flash (alexandria:copy-array grid)))


;;;; Other

(defun step-grid (grid &optional (count 1))
  (step-grid! (alexandria:copy-array grid) count))

(defun find-loop (start-grid)
  (let ((states (fset:empty-map)))
    (labels ((find-loop-r (grid steps)
               (let ((flat-grid (aoc:flatten-array grid)))
                 (if (fset:lookup states flat-grid)
                     (values (fset:lookup states flat-grid)
                             (= (count 0 flat-grid)
                                (array-total-size grid)))
                     (progn
                       (setf (fset:lookup states flat-grid)
                             steps)
                       (find-loop-r (step-grid! (alexandria:copy-array grid))
                                    (1+ steps)))))))
      (find-loop-r (alexandria:copy-array start-grid) 0))))

(defun random-grid ()
  (let ((result (make-array '(10 10))))
    (dotimes (y (array-dimension result 0))
      (dotimes (x (array-dimension result 1))
        (setf (aref result y x) (random 10))))
    result))

(defparameter *sample-chunk-size* 100)

(defun sample-random-grids (result-file count)
  (with-open-file (output result-file
                          :direction :output
                          :if-does-not-exist :create
                          :if-exists :supersede)
    (let ((lp-channel (lparallel:make-channel)))
      (dotimes (_ (truncate count *sample-chunk-size*))
        (dotimes (_ *sample-chunk-size*)
          (lparallel:submit-task
           lp-channel
           (lambda () (multiple-value-list (find-loop (random-grid))))))
        (dotimes (_ *sample-chunk-size*)
          (destructuring-bind (steps synchronized)
              (lparallel:receive-result lp-channel)
            (format output "~A,~A~%" synchronized steps)))))))
