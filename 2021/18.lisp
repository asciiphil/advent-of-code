(in-package :aoc-2021-18)

(aoc:define-day 3524 4656)


;;;; Data Structures

;;; A snailfish number will be represented as an FSet seq, where each
;;; element is a cons pair.  The car will be the numeric value of the
;;; element and the cdr will be its nesting depth.
;;;
;;; Example:
;;;   [[1,2],3] -> #[ (1 . 1) (2 . 1) (3 . 0) ]

(defun sn-value (sn-seq pos)
  (car (fset:lookup sn-seq pos)))

(defun sn-with-value (sn-seq pos new-value)
  (let ((old-elt (fset:lookup sn-seq pos)))
    (fset:with sn-seq pos
               (cons new-value (cdr old-elt)))))

(defun sn-depth (sn-seq pos)
  (cdr (fset:lookup sn-seq pos)))

(defun sn-with-depth (sn-seq pos new-depth)
  (let ((old-elt (fset:lookup sn-seq pos)))
    (fset:with sn-seq pos
               (cons (car old-elt) new-depth))))

(defun sn-change-depth (sn-seq delta)
  (fset:image (lambda (elt)
                (cons (car elt) (+ delta (cdr elt))))
              sn-seq))

(defun sn-to-tree (sn-seq &optional (cur-depth 0))
  (if (< (sn-depth sn-seq 0) cur-depth)
      (values (sn-value sn-seq 0)
              (fset:less-first sn-seq))
      (multiple-value-bind (left-subtree left-remaining)
          (sn-to-tree sn-seq (1+ cur-depth))
        (multiple-value-bind (right-subtree right-remaining)
            (sn-to-tree left-remaining (1+ cur-depth))
          (values (cons left-subtree right-subtree)
                  right-remaining)))))


;;;; Parsing

(defun snailify (elt)
  (if (numberp elt)
      (fset:seq (cons elt 0))
      (sn-change-depth elt 1)))

(parseq:defrule snailfish ()
    (and "[" (or aoc:integer-string snailfish) "," (or aoc:integer-string snailfish) "]")
  (:choose 1 3)
  (:lambda (left right)
    (fset:concat (snailify left) (snailify right))))


;;;; Input

(defparameter *numbers* (aoc:input :parse-line 'snailfish))
(defparameter *example-explode*
  (parseq:parseq 'snailfish "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"))
(defparameter *example* (aoc:input :parse-line 'snailfish :file "examples/18.txt"))

;;;; Part 1

(defun explode (sn-seq)
  (flet ((add-pos (seq pos n)
           (if (< -1 pos (fset:size sn-seq))
               (sn-with-value seq pos (+ n (sn-value seq pos)))
               seq)))
    (let ((idx (fset:position-if (lambda (elt) (<= 4 (cdr elt)))
                                 sn-seq)))
      (if idx
          (values (fset:less (fset:with (add-pos (add-pos sn-seq (1- idx) (sn-value sn-seq idx))
                                                 (+ 2 idx) (sn-value sn-seq (1+ idx)))
                                        idx (cons 0 (1- (sn-depth sn-seq idx))))
                             (1+ idx))
                  t)
          (values sn-seq nil)))))

(defun split (sn-seq)
  (let ((idx (fset:position-if (lambda (elt) (< 9 (car elt)))
                               sn-seq)))
    (if idx
        (let ((value (sn-value sn-seq idx))
              (depth (1+ (sn-depth sn-seq idx))))
          (values (fset:insert (fset:with sn-seq idx
                                          (cons (ceiling value 2) depth))
                               idx (cons (floor value 2) depth))
                  t))
        (values sn-seq nil))))

(defun sn-reduce (sn-seq)
  (multiple-value-bind (exploded-seq exploded) (explode sn-seq)
    (if exploded
        (sn-reduce exploded-seq)
        (multiple-value-bind (split-seq split) (split sn-seq)
          (if split
              (sn-reduce split-seq)
              sn-seq)))))

(defun sn-add (sn1 sn2)
  (sn-reduce (sn-change-depth (fset:concat sn1 sn2) 1)))

(defun magnitude (sn-seq)
  (labels ((magnitude-r (pair)
             (if (numberp pair)
                 pair
                 (+ (* 3 (magnitude-r (car pair)))
                    (* 2 (magnitude-r (cdr pair)))))))
    (magnitude-r (sn-to-tree sn-seq))))

(defun get-answer-1 (&optional (numbers *numbers*))
  (magnitude (fset:reduce #'sn-add numbers)))


;;;; Part 2

(defun get-answer-2 (&optional (numbers *numbers*))
  (let ((max-magnitude 0))
    (flet ((try-pair (pair)
             (setf max-magnitude
                   (max max-magnitude
                        (magnitude (sn-add (first pair) (second pair)))
                        (magnitude (sn-add (second pair) (first pair)))))))
      (aoc:visit-subsets #'try-pair numbers 2))
    max-magnitude))
