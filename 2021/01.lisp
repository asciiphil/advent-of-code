(in-package :aoc-2021-01)

(aoc:define-day 1342 1378)

;;; Okay, so I actually did this on the fly from the REPL.  My commands
;;; were as follows.
;;;
;;; For part 1:
;;;
;;;   (defparameter *input* (aoc:input :parse-line 'aoc:integer-string))
;;;   (fset:size (fset:filter #'plusp (mapcar #'- (cdr *input*) *input*)))
;;;
;;; For part 2:
;;;
;;;   (setf w (mapcar #'+ (cddr *input*) (cdr *input*) *input*))
;;;   (fset:size (fset:filter #'plusp (mapcar #'- (cdr w) w)))
;;;
;;; The code below is an after-the fact implementation made to look nicer.


;;;; Input

(defparameter *depths* (aoc:input :parse-line 'aoc:integer-string))


;;;; Part 1

;; The function body for get-answer-1 *could* be this native-library
;; implementation:
;;   (count t (mapcar #'< depths (cdr depths)))
;; But that conses an intermediate list.
;;
;; It could also be done with loop:
;;   (loop for a in depths
;;         for b in (cdr depths)
;;         counting (< a b))
;; But I always feel weird about loop.  It doesn't feel quite like Lisp to
;; me.
;;
;; If I really wanted iteration, I'd use iterate, which at least looks
;; more like Lisp:
;;   (iter (for a in depths)
;;         (for b in (cdr depths))
;;         (counting (< a b)))
;; But when I can do things functionally, I prefer that.
;;
;; So I went with GMap, despite its clunky parameter structure.
(defun get-answer-1 (&key (depths *depths*) (window-size 1))
  (gmap:gmap :count-if #'< (:list depths) (:list (nthcdr window-size depths))))


;;;; Part 2

(defun get-answer-2 ()
  (get-answer-1 :window-size 3))
