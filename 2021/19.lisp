(in-package :aoc-2021-19)

;;; Part 1 is 432, but it takes ~50 seconds
;;; Part 2 is 14414, but it takes ~50 seconds
(aoc:define-day nil nil)


;;;; Data Structures

;;; Each scanner will be an FSet seq of points.


;;;; Parsing

(parseq:defrule scanners ()
    (+ scanner)
  (:lambda (&rest scanners)
    (fset:convert 'fset:seq scanners)))

(parseq:defrule scanner ()
    (and "--- scanner " aoc:integer-string " ---" #\Newline
         (+ point)
         (? #\Newline))
  (:choose 4)
  (:lambda (&rest points)
    ;; The sort doesn't *really* matter, but it makes them look nicer in
    ;; the REPL.
    (fset:sort (fset:convert 'fset:seq points)
               #'point:total<)))

(parseq:defrule point ()
    (and (aoc:comma-list aoc:integer-string) (? #\Newline))
  (:choose 0)
  (:function #'point:make-point))


;;;; Input

(defparameter *example* (aoc:input :parse 'scanners :file "examples/19.txt"))
(defparameter *scanners* (aoc:input :parse 'scanners))


;;;; Part 1

(defun rotate-x (point)
  "Rotates 3D point along the X axis."
  (point:make-point (point:x point) (point:z point) (- (point:y point))))

(defun rotate-y (point)
  "Rotates 3D point along the Y axis."
  (point:make-point (point:z point) (point:y point) (- (point:x point))))

(defun rotate-scanner (axis scanner)
  (fset:image (ecase axis
                (i #'identity)
                (x #'rotate-x)
                (y #'rotate-y))
              scanner))

;; Tree derived from:
;;   http://www.euclideanspace.com/maths/discrete/groups/categorise/finite/cube/
;;
;; For each list, the first element is the axis over which to rotate and
;; the remaining elements are lists of derivatives of that rotation.
;; Walking the tree and applying all rotations will go through all
;; possible 90° rotations of a cube.
(defparameter +rotations+
  '(i (x (x (x (y (x)))
            (y (x)
               (y)))
         (y (x (x (x)))
            (y (y (x)))))
      (y (x (x (x)))
         (y (x)
            (y (x))))))

(defun try-rotations (scanner terminate-fn &optional (rotation-tree +rotations+))
  "Tries every rotation of SCANNER around its origin.  Calls TERMINATE-FN
  with each rotation (including the initial, unrotated value).  Returns
  the first rotation for which (funcall TERMINATE-FN rotation) returns true."
  (if (endp rotation-tree)
      nil
      (let ((rotated (rotate-scanner (car rotation-tree) scanner)))
        (if (funcall terminate-fn rotated)
            rotated
            (series:collect-or
             (series:mapping ((tree (series:scan (cdr rotation-tree))))
               (try-rotations rotated terminate-fn tree)))))))

(defun make-offset-fn (fixed-points)
  (lambda (scanner)
    (let ((offsets (bag-filter-multiplicity-<
                    (fset:reduce (lambda (offsets fixed-point)
                                   (fset:reduce (lambda (inner-offsets scanner-point)
                                                  (fset:with inner-offsets
                                                             (point:- fixed-point scanner-point)))
                                                scanner
                                                :initial-value offsets))
                                 fixed-points
                                 :initial-value (fset:empty-bag))
                    11)))
      (and (not (fset:empty? offsets))
           (fset:arb offsets)))))

(defun bag-filter-multiplicity-< (bag multiplicity)
  (fset:filter (lambda (e) (< multiplicity (fset:multiplicity bag e))) bag))

(defun align-scanners (scanners)
  "Returns a new set of scanners that are all aligned with each other."
  (find-scanner-alignment scanners
                          (fset:set 0)
                          (fset:convert 'fset:set (fset:lookup scanners 0))
                          (fset:set (point:make-point 0 0 0))))

(defun find-scanner-alignment (scanners fixed-scanners fixed-points offsets)
  (if (= (fset:size scanners) (fset:size fixed-scanners))
      (values fixed-points offsets)
      (multiple-value-bind (new-points aligned-id offset)
          (find-aligned-scanner scanners fixed-scanners fixed-points 0)
        (find-scanner-alignment scanners
                                (fset:with fixed-scanners aligned-id)
                                (fset:union (fset:convert 'fset:set new-points)
                                            fixed-points)
                                (fset:with offsets offset)))))

(defun find-aligned-scanner (scanners fixed-scanners fixed-points trial-id)
  (assert (< trial-id (fset:size scanners)))
  (if (fset:contains? fixed-scanners trial-id)
      (find-aligned-scanner scanners fixed-scanners fixed-points (1+ trial-id))
      (let* ((offset-fn (make-offset-fn fixed-points))
             (rotated-scanner (try-rotations (fset:lookup scanners trial-id)
                                             offset-fn)))
        (if rotated-scanner
            (values (fset:image (alexandria:curry
                                 #'point:+ (funcall offset-fn rotated-scanner))
                                rotated-scanner)
                    trial-id
                    (funcall offset-fn rotated-scanner))
            (find-aligned-scanner scanners fixed-scanners fixed-points (1+ trial-id))))))

(defun get-answer-1 (&optional (scanners *scanners*))
  (fset:size (align-scanners scanners)))


;;;; Part 2

(defun get-answer-2 (&optional (scanners *scanners*))
  (let ((max-distance 0))
    (aoc:visit-subsets (lambda (pair)
                         (destructuring-bind (s1 s2) (fset:convert 'list pair)
                           (let ((distance (point:manhattan-distance s1 s2)))
                             (when (< max-distance distance)
                               (setf max-distance distance)))))
                       (fset:convert 'fset:seq
                                     (nth-value 1 (align-scanners scanners)))
                       2)
    max-distance))
