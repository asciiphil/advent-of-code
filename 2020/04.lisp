(in-package :aoc-2020-04)

(aoc:define-day 235 194)


;;;; Parsing

(parseq:defrule batch-file ()
  (+ passport))

(parseq:defrule passport ()
  ;; The end has to be zero-or-one newlines because the last passport in
  ;; the file doesn't have an extra newline.  Parseq's greedy matching
  ;; means that all the preceding passports will always consume the extra
  ;; newline if it's there.
  (and (+ (and field-line #\Newline))
       (? #\Newline))
  (:choose 0)
  (:lambda (&rest rulesets)
    (reduce #'aoc:distinct-map-union (mapcar #'first rulesets))))

(parseq:defrule field-line ()
  (and field (* (and " "  field)))
  (:lambda (first-field other-fields)
    (reduce #'aoc:distinct-map-union
            (cons first-field (mapcar #'second other-fields))
            :initial-value (fset:empty-map))))

(parseq:defrule field ()
  (or year-field hgt hcl ecl pid cid invalid-field))

(parseq:defrule year-field ()
  (and (or "byr" "iyr" "eyr") ":" year)
  (:choose 0 2)
  (:lambda (key-str value) (fset:map ((intern (string-upcase key-str)) value))))

(parseq:defrule year ()
  (rep 4 digit)
  (:string)
  (:function #'parse-integer))

(parseq:defrule hgt ()
  (and "hgt:" aoc:integer-string (or "cm" "in"))
  (:choose 1 2)
  (:lambda (value units) (fset:map ('hgt (cons value (intern (string-upcase units)))))))

(defun hex-str-to-triplet (hex-str)
  (iter (repeat 3)
        (for base first (parse-integer hex-str :radix 16) then (truncate base 256))
        (collecting (mod base 256) at start)))

(parseq:defrule hcl ()
  (and "hcl:#" (rep 6 (char "0-9a-f")))
  (:choose 1)
  (:string)
  (:lambda (hex-str) (fset:map ('hcl (hex-str-to-triplet hex-str)))))

(parseq:defrule ecl ()
  (and "ecl:" (or "amb" "blu" "brn" "gry" "grn" "hzl" "oth"))
  (:choose 1)
  (:lambda (color) (fset:map ('ecl (intern (string-upcase color))))))

(parseq:defrule pid ()
  ;; Theoretically, we should just have (rep 9 digit) here, but if there
  ;; are more digits in a PID field, that leaves the extra digits for the
  ;; parser's next field, which fails.
  (and "pid:" (+ digit))
  (:choose 1)
  (:string)
  (:test (id-str) (= 9 (length id-str)))
  (:lambda (id-str) (fset:map ('pid (parse-integer id-str)))))

(parseq:defrule cid ()
  (and "cid:" aoc:integer-string)
  (:choose 1)
  (:lambda (cid) (fset:map ('cid cid))))

(parseq:defrule invalid-field ()
  (and (rep 3 alpha) ":" (+ (char "a-zA-Z0-9#")))
  (:choose 0 2)
  (:lambda (key-chars value-chars)
    (fset:map ((intern (string-upcase (format nil "invalid-~{~A~}" key-chars)))
               (coerce value-chars 'string)))))


;;;; Input

(defparameter *example*
  (parseq:parseq
   'batch-file
   "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in
"))
(defparameter *batch-file* (aoc:input :parse 'batch-file))


;;;; Part 1

(defparameter +required-fields+ (fset:set 'byr 'iyr 'eyr 'hgt 'hcl 'ecl 'pid))

(defun all-fields-present-p (passport)
  (let ((missing-fields (fset:filter
                         (lambda (field)
                           (not (or (nth-value 1 (fset:lookup passport field))
                                    (nth-value 1 (fset:lookup passport (intern (format nil "INVALID-~A" field)))))))
                         +required-fields+)))
    (values (fset:empty? missing-fields) missing-fields)))

(defun get-answer-1 (&optional (passports *batch-file*))
  (fset:count-if #'all-fields-present-p passports))

(aoc:given 1
  (= 2 (get-answer-1 *example*)))


;;;; Part 2

(defmacro field-valid ((field passport) &body body)
  (alexandria:with-gensyms (foundp)
    `(multiple-value-bind (,field ,foundp) (fset:lookup ,passport ',field)
       (declare (ignorable ,field))
       (and ,foundp
            (progn ,@body)))))

(defun all-fields-valid-p (passport)
  (and (field-valid (byr passport) (<= 1920 byr 2002))
       (field-valid (iyr passport) (<= 2010 iyr 2020))
       (field-valid (eyr passport) (<= 2020 eyr 2030))
       (field-valid (hcl passport) t)  ; Valid if present; invalid values won't parse
       (field-valid (ecl passport) t)  ; Valid if present; invalid values won't parse
       (field-valid (pid passport) t)  ; Valid if present; invalid values won't parse
       (field-valid (hgt passport)
         (destructuring-bind (value . units) hgt
           (ecase units
             (cm (<= 150 value 193))
             (in (<= 59 value 76)))))))

(defun get-answer-2 (&optional (passports *batch-file*))
  (fset:count-if #'all-fields-valid-p passports))
