(in-package :aoc-2020-20)

(aoc:define-day 8272903687921 2304)


;;; Data Structures

(defparameter +sides+ '(:top :bottom :left :right))

(defun side-to-vector (side)
  (ecase side
    (:top    #.(point:make-point  0 -1))
    (:bottom #.(point:make-point  0  1))
    (:left   #.(point:make-point -1  0))
    (:right  #.(point:make-point  1  0))))

(defun side-to-angle (side)
  (ecase side
    (:right  0)
    (:top    90)
    (:left   180)
    (:bottom 270)))

(defun side-axis (side)
  (ecase side
    (:right  0)
    (:top    1)
    (:left   0)
    (:bottom 1)))

(defun side-complement (side)
  (ecase side
    (:top    :bottom)
    (:bottom :top)
    (:left   :right)
    (:right  :left)))

(defun side-to-border-index (side)
  (ecase side
    (:top    0)
    (:right  1)
    (:bottom 2)
    (:left   3)))

(defstruct (tile (:print-object print-tile))
  (id 0 :type (integer 0))
  (image (make-array '(0 0) :element-type 'bit)
         :type (array bit (* *)))
  (position nil :type (or null point:point))
  (rotation 0 :type real)           ; Only used for visualization purposes
  (y-scale 1 :type real)            ; For animating flips
  (x-scale 1 :type real)            ; For animating flips
  (scale 1 :type real)              ; Only used for visualization purposes
  (side-colors (fset:seq :dark-secondary :dark-secondary :dark-secondary :dark-secondary)
               :type fset:seq)          ; Only for visualization
  (draw-borders (fset:set :top :bottom :left :right)) ; Only for visualization
  (transforms (fset:empty-seq)))

(defun update-tile (tile &key image transforms position rotation scale side-colors draw-borders y-scale x-scale)
  (with-slots (id (old-image image) (old-transforms transforms) (old-position position)
                  (old-rotation rotation) (old-scale scale) (old-side-colors side-colors)
                  (old-draw-borders draw-borders) (old-y-scale y-scale) (old-x-scale x-scale))
      tile
    (make-tile :id id
               :image (or image old-image)
               :transforms (or transforms old-transforms)
               :position (or position old-position)
               :rotation (or rotation old-rotation)
               :y-scale (or y-scale old-y-scale)
               :x-scale (or x-scale old-x-scale)
               :scale (or scale old-scale)
               :side-colors (or side-colors old-side-colors)
               :draw-borders (or draw-borders old-draw-borders))))

(defun print-tile (tile &optional (stream t) (with-image nil))
  (with-slots (id image transforms position) tile
    (format stream "#<TILE ~A ~A ~A" id position transforms)
    (when with-image
      (princ #\Newline stream)
      (aoc:print-array-braille image :key #'plusp :stream stream))
    (format stream ">")))

(defun tile-side-id (tile side)
  (with-slots (image) tile
    (ecase side
      (:top (aref (aops:split image 1) 0))
      (:bottom (aref (aops:split image 1) (1- (array-dimension image 0))))
      (:left (aref (aops:split (aops:permute '(1 0) image) 1) 0))
      (:right (aref (aops:split (aops:permute '(1 0) image) 1) (1- (array-dimension image 1)))))))

(defun tile-side-ids (tile &key with-reverse)
  (reduce (lambda (result side)
            (let* ((side-id (tile-side-id tile side))
                   (new-result (fset:with result side-id)))
              (if with-reverse
                  (fset:with new-result (reverse side-id))
                  new-result)))
          +sides+
          :initial-value (fset:empty-set)))

(defun tile-width (tile)
  "Returns the width of the tile in pixels, according to its scale."
  (with-slots (image scale) tile
    (* (array-dimension image 0) scale)))


;;;; Parsing

(defun tile-rows-to-array (rows)
  (let ((array (make-array (list (length (car rows)) (length rows))
                           :element-type 'bit
                           :initial-element 0)))
    (iter (for row in rows)
          (for j from 0)
          (iter (for char in row)
                (for i from 0)
                (when (char= char #\#)
                  (setf (aref array j i) 1))))
    array))

(parseq:defrule tiles ()
  (+ (and tile (? #\Newline)))
  (:lambda (&rest tiles) (mapcar #'first tiles)))

(parseq:defrule tile ()
  (and tile-number tile-rows)
  (:lambda (number rows)
    (make-tile :id number
               :image (tile-rows-to-array rows))))

(parseq:defrule tile-number ()
  (and "Tile " aoc:integer-string ":" #\Newline)
  (:choose 1))

(parseq:defrule tile-rows ()
  (+ (and tile-row (? #\Newline)))
  (:lambda (&rest rows) (mapcar #'first rows)))

(parseq:defrule tile-row ()
  (+ (char ".#")))


;;;; Input

(defparameter *example*
  (parseq:parseq 'tiles (aoc:read-example)))

(defparameter *tiles* (aoc:input :parse 'tiles))


;;;; Part 1

(defun map-set-with (map key new-value)
  "If MAP contains sets, updates the set at KEY to also contain NEW-VALUE."
  (fset:with map key
             (fset:with (fset:lookup map key)
                        new-value)))

(defun map-tiles-by (key-fn tiles)
  (fset:reduce (lambda (new-index tile)
                 (fset:with new-index (funcall key-fn tile) tile))
               tiles
               :initial-value (fset:empty-map)))

;;; Tile indexing

(defun index-tiles (tiles)
  (fset:reduce #'index-tile
               tiles
               :initial-value (fset:empty-map (fset:set))))

(defun index-tile (index tile)
  (with-slots (id) tile
    (fset:reduce (lambda (new-index side-id)
                   (map-set-with new-index side-id id))
                 (tile-side-ids tile :with-reverse t)
                 :initial-value index)))

;;; Find arrangement

(defun find-connection-edges (tiles)
  (fset:image (alexandria:curry #'fset:convert 'list)
              (fset:filter (lambda (connection)
                             (assert (<= 1 (fset:size connection) 2))
                             (= 2 (fset:size connection)))
                           (fset:range (index-tiles tiles)))))

(defun aggregate-connections (connections)
  (fset:reduce (lambda (result edge)
                 (map-set-with (map-set-with result (first edge) (second edge))
                               (second edge) (first edge)))
               connections
               :initial-value (fset:empty-map (fset:empty-set))))

(defun corner-set (tiles)
  (let ((connections (aggregate-connections (find-connection-edges tiles))))
    (fset:domain (fset:filter (lambda (source targets)
                                (declare (ignore source))
                                (= 2 (fset:size targets)))
                              connections))))

(defun get-answer-1 (&optional (tiles *tiles*))
  (fset:reduce #'* (corner-set tiles)))

(aoc:given 1
  (= 20899048083289 (get-answer-1 *example*)))


;;;; Part 2

;;; Tile Placement

(defun place-tiles (tiles &optional (placements (fset:empty-map)) connections)
  (cond
    ((null connections)
     (assert (fset:empty? placements))
     (place-tiles tiles placements (aggregate-connections (find-connection-edges (fset:range tiles)))))
    ((fset:empty? placements)
     (multiple-value-bind (first-id first-tile) (fset:arb tiles)
       (let ((positioned-tile (update-tile first-tile :position (point:make-point 0 0))))
         (place-tiles tiles (fset:with placements first-id positioned-tile) connections))))
    ((fset:equal? (fset:domain tiles) (fset:domain placements))
     (fset:range placements))
    (t
     (let ((next-tile (fset:arb (placeable-tiles tiles placements connections))))
       (place-tiles tiles (place-tile next-tile placements connections) connections)))))

(defun placeable-tiles (tiles placements connections)
  (fset:image
   (alexandria:curry #'fset:lookup tiles)
   (fset:set-difference
    (fset:reduce (lambda (result tile)
                   (fset:union result (fset:lookup connections (tile-id tile))))
                 (fset:range placements)
                 :initial-value (fset:empty-set))
    (fset:domain placements))))

(defun place-tile (tile placements connections)
  (let* ((adjacent-tile-ids (fset:lookup connections (tile-id tile)))
         (reference-tile-id (fset:arb (fset:intersection adjacent-tile-ids (fset:domain placements))))
         (reference-tile (fset:lookup placements reference-tile-id))
         (placement-direction (find-placement-direction tile reference-tile))
         (new-position (point:+ (tile-position reference-tile) (side-to-vector placement-direction)))
         (transformed-tile (transform-for-placement tile reference-tile)))
    (fset:with placements (tile-id tile) (update-tile transformed-tile :position new-position))))

(defun find-placement-direction (tile reference-tile)
  (let ((target-side-ids (tile-side-ids tile :with-reverse t)))
    (find-if (lambda (side)
               (fset:member? (tile-side-id reference-tile side) target-side-ids))
             +sides+)))

(defun transform-for-placement (tile reference-tile)
  (let* ((reference-side (find-placement-direction tile reference-tile))
         (this-side (find-placement-direction reference-tile tile))
         (needed-rotation (mod (+ (- (side-to-angle reference-side) (side-to-angle this-side))
                                  180)
                               360))
         (rotated-tile (rotate-tile tile needed-rotation)))
    (if (equal (tile-side-id reference-tile reference-side)
               (tile-side-id rotated-tile (side-complement reference-side)))
        rotated-tile
        (flip-tile rotated-tile (side-axis reference-side)))))

(defun rotate-tile (tile degrees)
  (if (zerop (mod degrees 360))
      tile
      (update-tile tile
                   :image (array-rot90 (tile-image tile) (/ degrees 90))
                   :side-colors (aoc:rotate-seq (tile-side-colors tile) (/ degrees 90))
                   :transforms (fset:with-last (tile-transforms tile) (cons :rot degrees)))))

(defun sequence-swap-indices (sequence i0 i1)
  (fset:with (fset:with sequence i0 (fset:lookup sequence i1))
             i1 (fset:lookup sequence i0)))

(defun flip-side-colors (side-colors axis)
  (ecase axis
    (0 (sequence-swap-indices side-colors 0 2))
    (1 (sequence-swap-indices side-colors 1 3))))

(defun flip-tile (tile axis)
  (with-slots (image side-colors transforms) tile
    (update-tile tile
                 :image (array-flip image axis)
                 :side-colors (flip-side-colors side-colors axis)
                 :transforms (fset:with-last transforms (cons :flip axis)))))

(defun index-by-position (placements)
  (let ((result (fset:empty-map)))
    (fset:do-set (tile placements)
      (assert (not (nth-value 1 (fset:lookup result (tile-position tile)))))
      (fset:adjoinf result (tile-position tile) tile))
    result))

(defun verify-placements (placements)
  (let ((position-index (index-by-position placements)))
    (multiple-value-bind (min-point max-point)
        (point:bbox (fset:domain position-index))
      (iter (for x from (point:elt min-point 0) to (point:elt max-point 0))
            (iter (for y from (point:elt min-point 1) to (point:elt max-point 1))
                  (for position = (point:make-point x y))
                  (for (values tile tile-found-p) = (fset:lookup position-index position))
                  (assert tile-found-p ()
                          "No tile found at position: ~A" position)
                  (iter (for side in +sides+)
                        (for neighbor-position = (point:+ position (side-to-vector side)))
                        (when (point:<= min-point neighbor-position max-point)
                          (for (values neighbor neighbor-found-p) = (fset:lookup position-index neighbor-position))
                          (assert neighbor-found-p ()
                                  "No neighbor for ~A found at position: ~A" tile position)
                          (assert (equal (tile-side-id tile side)
                                         (tile-side-id neighbor (side-complement side)))
                                  ()
                                  "Sides between ~A and ~A do not match" tile neighbor)))))))
  t)

;;; Assemble the placed tiles

(defun assemble-tiles (placements)
  (multiple-value-bind (min-point max-point)
      (point:bbox (fset:image #'tile-position placements))
    (let* ((side-length (- (array-dimension (tile-image (fset:arb placements)) 0) 2))
           (result (make-array (list (* side-length (1+ (- (point:elt max-point 0) (point:elt min-point 0))))
                                     (* side-length (1+ (- (point:elt max-point 1) (point:elt min-point 1)))))
                               :element-type 'bit
                               :initial-element 0)))
      (fset:do-set (tile placements)
        (with-slots (image position) tile
          (let ((dtile (point:* (point:- position min-point) side-length)))
            (dotimes (i (- (array-dimension image 0) 2))
              (dotimes (j (- (array-dimension image 1) 2))
                (when (plusp (aref image (1+ i) (1+ j)))
                  (setf (aref result
                              (+ (point:elt dtile 1) i)
                              (+ (point:elt dtile 0) j))
                        1)))))))
      result)))

;;; Find the Sea Monster

(defparameter *sea-monster*
  #2A((0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0)
      (1 0 0 0 0 1 1 0 0 0 0 1 1 0 0 0 0 1 1 1)
      (0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 0)))
(defparameter *sea-monster-indices*
  (iter (with result = (fset:empty-set))
        (for x from 0 below (array-dimension *sea-monster* 1))
        (iter (for y from 0 below (array-dimension *sea-monster* 0))
              (when (plusp (aref *sea-monster* y x))
                (fset:adjoinf result (point:make-point x y))))
        (finally (return result))))
(defparameter *sea-monster-max-index*
  (nth-value 1 (point:bbox *sea-monster-indices*)))

(defun sea-monster-p (image position)
  (fset:do-set (index *sea-monster-indices*)
    (when (zerop (point:aref image (point:+ position index)))
      (return-from sea-monster-p nil)))
  t)

(defun sea-monster-positions (image)
  (iter (with result = (fset:empty-set))
        (for x from 0 below (- (array-dimension image 1) (point:elt *sea-monster-max-index* 0)))
        (iter (for y from 0 below (- (array-dimension image 0) (point:elt *sea-monster-max-index* 1)))
              (for position = (point:make-point x y))
              (when (sea-monster-p image position)
                (fset:adjoinf result position)))
        (finally (return result))))

(defun orient-image-and-find-sea-monsters (image &optional (max-rotation 0) flipped)
  (let ((positions (sea-monster-positions image)))
    (if (fset:empty? positions)
        (if (< max-rotation 3)
            (orient-image-and-find-sea-monsters (array-rot90 image) (1+ max-rotation) flipped)
            (progn
              (assert (not flipped))
              (orient-image-and-find-sea-monsters (array-flip (array-rot90 image) 0) 0 t)))
        (values positions max-rotation flipped))))

;;; Put it all together

(defun get-answer-2 (&optional (tiles *tiles*))
  (let* ((image (assemble-tiles (place-tiles (map-tiles-by #'tile-id tiles))))
         (sea-monster-count (fset:size (orient-image-and-find-sea-monsters image))))
    (- (reduce #'+ (aops:flatten image))
       (* sea-monster-count (fset:size *sea-monster-indices*)))))

(aoc:given 2
  (= 273 (get-answer-2 *example*)))


;;;; Basic visualization

(defun print-bitmap (image &optional (stream t))
  (aoc:print-braille (array-dimension image 1) (array-dimension image 0)
                     (lambda (point) (plusp (point:aref image point)))
                     :stream stream))


;;;; Generic array functions

(defun array-rot90 (m &optional (k 1))
  (declare (type (array * (* *)) m)
           (type integer k))
  (cond
    ((zerop (mod k 4))
     m)
    ((= 2 (mod k 4))
     (aops:reshape (reverse (aops:flatten m))
                   (array-dimensions m)))
    (t
     (let ((result (make-array (list (array-dimension m 1)
                                     (array-dimension m 0))
                               :element-type (array-element-type m))))
       (dotimes (i (array-dimension result 0))
         (dotimes (j (array-dimension result 1))
           (let ((mi (if (= 1 (mod k 4))
                         j
                         (- (array-dimension m 0) j 1)))
                 (mj (if (= 3 (mod k 4))
                         i
                         (- (array-dimension m 1) i 1))))
             (setf (aref result i j)
                   (aref m mi mj)))))
       result))))

(defun array-flip (m &optional axis)
  "If AXIS is NIL, both axes are flipped."
  (declare (type (array * (* *)) m)
           (type (or null (integer 0)) axis))
  (let ((result (make-array (array-dimensions m)
                            :element-type (array-element-type m))))
    (dotimes (i (array-dimension m 0))
      (dotimes (j (array-dimension m 1))
        (let ((mi (if (or (null axis)
                          (= axis 0))
                      (- (array-dimension m 0) i 1)
                      i))
              (mj (if (or (null axis)
                          (= axis 1))
                      (- (array-dimension m 1) j 1)
                      j)))
          (setf (aref result i j)
                (aref m mi mj)))))
    result))


;;;; Static Visualization

;;; Each tile carries scale and rotation information.  Coming in, the
;;; position slots only give their position relative to the other tiles.
;;; We'll change those so that the position is in visualization pixels,
;;; with the origin at the center of the visualization image.

(defun write-placement-image (placements filename &key (width 1024) (height 1024) (margin 32) with-borders (with-monsters t) (upscale t))
  (assert (not (and with-monsters with-borders)))
  (cairo:with-png-file (filename :rgb24 width height :surface surface)
    (viz:clear-surface surface :dark-background)
    (cairo:translate (/ width 2) (/ height 2))
    (multiple-value-bind (min-point max-point)
        (point:bbox (fset:image #'tile-position placements))
      (let* ((tile-size (- (array-dimension (tile-image (fset:arb placements)) 0)
                           (if with-borders 0 2)))
             (image-size (* tile-size (1+ (apply #'max (point:list-coordinates (point:- max-point min-point))))))
             (scale (/ (- (max width height) margin) image-size))
             (positioned-tiles (position-tiles-on-canvas placements
                                                         scale
                                                         (point:/ (point:+ min-point max-point) 2)
                                                         with-borders)))
        (if with-borders
            (fset:do-set (tile positioned-tiles)
              (draw-tile tile with-borders))
            (let ((image (assemble-tiles placements)))
              (multiple-value-bind (monster-locations rotations flip)
                  (orient-image-and-find-sea-monsters image)
                (draw-image (add-monsters-to-image image monster-locations rotations flip) scale
                            :upscale upscale))))))))

(defun position-tiles-on-canvas (tiles tile-scale center-point with-borders)
  (fset:image (alexandria:rcurry #'position-tile-on-canvas tile-scale center-point with-borders)
              tiles))

(defun position-tile-on-canvas (tile tile-scale center-point with-borders)
  "CENTER-POINT is relative to the original tile positions."
  (with-slots (position image) tile
    (let* ((tile-offset-from-center (point:- position center-point))
           (pixel-width (* tile-scale (if with-borders
                                          (array-dimension image 0)
                                          (- (array-dimension image 0) 2))))
           (pixel-offset-from-center (point:* tile-offset-from-center pixel-width)))
      (update-tile tile
                   :scale tile-scale
                   :position pixel-offset-from-center))))

(defun add-monsters-to-image (image monster-locations rotations flip)
  (let* ((adjusted-image (array-rot90 (if flip
                                          (array-flip image 0)
                                          image)
                                      rotations))
         (result (make-array (array-dimensions adjusted-image)
                             :element-type '(integer 0 2)
                             :initial-contents (aops:split adjusted-image 1))))
    (fset:do-set (monster-origin monster-locations)
      (fset:do-set (pixel *sea-monster-indices*)
        (setf (point:aref result (point:+ monster-origin pixel)) 2)))
    result))

(defun draw-tile (tile with-borders)
  (with-slots (position image scale) tile
    (let ((side-length (if with-borders
                           (array-dimension image 0)
                           (- (array-dimension image 0) 2)))
          (offset (if with-borders 0 1)))
      (cairo:save)
      (cairo:translate (point:x position) (point:y position))
      (cairo:scale scale scale)
      (cairo:translate (- (/ side-length 2)) (- (/ side-length 2)))
      (viz:set-color :dark-highlight)
      (cairo:rectangle 0 0 side-length side-length)
      (cairo:fill-path)
      (dotimes (i side-length)
        (dotimes (j side-length)
          (when (plusp (aref image (+ i offset) (+ j offset)))
            (ecase (aref image (+ i offset) (+ j offset))
              (1 (viz:set-color :dark-secondary))
              (2 (viz:set-color :green)))
            (cairo:rectangle j i 1 1)
            (cairo:fill-path))))
      (cairo:restore))))

(defun draw-image (image scale &key upscale line-callback)
  (if (and upscale (< 1 scale))
      (draw-image (scale3x image) (/ scale 3) :upscale t)
      (let ((side-length (array-dimension image 0)))
        (cairo:save)
        (cairo:scale scale scale)
        (cairo:translate (- (/ side-length 2)) (- (/ side-length 2)))
        (dotimes (i side-length)
          (viz:set-color :dark-highlight)
          (cairo:rectangle 0 i side-length 1)
          (cairo:fill-path)
          (dotimes (j side-length)
            (when (plusp (aref image i j))
              (ecase (aref image i j)
                (1 (viz:set-color :dark-primary))
                (2 (viz:set-color :green)))
              (cairo:rectangle j i 1 1)
              (cairo:fill-path)))
          (when line-callback
            (funcall line-callback i)))
        (cairo:restore))))


;;;; Scaling

(defun scale3x (image)
  (let ((result (make-array (list (* 3 (array-dimension image 0))
                                  (* 3 (array-dimension image 1)))
                            :element-type (array-element-type image))))
    (dotimes (i (array-dimension image 0))
      (dotimes (j (array-dimension image 1))
        (scale3x-pixel image result i j)))
    result))

;; This is an extremely literal implentation of AdvMAME3x
(defun scale3x-pixel (source target i j)
  (let ((sa (clamped-aref source (1- i) (1- j)))
        (sb (clamped-aref source (1- i)     j))
        (sc (clamped-aref source (1- i) (1+ j)))
        (sd (clamped-aref source     i  (1- j)))
        (se (clamped-aref source     i      j))
        (sf (clamped-aref source     i  (1+ j)))
        (sg (clamped-aref source (1+ i) (1- j)))
        (sh (clamped-aref source (1+ i)     j))
        (si (clamped-aref source (1+ i) (1+ j)))
        (ti0 (* 3 i))
        (tj0 (* 3 j)))
    (setf (aref target ti0 tj0)
          (if (and (= sd sb)
                   (/= sd sh)
                   (/= sb sf))
              sd
              se))
    (setf (aref target ti0 (1+ tj0))
          (if (or (and (= sd sb)
                       (/= sd sh)
                       (/= sb sf)
                       (/= se sc))
                  (and (= sb sf)
                       (/= sb sd)
                       (/= sf sh)
                       (/= se sa)))
              sb
              se))
    (setf (aref target ti0 (+ 2 tj0))
          (if (and (= sb sf)
                   (/= sb sd)
                   (/= sf sh))
              sf
              se))
    (setf (aref target (1+ ti0) tj0)
          (if (or (and (= sd sh)
                       (/= sh sf)
                       (/= sd sb)
                       (/= se sa))
                  (and (= sd sb)
                       (/= sd sh)
                       (/= sb sf)
                       (/= se sg)))
              sd
              se))
    (setf (aref target (1+ ti0) (1+ tj0))
          se)
    (setf (aref target (1+ ti0) (+ 2 tj0))
          (if (or (and (= sb sf)
                       (/= sb sd)
                       (/= sf sh)
                       (/= se si))
                  (and (= sf sh)
                       (/= sf sb)
                       (/= sh sd)
                       (/= se sc)))
              sf
              se))
    (setf (aref target (+ 2 ti0) tj0)
          (if (and (= sh sd)
                   (/= sh sf)
                   (/= sd sb))
              sd
              se))
    (setf (aref target (+ 2 ti0) (1+ tj0))
          (if (or (and (= sf sh)
                       (/= sf sb)
                       (/= sh sd)
                       (/= se sg))
                  (and (= sh sd)
                       (/= sh sf)
                       (/= sd sb)
                       (/= se si)))
              sh
              se))
    (setf (aref target (+ 2 ti0) (+ 2 tj0))
          (if (and (= sf sh)
                   (/= sf sb)
                   (/= sh sd))
              sf
              se))))

(defun clamped-aref (array i j)
  (aref array
        (min (max i 0) (1- (array-dimension array 0)))
        (min (max j 0) (1- (array-dimension array 1)))))


;;;; Video

;;; Fundamental (input-independent) video parameters

(defparameter *outro-time* 15)
(defparameter *add-tile-time* 0.25)
(defparameter *move-to-image-time* 0.5)
(defparameter *border-color-pause-time* 0.1)
(defparameter *shift-placed-time* 0.5)
(defparameter *hold-time* 0.5)
(defparameter *transform-time* 0.25)

(defparameter *connection-line-width* 1.5)
(defparameter *holding-relative-scale* 0.8)

(defparameter *debug* t)

;;; Video state

(defstruct tile-state
  (incoming nil :type list)
  (active nil :type (or null tile)) ; If there's a tile here, it overrides its data in the TILES slot
  (tiles (fset:empty-map) :type fset:map)  ; Map from tile IDs to tiles
  (placed (fset:empty-set) :type fset:set) ; Set of IDs of placed tiles
  (waiting (fset:empty-set) :type fset:set) ; Set of IDs of tiles waiting to be placed
  (connections (fset:empty-map (fset:empty-set)) :type fset:map))  ; Map from ID to a set of IDs

(defun update-tile-state (tile-state &key (incoming nil incomingp) (active nil activep) tiles placed waiting connections)
  (with-slots ((old-incoming incoming) (old-active active) (old-tiles tiles) (old-placed placed) (old-waiting waiting) (old-connections connections))
      tile-state
    (make-tile-state :incoming (if incomingp
                                   incoming
                                   old-incoming)
                     :active (if activep
                                 active
                                 old-active)
                     :tiles (or tiles old-tiles)
                     :placed (or placed old-placed)
                     :waiting (or waiting old-waiting)
                     :connections (or connections old-connections))))

(defun arb-tile (tile-state)
  (with-slots (incoming tiles) tile-state
    (if (endp incoming)
        (nth-value 1 (fset:arb tiles))
        (car incoming))))

(defun print-state-stats (tile-state)
  (with-slots (incoming placed waiting) tile-state
    (format t "incoming: ~A  waiting: ~A  placed: ~A"
            (length incoming) (fset:size waiting) (fset:size placed))))

(defun state-with-active (tile-state tile)
  (update-tile-state tile-state :active tile))

(defun add-tile-to-state (tile-state tile location)
  (when (not (member location '(:waiting :placed)))
    (error "LOCATION must be either :waiting or :placed"))
  (with-slots (tiles placed waiting) tile-state
    (with-slots (id) tile
      (update-tile-state tile-state
                         :tiles (fset:with tiles id tile)
                         :placed (if (eq location :placed)
                                     (fset:with placed id)
                                     (fset:less placed id))
                         :waiting (if (eq location :waiting)
                                      (fset:with waiting id)
                                      (fset:less waiting id))))))

(defun state-tile-by-id (tile-state tile-id)
  (with-slots (active tiles) tile-state
    (if (and active (= tile-id (tile-id active)))
        active
        (fset:lookup tiles tile-id))))

(defun state-placed-bbox (tile-state)
  (with-slots (placed) tile-state
    (point:bbox (gmap:gmap :list
                           (lambda (id)
                             (tile-position (state-tile-by-id tile-state id)))
                           (:set placed)))))

(defun state-shift-placed-tiles (tile-state delta)
  (with-slots (tiles placed) tile-state
    (update-tile-state
     tile-state
     :tiles (fset:map-union
             tiles
             (gmap:gmap :map
                        (lambda (id)
                          (let ((tile (fset:lookup tiles id)))
                            (with-slots (position) tile
                              (values id (update-tile tile :position (point:+ position delta))))))
                        (:set placed))))))

;;; Main video entry point

(defun write-placement-video (tiles filename &key (width 1920) (height 1080) (margin 32) (fps 60))
  (viz:with-video (video (viz:create-video filename width height :fps fps))
    (let* ((test-image (assemble-tiles (place-tiles (map-tiles-by #'tile-id tiles))))
           (scale (/ (- (min width height) margin) (apply #'max (array-dimensions test-image))))
           (image (video-add-tile (make-tile-state :incoming tiles) video scale margin fps)))
      (write-final-image image video fps margin fps))))

(defun output-state-frame (tile-state video)
  (let ((frame (make-state-frame tile-state video)))
    (viz:write-video-surface video frame)))

(defun make-state-frame (tile-state video)
  (let ((frame (viz:create-video-surface video)))
    (viz:clear-surface frame :dark-background)
    (video-draw-connections frame tile-state)
    (video-draw-tiles frame tile-state)
    frame))

(defun draw-line-segment (start end)
  "If START is NIL, this will continue an existing line from the previous
  point."
  (when start
    (cairo:move-to (point:x start) (point:y start)))
  (cairo:line-to (point:x end) (point:y end)))

(defun video-draw-connections (frame tile-state)
  (with-slots (tiles connections) tile-state
    (cairo:with-context-from-surface (frame)
      (viz:set-color :green)
      (cairo:set-line-width *connection-line-width*)
      (fset:do-map (source-id target-ids connections)
        (let ((source (state-tile-by-id tile-state source-id)))
          (fset:do-set (target-id target-ids)
            (let ((target (state-tile-by-id tile-state target-id)))
              (draw-line-segment (tile-position source) (tile-position target))
              (cairo:stroke))))))))

(defun video-draw-tiles (frame tile-state)
  (with-slots (tiles active) tile-state
    (cairo:with-context-from-surface (frame)
      (fset:do-set (tile (fset:range tiles))
        (unless (and active (= (tile-id tile) (tile-id active)))
          (video-draw-tile tile)))
      (when active
        (video-draw-tile active)))))

(defun video-draw-tile (tile)
  (with-slots (id position rotation image scale side-colors draw-borders x-scale y-scale) tile
    (unless (or (zerop x-scale)
                (zerop y-scale))
      (let ((width (array-dimension image 1))
            (height (array-dimension image 0)))
        (cairo:save)
        (cairo:translate (point:x position) (point:y position))
        (cairo:rotate rotation)
        (cairo:scale scale scale)
        (cairo:scale x-scale y-scale)
        (cairo:translate (- (/ width 2)) (- (/ height 2)))
        (viz:set-color :dark-highlight)
        (cairo:rectangle (if (fset:member? :left draw-borders) 0 1)
                         (if (fset:member? :top draw-borders) 0 1)
                         (- width (fset:size (fset:set-difference (fset:set :left :right) draw-borders)))
                         (- height (fset:size (fset:set-difference (fset:set :top :bottom) draw-borders))))
        (cairo:fill-path)
        (dotimes (i height)
          (dotimes (j width)
            (when (plusp (aref image i j))
              (cond
                ((zerop i)
                 (when (fset:member? :top draw-borders)
                   (cairo:rectangle j i 1 1)
                   (viz:set-color (fset:lookup side-colors 0))
                   (cairo:fill-path)))
                ((zerop j)
                 (when (fset:member? :left draw-borders)
                   (cairo:rectangle j i 1 1)
                   (viz:set-color (fset:lookup side-colors 3))
                   (cairo:fill-path)))
                ((= i (1- height))
                 (when (fset:member? :bottom draw-borders)
                   (cairo:rectangle j i 1 1)
                   (viz:set-color (fset:lookup side-colors 2))
                   (cairo:fill-path)))
                ((= j (1- width))
                 (when (fset:member? :right draw-borders)
                   (cairo:rectangle j i 1 1)
                   (viz:set-color (fset:lookup side-colors 1))
                   (cairo:fill-path)))
                (t
                 (cairo:rectangle j i 1 1)
                 (viz:set-color :dark-primary)
                 (cairo:fill-path))))))
        (cairo:restore)))))

(defun point-along-path (distance path)
  (let* ((next-vector (point:- (second path) (first path)))
         (distance-to-next-point (point:norm next-vector)))
    (if (< distance distance-to-next-point)
        (point:+ (first path) (point:* (point:unit-vector next-vector) distance))
        (point-along-path (- distance distance-to-next-point) (cdr path)))))

(defun animate-tile-along-path (tile path frames tile-state video &key start-scale end-scale)
  (with-slots ((tile-scale scale)) tile
    (let* ((real-start-scale (or start-scale tile-scale))
           (real-end-scale (or end-scale tile-scale))
           (total-scale-change (- real-end-scale real-start-scale))
           (scale-change-per-frame (/ total-scale-change frames))
           (total-distance (gmap:gmap :sum (alexandria:compose #'point:norm #'point:-)
                                      (:list (cdr path))
                                      (:list path)))
           (distance-per-frame (/ total-distance frames)))
      (dotimes (frame frames)
        (let* ((distance (* frame distance-per-frame))
               (position (point-along-path distance path))
               (scale (+ real-start-scale (* scale-change-per-frame frame))))
          (output-state-frame (state-with-active tile-state
                                                 (update-tile tile
                                                              :position position
                                                              :scale scale))
                              video)))
      (output-state-frame (state-with-active tile-state
                                             (update-tile tile
                                                          :position (fset:last path)
                                                          :scale real-end-scale))
                          video))))

(defun tile-state-collapse-active (tile-state)
  "Returns a tile-state without an active tile (but with the active tile
  merged into the TILES slot)."
  (with-slots (active tiles) tile-state
    (if active
        (update-tile-state tile-state
                           :active nil
                           :tiles (fset:with tiles (tile-id active) active))
        tile-state)))

(defun animate-state-transition (start-state end-state video frames)
  (with-slots ((start-tiles tiles)) (tile-state-collapse-active start-state)
    (with-slots ((end-tiles tiles)) (tile-state-collapse-active end-state)
      (flet ((add-tile-vector (vectors tile-id)
               (fset:with vectors tile-id
                          (point:- (tile-position (fset:lookup end-tiles tile-id))
                                   (tile-position (fset:lookup start-tiles tile-id)))))
             (add-tile-dscale (dscales tile-id)
               (fset:with dscales tile-id
                          (- (tile-scale (fset:lookup end-tiles tile-id))
                             (tile-scale (fset:lookup start-tiles tile-id)))))
             (shift-tile (tiles tile-id distance vectors dscales)
               (let ((tile (fset:lookup tiles tile-id))
                     (vector (fset:lookup vectors tile-id))
                     (dscale (fset:lookup dscales tile-id)))
                 (with-slots (position scale) tile
                   (fset:with tiles tile-id
                              (update-tile tile
                                           :position (point:+ position
                                                              (point:* vector distance))
                                           :scale (+ scale (* dscale distance))))))))
        (let ((vectors (fset:reduce #'add-tile-vector
                                    (fset:domain start-tiles)
                                    :initial-value (fset:empty-map)))
              (dscales (fset:reduce #'add-tile-dscale
                                    (fset:domain start-tiles)
                                    :initial-value (fset:empty-map))))
          (dotimes (frame frames)
            (let ((frame-tiles (fset:reduce
                                (alexandria:rcurry #'shift-tile (/ frame frames) vectors dscales)
                                (fset:domain start-tiles)
                                :initial-value start-tiles)))
              (output-state-frame (update-tile-state end-state
                                                     :tiles frame-tiles)
                                  video)))
          (output-state-frame end-state video))))))

;;; Overall positioning
;;;
;;; Each function here will return a point.
;;;
;;; They assume that the final image will occupy the right side of the
;;; surface, minus the margin.  The remaing lefthand space will be split
;;; into the area for new tiles (upper) and holding area (lower).

(defun image-center (video margin)
  (let ((width (viz:video-width video))
        (height (viz:video-height video)))
    (point:make-point (- width margin (/ height 2)) (/ height 2))))

(defun new-tile-area-height (tile scale)
  (with-slots (image) tile
    (* scale (+ 2 (array-dimension image 0)))))

(defun new-tile-center (tile scale video margin)
  (let* ((width (viz:video-width video))
         (height (viz:video-height video))
         (left-block-width (- width height))
         (area-height (new-tile-area-height tile scale)))
    (point:make-point (+ margin (/ left-block-width 2)) (+ margin (/ area-height 2)))))

(defun holding-area-center (tile scale video margin)
  (let* ((width (viz:video-width video))
         (height (viz:video-height video))
         (left-block-width (- width height)))
    (point:make-point (+ margin (/ left-block-width 2))
                      (- height margin (/ (holding-area-height tile scale video margin) 2)))))

(defun holding-area-height (tile scale video margin)
  (- (viz:video-height video)
     (* 2 margin)
     (new-tile-area-height tile scale)))

(defun holding-area-width (tile scale video margin)
  (let ((height (viz:video-height video)))
    (min (holding-area-height tile scale video margin)
         (- (/ height 2) margin))))

;;; Algorithm

(defun video-add-tile (tile-state video scale margin fps)
  (sb-ext:gc)         ; Force a GC of the last tile's video frames.  Sigh.
  (print-state-stats tile-state)
  (let ((new-state (video-get-next-tile tile-state video scale margin fps)))
    (with-slots (active placed connections) new-state
      (if (null active)
          (with-slots (tiles) tile-state
            (assemble-tiles (scale-tile-positions (fset:range tiles) scale)))
          (cond
            ;; Place the first tile
            ((fset:empty? placed)
             (when *debug*
               (format t "  first tile: ~A~%" active))
             (move-first-tile-to-image active new-state scale video margin fps)
             (video-add-tile (add-tile-to-state new-state
                                                (update-tile active :position (image-center video margin))
                                                :placed)
                             video scale margin fps))
            ;; The incoming tile can go directly into the image.
            ((not (fset:disjoint? placed (fset:lookup connections (tile-id active))))
             (when *debug*
               (format t "  direct to image: ~A~%" active))
             (video-add-tile (video-place-tile active (state-with-active new-state nil)
                                               video margin fps)
                             video scale margin fps))
            ;; The incoming tile has to go into holding.
            (t
             (when *debug*
               (format t "  holding: ~A~%" active))
             (video-add-tile (video-hold-tile active (state-with-active new-state nil)
                                              video scale margin fps)
                             video scale margin fps)))))))

(defun video-get-next-tile (tile-state video scale margin fps)
  "Returns a tile-state structure with the next tile as the active tile.
  \"The next tile\" is, in order of preference:
    * A tile from the holding area that can be placed in the image
    * The next tile from the incoming queue
    * NIL (if the process is complete)

  The animation for getting the tile will be complete."
  (with-slots (incoming placed connections waiting) tile-state
    (cond
      ((and
        (not (fset:empty? waiting))
        (not (fset:empty? placed))
        (not (fset:empty? (fset:restrict connections waiting)))
        (not (fset:disjoint? (fset:reduce #'fset:union
                                          (fset:range (fset:restrict connections waiting)))
                             placed)))
       (video-get-from-holding tile-state video scale margin fps))
      ((not (endp incoming))
       (video-get-from-incoming tile-state video scale margin fps))
      (t
       (state-with-active tile-state nil)))))

(defun video-get-from-incoming (tile-state video scale margin fps)
  (with-slots (incoming tiles) tile-state
    (let* ((new-tile (update-tile (car incoming) :scale scale :position (new-tile-center (arb-tile tile-state) scale video margin)))
           (new-connections (connect-tile new-tile tile-state))
           (new-state (update-tile-state
                       tile-state
                       :tiles (fset:with tiles (tile-id new-tile) new-tile)
                       :incoming (cdr incoming)
                       :connections new-connections)))
      (let* ((frames (truncate (* fps *add-tile-time*)))
             (final-position (new-tile-center new-tile scale video margin))
             (initial-position (point:make-point (- (/ (tile-width new-tile) 2))
                                                 (point:y final-position))))
        (animate-tile-along-path new-tile (list initial-position final-position) frames new-state video))
      (state-with-active new-state new-tile))))

(defun connect-tile (new-tile tile-state)
  (with-slots (tiles connections) tile-state
    (with-slots ((new-id id)) new-tile
      (let ((new-side-ids (tile-side-ids new-tile :with-reverse t))
            (new-connections connections))
        (fset:do-set (old-tile (fset:range tiles))
          (when (not (fset:empty? (fset:intersection new-side-ids (tile-side-ids old-tile))))
            (setf new-connections
                  (aoc:map-set-with (aoc:map-set-with new-connections new-id (tile-id old-tile))
                                    (tile-id old-tile) new-id))))
        new-connections))))

(defun video-get-from-holding (tile-state video scale margin fps)
  (with-slots (placed waiting connections tiles) tile-state
    (let* ((waiting-connections (fset:restrict connections waiting))
           (connections-to-placed (fset:filter (lambda (source-id target-ids)
                                                 (declare (ignore source-id))
                                                 (not (fset:disjoint? target-ids placed)))
                                               waiting-connections))
           (new-tile-id (fset:arb connections-to-placed))
           (new-tile (update-tile (fset:lookup tiles new-tile-id)
                                  :position (new-tile-center (arb-tile tile-state) scale video margin)
                                  :scale scale))
           (new-state (reposition-holding-area
                       (update-tile-state tile-state
                                          :tiles (fset:with tiles new-tile-id new-tile)
                                          :waiting (fset:less waiting new-tile-id))
                       scale video margin)))
      (animate-state-transition tile-state new-state video (truncate (* fps *hold-time*)))
      (state-with-active new-state new-tile))))

(defun scale-tile-positions (tiles scale)
  (let* ((tile-image-width (array-dimension (tile-image (fset:arb tiles)) 0))
         (total-scale (* scale (- tile-image-width 2))))
    (fset:image (lambda (tile)
                  (update-tile tile :position (point:/ (tile-position tile) total-scale)))
                tiles)))

;;; First tile

(defun move-first-tile-to-image (new-tile tile-state scale video margin fps)
  (let* ((frames (truncate (* fps *move-to-image-time*)))
         (final-position (image-center video margin))
         (initial-position (new-tile-center new-tile scale video margin))
         (path (list initial-position
                     (point:make-point (point:x final-position) (point:y initial-position))
                     final-position)))
    (animate-tile-along-path new-tile path frames tile-state video)))

;;; Placing tile in image

(defun video-place-tile (new-tile tile-state video margin fps)
  (let* ((colored-state (color-borders (state-with-active tile-state new-tile)))
         (rotated-tile (video-orient-tile (tile-state-active colored-state) colored-state video fps))
         (rotated-state (state-with-active colored-state rotated-tile)))
    (dotimes (i (truncate (* fps *border-color-pause-time*)))
      (output-state-frame rotated-state video))
    (let* ((debordered-state (remove-colored-borders rotated-state))
           (trial-new-position (get-placed-tile-position new-tile debordered-state))
           (shifted-state (shift-placed-tiles-for-position trial-new-position debordered-state video margin))
           (new-position (get-placed-tile-position new-tile shifted-state))
           (final-tile (update-tile (tile-state-active shifted-state)
                                    :position new-position))
           (final-state (add-tile-to-state (state-with-active shifted-state nil)
                                           final-tile :placed)))
      (animate-state-transition debordered-state final-state video
                                (truncate (* fps *add-tile-time*)))
      final-state)))

(defun color-borders (tile-state)
  "Colors the borders from the active tile to placed tiles."
  (with-slots (active connections placed tiles) tile-state
    (let* ((color-map (fset:reduce (lambda (new-map target-id)
                                     (connection-color-map new-map active (fset:lookup tiles target-id)))
                                   (fset:intersection placed (fset:lookup connections (tile-id active)))
                                   :initial-value (fset:empty-map :dark-secondary)))
           (new-active (color-tile-sides active color-map))
           (new-tiles (fset:map-union
                       tiles
                       (gmap:gmap :map
                                  (lambda (id)
                                    (values id (color-tile-sides (fset:lookup tiles id)
                                                                 color-map)))
                                  (:set (fset:lookup connections (tile-id active)))))))
      (update-tile-state tile-state
                         :active new-active
                         :tiles new-tiles))))

(let ((candidate-colors (fset:set :magenta :violet :blue :cyan)))
  (defun connection-color-map (previous-map source target)
    (let ((side-ids (fset:intersection (tile-side-ids source :with-reverse t)
                                       (tile-side-ids target :with-reverse t)))
          (color (fset:arb (fset:set-difference candidate-colors (fset:range previous-map)))))
      (fset:reduce (lambda (new-map side-id)
                     (fset:with new-map side-id color))
                   side-ids
                   :initial-value previous-map))))

(defun color-tile-sides (tile color-map)
  (update-tile tile
               :side-colors (fset:image (lambda (side)
                                          (fset:lookup color-map (tile-side-id tile side)))
                                        (fset:seq :top :right :bottom :left))))

(defun remove-colored-borders (tile-state)
  (with-slots (active tiles) tile-state
    (update-tile-state tile-state
                       :active (tile-remove-colored-borders active)
                       :tiles (fset:image (lambda (id tile)
                                            (values id (tile-remove-colored-borders tile)))
                                          tiles))))

(defun tile-remove-colored-borders (tile)
  (with-slots (side-colors draw-borders) tile
    (update-tile
     tile
     :draw-borders (fset:filter
                    (lambda (side)
                      (eq :dark-secondary (fset:lookup side-colors (side-to-border-index side))))
                    draw-borders))))

(defun video-orient-tile (new-tile tile-state video fps)
  (with-slots (tiles connections placed) tile-state
    (with-slots ((new-tile-id id)) new-tile
      (let* ((placed-connections (fset:intersection placed (fset:lookup connections new-tile-id)))
             (target (fset:lookup tiles (fset:arb placed-connections)))
             (transformed-tile (transform-for-placement new-tile target)))
        (animate-transforms (tile-transforms transformed-tile) new-tile tile-state video fps)
        transformed-tile))))

(defun animate-transforms (transforms new-tile tile-state video fps)
  (unless (zerop (fset:size transforms))
    (destructuring-bind (op . param) (fset:first transforms)
      (ecase op
        (:rot (progn
                (animate-rotation param new-tile tile-state video fps)
                (animate-transforms (fset:less-first transforms)
                                    (rotate-tile new-tile param)
                                    tile-state video fps)))
        (:flip (progn
                 (animate-flip param new-tile tile-state video fps)
                 (animate-transforms (fset:less-first transforms)
                                     (flip-tile new-tile param)
                                     tile-state video fps)))))))

(defun animate-rotation (degrees tile tile-state video fps)
  (if (< 180 degrees)
      (animate-rotation (- degrees 360) tile tile-state video fps)
      (let* ((frames (truncate (* fps *transform-time* (/ (abs degrees) 90))))
             (radians (- (* pi (/ degrees 180))))
             (radians-per-frame (/ radians frames)))
        (dotimes (frame frames)
          (output-state-frame (state-with-active tile-state
                                                 (update-tile tile
                                                              :rotation (* frame radians-per-frame)))
                              video))
        (output-state-frame (state-with-active tile-state
                                               (update-tile tile :rotation radians))
                            video))))

(defun animate-flip (axis tile tile-state video fps)
  (let* ((frames (truncate (* fps *transform-time*))))
    (dotimes (frame frames)
      (let ((axis-scale (- 1 (* 2 (/ frame frames)))))
        (output-state-frame
         (state-with-active tile-state
                            (update-tile tile
                                         :y-scale (if (= axis 0) axis-scale 1)
                                         :x-scale (if (= axis 1) axis-scale 1)))
         video)))
    (output-state-frame
     (state-with-active tile-state
                        (update-tile tile
                                     :y-scale (if (= axis 0) -1 1)
                                     :x-scale (if (= axis 1) -1 1)))
     video)))

(defun get-placed-tile-position (new-tile tile-state)
  (with-slots (tiles connections placed) tile-state
    (with-slots ((new-tile-id id) scale image) new-tile
      (let* ((placed-connections (fset:intersection placed (fset:lookup connections new-tile-id)))
             (target (fset:lookup tiles (fset:arb placed-connections)))
             (placement (find-placement-direction new-tile target))
             (placement-vector (side-to-vector placement)))
        (point:+ (tile-position target)
                 (point:* placement-vector (* scale (- (array-dimension image 0) 2))))))))

(defun shift-placed-tiles-for-position (new-position tile-state video margin)
  (multiple-value-bind (min-point max-point) (state-placed-bbox tile-state)
    (if (point:<= min-point new-position max-point)
        tile-state
        (multiple-value-bind (new-min-point new-max-point)
            (point:bbox (list min-point new-position max-point))
          (let* ((old-center (image-center video margin))
                 (new-center (point:/ (point:+ new-min-point new-max-point) 2))
                 (total-movement (point:- old-center new-center)))
            (state-shift-placed-tiles tile-state total-movement))))))

;;; Holding

(defun add-tile-to-holding-area (new-tile tile-state default-scale video margin)
  (with-slots (waiting) tile-state
    (with-slots ((new-id id)) new-tile
      (reposition-holding-area
       (update-tile-state tile-state
                          :waiting (fset:with waiting new-id))
       default-scale video margin))))

(defun video-hold-tile (new-tile tile-state video default-scale margin fps)
  (let ((shifted-state (add-tile-to-holding-area new-tile tile-state default-scale video margin)))
    (animate-state-transition tile-state shifted-state video (truncate (* fps *hold-time*)))
    shifted-state))

(defun reposition-holding-area (tile-state default-scale video margin)
  (with-slots (waiting tiles) tile-state
    (if (fset:empty? waiting)
        tile-state
        (let* ((new-tiles tiles)
               (tiles-per-row (ceiling (sqrt (fset:size waiting))))
               (area-width (holding-area-width (arb-tile tile-state) default-scale video margin))
               (tile-width ( / area-width tiles-per-row)) ; Really the width of the tile's space
               (new-scale (min default-scale
                               (/ (* *holding-relative-scale* tile-width)
                                  (array-dimension (tile-image (nth-value 1 (fset:arb tiles))) 0))))
               (area-center (holding-area-center (arb-tile tile-state) default-scale video margin))
               (area-ul (point:- area-center (point:make-point (/ area-width 2) (/ area-width 2))))
               (tile-origin (point:+ area-ul (point:make-point (/ tile-width 2) (/ tile-width 2)))))
          (fset:do-seq (tile-id (fset:sort (fset:convert 'fset:seq waiting) #'<)
                                :index i)
            (multiple-value-bind (row col) (truncate i tiles-per-row)
              (let ((x (* (if (zerop (mod row 2))
                              col
                              (1- (- tiles-per-row col)))
                          tile-width))
                    (y (* row tile-width)))
                (fset:adjoinf new-tiles tile-id
                              (update-tile (fset:lookup tiles tile-id)
                                           :position (point:+
                                                      tile-origin
                                                      (point:make-point x y))
                                           :scale new-scale)))))
          (update-tile-state tile-state :tiles new-tiles)))))

;;; Final image

(defun write-final-image (image video frames-per-image margin fps)
  (format t "write-final-image~%")
  (let* ((surface (viz:create-video-surface video))
         (width (cairo:image-surface-get-width surface))
         (height (cairo:image-surface-get-height surface))
         (scale (/ (- (min width height) margin) (apply #'max (array-dimensions image)))))
    (slide-final-image-to-center image scale video margin fps)
    (multiple-value-bind (monster-locations rotations flip)
        (orient-image-and-find-sea-monsters image)
      (if flip
          (progn
            (flip-final-image image video scale fps)
            (rotate-final-image rotations (array-flip image 0) video scale fps))
          (rotate-final-image rotations image video scale fps))
      (viz:clear-surface surface :dark-background)
      (let ((final-image (add-monsters-to-image image monster-locations rotations flip)))
        (cairo:with-context-from-surface (surface)
          (cairo:translate (/ width 2) (/ height 2))
          (draw-image final-image scale))
        (viz:write-video-surface video surface)
        (draw-final-image (scale3x final-image) video surface (/ scale 3) frames-per-image fps)))))

(defun draw-final-image (image video surface scale frames-per-image fps &key (offset (point:make-point 0 0)))
  (when *debug* (:printv "draw-final-image" (float scale)))
  (let* ((width (cairo:image-surface-get-width surface))
         (height (cairo:image-surface-get-height surface)))
    (cairo:with-context-from-surface (surface)
      (cairo:translate (+ (/ width 2) (point:x offset)) (+ (/ height 2) (point:y offset)))
      (let ((total-lines (array-dimension image 0))
            (frame-count 0))
        (draw-image image scale
                    :line-callback (lambda (line)
                                     (when (< (/ frame-count frames-per-image) (/ line total-lines))
                                       (incf frame-count)
                                       (viz:write-video-surface video surface))))))
    (viz:write-video-surface video surface)
    (if (< scale 1)
        (dotimes (i (truncate (* fps *outro-time*)))
          (viz:write-video-surface video surface))
        (draw-final-image (scale3x image) video surface (/ scale 3) frames-per-image fps))))

(defun slide-final-image-to-center (image scale video margin fps)
  (when *debug* (:printv "slide-final-image-to-center"))
  (let* ((frames (truncate (* fps *shift-placed-time*)))
         (width (viz:video-width video))
         (height (viz:video-height video))
         (start-center (image-center video margin))
         (end-center (point:make-point (/ width 2) (/ height 2)))
         (center-vector (point:- end-center start-center)))
    (dotimes (frame frames)
      (let ((surface (viz:create-video-surface video))
            (center (point:+ start-center (point:* center-vector (/ frame frames)))))
        (viz:clear-surface surface :dark-background)
        (cairo:with-context-from-surface (surface)
          (cairo:translate (point:x center) (point:y center))
          (draw-image image scale))
        (viz:write-video-surface video surface)))
    (let ((surface (viz:create-video-surface video)))
      (viz:clear-surface surface :dark-background)
      (cairo:with-context-from-surface (surface)
        (cairo:translate (point:x end-center) (point:y end-center))
        (draw-image image scale))
      (viz:write-video-surface video surface))))

(defun flip-final-image (image video scale fps)
  (when *debug* (:printv "flip-final-image"))
  (let* ((frames (truncate (* 4 fps *transform-time*)))
         (width (viz:video-width video))
         (height (viz:video-height video)))
    (dotimes (frame frames)
      (let ((surface (viz:create-video-surface video)))
        (viz:clear-surface surface :dark-background)
        (cairo:with-context-from-surface (surface)
          (cairo:translate (/ width 2) (/ height 2))
          (cairo:scale 1 (- 1 (* 2 (/ frame frames))))
          (draw-image image scale))
        (viz:write-video-surface video surface)))
    (let ((surface (viz:create-video-surface video)))
      (viz:clear-surface surface :dark-background)
      (cairo:with-context-from-surface (surface)
        (cairo:translate (/ width 2) (/ height 2))
        (cairo:scale 1 -1)
        (draw-image image scale))
      (viz:write-video-surface video surface))))

(defun rotate-final-image (rotations image video scale fps)
  (when *debug* (:printv "rotate-final-image"))
  (if (< 2 rotations)
      (rotate-final-image (- rotations 4) image video scale fps)
      (let* ((frames (truncate (* 4 fps *transform-time*)))
             (width (viz:video-width video))
             (height (viz:video-height video))
             (final-rotation (- (* rotations (/ pi 2)))))
        (dotimes (frame frames)
          (let ((surface (viz:create-video-surface video)))
            (viz:clear-surface surface :dark-background)
            (cairo:with-context-from-surface (surface)
              (cairo:translate (/ width 2) (/ height 2))
              (cairo:rotate (* final-rotation (/ frame frames)))
              (draw-image image scale))
            (viz:write-video-surface video surface)))
        (let ((surface (viz:create-video-surface video)))
          (viz:clear-surface surface :dark-background)
          (cairo:with-context-from-surface (surface)
            (cairo:translate (/ width 2) (/ height 2))
            (cairo:rotate final-rotation)
            (draw-image image scale))
          (viz:write-video-surface video surface)))))
