(in-package :aoc-2020-08)

(aoc:define-day 2025 2001)


;;;; Parsing

(parseq:defrule program ()
  (+ instruction)
  (:lambda (&rest instructions) (fset:convert 'fset:seq instructions)))

(parseq:defrule instruction ()
  (and operation " " argument (? #\Newline))
  (:choose 0 2)
  (:lambda (op arg) (cons op arg)))

(parseq:defrule operation ()
  (or "acc" "jmp" "nop")
  (:lambda (op) (aoc:make-keyword op)))

(parseq:defrule argument ()
  (and (char "+-") aoc:integer-string)
  (:lambda (sign number)
    (if (char= #\- sign)
        (- number)
        number)))


;;;; Input

(defparameter *example*
  (parseq:parseq 'program
                 "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"))
(defparameter *program* (aoc:input :parse 'program))


;;;; Part 1

(defun next-acc (acc instruction)
  (if (eq :acc (car instruction))
      (+ acc (cdr instruction))
      acc))

(defun next-ip (ip instruction)
  (if (eq :jmp (car instruction))
      (+ ip (cdr instruction))
      (1+ ip)))

(defun find-loop (program acc ip visited-positions)
  (cond
    ((<= (fset:size program) ip)
     (values acc nil))
    ((fset:member? ip visited-positions)
     (values acc ip))
    (t
     (let ((instruction (fset:lookup program ip)))
       (find-loop program
                  (next-acc acc instruction)
                  (next-ip ip instruction)
                  (fset:with visited-positions ip))))))

(defun get-answer-1 (&optional (program *program*))
  (find-loop program 0 0 (fset:empty-set)))

(aoc:given 1
  (= 5 (get-answer-1 *example*)))


;;;; Part 2

(defun toggle-instruction (program ip)
  (destructuring-bind (op . arg) (fset:lookup program ip)
    (fset:with program ip (cons (ecase op
                                  (:jmp :nop)
                                  (:nop :jmp)
                                  (:acc :acc))
                                arg))))

(defun find-valid-program (program &optional (start 0))
  (let ((cur-op (car (fset:lookup program start))))
    (if (eq cur-op :acc)
        (find-valid-program program (1+ start))
        (let ((new-program (toggle-instruction program start)))
          (multiple-value-bind (acc loop) (get-answer-1 new-program)
            (if loop
                (find-valid-program program (1+ start))
                (values new-program acc)))))))

(defun get-answer-2 (&optional (program *program*))
  (nth-value 1 (find-valid-program program)))

(aoc:given 2
  (= 8 (get-answer-2 *example*)))


;;;; Part 2, backtracking solution

(defun run-and-fix-program (program &optional (acc 0) (ip 0) (changed nil) (visited (fset:empty-set)))
  (flet ((next-step (new-program new-changed)
           (let ((instruction (fset:lookup new-program ip)))
             (run-and-fix-program new-program
                                  (next-acc acc instruction)
                                  (next-ip ip instruction)
                                  new-changed
                                  (fset:with visited ip)))))
    (cond
      ((<= (fset:size program) ip)
       (values acc program changed))
      ((fset:member? ip visited)
       nil)
      (t
       (multiple-value-bind (final-acc final-program final-changed) (next-step program changed)
         (if (and (null final-acc)
                  (fset:member? (car (fset:lookup program ip))
                                (fset:set :jmp :nop))
                  (null changed))
             (next-step (toggle-instruction program ip) ip)
             (values final-acc final-program final-changed)))))))

(aoc:deftest backtracking
  (5am:is (= 8 (run-and-fix-program *example*)))
  (5am:is (= 2001 (run-and-fix-program *program*))))


;;;; Visualization

(defparameter *colors*
  (fset:map (:nop :violet)
            (:jmp :blue)
            (:acc :magenta)))
(defparameter *inactive-alpha* 0.2)
(defparameter *program-line-width* 0.1)

(defun draw-base-program (program)
  (fset:do-seq (instruction program :index ip)
    (destructuring-bind (op . arg) instruction
      (declare (ignore arg))
      (viz:set-color (fset:lookup *colors* op) :alpha *inactive-alpha*)
      (cairo:arc ip 0 0.4 0 (* 2 pi))
      (cairo:fill-path))))

(define-condition program-loop (condition)
  ((ip :initarg :ip
       :accessor loop-ip)))

(defun draw-running-program (program draw-fn &optional (acc 0) (ip 0) (last-ip 0) (visited (fset:empty-set)))
  (funcall draw-fn program acc ip last-ip visited)
  (cond
    ((fset:member? ip visited)
     (error 'program-loop :ip ip))
    ((< ip 0)
     (error "IP less than zero: ~A" ip))
    ((<= (fset:size program) ip)
     acc)
    (t
     (let ((instruction (fset:lookup program ip)))
       (draw-running-program program
                             draw-fn
                             (next-acc acc instruction)
                             (next-ip ip instruction)
                             ip
                             (fset:with visited ip))))))

(defun jump-arc (start end)
  (cairo:curve-to start 0
                  (/ (+ start end) 2) (/ (- start end) 2)
                  end 0))

(defun draw-program-line (program)
  (flet ((draw-segment (program acc ip last-ip visited)
           (declare (ignore acc visited))
           (let ((last-op (car (fset:lookup program last-ip))))
             (if (eq :jmp last-op)
                 (jump-arc last-ip ip)
                 (cairo:line-to ip 0)))))
    (viz:set-color :dark-primary)
    (cairo:set-line-cap :round)
    (cairo:set-line-width *program-line-width*)
    (cairo:move-to 0 0)
    (handler-case (draw-running-program program #'draw-segment)
      (program-loop (c)
        (declare (ignore c))))
    (cairo:stroke)))

(defun draw-program-instructions (program)
  (flet ((draw-instruction (program acc ip last-ip visited)
           (declare (ignore acc last-ip visited))
           (let ((op (car (fset:lookup program ip))))
             (when op
               (viz:set-color (fset:lookup *colors* op))
               (cairo:arc ip 0 0.4 0 (* 2 pi))
               (cairo:fill-path)))))
    (handler-case (draw-running-program program #'draw-instruction)
      (program-loop (c)
        (viz:set-color :red)
        (cairo:set-line-width 0.4)
        (cairo:set-line-cap :butt)
        (let ((ip (loop-ip c)))
          (cairo:move-to ip -2)
          (cairo:line-to ip -0.5)
          (cairo:move-to ip 0.5)
          (cairo:line-to ip 2)
          (cairo:stroke)))
      (:no-error (acc)
        (declare (ignore acc))
        (viz:set-color :green)
        (cairo:save)
        (cairo:translate (fset:size program) 0)
        (cairo:rotate (/ pi 4))
        (cairo:rectangle -0.3 -0.3 0.6 0.6)
        (cairo:fill-path)
        (cairo:restore)))))

(defun max-jump (program)
  (fset:reduce (lambda (max instr)
                 (destructuring-bind (op . arg) instr
                   (if (and (or (eq :jmp op)
                                (eq :nop op))
                            (< max (abs arg)))
                       (abs arg)
                       max)))
               program
               :initial-value 0))

(defun draw-program-arcs (filename &key (program *program*) (cell-size 4) (margin 32))
  (let ((width (+ (* 2 margin) (* cell-size (1+ (fset:size program)))))
        (height (+ (* 2 margin) (* cell-size (+ 2 (/ (max-jump program) 2))))))
    (cairo:with-png-file (filename :rgb24 width height :surface surface)
      (viz:clear-surface surface :dark-background)
      (cairo:translate (+ margin (/ cell-size 2))
                       (/ height 2))
      (cairo:scale cell-size cell-size)
      (draw-base-program program)
      (draw-program-line program)
      (draw-program-instructions program))))
