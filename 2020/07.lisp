(in-package :aoc-2020-07)

(aoc:define-day 103 1469)


;;;; Parsing

(parseq:defrule ruleset ()
  (+ contains)
  (:lambda (&rest rules)
    (reduce #'aoc:distinct-map-union rules)))

(parseq:defrule contains ()
  (and bag " contain " (or empty (aoc:comma-list bag-quantity)) "." (? #\Newline))
  (:choose 0 2)
  (:lambda (source target)
    (fset:map (source target))))

(parseq:defrule empty ()
  "no other bags"
  (:constant nil))

(parseq:defrule bag-quantity ()
  (and aoc:integer-string " " bag)
  (:choose 0 2)
  (:lambda (qty bag) (cons qty bag)))

(parseq:defrule bag ()
  (and (+ (char "a-z")) " " (+ (char "a-z")) " bag" (? "s"))
  (:choose 0 2)
  (:lambda (adjective color)
    (format nil "~{~A~} ~{~A~}" adjective color)))


;;;; Input

(defparameter *example*
  (parseq:parseq 'ruleset
                 "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."))

(defparameter *ruleset* (aoc:input :parse 'ruleset))


;;;; Part 1

(defun invert-rule (source target)
  (reduce (lambda (result bag)
            (fset:with result bag (fset:set source)))
          (mapcar #'cdr target)
          :initial-value (fset:empty-map)))

(defun invert-ruleset (ruleset)
  (fset:reduce (lambda (result source target)
                 (fset:map-union result
                                 (invert-rule source target)
                                 (lambda (s1 s2)
                                   (or (and s1 s2
                                            (fset:union s1 s2))
                                       s1
                                       s2))))
               ruleset
               :initial-value (fset:empty-map (fset:empty-set))))

(defun count-nodes-from (map start)
  (labels ((count-r (seen-bags current-bag)
             (let ((new-bags (fset:set-difference (fset:lookup map current-bag)
                                                  seen-bags)))
               (if (fset:empty? new-bags)
                   seen-bags
                   (fset:reduce #'count-r new-bags
                                :initial-value (fset:union seen-bags new-bags))))))
    (fset:size (count-r (fset:empty-set) start))))

(defun get-answer-1 (&optional (ruleset *ruleset*))
  (count-nodes-from (invert-ruleset ruleset) "shiny gold"))

(aoc:given 1
  (= 4 (get-answer-1 *example*)))


;;;; Part 2

(aoc:defmemo count-contained-bags (ruleset bag)
  (reduce (lambda (result contents)
            (destructuring-bind (quantity . next-bag) contents
              (+ result
                 (* quantity (1+ (count-contained-bags ruleset next-bag))))))
          (fset:lookup ruleset bag)
          :initial-value 0))

(defun get-answer-2 (&optional (ruleset *ruleset*))
  (count-contained-bags ruleset "shiny gold"))


;;;; Visualization

(defun node-name (bag)
  (substitute #\_ #\Space bag))

(defun node-label (bag)
  (cl-ppcre:regex-replace-all " " bag "\\n"))

(defun node-color (bag)
  (cl-ppcre:regex-replace "^[^ ]+ " bag ""))

(defun print-nodes (stream nodes)
  (fset:do-set (bag nodes)
    (format stream "  ~A [label=\"~A\",fillcolor=\"~A\"]~%"
            (node-name bag)
            (node-label bag)
            (node-color bag))))

(defun print-link (stream source target)
  (destructuring-bind (quantity . target-bag) target
    (format stream "  ~A -> ~A [label=\"~A\"]~%"
            (node-name source)
            (node-name target-bag)
            quantity)))

(defun print-node-links (stream source targets)
  (dolist (target targets)
    (print-link stream source target)))

(defun print-links (stream ruleset)
  (fset:do-map (source targets ruleset)
    (print-node-links stream source targets)))

(defun write-digraph (ruleset filename)
  (with-open-file (output filename :direction :output :if-exists :supersede :if-does-not-exist :create)
    (format output "digraph bags {~%")
    (format output "  rankdir=LR~%")
    (format output "  node [style=filled]~%")
    (print-nodes output (fset:domain ruleset))
    (print-links output ruleset)
    (format output "}~%")))
