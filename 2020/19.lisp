(in-package :aoc-2020-19)

(aoc:define-day 111 343)


;;;; Parsing

(parseq:defrule rules-and-messages ()
  (and ruleset #\Newline message-list)
  (:choose 0 2))

(parseq:defrule ruleset ()
  (+ (and rule (? #\Newline)))
  (:lambda (&rest rules) (mapcar #'first rules))
  (:lambda (&rest rules) (reduce #'fset:map-union rules)))

(parseq:defrule rule ()
  (and aoc:integer-string ": " rule-definition)
  (:choose 0 2)
  (:lambda (number subrule) (fset:map (number subrule))))

(parseq:defrule rule-definition ()
  (or string-literal compound-rule-list))

(parseq:defrule compound-rule-list ()
  (and rule-list (* (and " | " rule-list)))
  (:lambda (first rest) (cons first (mapcar #'second rest))))

(parseq:defrule rule-list ()
  (and aoc:integer-string (* (and " " aoc:integer-string)))
  (:lambda (first rest) (cons first (mapcar #'second rest))))

(parseq:defrule string-literal ()
  (and #\" (char "a-z") #\")
  (:choose 1))

(parseq:defrule message-list ()
  (+ (and message (? #\Newline)))
  (:lambda (&rest messages) (mapcar #'first messages)))

(parseq:defrule message ()
  (+ (char "a-z"))
  (:string))


;;;; Input

(defparameter *rules1*
  (parseq:parseq 'ruleset
                 "0: 1 2
1: \"a\"
2: 1 3 | 3 1
3: \"b\""))

(defparameter *example-str*
  "0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: \"a\"
5: \"b\"

ababbb
bababa
abbbab
aaabbb
aaaabbb")
(destructuring-bind (rules messages) (parseq:parseq 'rules-and-messages *example-str*)
  (defparameter *example-rules* rules)
  (defparameter *example-messages* messages))

(destructuring-bind (rules messages) (aoc:input :parse 'rules-and-messages)
  (defparameter *rules* rules)
  (defparameter *messages* messages))


;;;; Part 1

;;; Regex-based approach

(defparameter *advanced-matching-p* nil)

(defun rule-to-regex (ruleset rule-number)
  (cond
    ((and *advanced-matching-p* (= rule-number 8))
     (format nil "(?:~A)+" (rule-to-regex ruleset 42)))
    ;; WARNING: This does not work.  cl-ppcre does not support recursive
    ;; patterns.
    ((and *advanced-matching-p* (= rule-number 11))
     (format nil "(~A(?-1)?~A)" (rule-to-regex ruleset 42) (rule-to-regex ruleset 31)))
    (t
     (multiple-value-bind (rule foundp) (fset:lookup ruleset rule-number)
       (assert foundp)
       (etypecase rule
         (character rule)
         (cons (parent-rule-to-regex ruleset rule)))))))

(defun parent-rule-to-regex (ruleset rule)
  (format nil "(?:~{~A~^|~})"
          (mapcar (alexandria:curry #'rule-numbers-to-regex ruleset) rule)))

(defun rule-numbers-to-regex (ruleset rule-numbers)
  (format nil "~{~A~}"
          (mapcar (alexandria:curry #'rule-to-regex ruleset) rule-numbers)))

(defun anchor-regex (regex)
  (format nil "^~A$" regex))

(defun count-matching-messages-regex (ruleset messages &optional (rule-number 0))
  (let ((regex (anchor-regex (rule-to-regex ruleset rule-number))))
    (count-if (lambda (message) (cl-ppcre:scan regex message)) messages)))

;;; Direct recursive approach

(defun rule-match (ruleset rule-number string positions)
  (if (fset:empty? positions)
      positions
      (multiple-value-bind (rule foundp) (fset:lookup ruleset rule-number)
        (assert foundp)
        (if (typep rule 'character)
            (fset:less (fset:image (lambda (position)
                                     (and (< position (length string))
                                          (char= rule (schar string position))
                                          (1+ position)))
                                   positions)
                       nil)
            (alternates-match ruleset rule string positions)))))

(defun alternates-match (ruleset alternates string positions)
  (reduce (lambda (results rule-numbers)
            (fset:union (sequence-match ruleset rule-numbers string positions)
                        results))
          alternates
          :initial-value (fset:empty-set)))

(defun sequence-match (ruleset rule-numbers string positions)
  (if (endp rule-numbers)
      positions
      (let ((new-positions (rule-match ruleset (car rule-numbers) string positions)))
        (sequence-match ruleset (cdr rule-numbers) string new-positions))))

(defun complete-rule-match (ruleset rule-number string)
  (let ((match-ends (rule-match ruleset rule-number string (fset:set 0))))
    (fset:member? (length string) match-ends)))

(defun count-matching-messages (ruleset messages &optional (rule-number 0))
  (count-if (lambda (message) (complete-rule-match ruleset rule-number message)) messages))

(defun get-answer-1 (&optional (rules *rules*) (messages *messages*))
  (count-matching-messages rules messages))

(aoc:given 1
  (= 2 (get-answer-1 *example-rules* *example-messages*)))


;;;; Part 2

(defparameter *example2-str*
  "42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: \"a\"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: \"b\"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba")
(destructuring-bind (rules messages) (parseq:parseq 'rules-and-messages *example2-str*)
  (defparameter *example2-rules* rules)
  (defparameter *example2-messages* messages))

(defun replace-rules (ruleset)
  (fset:with (fset:with ruleset 8 '((42) (42 8)))
             11 '((42 31) (42 11 31))))

(defun get-answer-2 (&optional (rules *rules*) (messages *messages*))
  (count-matching-messages (replace-rules rules) messages))

(aoc:given 2
  (= 3 (get-answer-1 *example2-rules* *example2-messages*))
  (= 12 (get-answer-2 *example2-rules* *example2-messages*)))


;;; Visualization

(defun print-node-definition (stream ruleset rule-number)
  (format stream "    ~A [label=<<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\">~%" rule-number)
  (print-node-alternates stream ruleset rule-number)
  (format stream "</TABLE>>]~%"))

(defun print-node-alternates (stream ruleset rule-number)
  (iter (with alternates = (fset:lookup ruleset rule-number))
        (for rule-numbers in alternates)
        (for i from 0)
        (if (zerop i)
            (format stream "<TR><TD BORDER=\"0\" ROWSPAN=\"~A\" PORT=\"~A\">~:*~A:</TD>"
                    (1- (* 2 (length alternates)))
                    rule-number)
            (format stream "<TR><TD BORDER=\"0\"></TD></TR>~%<TR>"))
        (iter (for subrule-number in rule-numbers)
              (for j from 0)
              (for subrule = (fset:lookup ruleset subrule-number))
              (format stream "<TD PORT=\"i~Aj~A\">~A</TD>"
                      i j (if (typep subrule 'cons)
                              subrule-number
                              subrule)))
        (format stream "</TR>~%")))

(defun print-node-links (stream ruleset rule-number)
  (iter (with alternates = (fset:lookup ruleset rule-number))
        (for rule-numbers in alternates)
        (for i from 0)
        (iter (for subrule-number in rule-numbers)
              (for j from 0)
              (for subrule = (fset:lookup ruleset subrule-number))
              (when (typep subrule 'cons)
                (format stream "    ~A:i~Aj~A -> ~A:~:*~A:w~%"
                        rule-number i j subrule-number)))))

(defun write-ruleset-dot (ruleset filename)
  (with-open-file (stream filename
                          :direction :output
                          :if-exists :supersede)
    (format stream "digraph y2020d19 {~%")
    (format stream "    rankdir=LR~%")
    (format stream "    ranksep=2~%")
    (format stream "    node [shape=none,margin=0]~%")
    (format stream "    root=0~%")
    (format stream "~%")
    (fset:do-map (rule-number rule ruleset)
      (when (consp rule)
        (print-node-definition stream ruleset rule-number)
        (print-node-links stream ruleset rule-number)
        (princ #\Newline stream)))
    (format stream "}~%")))
