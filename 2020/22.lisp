(in-package :aoc-2020-22)

(aoc:define-day 32199 33780)


;;;; Parsing

(parseq:defrule decks ()
  (+ (and deck (? #\Newline)))
  (:lambda (&rest decks)
    (reduce #'aoc:distinct-map-union (mapcar #'first decks))))

(parseq:defrule deck ()
  (and player #\Newline
       (+ (and aoc:integer-string (? #\Newline))))
  (:choose 0 2)
  (:lambda (player-number cards)
    (fset:map (player-number (fset:convert 'fset:seq (mapcar #'first cards))))))

(parseq:defrule player ()
  (and "Player " aoc:integer-string ":")
  (:choose 1))


;;;; Input

(defparameter *example* (parseq:parseq 'decks (aoc:read-example)))

(defparameter *decks* (aoc:input :parse 'decks))


;;;; Part 1

(defparameter *recursive* nil)

(defun draw-cards (decks &optional to-draw (drawn (fset:empty-map)))
  (cond
    ((null to-draw)
     (draw-cards decks (fset:domain decks) drawn))
    ((fset:empty? to-draw)
     (values drawn decks))
    (t
     (let ((player (fset:arb to-draw)))
       (draw-cards (fset:with decks player (fset:less-first (fset:lookup decks player)))
                   (fset:less to-draw player)
                   (fset:with drawn player (fset:first (fset:lookup decks player))))))))

(defun find-winner (drawn-cards)
  (let* ((cards (fset:sort (fset:convert 'fset:seq (fset:range drawn-cards))
                          #'>))
         (winner (fset:reduce (lambda (result player card)
                                (if (= card (fset:first cards))
                                    player
                                    result))
                              drawn-cards
                              :initial-value nil)))
    (assert (= (fset:size cards) (fset:size drawn-cards)) ()
            "There were duplicate card values.")
    (values winner cards)))

(defun award-cards (decks winner winners-cards)
  (fset:with decks winner
             (fset:concat (fset:lookup decks winner) winners-cards)))

(defun play-round (decks)
  (multiple-value-bind (drawn remaining-decks) (draw-cards decks)
    (multiple-value-bind (winner winners-cards)
        (if (and *recursive* (can-recurse-p drawn remaining-decks))
            (find-winner-recursive drawn remaining-decks)
            (find-winner drawn))
      (award-cards remaining-decks winner winners-cards))))

(defun remove-losers (decks)
  (fset:filter (lambda (player cards)
                 (declare (ignore player))
                 (plusp (fset:size cards)))
               decks))

(defun play-game (decks &optional (previous-rounds (fset:empty-set)))
  "Returns two values: the winning player number; and the cards in that player's deck."
  (cond
    ((and *recursive* (fset:member? decks previous-rounds))
     (fset:arb (fset:restrict decks (fset:set 1))))
    ((= 1 (fset:size decks))
     (fset:arb decks))
    (t
     (play-game (remove-losers (play-round decks)) (fset:with previous-rounds decks)))))

(defun score-cards (cards)
  (gmap:gmap :sum #'*
             (:seq cards)
             (:index-inc (fset:size cards) 1 :incr -1)))

(defun get-answer-1 (&optional (decks *decks*))
  (let ((cards (nth-value 1 (play-game decks))))
    (score-cards cards)))

(aoc:given 1
  (= 306 (get-answer-1 *example*)))


;;;; Part 2

(defun can-recurse-p (drawn remaining-decks)
  (gmap:gmap :and (lambda (player card)
                    (<= card (fset:size (fset:lookup remaining-decks player))))
             (:map drawn)))

(defun find-winner-recursive (drawn remaining-decks)
  (let ((winner (play-game (make-recursive-decks drawn remaining-decks))))
    ;; Everything else will work with multiple players (so long as no
    ;; cards are duplicated), but this code requires there only be two
    ;; players.  With more than two players, there's no fixed ordering for
    ;; the cards.
    (assert (= 2 (fset:size drawn)) () "Recursive play only works for two players.")
    (values winner
            (fset:with-first (fset:convert 'fset:seq (fset:range (fset:less drawn winner)))
                             (fset:lookup drawn winner)))))

(defun make-recursive-decks (drawn remaining-decks)
  (fset:image (lambda (player card)
                (values player
                        (fset:subseq (fset:lookup remaining-decks player)
                                     0 card)))
              drawn))

(defun get-answer-2 (&optional (decks *decks*))
  (let ((*recursive* t))
    (get-answer-1 decks)))

(aoc:given 2
  (= 291 (get-answer-2 *example*)))
