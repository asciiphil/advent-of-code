(in-package :aoc-2020-03)

(aoc:define-day 240 2832009600)


;;;; Input

(defparameter *example*
  '("..##......."
    "#...#...#.."
    ".#....#..#."
    "..#.#...#.#"
    ".#...##..#."
    "..#.##....."
    ".#.#.#....#"
    ".#........#"
    "#.##...#..."
    "#...##....#"
    ".#..#...#.#"))
(defparameter *input* (aoc:input))


;;;; Parsing

(defstruct (area (:print-object print-area))
  (width 0 :type (integer 0))
  (height 0 :type (integer 0))
  (trees (fset:empty-set) :type fset:set))

(defun print-area (area stream)
  (with-slots (width height trees) area
    (format stream "#<AREA~%")
    (aoc:print-set-braille trees :stream stream)
    (princ ">" stream)
    (values)))

(defun parse-area (string-list)
  (iter (with trees = (fset:empty-set))
        (for string in string-list)
        (for y from 0)
        (iter (for c in-string string with-index x)
              (ecase c
                (#\# (fset:adjoinf trees (point:make-point x y)))
                (#\. t)))
        (finally (return (make-area :width (length string)
                                    :height (1+ y)
                                    :trees trees)))))


;;;; Part 1

(defun list-trees (area dx dy)
  (with-slots (width height trees) area
    (iter (for y from 0 below height by dy)
          (for x = (mod (* (/ y dy) dx) width))
          (for point = (point:make-point x y))
          (when (fset:member? point trees)
            (collecting point)))))


(defun get-answer-1 (&optional (input *input*))
  (length (list-trees (parse-area input) 3 1)))

(aoc:given 1
  (= 7 (get-answer-1 *example*)))


;;;; Part 2

(defun get-answer-2 (&optional (input *input*))
  (let ((count-fn (lambda (dx dy)
                    (length (list-trees (parse-area input) dx dy)))))
    (reduce #'* (mapcar (lambda (slope) (apply count-fn slope))
                        '((1 1) (3 1) (5 1) (7 1) (1 2))))))

(aoc:given 2
  (= 336 (get-answer-2 *example*)))


;;;; Part 3
;;;; https://www.reddit.com/r/adventofcode/comments/k5zbg3/2020_day_3_part_3_missing_the_trees_for_the_forest/

(defun list-all-slope-ratios (max-distance)
  (iter outer
        (for x from 1 below max-distance)
        (iter (for y from 1 to (- max-distance x))
              (in outer (collecting (/ x y))))))

(defun list-all-slopes (max-distance)
  (fset:convert 'fset:set
                (mapcar (lambda (n) (point:make-point (numerator n) (denominator n)))
                        (list-all-slope-ratios max-distance))))

(defun collect-all-hit-trees (area max-distance)
  (fset:reduce (lambda (result slope)
                 (fset:bag-sum result
                               (fset:convert 'fset:bag (list-trees area (point:elt slope 0) (point:elt slope 1)))))
               (list-all-slopes max-distance)
               :initial-value (fset:empty-bag)))

(defun get-answer-3a (&optional (input *input*))
  (let ((area (parse-area input)))
    (fset:size (fset:set-difference (area-trees area)
                                    (fset:convert 'fset:set (collect-all-hit-trees area 8))))))

(defun get-answer-3b (&optional (input *input*))
  (let ((all-hit-trees (collect-all-hit-trees (parse-area input) 8))
        (max-hits 0)
        (max-trees (fset:set)))
    (fset:do-bag-pairs (tree hits all-hit-trees)
      (cond
        ((= max-hits hits)
         (fset:adjoinf max-trees tree))
        ((< max-hits hits)
         (setf max-hits hits)
         (setf max-trees (fset:set tree)))))
    (assert (= 1 (fset:size max-trees)))
    (values max-trees max-hits)))


;;;; Visualization

(defun set-trans-matrix-for-area (area surface)
  (with-slots (width height) area
    (cairo:set-trans-matrix
     (viz:trans-matrix-for-points (list (point:make-point 0 0)
                                        (point:make-point (1- width) (1- height)))
                                  (cairo:image-surface-get-width surface)
                                  (cairo:image-surface-get-height surface)))))

(defun draw-tree (hit)
  (if hit
      (viz:set-color :red)
      (viz:set-color :green))
  (cairo:arc 0
             0
             0.45
             0
             (* 2 pi))
  (cairo:fill-path))

(defun draw-slope (area slope)
  (with-slots (width height) area
    (viz:set-color :dark-primary)
    (cairo:set-line-width 0.05)
    (iter (for left-y
               first 0
               then right-y)
          (for right-y = (+ left-y
                            (* slope width)))
          (cairo:move-to 0 left-y)
          (while (< right-y height))
          (cairo:line-to width right-y)
          (cairo:stroke)
          (finally
           (let ((final-x (/ (- height left-y) slope)))
             (cairo:line-to final-x height)
             (cairo:stroke))))))

(defun draw-cells (area slope)
  (let ((hit-trees (fset:convert 'fset:set (list-trees area (denominator slope) (numerator slope)))))
    (with-slots (width height trees) area
                                        ;(viz:set-color :dark-highlight)
                                        ;(cairo:rectangle 0 0 width height)
                                        ;(cairo:fill-path)
      (fset:do-set (tree trees)
        (cairo:save)
        (cairo:translate (point:elt tree 0) (point:elt tree 1))
        (draw-tree (fset:member? tree hit-trees))
        (cairo:restore)))))

(defun draw-area (area slope filename &key (width 1080) (height 1920))
  (cairo:with-png-file (filename :rgb24 width height :surface surface)
    (viz:clear-surface surface :dark-background)
    (set-trans-matrix-for-area area surface)
    (cairo:translate 0.5 0.5)
    (draw-slope area slope)
    (draw-cells area slope))
  (values))
