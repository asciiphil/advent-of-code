(in-package :aoc-2020-25)

;; As usual, part two is automatic once all other days are complete.
(aoc:define-day 11576351 nil)


;;;; Input

(defparameter *example-keys* '(5764801 17807724))

(defparameter *keys* (mapcar #'parse-integer (aoc:input)))


;;;; Part 1

(defparameter *initial-subject* 7)
(defparameter *modulus* 20201227)

(defun transform (subject value)
  (mod (* subject value) *modulus*))

(defun find-key-loop (key &optional (value 1) (loop-count 0))
  (if (= value key)
      loop-count
      (find-key-loop key (transform *initial-subject* value) (1+ loop-count))))

(defun n-transforms (subject n &optional (value 1))
  (if (zerop n)
      value
      (n-transforms subject (1- n) (transform subject value))))

(defun get-answer-1 (&optional (keys *keys*))
  (n-transforms (first keys) (find-key-loop (second keys))))

(aoc:given 1
  (= 14897079 (get-answer-1 *example-keys*)))
