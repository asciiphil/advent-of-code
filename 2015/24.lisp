(in-package :aoc-2015-24)

(aoc:define-day 10439961859 72050269)


;;;; Input

(defparameter *example* '(1 2 3 4 5 7 8 9 10 11))
(defparameter *weights* (mapcar #'parse-integer (aoc:input)))


;;;; Part 1

(defun list-sets-for-weight (elements target-weight)
  (iter outer
        (for available-elements on elements)
        (for element = (first available-elements))
        (cond
          ((= target-weight element)
           (collecting (list element)))
          ((> target-weight element)
           (iter (for smaller-set in (list-sets-for-weight (rest available-elements)
                                                           (- target-weight element)))
                 (in outer
                     (collecting (cons element smaller-set))))))))

(defun list-partitioning-groups (elements &optional (partitions 3))
  (list-sets-for-weight elements (/ (reduce #'+ elements) partitions)))

(defun find-smallest-sets (set-list)
  (let ((min-size (apply #'min (mapcar #'length set-list))))
    (remove-if (lambda (s) (/= min-size (length s))) set-list)))

(defun calculate-quantum-entanglement (set)
  (reduce #'* set))

(defun find-smallest-qe-set (set-list)
  (iter (for set in set-list)
        (finding set minimizing (calculate-quantum-entanglement set))))

(defun get-answer-1 (&optional (weights *weights*))
  (calculate-quantum-entanglement
   (find-smallest-qe-set
    (find-smallest-sets
     (list-partitioning-groups weights)))))


;;;; Part 2

(defun get-answer-2 (&optional (weights *weights*))
  (calculate-quantum-entanglement
   (find-smallest-qe-set
    (find-smallest-sets
     (list-partitioning-groups weights 4)))))
