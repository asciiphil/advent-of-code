(in-package :aoc-2015-14)

(aoc:define-day 2640 1102)


;;;; Input

(defparameter +input-re+ "(\\w+) can fly (\\d+) km/s for (\\d+) seconds, but then must rest for (\\d+) seconds\.")

(defparameter *test-input*
  '("Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds."
    "Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds."))

(defstruct reindeer
  name
  speed
  fly-time
  rest-time)

(defun parse-reindeer (string)
  (let ((groups (nth-value 1 (ppcre:scan-to-strings +input-re+ string))))
    (assert groups)
    (make-reindeer :name (svref groups 0)
                   :speed (parse-integer (svref groups 1))
                   :fly-time (parse-integer (svref groups 2))
                   :rest-time (parse-integer (svref groups 3)))))

(defparameter *test-reindeer* (mapcar #'parse-reindeer *test-input*))
(defparameter *reindeer* (mapcar #'parse-reindeer (aoc:input)))


;;;; Part 1

(defun reindeer-travel-interval (reindeer)
  "Returns two values: the interval time and the distance traveled during
  the interval."
  (with-slots (speed fly-time rest-time) reindeer
    (values (+ fly-time rest-time)
            (* speed fly-time))))

(defun distance-traveled (reindeer time)
  (multiple-value-bind (interval-time interval-distance) (reindeer-travel-interval reindeer)
    (multiple-value-bind (intervals remaining-time) (truncate time interval-time)
      (with-slots (speed fly-time) reindeer
        (+ (* intervals interval-distance)
           (if (<= fly-time remaining-time)
               interval-distance
               (* speed remaining-time)))))))

(defun winner (reindeer time)
  (values-list
   (iter (for r in reindeer)
         (for distance = (distance-traveled r time))
         (finding (list distance r) maximizing distance))))

(aoc:given 1
  (= 1120 (winner *test-reindeer* 1000)))

(defun get-answer-1 ()
  (winner *reindeer* 2503))


;;;; Part 2

(defun point-winner (reindeer time)
  (let ((points (make-array (length reindeer) :element-type '(integer 0) :initial-element 0)))
    (iter (for seconds from 1 to time)
          (for distances = (mapcar (lambda (r) (distance-traveled r seconds)) reindeer))
          (for max-distance = (reduce #'max distances))
          (iter (for d in distances)
                (for i from 0)
                (when (= d max-distance)
                  (incf (svref points i)))))
    (let ((max-points (reduce #'max points)))
      (values max-points
              (elt reindeer (position max-points points))))))

(aoc:given 2
  (= 689 (point-winner *test-reindeer* 1000)))

(defun get-answer-2 ()
  (point-winner *reindeer* 2503))
