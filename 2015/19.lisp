(in-package :aoc-2015-19)

(aoc:define-day 509 195)


;;;; Input

(defparameter *test-input*
  '("H => HO"
    "H => OH"
    "O => HH"
    ""
    "HOH"))
(defparameter *test-input-2*
  '("H => HO"
    "H => OH"
    "O => HH"
    ""
    "H2O"))
(defparameter *test-input-3*
  '("e => H"
    "e => O"
    "H => HO"
    "H => OH"
    "O => HH"))

(defun tokenize-patterns (strings)
  (let ((replacements (make-hash-table :test #'eq)))
    (iter (for string in strings)
          (until (string= string ""))
          (for (pattern replacement) = (ppcre:split " => " string))
          (for pattern-token = (intern (string-upcase pattern)))
          (setf (gethash pattern-token replacements)
                (cons replacement (gethash pattern-token replacements nil))))
    replacements))

;;; ASSUMPTION: All elements begin with capital letters and contain only
;;; lower case letters.
(defun tokenize-string (string)
  (labels ((tokenize-r (position pending-position)
             (if (<= (length string) position)
                 (list (intern (string-upcase (subseq string pending-position (length string)))))
                 (if (lower-case-p (schar string position))
                     (tokenize-r (1+ position)  pending-position)
                     (cons (intern (string-upcase
                                    (subseq string pending-position position)))
                           (tokenize-r (1+ position) position))))))
    (tokenize-r 1 0)))

(defun tokenize-replacements! (replacements)
  (iter (for pattern in (alexandria:hash-table-keys replacements))
        (setf (gethash pattern replacements)
              (mapcar (lambda (r) (tokenize-string r))
                      (gethash pattern replacements)))))

(defun invert-hash (hash)
  (iter (with result = (make-hash-table :test #'equal))
        (for (source targets) in-hashtable hash)
        (iter (for target in targets)
              (setf (gethash target result) source))
        (finally (return result))))

(defstruct machine
  replacements
  consolidation
  input)

(defun parse-machine (strings)
  (let* ((replacements (tokenize-patterns strings))
         (input (tokenize-string (car (last strings)))))
    (tokenize-replacements! replacements)
    (let ((consolidation (invert-hash replacements)))
      (make-machine :replacements replacements :consolidation consolidation :input input))))

(defparameter *test-machine* (parse-machine *test-input*))
(defparameter *test-machine-2* (parse-machine *test-input-2*))
(defparameter *test-machine-3* (parse-machine *test-input-3*))
(defparameter *machine* (parse-machine (aoc:input)))


;;;; Part 1

(defun perform-all-replacements (machine string)
  (labels ((add-results (rhead middles tail results)
             (if (endp middles)
                 results
                 (add-results rhead (cdr middles) tail
                              (cons (append rhead (car middles) tail)
                                    results))))
           (replace-r (head tail results)
             (if (endp tail)
                 results
                 (let ((substitutions (gethash (car tail) (machine-replacements machine))))
                   (if substitutions
                       (replace-r (cons (car tail) head)
                                  (cdr tail)
                                  (add-results (reverse head)
                                               substitutions
                                               (cdr tail)
                                               results))
                       (replace-r (cons (car tail) head)
                                  (cdr tail)
                                  results))))))
    (remove-duplicates (replace-r nil string nil)
                       :test 'equal)))

(defun num-replacements (machine)
  (length (perform-all-replacements machine (machine-input machine))))

(aoc:given 1
  (= 4 (num-replacements *test-machine*))
  (= 3 (num-replacements *test-machine-2*)))

(defun get-answer-1 ()
  (num-replacements *machine*))


;;;; Part 2

;;; Let's start by turning the reaction around.  Rather than going from
;;; one atom to multiple atoms, we work backwards from the desired result.
;;; Going in this direction, from multiple groups of atoms to fewer, we
;;; see that there are just two patterns in the replacements.
;;;
;;; Most replacements are AB -> C; replacing two atoms with one.
;;;
;;; All of the rest are of the form ARn...Ar -> C, which replaces all of
;;; the atoms between the "Rn" and "Ar".  Moreover, the atoms between the
;;; "Rn" and "Ar" are either a single atom ("RnBAr") or a series of single
;;; atoms separated by "Y"s ("RnBYDYEAr").

(defun get-answer-2 ()
  (let ((input (machine-input *machine*)))
    (- (length input)           ; Total number of atoms
       (* 2 (count 'rn input))  ; Ignore the "Rn" and "Ar" brackets
       (* 2 (count 'y input))   ; Each "Y" consumes itself and the next atom
       1)))                     ; We have one atom at the end (start)
