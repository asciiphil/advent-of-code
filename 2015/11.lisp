(in-package :aoc-2015-11)

(aoc:define-day "hepxxyzz" "heqaabcc")


;;;; Input

(defun encode-password (password)
  (map 'simple-vector
       (lambda (c) (- (char-code c) (char-code #\a)))
       password))

(defun decode-password (password)
  (map 'string
       (lambda (c) (code-char (+ c (char-code #\a))))
       password))

(defparameter *password* (encode-password (aoc:input)))


;;;; Part 1

(defparameter +confusing-characters+ (encode-password "iol"))

(defun increment-char (char)
  (multiple-value-bind (quotient remainder) (truncate (1+ char) 26)
    (values remainder quotient)))

(defun increment-password-char (password position)
  (let ((new-password (copy-seq password)))
    (multiple-value-bind (new-char overflow) (increment-char (svref password position))
      (setf (svref new-password position) new-char)
      (if (zerop overflow)
          new-password
          (increment-password-char new-password (1- position))))))

(defun increment-password (password)
  (let* ((new-password (increment-password-char password (1- (length password))))
         (confusing-position (position-if (lambda (c) (position c +confusing-characters+))
                                          new-password)))
    (when confusing-position
      (setf (svref new-password confusing-position)
            (increment-char (svref new-password confusing-position)))
      (iter (for i from (1+ confusing-position) below (length new-password))
            (setf (svref new-password i) 0)))
    new-password))

(defun has-sequential-characters-p (password)
  (iter (with sequential-count = 0)
        (for c in-vector password)
        (for cp previous c)
        (if (and cp (= 1 (- c cp)))
            (incf sequential-count)
            (setf sequential-count 0))
        (maximizing sequential-count into max-sequence)
        (thereis (<= 2 max-sequence))))

(defun has-confusing-characters-p (password)
  (iter (for c in-vector password)
        (thereis (position c +confusing-characters+))))

(defun has-character-pairs-p (password)
  (labels ((search-r (position matches)
             (cond
               ((<= 2 matches)
                t)
               ((<= (length password) (1+ position))
                nil)
               ((= (svref password position) (svref password (1+ position)))
                (search-r (+ 2 position) (1+ matches)))
               (t
                (search-r (1+ position) matches)))))
    (search-r 0 0)))

(defun valid-password-p (password)
  (and (has-sequential-characters-p password)
       (has-character-pairs-p password)
       (not (has-confusing-characters-p password))))

(defun next-password (password)
  (let ((new-password (increment-password password)))
    (if (valid-password-p new-password)
        new-password
        (next-password new-password))))

(aoc:deftest given-1-requirements
  (5am:is-true (has-sequential-characters-p (encode-password "hijklmmn")))
  (5am:is-true (has-confusing-characters-p (encode-password "hijklmmn")))
  (5am:is-true (has-character-pairs-p (encode-password "abbceffg")))
  (5am:is-false (has-sequential-characters-p (encode-password "abbceffg")))
  (5am:is-false (has-character-pairs-p (encode-password "abbcegjk"))))

(aoc:deftest given-1-next-password
  (5am:is (string= "abcdffaa" (decode-password (next-password (encode-password "abcdefgh")))))
  (5am:is (string= "ghjaabcc" (decode-password (next-password (encode-password "ghijklmn"))))))

(defun get-answer-1 ()
  (decode-password (next-password *password*)))


;;;; Part 2

(defun get-answer-2 ()
  (decode-password (next-password (next-password *password*))))


;;;; Visualization

(defconstant +max-character+ 25)

(defconstant +image-border+ 16)
(defconstant +image-margin+ 8)
(defconstant +density-array-side+ 676)
(defconstant +density-array-cell-side+ (/ (truncate (sqrt (expt 26 8)))
                                          +density-array-side+))

(defun make-density-array ()
  (make-array (list +density-array-side+ +density-array-side+)
              :element-type (list 'integer 0 (expt +density-array-cell-side+ 2))
              :initial-element 0))

(defparameter +valid-characters+
  (iter (for c from 0 to (- (char-code #\z) (char-code #\a)))
        (when (not (position c +confusing-characters+))
          (collecting c))))

(defun visit-passwords (visitor length so-far pair1-end pair2-end has-sequence)
  (labels ((visit-next (c)
             (when (and (<= c +max-character+)
                        (not (position c +confusing-characters+)))
               (visit-passwords visitor
                                (1- length)
                                (cons c so-far)
                                (cond
                                  (pair1-end
                                   (1+ pair1-end))
                                  ((and (car so-far)
                                        (= c (car so-far)))
                                   0)
                                  (t
                                   nil))
                                (cond
                                  (pair2-end
                                   (1+ pair2-end))
                                  ((and pair1-end
                                        (plusp pair1-end)
                                        (= c (car so-far)))
                                   0)
                                  (t
                                   nil))
                                (or has-sequence
                                    (and (second so-far)
                                         (= 1 (- c (first so-far)))
                                         (= 2 (- c (second so-far))))))))
           (visit-any ()
             (mapc #'visit-next +valid-characters+)))
    (case length
      (0 (let ((password (coerce (reverse so-far) 'simple-vector)))
           (funcall visitor password)))
      (1 (cond
           ((not has-sequence)
            (visit-next (1+ (car so-far))))
           ((not pair2-end)
            (visit-next (car so-far)))
           (t
            (visit-any))))
      (2 (if has-sequence
             (visit-any)
             (visit-next (1+ (car so-far)))))
      (3 (cond
           ((not pair1-end)
            (visit-next (car so-far)))
           ((and (not has-sequence)
                 (not pair2-end))
            (when (plusp pair1-end)
              (visit-next (car so-far)))
            (visit-next (1+ (car so-far))))
           (t
            (visit-any))))
      (4 (if (and (not has-sequence)
                  (not pair1-end))
             (visit-next (car so-far))
             (visit-any)))
      (t (visit-any)))))

(defun all-passwords (length visitor)
  (visit-passwords visitor length nil nil nil nil))

(defun password-index (password)
  (let ((raw-index (iter (with result = 0)
                         (for n in-vector password)
                         (setf result (+ n (* result 26)))
                         (finally (return result)))))
    (multiple-value-bind (raw-y raw-x)
        (truncate raw-index (* +density-array-side+ +density-array-cell-side+))
      (values (truncate raw-x +density-array-cell-side+)
              (truncate raw-y +density-array-cell-side+)))))

(defun add-password (density-array password)
  (when (valid-password-p password)
    (multiple-value-bind (x y) (password-index password)
      (incf (aref density-array y x)))))

(defun fill-density-array (array)
  (all-passwords 8 (lambda (p) (add-password array p)))
  array)

(defun draw-password-density (filename)
  (let* ((density-array (fill-density-array (make-density-array)))
         (left-offset (+ +image-border+
                         (truncate (viz:text-bbox "ma.."))
                         +image-margin+))
         (image-width (+ left-offset
                         +density-array-side+
                         +image-border+))
         (top-offset (+ +image-border+
                        (truncate (nth-value 1 (viz:text-bbox "ta..")))
                        +image-margin+))
         (image-height (+ top-offset
                          +density-array-side+
                          +image-border+)))
    (destructuring-bind (min-density max-density)
        (iter outer
              (for i below (aops:dim density-array 0))
              (iter (for j below (aops:dim density-array 1))
                    (for d = (aref density-array i j))
                    (in outer
                        (maximizing d into max-d)
                        (minimizing d into min-d)))
              (finally (return-from outer (list min-d max-d))))
      (let ((color-func (viz:make-color-func '(:light-background :green) min-density max-density :logarithmic)))
        (cairo:with-png-file (filename
                              :rgb24
                              image-width
                              image-height)
          (viz:set-color :light-background)
          (cairo:paint)
          (cairo:translate left-offset top-offset)
          (iter (for i below (aops:dim density-array 0))
                (iter (for j below (aops:dim density-array 1))
                      (apply #'cairo:set-source-rgb (funcall color-func (aref density-array i j)))
                      (cairo:rectangle i j 1 1)
                      (cairo:fill-path)))
          (viz:set-color :light-primary)
          (iter (for label in-string "abcdefghijklmnopqrstuvwxyz")
                (for i from 0.5 by 26)
                (viz:draw-text (format nil "~Aa.." label)
                               (- +image-margin+) i
                               :horizontal :right
                               :vertical :middle)
                (viz:draw-text (format nil "..~Aa" label)
                               i (- +image-margin+)
                               :horizontal :center
                               :vertical :baseline)))))))
