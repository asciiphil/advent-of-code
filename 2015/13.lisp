(in-package :aoc-2015-13)

(aoc:define-day 733 725)


;;;; Input

(defparameter +input-re+ "(\\w+) would (gain|lose) (\\d+) happiness units by sitting next to (\\w+)\\.")

(defparameter *test-input*
  '("Alice would gain 54 happiness units by sitting next to Bob."
    "Alice would lose 79 happiness units by sitting next to Carol."
    "Alice would lose 2 happiness units by sitting next to David."
    "Bob would gain 83 happiness units by sitting next to Alice."
    "Bob would lose 7 happiness units by sitting next to Carol."
    "Bob would lose 63 happiness units by sitting next to David."
    "Carol would lose 62 happiness units by sitting next to Alice."
    "Carol would gain 60 happiness units by sitting next to Bob."
    "Carol would gain 55 happiness units by sitting next to David."
    "David would gain 46 happiness units by sitting next to Alice."
    "David would lose 7 happiness units by sitting next to Bob."
    "David would gain 41 happiness units by sitting next to Carol."))

(defun tokenize-input (input)
  (iter (for line in input)
        (for groups = (nth-value 1 (ppcre:scan-to-strings +input-re+ line)))
        (assert groups)
        (for source = (intern (string-upcase (svref groups 0))))
        (for adjustment = (* (parse-integer (svref groups 2))
                             (if (string= "lose" (svref groups 1))
                                 -1
                                 1)))
        (for target = (intern (string-upcase (svref groups 3))))
        (collecting (list source target adjustment))))

(defun build-matrix (tokenized-input)
  (let* ((nodes (sort (remove-duplicates (mapcar #'first tokenized-input))
                      #'string<))
         (matrix (make-array (list (length nodes) (length nodes))
                             :element-type 'integer
                             :initial-element 0)))
    (iter (for (source target adjustment) in tokenized-input)
          (setf (aref matrix (position source nodes) (position target nodes))
                adjustment))
    matrix))

(defun parse-input (input)
  (build-matrix (tokenize-input input)))

(defparameter *test-happiness-matrix* (parse-input *test-input*))
(defparameter *happiness-matrix* (parse-input (aoc:input)))


;;;; Part 1

(defun happiness-change (happiness-matrix arrangement)
  (iter (for source in-vector arrangement with-index i)
        (for left = (svref arrangement (mod (1- i) (length arrangement))))
        (for right = (svref arrangement (mod (1+ i) (length arrangement))))
        (summing (aref happiness-matrix source left))
        (summing (aref happiness-matrix source right))))

(defun find-maximum-happiness (happiness-matrix)
  (let* ((max-arrangement (aops:generate #'identity (aops:dim happiness-matrix 0) :position))
         (max-happiness (happiness-change happiness-matrix max-arrangement)))
    (aoc:visit-permutations
     (lambda (p)
       (let ((happiness (happiness-change happiness-matrix p)))
         (when (< max-happiness happiness)
           (setf max-arrangement p
                 max-happiness happiness))))
     max-arrangement
     :copy nil)
    (values max-happiness max-arrangement)))

(aoc:given 1
  (= 330 (find-maximum-happiness *test-happiness-matrix*)))

(defun get-answer-1 ()
  (find-maximum-happiness *happiness-matrix*))


;;;; Part 2

(defun get-answer-2 ()
  (let ((graph (build-matrix (cons '(you you 0) (tokenize-input (aoc:input))))))
    (find-maximum-happiness graph)))
