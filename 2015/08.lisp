(in-package :aoc-2015-08)

(aoc:define-day 1350 2085)


;;;; Input

(defparameter *input* (aoc:input))


;;;; Part 1

(defun extra-chars (string)
  (let ((extra-char-count 0))
    (iter (generate c in-string string)
          (for c0 = (next c))
          (cond
            ((eq c0 #\")
             (incf extra-char-count))
            ((eq c0 #\\)
             (if (eq #\x (next c))
                 (progn
                   (next c)
                   (next c)
                   (incf extra-char-count 3))
                 (incf extra-char-count)))))
    extra-char-count))

(aoc:given given-1
  (= 2 (extra-chars "\"\""))
  (= 2 (extra-chars "\"abc\""))
  (= 3 (extra-chars "\"aaa\\\"aaa\""))
  (= 5 (extra-chars "\"\\x27\"")))

(aoc:deftest slashes
  (5am:is (= 3 (extra-chars "\"a\\\\b\"")))
  (5am:is (= 3 (extra-chars "\"a\\\\xb\""))))

(defun get-answer-1 ()
  (iter (for string in *input*)
        (sum (extra-chars string) into result)
        (finally (return result))))


;;;; Part 2

(defun quote-string (string)
  (concatenate 'string
               (reverse
                (cons #\"
                      (reduce
                       (lambda (r e)
                         (if (or (eq e #\")
                                 (eq e #\\))
                             (cons e (cons #\\ r))
                             (cons e r)))
                       string
                       :initial-value '(#\"))))))

(defun extra-chars2 (string)
  (extra-chars (quote-string string)))

(aoc:given 2
  (= 4 (extra-chars2 "\"\""))
  (= 4 (extra-chars2 "\"abc\""))
  (= 6 (extra-chars2 "\"aaa\\\"aaa\""))
  (= 5 (extra-chars2 "\"\\x27\"")))

(defun get-answer-2 ()
  (iter (for string in *input*)
        (sum (extra-chars2 string))))
