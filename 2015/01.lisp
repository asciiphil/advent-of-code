(in-package :aoc-2015-01)

(aoc:define-day 74 1795)


;;;; Input

(defvar *input* (aoc:input))


;;;; Part 1

(defun count-parens (paren-str)
  (iter (for c in-string paren-str)
        (cond
          ((char= c #\()
           (sum 1 into floor))
          ((char= c #\))
           (sum -1 into floor)))
        (finally (return floor))))

(defun get-answer-1 ()
  (count-parens *input*))

(aoc:given 1
  (= 0 (count-parens "(())"))
  (= 0 (count-parens "()()"))
  (= 3 (count-parens "((("))
  (= 3 (count-parens "(()(()("))
  (= 3 (count-parens "))((((("))
  (= -1 (count-parens "())"))
  (= -1 (count-parens "))("))
  (= -3 (count-parens ")))"))
  (= -3 (count-parens ")())())")))


;;;; Part 2

(defun basement-position (paren-str)
  (iter (for c in-string paren-str with-index i)
        (cond
          ((char= c #\()
           (sum 1 into floor))
          ((char= c #\))
           (progn
             (sum -1 into floor)
             (if (< floor 0)
                 (return (1+ i))))))
        (finally (return nil))))

(defun get-answer-2 ()
  (basement-position *input*))

(aoc:given 2
  (= 1 (basement-position ")"))
  (= 5 (basement-position "()())")))
