(in-package :aoc-2015-07)

(aoc:define-day 3176 14710)


;;;; Input

(defparameter *input* (aoc:input))
(defparameter *test-input*
  '("123 -> x"
    "456 -> y"
    "x AND y -> d"
    "x OR y -> e"
    "x LSHIFT 2 -> f"
    "y RSHIFT 2 -> g"
    "NOT x -> h"
    "NOT y -> i"))


;;;; Part 1

(defun parse-program (instructions)
  (let ((variables (make-hash-table :test #'equal)))
    (iter (for instruction in instructions)
          (for (value var) = (ppcre:split " -> " instruction))
          (assert (not (nth-value 1 (gethash var variables)))
                  (var)
                  "Duplicate variable name: ~A" var)
          (setf (gethash var variables) value))
    variables))

(defun get-value-uncached (variable var-hash cache)
  (multiple-value-bind (raw-value found) (gethash variable var-hash)
    (if (not found)
        (parse-number:parse-number variable) ; Assume integer literal
        (let ((tokens (ppcre:split "\\s+" raw-value)))
          (cond
            ; Integer literal
            ((and (= 1 (length tokens))
                  (ppcre:scan "^\\d+$" raw-value))
             (parse-number:parse-number raw-value))
            ; Other variable
            ((= 1 (length tokens))
             (get-value (first tokens) var-hash cache))
            ((= 2 (length tokens))
             (unary-operator (first tokens)
                             (get-value (second tokens) var-hash cache)))
            ((= 3 (length tokens))
             (binary-operator (second tokens)
                              (get-value (first tokens) var-hash cache)
                              (get-value (third tokens) var-hash cache)))
            (t (assert nil
                       (tokens)
                       "Don't know what to do with these tokens: ~A" tokens)))))))

(defun get-value (variable var-hash &optional (cache nil))
  (let ((real-cache (or cache (make-hash-table :test #'equal))))
    (multiple-value-bind (cached-value cache-found) (gethash variable real-cache)
      (if cache-found
          cached-value
          (let ((uncached-value (get-value-uncached variable var-hash real-cache)))
            (setf (gethash variable real-cache) uncached-value)
            uncached-value)))))

(defun unary-operator (op v)
  (cond
    ((string= op "NOT")
     (logxor #xffff v))
    (t (assert nil
               (op)
               "Unknown unary operator: ~A" op))))

(defun binary-operator (op v1 v2)
  (cond
    ((string= op "AND") (logand v1 v2))
    ((string= op "OR")  (logior v1 v2))
    ((string= op "LSHIFT") (ash v1 v2))
    ((string= op "RSHIFT") (ash v1 (- v2)))
    (t (assert nil
               (op)
               "Unknown binary operator: ~A" op))))

(aoc:deftest given-1
  (let ((variable-hash (parse-program *test-input*)))
    (5am:is (=    72 (get-value "d" variable-hash)))
    (5am:is (=   507 (get-value "e" variable-hash)))
    (5am:is (=   492 (get-value "f" variable-hash)))
    (5am:is (=   114 (get-value "g" variable-hash)))
    (5am:is (= 65412 (get-value "h" variable-hash)))
    (5am:is (= 65079 (get-value "i" variable-hash)))
    (5am:is (=   123 (get-value "x" variable-hash)))
    (5am:is (=   456 (get-value "y" variable-hash)))))

(defun get-answer-1 ()
  (get-value "a" (parse-program *input*)))


;;;; Part 2

(defun get-answer-2 ()
  (let ((parsed (parse-program *input*)))
    (setf (gethash "b" parsed)
          (format nil "~A" (get-answer-1)))
    (get-value "a" parsed)))
