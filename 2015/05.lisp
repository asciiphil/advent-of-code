(in-package :aoc-2015-05)

(aoc:define-day 258 53)


;;;; Input

(defparameter *input* (aoc:input))


;;;; Part 1

(defvar *vowels* "aeiou")
(defvar *vowel-count* 3)
(defvar *excluded-strings* '("ab" "cd" "pq" "xy"))

(defun my-count (item sequence)
  (count item sequence))

(defun has-chars-p (string chars count)
  (iter (for c in-string chars)
        (sum (my-count c string) into chars-found)
        (if (<= count chars-found)
            (return t))
        (finally (return nil))))

(defun has-double-letter-p (string)
  (iter (for c in-string string)
        (for cp previous c)
        (thereis (and cp (char= c cp)))))

(defun has-substrings-p (string substrings)
  (iter (for substring in substrings)
        (thereis (search substring string))))

(defun nice-p (string)
  (and (has-chars-p string *vowels* *vowel-count*)
       (has-double-letter-p string)
       (not (has-substrings-p string *excluded-strings*))))

(aoc:deftest given-1
  (5am:is-true (nice-p "ugknbfddgicrmopn"))
  (5am:is-true (nice-p "aaa"))
  (5am:is-false (nice-p "jchzalrnumimnmhp"))
  (5am:is-false (nice-p "haegwjzuvuyypxyu"))
  (5am:is-false (nice-p "dvszwmarrgswjxmb")))

(defun get-answer-1 ()
  (count-if #'nice-p *input*))


;;;; Part 2

(defun has-letter-pair-p (string)
  (ppcre:scan "(..).*\\1" string))

(defun has-letter-repeat-p (string)
  (ppcre:scan "(.).\\1" string))

(defun nice2-p (string)
  (and (has-letter-pair-p string)
       (has-letter-repeat-p string)))

(aoc:deftest given-2
  (5am:is-true (nice2-p "qjhvhtzxzqqjkmpb"))
  (5am:is-true (nice2-p "xxyxx"))
  (5am:is-false (nice2-p "uurcxstgmygtbstg"))
  (5am:is-false (nice2-p "ieodomkazucvgmuy")))

(defun get-answer-2 ()
  (count-if #'nice2-p *input*))
