(in-package :aoc-2015-06)

(aoc:define-day 543903 14687245)


;;;; Input

(defparameter *input* (aoc:input))


;;;; Part 1

(defparameter *instruction-re* "(turn on|turn off|toggle) (\\d+),(\\d+) through (\\d+),(\\d+)")

(defun instruction-to-keyword (instruction)
  (cond
    ((string= instruction "turn on")  :turn-on)
    ((string= instruction "turn off") :turn-off)
    ((string= instruction "toggle")   :toggle)
    (t (assert nil (instruction) "Unrecognized instruction: ~A" instruction))))

(defun parse-instruction (instruction-str)
  (let ((groups (nth-value 1 (ppcre:scan-to-strings *instruction-re* instruction-str))))
    (values (instruction-to-keyword (svref groups 0))
            (complex (parse-integer (svref groups 1))
                     (parse-integer (svref groups 2)))
            (complex (parse-integer (svref groups 3))
                     (parse-integer (svref groups 4))))))

(defun apply-instruction! (matrix instruction start end)
  (iter (for x from (min (realpart start) (realpart end))
               to (max (realpart start) (realpart end)))
        (iter (for y from (min (imagpart start) (imagpart end))
                     to (max (imagpart start) (imagpart end)))
              (cond
                ((eq instruction :turn-on)  (setf (aref matrix y x) 1))
                ((eq instruction :turn-off) (setf (aref matrix y x) 0))
                ((eq instruction :toggle)
                 (setf (aref matrix y x)
                       (mod (1+ (aref matrix y x)) 2)))
                (t (assert nil (instruction) "Unrecognized instruction: ~A" instruction))))))

(defun count-on (matrix)
  (reduce #'+ (make-array (array-total-size matrix)
                          :displaced-to matrix
                          :element-type (array-element-type matrix))))

(5am:def-fixture blank-matrix ()
  (let ((matrix (make-array '(1000 1000) :element-type 'bit)))
    (&body)))

(aoc:deftest given-1
  (5am:with-fixture blank-matrix ()
    (apply #'apply-instruction!
           matrix (multiple-value-list (parse-instruction "turn on 0,0 through 999,999")))
    (5am:is (= (* 1000 1000) (count-on matrix))))
  (5am:with-fixture blank-matrix ()
    (apply #'apply-instruction!
           matrix (multiple-value-list (parse-instruction "turn on 0,0 through 999,0")))
    (5am:is (= 1000 (count-on matrix))))
  (5am:with-fixture blank-matrix ()
    (apply #'apply-instruction!
           matrix (multiple-value-list (parse-instruction "turn on 499,499 through 500,500")))
    (5am:is (= 4 (count-on matrix)))))

(defun get-answer-1 ()
  (let ((matrix (make-array '(1000 1000) :element-type 'bit)))
    (iter (for instruction-str in *input*)
          (for (values instruction start end) = (parse-instruction instruction-str))
          (apply-instruction! matrix instruction start end)
          (finally (return (count-on matrix))))))


;;;; Part 2

(defun apply-instruction2! (matrix instruction start end)
  (iter (for x from (min (realpart start) (realpart end))
               to (max (realpart start) (realpart end)))
        (iter (for y from (min (imagpart start) (imagpart end))
                     to (max (imagpart start) (imagpart end)))
              (cond
                ((eq instruction :turn-on)  (incf (aref matrix y x)))
                ((eq instruction :turn-off) (decf (aref matrix y x) (min 1 (aref matrix y x))))
                ((eq instruction :toggle)   (incf (aref matrix y x) 2))
                (t (assert nil (instruction) "Unrecognized instruction: ~A" instruction))))))

(defun get-answer-2 ()
  (let ((matrix (make-array '(1000 1000) :initial-element 0)))
    (iter (for instruction-str in *input*)
          (for (values instruction start end) = (parse-instruction instruction-str))
          (apply-instruction2! matrix instruction start end)
          (finally (return (count-on matrix))))))
