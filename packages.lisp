(defpackage :advent-of-code
  (:nicknames :aoc)
  (:use :common-lisp
        :iterate)
  (:export :define-day
           :given
           :deftest
           :test
           :input
           :submit
           :integer-string
           :comma-list
           :parse-grid-to-array
           :parse-grid-to-map
           :extract-ints
           :read-example
           :split-at
           :map-set-with
           :rotate-seq
           :invert-map
           :arb-element
           :iota
           :matrix-add
           :matrix-multiply
           :matrix-exp
           :defmemo
           :digit-list
           :digit-vector
           :join-digit-list
           :row-order<
           :vector-shared-subseq
           :flatten-array
           :make-keyword
           :sum-from-to
           :distinct-map-union
           :cref
           :manhattan-distance
           :bbox-left
           :bbox-bottom
           :bbox-top
           :bbox-right
           :find-bbox
           :print-2d-hash-table
           :print-braille
           :print-set-braille
           :print-map-braille
           :print-array-braille
           :show-svg
           :with-svg-surface
           :draw-svg-grid
           :draw-set-svg-grid
           :draw-map-svg-grid
           :draw-array-svg-grid
           :shortest-path
           :visit-subsets
           :visit-permutations
           :visit-permutations-lexicographic
           :visit-n-integer-partitions))

(defpackage :circular-list
  (:nicknames :clist)
  (:use :common-lisp
        :iterate)
  (:shadow :equal)
  (:export :make-circular-list
           :equal
           :focused
           :insert
           :rotate
           :remove-focused))

(defpackage :queue
  (:use :common-lisp)
  (:export :make-queue
           :size
           :head
           :tail
           :snoc
           :empty-p))

(defpackage :assembunny
  (:use :common-lisp
   :iterate)
  (:shadow :compile)
  (:export :compile
   :run))

(defpackage :elfcode
  (:use :common-lisp)
  (:shadow :compile
           :parse-error)
  (:export :compile
           :execute
           :breakpoint
           :breakpoint-ip
           :breakpoint-registers
           :breakpoint-instruction))

(defpackage :intcode
  (:use :common-lisp
        :iterate)
  (:shadow :compile
           :disassemble
           :parse-error)
  (:export :rpc-call
           :rpc-finish
           :load-program
           :dissassemble
           :run
           :run-for-rpc
           :with-rpc
           :make-output-fn))

(defpackage :point
  (:use :common-lisp
        :iterate)
  (:shadow :elt
           :=
           :+
           :-
           :*
           :/
           :<
           :<=
           :aref
           :mod)
  (:export :point
           :make-point
           :list-coordinates
           :elt
           :x
           :y
           :z
           :w
           :=
           :+
           :-
           :*
           :/
           :mod
           :<
           :<=
           :total<
           :norm
           :unit-vector
           :aref
           :neighbor-set
           :turn
           :manhattan-distance
           :manhattan-circle
           :bbox
           :map-coordinates
           :print-2d-map
           :print-2d-hash-table
           :print-3d-set
           :print-3d-hash-table))

(defpackage :visualization
  (:nicknames :viz)
  (:use :common-lisp
        :iterate)
  (:export :color
           :set-color
           :color-hex
           :make-color-func
           :make-category-colors
           :clear-surface
           :clone-surface
           :trans-matrix-for-points
           :text-bbox
           :draw-text
           :video-width
           :video-height
           :with-video
           :create-video
           :create-video-surface
           :write-video-surface
           :finish-video))

(defpackage :colorcet
  (:use :common-lisp)
  (:export :interpolate
           :+L08+
           :+L10+
           :+L13+
           :+L14+
           :+L15+
           :+L19+))

(eval
 `(progn
    ,@(loop for year in '(2015 2016 2017 2018 2019 2020 2021 2022 2023) ; YEAR-MARKER - do not edit this line.
            append (loop for number from 1 to 25
                         for package-name = (intern (format nil "ADVENT-OF-CODE-~D-~2,'0D" year number)
                                                    (find-package "KEYWORD"))
                         for nickname = (intern (format nil "AOC-~D-~2,'0D" year number)
                                                (find-package "KEYWORD"))
                         collect `(defpackage ,package-name
                                    (:nicknames ,nickname)
                                    (:use :common-lisp
                                          :iterate
                                          :cl-coroutine))))))

