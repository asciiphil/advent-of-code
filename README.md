These are my Advent of Code modules.

Cheat sheet for loading them:

    mkdir -p ~/common-lisp/advent-of-code
    ln -s `realpath advent-of-code.asd` ~/common-lisp/advent-of-code/

You need to create a `~/.aocrc` file with the following contents:

    session = "..."
    cachedir = "~/.cache/aoc/"
    userid = 12345

The "session" value should be the contents of the session cookie from a
browser logged in to adventofcode.com.  The "cachedir" value should be a
writeable directory on your system where the code can cache content
downloaded from adventofcode.com.

"userid" should be your Advent of Code account's ID number.  You can get
it by going to https://adventofcode.com/2021/leaderboard/private and
clicking the "View" link to your private leaderboard.

Then, assuming you have [quicklisp](https://www.quicklisp.org) installed,

    (ql:quickload "advent-of-code")

(Straight ASDF will work, too, with `(asdf:load-system "advent-of-code")`,
but you'll need to make sure all the dependencies are already in place.)
