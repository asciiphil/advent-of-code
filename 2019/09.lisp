(in-package :aoc-2019-09)

(aoc:define-day 3507134798 84513)


;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))



;;; Part 1

(defun get-answer-1 ()
  (first (intcode:run *input* :input-list '(1))))


;;; Part 2

(defun get-answer-2 ()
  (first (intcode:run *input* :input-list '(2))))
