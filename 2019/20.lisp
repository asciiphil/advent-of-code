(in-package :aoc-2019-20)

(aoc:define-day 616 7498)


;;;; Input

(defparameter *example-1-23*
  '("         A           "
    "         A           "
    "  #######.#########  "
    "  #######.........#  "
    "  #######.#######.#  "
    "  #######.#######.#  "
    "  #######.#######.#  "
    "  #####  B    ###.#  "
    "BC...##  C    ###.#  "
    "  ##.##       ###.#  "
    "  ##...DE  F  ###.#  "
    "  #####    G  ###.#  "
    "  #########.#####.#  "
    "DE..#######...###.#  "
    "  #.#########.###.#  "
    "FG..#########.....#  "
    "  ###########.#####  "
    "             Z       "
    "             Z       "))
(defparameter *example-2-396*
  '("             Z L X W       C                 "
    "             Z P Q B       K                 "
    "  ###########.#.#.#.#######.###############  "
    "  #...#.......#.#.......#.#.......#.#.#...#  "
    "  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  "
    "  #.#...#.#.#...#.#.#...#...#...#.#.......#  "
    "  #.###.#######.###.###.#.###.###.#.#######  "
    "  #...#.......#.#...#...#.............#...#  "
    "  #.#########.#######.#.#######.#######.###  "
    "  #...#.#    F       R I       Z    #.#.#.#  "
    "  #.###.#    D       E C       H    #.#.#.#  "
    "  #.#...#                           #...#.#  "
    "  #.###.#                           #.###.#  "
    "  #.#....OA                       WB..#.#..ZH"
    "  #.###.#                           #.#.#.#  "
    "CJ......#                           #.....#  "
    "  #######                           #######  "
    "  #.#....CK                         #......IC"
    "  #.###.#                           #.###.#  "
    "  #.....#                           #...#.#  "
    "  ###.###                           #.#.#.#  "
    "XF....#.#                         RF..#.#.#  "
    "  #####.#                           #######  "
    "  #......CJ                       NM..#...#  "
    "  ###.#.#                           #.###.#  "
    "RE....#.#                           #......RF"
    "  ###.###        X   X       L      #.#.#.#  "
    "  #.....#        F   Q       P      #.#.#.#  "
    "  ###.###########.###.#######.#########.###  "
    "  #.....#...#.....#.......#...#.....#.#...#  "
    "  #####.#.###.#######.#######.###.###.#.#.#  "
    "  #.......#.......#.#.#.#.#...#...#...#.#.#  "
    "  #####.###.#####.#.#.#.#.###.###.#.###.###  "
    "  #.......#.....#.#...#...............#...#  "
    "  #############.#.#.###.###################  "
    "               A O F   N                     "
    "               A A D   M                     "))
(defparameter *input* (aoc:input))


;;;; Part 1

(defparameter +cardinal-vectors+ '(#C(0 1) #C(0 -1) #C(1 0) #C(-1 0)))

(defun portal-symbol (first-line first-char second-line second-char)
  (intern (coerce (list (schar first-line first-char)
                        (schar second-line second-char))
                  'string)))

(defun parse-portal (col last-line this-line next-line)
  (cond
    ((and last-line
          next-line
          (char= #\. (schar last-line col)))
     (portal-symbol this-line col next-line col))
    ((and last-line
          next-line
          (char= #\. (schar next-line col)))
     (portal-symbol last-line col this-line col))
    ((and (< 0 col)
          (< col (1- (length this-line)))
          (char= #\. (schar this-line (1- col))))
     (portal-symbol this-line col this-line (1+ col)))
    ((and (< 0 col)
          (< col (1- (length this-line)))
          (char= #\. (schar this-line (1+ col))))
     (portal-symbol this-line (1- col) this-line col))
    (t nil)))

(defun parse-maze (input)
  (iter (with maze = (make-hash-table))
        (for line in input)
        (for row from 0)
        (iter (for char in-string line)
              (for col from 0)
              (for position = (complex col row))
              (case char
                (#\#
                 (setf (gethash position maze) :wall))
                (#\.
                 (setf (gethash position maze) :open))
                (#\ )                   ; Do nothing
                (t
                 (let ((portal-name (parse-portal col
                                                  (and (< 0 row)
                                                       (nth (1- row) input))
                                                  line
                                                  (nth (1+ row) input))))
                   (when portal-name
                     (setf (gethash position maze) portal-name))))))
        (finally (return maze))))

(defparameter +portal-symbols+
  '(#\* #\+ #\! #\@ #\$ #\% #\^ #\& #\?))

(defun make-portal-char-fn ()
  (let ((symbols (copy-list +portal-symbols+))
        (portal-chars (make-hash-table)))
    (setf (gethash 'AA portal-chars) #\U240F)
    (setf (gethash 'ZZ portal-chars) #\U240E)
    (setf (gethash 'AA portal-chars) #\U240F)
    (setf (gethash 'ZZ portal-chars) #\U240E)
    (lambda (tile)
      (case tile
        (:wall #\#)
        (:open #\.)
        (t (or (gethash tile portal-chars nil)
               (setf (gethash tile portal-chars) (pop symbols))))))))

(defun print-unconnected-maze (maze)
  (aoc:print-2d-hash-table maze (make-portal-char-fn)))

(defun adjacent-open-position (maze position)
  (iter (for vector in +cardinal-vectors+)
        (for adjacent-position = (+ position vector))
        (finding adjacent-position such-that (eq :open (gethash adjacent-position maze)))))

(defun connect-portal-pair (maze portal-1 portal-2)
  (destructuring-bind ((portal-pos-1 . open-pos-1) (portal-pos-2 . open-pos-2))
      (list portal-1 portal-2)
    (let ((bbox (aoc:find-bbox (alexandria:hash-table-keys maze))))
      (if (not (or (= (aoc:bbox-bottom bbox) (imagpart portal-pos-1))
                   (= (aoc:bbox-top bbox) (imagpart portal-pos-1))
                   (= (aoc:bbox-left bbox) (realpart portal-pos-1))
                   (= (aoc:bbox-right bbox) (realpart portal-pos-1))))
          (connect-portal-pair maze portal-2 portal-1)
          (progn
            (setf (gethash portal-pos-1 maze) (cons open-pos-2 -1))
            (setf (gethash portal-pos-2 maze) (cons open-pos-1 1)))))))

(defun connect-portals (maze)
  (let ((portals (make-hash-table)))
    (iter (for (position element) in-hashtable maze)
          (when (not (or (eq element :wall)
                         (eq element :open)))
            (push (cons position (adjacent-open-position maze position))
                  (gethash element portals))))
    (iter (for (portal-symbol ((portal-pos-1 . open-pos-1) (portal-pos-2 . open-pos-2)))
               in-hashtable portals)
          (cond
            (portal-pos-2
             (connect-portal-pair maze
                                  (cons portal-pos-1 open-pos-1)
                                  (cons portal-pos-2 open-pos-2)))
            ((eq portal-symbol 'aa)
             (setf (gethash open-pos-1 maze) :start))
            ((eq portal-symbol 'zz)
             (setf (gethash open-pos-1 maze) :end)))))
  maze)

(defun tile-position (connected-maze tile-value)
  (iter (for (position element) in-hashtable connected-maze)
        (finding position such-that (eq element tile-value))))

(defun make-maze-options (connected-maze &key with-levels)
  (lambda (state)
    (destructuring-bind (position . level) state
      (iter (for vector in +cardinal-vectors+)
            (for new-position = (+ vector position))
            (for target = (gethash new-position connected-maze))
            (cond
              ((or (eq target :open)
                   (eq target :start)
                   (eq target :end))
               (collecting (list 1 (cons new-position level))))
              ((and (consp target)
                    (or (not with-levels)
                        (not (minusp (+ level (cdr target))))))
               (collecting (list 1 (cons (car target) (+ level (cdr target)))))))))))

(defun traverse-maze (connected-maze &key with-levels)
  (aoc:shortest-path (cons (tile-position connected-maze :start) 0)
                     (make-maze-options connected-maze :with-levels with-levels)
                     :finishedp (let ((end-tile (tile-position connected-maze :end)))
                                  (if with-levels
                                      (lambda (state) (equal state (cons end-tile 0)))
                                      (lambda (state) (equal (car state) end-tile))))))

(defun get-answer-1 ()
  (nth-value 1 (traverse-maze (connect-portals (parse-maze *input*)))))


;;;; Part 2

(defun get-answer-2 ()
  (nth-value 1 (traverse-maze (connect-portals (parse-maze *input*))
                              :with-levels t)))

;;;; Visualization

(defparameter *frame-width* 1280)
(defparameter *frame-height* 720)

(defun trans-matrix-for-maze (maze)
  (viz:trans-matrix-for-points (mapcar (lambda (c)
                                         (point:make-point (realpart c) (imagpart c)))
                                       (alexandria:hash-table-keys maze))
                               *frame-width*
                               *frame-height*))

(defun draw-square-tile (color)
  (viz:set-color color)
  (cairo:rectangle -1/2 -1/2 1 1)
  (cairo:fill-path))

(defun draw-portal (color)
  (apply #'cairo:set-source-rgb color)
  (cairo:rectangle 1/2 -1/2 1/2 1)
  (cairo:fill-path))

(defun draw-entrance ()
  (cairo:save)
  (cairo:rotate pi)
  (draw-exit)
  (cairo:restore))

(defun draw-exit ()
  (viz:set-color :dark-emphasized)
  (cairo:set-line-width 1/8)
  (cairo:move-to 1/2 0)
  (cairo:line-to (+ -1/2 1/16) 0)
  (cairo:stroke)
  (cairo:move-to 0 3/8)
  (cairo:line-to (+ -1/2 1/16) 0)
  (cairo:line-to 0 -3/8)
  (cairo:stroke))

(defun draw-tile (maze position element color-map &key portals)
  (let ((access-point (adjacent-open-position maze position)))
    (cairo:save)
    (cairo:translate (+ 1/2 (realpart position)) (+ 1/2 (imagpart position)))
    (when access-point
      (cairo:rotate (phase (- access-point position))))
    (case element
      (:wall (and (not portals) (draw-square-tile :dark-primary)))
      (:open (and (not portals) (draw-square-tile :dark-highlight)))
      (aa (and portals (draw-entrance)))
      (zz (and portals (draw-exit)))
      (t (and portals (draw-portal (gethash element color-map)))))
    (cairo:restore)))

(defun list-maze-portals (maze)
  (remove-duplicates
   (iter (for (_ element) in-hashtable maze)
         (when (not (member element '(:wall :open :start :end)))
           (collecting element)))))

(defun make-portal-color-map (maze)
  (let* ((portals (set-difference (list-maze-portals maze) '(aa zz)))
         (colors (viz:make-category-colors (length portals)))
         (color-map (make-hash-table)))
    (iter (while portals)
          (setf (gethash (pop portals) color-map) (pop colors)))
    color-map))

(defun draw-maze (surface maze)
  (viz:clear-surface surface :dark-background)
  (cairo:with-context-from-surface (surface)
    (cairo:set-trans-matrix (trans-matrix-for-maze maze))
    (iter (with color-map = (make-portal-color-map maze))
          (for (position element) in-hashtable maze)
          (draw-tile maze position element color-map :portals nil))
    (iter (with color-map = (make-portal-color-map maze))
          (for (position element) in-hashtable maze)
          (draw-tile maze position element color-map :portals t))))

(defun write-maze (input filename)
  (let ((maze (parse-maze input)))
    (cairo:with-png-file (filename
                          :rgb24
                          *frame-width*
                          *frame-height*
                          :surface surface)
      (draw-maze surface maze))))
