(in-package :aoc-2019-03)

(aoc:define-day 768 8684)


;;; Input

(defparameter *example-1* '("R8,U5,L5,D3" "U7,R6,D4,L4"))
(defparameter *example-2* '("R75,D30,R83,U83,L12,D49,R71,U7,L72"
                            "U62,R66,U55,R34,D71,R55,D58,R83"))
(defparameter *example-3* '("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"
                            "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"))

(defparameter *input* (aoc:input))


;;; Part 1

(defun path-to-coords (path)
  (iter (with coord = (complex 0 0))
        (with distance = 0)
        (with result = (make-hash-table))
        (for segment in (ppcre:split "," path))
        (for vector = (ecase (schar segment 0)
                        (#\R (complex  1  0))
                        (#\L (complex -1  0))
                        (#\U (complex  0  1))
                        (#\D (complex  0 -1))))
        (iter (repeat (aoc:extract-ints segment))
              (setf coord (+ coord vector))
              (incf distance)
              (setf (gethash coord result)
                    (gethash coord result distance)))
        (finally (return result))))

(defun path-intersections (paths)
  (flet ((intersect-pair (path1 path2)
           (iter (with result = (make-hash-table))
                 (for (coord distance) in-hashtable path1)
                 (when (plusp (gethash coord path2 0))
                   (setf (gethash coord result)
                         (+ distance (gethash coord path2))))
                 (finally (return result)))))
    (reduce #'intersect-pair (mapcar #'path-to-coords paths))))

(defun find-minimum-distance (paths)
  (values-list
   (iter (for (intersection _) in-hashtable (path-intersections paths))
         (for distance = (aoc:manhattan-distance intersection))
         (finding (list distance intersection) minimizing distance))))

(aoc:given 1
  (= 6 (find-minimum-distance *example-1*))
  (= 159 (find-minimum-distance *example-2*))
  (= 135 (find-minimum-distance *example-3*)))

(defun get-answer-1 ()
  (find-minimum-distance *input*))


;;; Part 2

(defun find-minimum-latency (paths)
  (values-list
   (iter (for (intersection distance) in-hashtable (path-intersections paths))
         (finding (list distance intersection) minimizing distance))))

(aoc:given 2
  (= 30 (find-minimum-latency *example-1*))
  (= 610 (find-minimum-latency *example-2*))
  (= 410 (find-minimum-latency *example-3*)))

(defun get-answer-2 ()
  (find-minimum-latency *input*))


;;; Visualization

(defparameter *cell-width* 1/16
  "The default works well for the full input.  For the examples, a cell
  width of 5-7 seems to work nicely.")
(defparameter *cell-margin* 8)
(defparameter *wire-width* 2)

(defun path-vertices (path)
  (iter (with coord = (complex 0 0))
        (if-first-time
         (collecting coord))
        (for segment in (ppcre:split "," path))
        (for vector = (ecase (schar segment 0)
                        (#\R (complex  1  0))
                        (#\L (complex -1  0))
                        (#\U (complex  0 -1))
                        (#\D (complex  0  1))))
        (setf coord (+ coord (* vector (aoc:extract-ints segment))))
        (collecting coord)))

(defun bbox (vertices)
  (let ((realparts (mapcar #'realpart vertices))
        (imagparts (mapcar #'imagpart vertices)))
    (list (complex (apply #'min realparts) (apply #'min imagparts))
          (complex (apply #'max realparts) (apply #'max imagparts)))))

(defun draw-wire (vertices)
  (cairo:move-to 0 0)
  (iter (for v in vertices)
        (cairo:line-to (realpart v) (imagpart v)))
  (cairo:stroke))

(defun draw-wires (filename paths)
  (let* ((vertices (mapcar #'path-vertices paths))
         (bbox (bbox (apply #'concatenate
                            (cons 'list (mapcar #'bbox vertices)))))
         (vertex-dimensions (+ (- (second bbox) (first bbox))
                               (complex 1 1)))
         (pixel-dimensions (+ (* 2 (complex *cell-margin* *cell-margin*))
                              (* vertex-dimensions
                                 *cell-width*)))
         (distance-intersection (nth-value 1 (find-minimum-distance paths)))
         (latency-intersection (nth-value 1 (find-minimum-latency paths))))
    (cairo:with-png-file (filename
                          :rgb24
                          (truncate (realpart pixel-dimensions))
                          (truncate (imagpart pixel-dimensions)))
      
      ; Set the background to a neutral shade.
      (viz:set-color :light-background)
      (cairo:rectangle 0 0 (realpart pixel-dimensions) (imagpart pixel-dimensions))
      (cairo:fill-path)

      ; Set up a transform so our native coordinates (the values in the
      ; vertices) will map to the appropriate pixels on the surface.
      (cairo:translate (+ *cell-margin*
                          (- (/ *cell-width* 2)
                             (/ *wire-width* 2))
                          (* *cell-width*
                             (- (realpart (first bbox)))))
                       (+ *cell-margin*
                          (- (/ *cell-width* 2)
                             (/ *wire-width* 2))
                          (* *cell-width*
                             (- (imagpart (first bbox))))))
      (cairo:scale *cell-width* *cell-width*)

      ; Line-drawing parameters.  We need to reverse the scale for the
      ; line width.  I think round caps and joins make the wires look
      ; nicer.
      (cairo:set-line-width (/ *wire-width* *cell-width*))
      (cairo:set-line-cap :round)
      (cairo:set-line-join :round)

      ; Draw the two wires in red and green, for Christmas.
      (viz:set-color :red)
      (draw-wire (first vertices))
      (viz:set-color :green)
      (draw-wire (second vertices))

      ; Draw a dot at the origin.
      (viz:set-color :light-primary)
      (cairo:arc 0 0 (* 2 *wire-width* (/ *cell-width*)) 0 (* 2 pi))
      (cairo:fill-path)

      ; Circle the intersections for each part.
      (cairo:arc (realpart distance-intersection)
                 (- (imagpart distance-intersection))
                 (* 3 *wire-width* (/ *cell-width*)) 0 (* 2 pi))
      (cairo:stroke)
      (viz:set-color :cyan)
      (cairo:arc (realpart latency-intersection)
                 (- (imagpart latency-intersection))
                 (* 3 *wire-width* (/ *cell-width*)) 0 (* 2 pi))
      (cairo:stroke))))
