(in-package :intcode)

(eval-when (:compile-toplevel :load-toplevel)
  (defparameter *opcodes* nil)
  (defparameter *opcode-metadata* (make-array 100 :initial-element nil)))


;;; Opcode metadata

(eval-when (:compile-toplevel :load-toplevel)
  (defstruct (opcode-metadata (:conc-name metadata-)
                              (:constructor make-metadata (name param-count)))
    (name nil :type symbol)
    (param-count 0 :type (integer 0 3))))


;;; Conditions

(define-condition invalid-opcode (error)
  ((opcode :initarg :opcode :reader opcode)
   (context :initarg :context :reader context))
  (:report (lambda (condition stream)
             (format stream "Program attempted to execute invalid opcode ~A."
                     (opcode condition)))))

(define-condition oob-memory-access (error)
  ((pointer :initarg :pointer :reader pointer)
   (context :initarg :context :reader context))
  (:report (lambda (condition stream)
             (format stream "Program attempted to access memory at location ~A."
                     (pointer condition)))))

(define-condition no-input-function (error)
  ((context :initarg :context :reader context))
  (:report (lambda (condition stream)
             (declare (ignore condition))
             (format stream "Program requested input, but no input function was provided."))))

(define-condition no-output-function (error)
  ((context :initarg :context :reader context))
  (:report (lambda (condition stream)
             (declare (ignore condition))
             (format stream "Program generated output, but no output function was provided."))))

(define-condition immediate-mode-write (error)
  ((context :initarg :context :reader context))
  (:report (lambda (condition stream)
             (format stream "Opcode at memory location ~A attempted to write to an immediate mode parameter."
                     (ip (context condition))))))


;;; Execution context

(defclass execution-context ()
  ((memory
    :initarg :mem
    :type simple-vector
    :accessor mem)
   (instruction-pointer
    :initform 0
    :type (integer 0)
    :accessor ip)
   (relative-base
    :initform 0
    :type integer
    :accessor relative-base)
   (input-function
    :initarg :input-fn
    :initform nil
    :type (or null (function () integer))
    :accessor input-fn)
   (output-function
    :initarg :output-fn
    :initform nil
    :type (or null (function (integer)))
    :accessor output-fn)))

(defun create-context (program &optional input-fn output-fn)
  (make-instance 'execution-context
                 :mem (make-array (length program)
                                  :initial-contents program)
                 :input-fn input-fn
                 :output-fn output-fn))

(defmethod initialize-instance :after ((context execution-context) &rest initargs)
  (declare (ignore initargs))
  (with-slots (input-function output-function) context
    (when (null input-function)
      (setf input-function (lambda ()
                             (error 'no-input-function :context context))))
    (when (null output-function)
      (setf output-function (lambda (v)
                              (declare (ignore v))
                              (error 'no-output-function :context context))))))


;;; Helper functions

(defun ensure-mem-size (context index)
  (declare (type (integer 0) index))
  (with-slots (memory) context
    (when (<= (length memory) index)
      (let ((new-memory (make-array (* 2 (length memory)) :initial-element 0)))
        (iter (for e in-vector memory with-index i)
              (when (not (zerop e))
                (setf (svref new-memory i) e)))
        (setf memory new-memory))
      (ensure-mem-size context index))))

(declaim (ftype (function (execution-context (integer 0 #.(1- array-total-size-limit))) integer) mem-ref))
(defun mem-ref (context index)
  (declare (optimize (speed 3)
                     (safety 0)))
  (with-slots (memory) context
    (declare (type simple-vector memory))
    (if (< index (length memory))
        (svref memory index)
        0)))

(defun (setf mem-ref) (new-value context index)
  (declare (type integer new-value)
           (type (integer 0) index))
  (ensure-mem-size context index)
  (setf (svref (mem context) index) new-value))


;;; Instructions

(defun access-parameter (context parameter mode)
  (declare (type execution-context context)
           (type (integer 0) parameter)
           (type (integer 0 2) mode)
           (optimize (speed 3)
                     (safety 0)))
  (mem-ref context
           (ecase mode
             (0 (mem-ref context parameter))
             (1 parameter)
             (2 (+ (the fixnum (slot-value context 'relative-base))
                   (the fixnum (mem-ref context parameter)))))))

(defun (setf access-parameter) (new-value context parameter mode)
  (declare (type integer new-value)
           (type execution-context context)
           (type (integer 0) parameter)
           (type (integer 0 2) mode))
  (when (= mode 1)
    (error 'immediate-mode-write :context context))
  (setf (mem-ref context
                 (ecase mode
                   (0 (mem-ref context parameter))
                   (2 (+ (relative-base context) (mem-ref context parameter)))))
        new-value))

;; I tested two methods of extracting the individual parameter modes from
;; the aggregated mode number:
;;
;;  * Calling a function separately for each mode (and calculating
;;    (m % 10^(n+1)) / 10^n) )
;;  * Doing all of the division at once and storing the results in a
;;    newly-consed vector
;;
;; The latter proved to be slightly faster, so that's what the below
;; function does.
(declaim (ftype (function ((integer 0 222) (integer 0 3)) (vector (integer 0 2))) mode-vector))
(defun mode-vector (mode-integer param-count)
  (let ((result (make-array param-count
                            :element-type '(integer 0 2)
                            :initial-element 0)))
    (labels ((mode-vector-r (integer-remaining i)
               (unless (or (= i param-count)
                           (zerop integer-remaining))
                 (multiple-value-bind (quotient remainder)
                     (truncate integer-remaining 10)
                   (setf (aref result i) remainder)
                   (mode-vector-r quotient (1+ i))))))
      (mode-vector-r mode-integer 0)
      result)))

;; This macro just stores instruction definitions for later use by the
;; `execute-opcode' macro.  If an instruction is added, any function using
;; the `execute-opcode' macro must be recompiled.
(defmacro instruction (opcode name params &body body)
  "Defines an instruction and its opcode.

  The definition should include a list of parameters for the opcode.  Each
  parameter will consume one integer following the instruction in memory.

  For example, the following code will double the value at memory location
  5 then store it at memory location 0:

      (instruction 98 dbl (a r)
        (setf r (* 2 a)))

  if its opcode and subsequent integers in memory are as follows:

      ... 98 5 0 ...

  Instructions may call one of two locally-defined control functions:

  `set-instruction-pointer' takes one parameter: the new value of the
  instruction pointer.  This changes where execution continues after this
  instruction returns.

  `terminate-program' takes no parameters.  It signals that the program
  should terminate as soon as this instruction returns.

  If neither control function is called, execution will proceed to the
  opcode following this one in memory.

  Other locally-defined functions include:

   * `get-input' will return an input value.
   * `write-output' will write an output value.
   * `incf-relative-base' will add the supplied parameter to the current relative base.
  "
  `(eval-when (:compile-toplevel :load-toplevel)
     (setf (svref *opcode-metadata* ,opcode) (make-metadata ',name ,(length params)))
     (setf *opcodes* (cons (list ,opcode ',name ',params ',body)
                           (delete ,opcode *opcodes* :key #'first)))))

(instruction 1 add (a b r)
  (setf r (+ a b)))

(instruction 2 mul (a b r)
  (setf r (* a b)))

(instruction 3 in (r)
  (setf r (get-input)))

(instruction 4 out (a)
  (write-output a))

(instruction 5 jnz (a b)
  (unless (zerop a)
    (set-instruction-pointer b)))

(instruction 6 jz (a b)
  (when (zerop a)
    (set-instruction-pointer b)))

(instruction 7 lt (a b r)
  (setf r (if (< a b)
              1
              0)))

(instruction 8 eq (a b r)
  (setf r (if (= a b)
              1
              0)))

(instruction 9 rb (a)
  (incf-relative-base a))

(instruction 99 hlt ()
  (terminate-program))


(eval-when (:compile-toplevel :load-toplevel)
  (defconstant +position-mode+ 0)
  (defconstant +immediate-mode+ 1)
  (defconstant +relative-mode+ 2)
  (defparameter +modes+ (list +position-mode+ +immediate-mode+ +relative-mode+)))


;; The `execute-opcode' macro essentially expands into an `ecase' across
;; the given opcode.  It replaced a mechanism where anonymous lambdas were
;; looked up in an array.
(defmacro execute-opcode (context opcode)
  (alexandria:with-gensyms (ip-modified-p
                            ip
                            relative-base
                            input-fn
                            output-fn)
    (labels ((mode-opcode (opcode modes)
               (iter (for mode in modes)
                     (for result first mode then (+ mode (* 10 result)))
                     (finally (return (+ opcode (* (or result 0) 100))))))
             (build-macrolet-list (params modes)
               (iter (for param in params)
                     (for mode in (reverse modes))
                     (for i from 0)
                     (collecting
                       `(,param (access-parameter ,context
                                                  (+ ,ip ,(1+ i))
                                                  ,mode)))))
             (list-all-modes (count)
               (if (zerop count)
                   (list nil)
                   (iter outer
                         (for sublist in (list-all-modes (1- count)))
                         (iter (for mode in +modes+)
                               (in outer
                                   (collecting (cons mode sublist))))))))
      `(with-slots ((,ip instruction-pointer)
                    (,relative-base relative-base)
                    (,input-fn input-function)
                    (,output-fn output-function))
           ,context
         (let ((,ip-modified-p nil))
           (declare (type boolean ,ip-modified-p))
           (flet ((set-instruction-pointer (new-ip)
                    (setf ,ip new-ip)
                    (setf ,ip-modified-p t))
                  (get-input ()
                    (funcall ,input-fn))
                  (write-output (value)
                    (funcall ,output-fn value))
                  (incf-relative-base (delta)
                    (incf ,relative-base delta))
                  (terminate-program ()
                    (throw 'halt (values))))
             (declare (ignorable (function set-instruction-pointer)
                                 (function incf-relative-base)
                                 (function terminate-program)))
             (case ,opcode
               ,@(iter (for (ref-opcode name params body) in *opcodes*)
                       (appending
                        (iter (for modes in (list-all-modes (length params)))
                              (collecting
                                `(,(mode-opcode ref-opcode modes)
                                   (symbol-macrolet (,@(build-macrolet-list params modes))
                                     ,@body)
                                 (when (not ,ip-modified-p)
                                   (incf ,ip ,(1+ (length params)))))))))
               (t (error 'invalid-opcode :opcode opcode :context ,context)))))))))

(defun execute-program (context)
  (catch 'halt
    (loop
       (let ((opcode (mem-ref context (ip context))))
         (declare (type (integer 0 22299) opcode))
         (execute-opcode context opcode)))))


;;; RPC

(defclass rpc-object ()
  ((thread
    :initarg :thread)
   (input-queue
    :initarg :input-queue)
   (output-queue
    :initarg :output-queue)
   (procedures
    :initarg :procedures)))

(defun rpc-call (rpc-object procedure &rest args)
  (with-slots (thread input-queue output-queue procedures) rpc-object
    (when (not (nth-value 1 (gethash procedure procedures)))
      (error "Unknown procedure: ~A" procedure))
    (when (not (bt:thread-alive-p thread))
      (error "Program halted."))
    (destructuring-bind (procedure-id param-count result-count) (gethash procedure procedures)
      (when (/= param-count (length args))
        (error "Wrong number of arguments for ~A; expected ~A, but got ~A." procedure param-count (length args)))
      (when procedure-id
        (lparallel.queue:push-queue procedure-id input-queue))
      (dolist (arg args)
        (lparallel.queue:push-queue arg input-queue))
      (iter (repeat result-count)
            (collecting (lparallel.queue:pop-queue output-queue) into result-list)
            (finally (return (values-list result-list)))))))

(defun rpc-finish (rpc-object)
  (with-slots (thread) rpc-object
    (when (bt:thread-alive-p thread)
      (bt:destroy-thread thread))
    (values)))


;;; The main functions

(defun load-program (sequence-or-string)
  (if (stringp sequence-or-string)
      (load-program (aoc:extract-ints sequence-or-string))
      (apply #'vector sequence-or-string)))

(defun run (program &key input-fn output-fn input-list return-memory)
  "When RETURN-MEMORY is true, the function will return the memory array.
  Otherwise, if no OUTPUT-FN is given, the program's output will be
  returned.  If OUTPUT-FN is given or the program generated no output, no
  values are returned."
  (let* ((output-list nil)
         (input-fn-real (or input-fn
                            (when input-list
                              (let ((real-list (copy-list input-list)))
                                (lambda ()
                                  (pop real-list))))))
         (output-fn-real (or output-fn
                             (when (not return-memory)
                               (lambda (value)
                                 (push value output-list)))))
         (context (create-context program input-fn-real output-fn-real)))
    (execute-program context)
    (cond
      (return-memory (mem context))
      (output-list   (nreverse output-list))
      (t             (values)))))

(defun run-for-rpc (program procedure-definitions)
  "Runs PROGRAM in a separate thread and returns an RPC object.  The
   object may be used with `rpc-call' to make RPC calls against the
   running program.

  PROCEDURE-DEFINITIONS should be a list defining the procedures to be
  called against the program.  Each list element should be a list with
  four elements in the following order:

   * Procedure name (symbol).  This is the symbol that will be passed to
     `rpc-call' to call this procedure.

   * Procedure id (integer or NIL).  If the procedure requires that a
     particular integer be sent to the program to select it, give that
     integer here.  If no integer is needed (i.e. program input begins
     with the procedure's argument list, give NIL.

   * Number of procedure arguments (integer).  The number of arguments
     that will be given to `rpc-call' to be sent to the program.

   * Number of procedure results (integer).  The number of output integers
     that this procedure will generate.  `rpc-call' will return each as a
     separate value.
  "
  (let ((input-queue (lparallel.queue:make-queue))
        (output-queue (lparallel.queue:make-queue))
        (procedure-hash (make-hash-table))
        (startup-queue (lparallel.queue:make-queue)))
    (iter (for procedure in procedure-definitions)
          (setf (gethash (car procedure) procedure-hash) (cdr procedure)))
    (flet ((input-fn () (lparallel.queue:pop-queue input-queue))
           (output-fn (value) (lparallel.queue:push-queue value output-queue)))
      (let ((thread (bt:make-thread
                     (lambda ()
                       (lparallel.queue:push-queue 'started startup-queue)
                       (intcode:run program :input-fn #'input-fn :output-fn #'output-fn))
                     :name "Intcode")))
        (lparallel.queue:pop-queue startup-queue)
        (make-instance 'rpc-object
                       :thread thread
                       :input-queue input-queue
                       :output-queue output-queue
                       :procedures procedure-hash)))))

(defmacro with-rpc ((rpc-object program procedure-definitions) &body body)
  "Runs PROGRAM for RPC with the RPC object bound to RPC-OBJECT.  Calls
  `rpc-finish' automatically when finished."
  `(let ((,rpc-object (run-for-rpc ,program ,procedure-definitions)))
     (unwind-protect
          (progn
            ,@body)
       (rpc-finish ,rpc-object))))


;;;; Supporting functions

(defmacro make-output-fn (params &body body)
  "Returns an anonymous lambda that can be passed as an :OUTPUT-FN for
  `run'.  It will function as if all of PARAMS were output at once."
  (alexandria:with-gensyms (queue
                            value)
    `(let ((,queue (cl-speedy-queue:make-queue ,(length params))))
       (lambda (,value)
         (cl-speedy-queue:enqueue ,value ,queue)
         (when (= ,(length params) (cl-speedy-queue:queue-count ,queue))
           (let (,@(iter (for param in params)
                         (collecting `(,param (cl-speedy-queue:dequeue ,queue)))))
             ,@body))))))


(defun list-opcode-params (context mem-location param-count mode-vector)
  (iter (for m from (1+ mem-location))
        (for i from 0)
        (repeat param-count)
        (for param-value = (mem-ref context m))
        (collecting
          (ecase (aref mode-vector i)
            (0 (format nil "*~D=~D" param-value (handler-case
                                                    (access-parameter context m 0)
                                                  (oob-memory-access () "?"))))
            (1 (format nil "~D" param-value))
            (2 (format nil "*~D(+~D)=~D"
                       param-value (relative-base context)
                       (handler-case
                           (access-parameter context m 2)
                         (oob-memory-access () "?"))))))))

(defun disassemble (context)
  (with-slots ((ip instruction-pointer) (mem memory)) context
    (iter (with param-count)
          (with initial-mem-length = (length mem))
          (for mem-location first ip then (+ mem-location param-count 1))
          (while (< mem-location initial-mem-length))
          (for raw-opcode = (mem-ref context mem-location))
          (for (values mode-integer opcode) = (truncate raw-opcode 100))
          (if (and (<= 0 opcode 99)
                   (svref *opcode-metadata* opcode))
              (progn
                (setf param-count (metadata-param-count (svref *opcode-metadata* opcode)))
                (for name = (metadata-name (svref *opcode-metadata* opcode)))
                (for mode-vector = (mode-vector mode-integer param-count))
                (format t "~4D: ~3@A(~5D) ~{~A ~}~%"
                        mem-location name raw-opcode
                        (list-opcode-params context mem-location param-count mode-vector)))
              (progn
                (setf param-count 1)
                (format t "~4D: ~D~%" mem-location raw-opcode)))))
  (values))


;;;; Programs

(defparameter *sum-of-primes*
  "3,100,1007,100,2,7,1105,-1,87,1007,100,1,14,1105,-1,27,101,-2,100,100,101,1,101,101,1105,1,9,101,105,101,105,101,2,104,104,101,1,102,102,1,102,102,103,101,1,103,103,7,102,101,52,1106,-1,87,101,105,102,59,1005,-1,65,1,103,104,104,101,105,102,83,1,103,83,83,7,83,105,78,1106,-1,35,1101,0,1,-1,1105,1,69,4,104,99")
(defparameter *ackermann*
  "109,99,21101,0,13,0,203,1,203,2,1105,1,16,204,1,99,1205,1,26,22101,1,2,1,2105,1,0,1205,2,40,22101,-1,1,1,21101,0,1,2,1105,1,16,21101,0,57,3,22101,0,1,4,22101,-1,2,5,109,3,1105,1,16,109,-3,22101,0,4,2,22101,-1,1,1,1105,1,16")
(defparameter *factor*
  "3,1,109,583,108,0,1,9,1106,-1,14,4,1,99,107,0,1,19,1105,-1,27,104,-1,102,-1,1,1,21101,0,38,0,20101,0,1,1,1105,1,138,2101,1,1,41,101,596,41,45,1101,1,596,77,1101,0,1,53,101,1,77,77,101,1,53,53,7,45,77,67,1105,-1,128,108,1,1,74,1105,-1,128,1005,-1,54,1,53,77,93,7,45,93,88,1105,-1,101,1101,0,1,-1,1,53,93,93,1105,1,83,21101,0,116,0,20101,0,1,1,20101,0,53,2,1105,1,235,1205,2,54,4,53,2101,0,1,1,1105,1,101,108,1,1,133,1105,-1,137,4,1,99,22101,0,1,2,22101,0,1,1,21101,0,163,3,22101,0,1,4,22101,0,2,5,109,3,1105,1,198,109,-3,22102,-1,1,1,22201,1,4,3,22102,-1,1,1,1208,3,0,182,2105,-1,0,1208,3,1,189,2105,-1,0,22101,0,4,1,1105,1,146,1207,1,1,203,2105,-1,0,21101,0,222,3,22101,0,2,4,22101,0,1,5,109,3,1105,1,235,109,-3,22201,1,4,1,21101,0,2,2,1105,1,235,1105,0,280,101,383,236,243,1107,-1,583,247,1106,-1,276,101,383,236,256,102,1,275,-1,102,2,275,275,1007,275,0,266,1105,-1,280,101,1,236,236,1105,1,238,1,101,-1,236,236,101,383,236,286,207,1,-1,289,1106,-1,-1,22101,0,1,3,2102,1,2,363,2102,-1,2,369,22102,0,1,1,22102,0,2,2,101,1,236,320,101,-1,320,320,1107,-1,0,324,2105,-1,0,22102,2,2,2,101,383,320,336,207,3,-1,339,1105,-1,361,22101,1,2,2,22102,-1,3,3,101,383,320,354,22001,-1,3,3,22102,-1,3,3,1207,2,-1,366,1105,-1,315,22101,-1,2,2,101,383,320,377,22001,-1,1,1,1105,1,315")


;;; Tests

(5am:def-suite :intcode :in :aoc-all)
(5am:in-suite :intcode)

(5am:test conditions
  (5am:signals invalid-opcode
    (run #(0)))
  (5am:signals no-input-function
    (run #(3 1 99)))
  (5am:signals immediate-mode-write
    (run #(11101 1 2 3 99))))

(5am:test day-02-examples
  (5am:is (equalp #(3500 9 10 70 2 3 11 0 99 30 40 50)
                  (run #(1 9 10 3 2 3 11 0 99 30 40 50) :return-memory t)))
  (5am:is (equalp #(2 0 0 0 99)
                  (run #(1 0 0 0 99) :return-memory t)))
  (5am:is (equalp #(2 3 0 6 99)
                  (run #(2 3 0 3 99) :return-memory t)))
  (5am:is (equalp #(2 4 4 5 99 9801)
                  (run #(2 4 4 5 99 0) :return-memory t)))
  (5am:is (equalp #(30 1 1 4 2 5 6 0 99)
                  (run #(1 1 1 4 99 5 6 0 99) :return-memory t))))

(5am:test day-05-examples
  (5am:is (equal '(54)
                 (run #(3 0 4 0 99) :input-list '(54))))
  (5am:is (equalp #(1002 4 3 4 99)
                  (run #(1002 4 3 4 33) :return-memory t)))
  (5am:is (equalp #(1101 100 -1 4 99)
                  (run #(1101 100 -1 4 0) :return-memory t)))

  ;; Tests that compare against 8
  (let ((eq-8-position #(3 9 8 9 10 9 4 9 99 -1 8))
        (lt-8-position #(3 9 7 9 10 9 4 9 99 -1 8))
        (eq-8-immediate #(3 3 1108 -1 8 3 4 3 99))
        (lt-8-immediate #(3 3 1107 -8 8 3 4 3 99)))
   (5am:is (equal '(1) (run eq-8-position :input-list '(8))))
   (5am:is (equal '(0) (run eq-8-position :input-list '(4))))
   (5am:is (equal '(0) (run lt-8-position :input-list '(8))))
   (5am:is (equal '(1) (run lt-8-position :input-list '(4))))
   (5am:is (equal '(1) (run eq-8-immediate :input-list '(8))))
   (5am:is (equal '(0) (run eq-8-immediate :input-list '(4))))
   (5am:is (equal '(0) (run lt-8-immediate :input-list '(8))))
   (5am:is (equal '(1) (run lt-8-immediate :input-list '(4)))))

  ;; Tests for nonzero
  (5am:is (equal '(1)
                 (run #(3 12 6 12 15 1 13 14 13 4 13 99 -1 0 1 9)
                      :input-list '(-1))))
  (5am:is (equal '(0)
                 (run #(3 12 6 12 15 1 13 14 13 4 13 99 -1 0 1 9)
                      :input-list '(0))))
  (5am:is (equal '(1)
                 (run #(3 12 6 12 15 1 13 14 13 4 13 99 -1 0 1 9)
                      :input-list '(1))))
  (5am:is (equal '(1)
                 (run #(3 3 1105 -1 9 1101 0 0 12 4 12 99 1)
                      :input-list '(-1))))
  (5am:is (equal '(0)
                 (run #(3 3 1105 -1 9 1101 0 0 12 4 12 99 1)
                      :input-list '(0))))
  (5am:is (equal '(1)
                 (run #(3 3 1105 -1 9 1101 0 0 12 4 12 99 1)
                      :input-list '(1))))

  ;;; CMP with 8
  (let ((program #(3 21 1008 21 8 20 1005 20 22 107 8 21 20 1006 20 31 1106 0 36 98 0 0 1002 21 125 20 4 20 1105 1 46 104 999 1105 1 46 1101 1000 1 20 4 20 1105 1 46 98 99)))
    (5am:is (equal '(999) (run program :input-list '(4))))
    (5am:is (equal '(1000) (run program :input-list '(8))))
    (5am:is (equal '(1001) (run program :input-list '(12))))))

(5am:test day-09-examples
  (let ((quine-program '(109 1 204 -1 1001 100 1 100 1008 100 16 101 1006 101 0 99)))
    (5am:is (equal quine-program (run (load-program quine-program))))))
