(in-package :aoc-2019-10)

(aoc:define-day 344 2732)


;;; Input

(defparameter *example-1a* '(".#..#"
                             "....."
                             "#####"
                             "....#"
                             "...##"))
(defparameter *example-2a* '(".#....#####...#.."
                             "##...##.#####..##"
                             "##...#...#.#####."
                             "..#.....#...###.."
                             "..#.#.....#....##"))
(defparameter *input* (aoc:input))



;;; Part 1

(defun map-to-coords (map)
  (iter outer
        (for line in map)
        (for row from 0)
        (iter (for char in-string line)
              (for col from 0)
              (when (char= #\# char)
                (in outer
                    (collecting (complex col row)))))))

;; Casting the values to the same type as pi (double-float in my SBCL
;; instance) means that calculations involving pi will be as exact as
;; possible.
(defun angle-to-point (source dest)
  (let ((vector (- dest source)))
    (atan (float (imagpart vector) pi)
          (float (realpart vector) pi))))

(defun list-visible-coords (source coords)
  "Returns a list of lists.  Each sublist begins with an angle (in
  radians) then has all of the coordinates in that direction, in order
  from closest to furthest."
  (let ((seen (make-hash-table)))
    (iter (for coord in coords)
          (when (= coord source)
            (next-iteration))
          (for distance = (abs (- coord source)))
          (for angle = (angle-to-point source coord))
          (push (list distance coord) (gethash angle seen nil)))
    (iter (for (angle coords-with-distance) in-hashtable seen)
          (for coords = (mapcar #' second (sort coords-with-distance #'< :key #'first)))
          (collecting (cons angle coords)))))

(defun count-visible-coords (source coords)
  (length (list-visible-coords source coords)))

(defun find-monitoring-location (map)
  (let ((coords (map-to-coords map)))
    (iter (for coord in coords)
          (for visible-coords = (count-visible-coords coord coords))
          (finding (list visible-coords coord) maximizing visible-coords into result)
          (finally (return (values-list result))))))

(defun get-answer-1 ()
  (find-monitoring-location *input*))


;;; Part 2

(defun list-destroyed-asteroids (map)
  (let ((monitoring-location (nth-value 1 (find-monitoring-location map)))
        (coords (map-to-coords map))
        (remaining-asteroids (make-hash-table))  ; Indexed by angle
        (angles nil))
    (iter (for item in (list-visible-coords monitoring-location coords))
          (for angle = (first item))
          (for asteroids = (rest item))
          (push angle angles)
          (setf (gethash angle remaining-asteroids) asteroids))
    ;; We start at an angle of -π/2 and proceed clockwise from there.
    (setf angles (sort angles #'< :key (lambda (a) (mod (+ a (/ pi 2))
                                                        (* 2 pi)))))
    (iter outer
          (while (plusp (hash-table-count remaining-asteroids)))
          (iter (for angle in angles)
                (for found = (nth-value 1 (gethash angle remaining-asteroids)))
                (when found
                  (for asteroid = (pop (gethash angle remaining-asteroids)))
                  (if asteroid
                      (in outer (collect asteroid))
                      (remhash angle remaining-asteroids)))))))
    

(defun get-answer-2 ()
  (let ((coord (nth 199 (list-destroyed-asteroids *input*))))
    (values (+ (* 100 (realpart coord))
               (imagpart coord))
            coord)))


;;; Visualization

(defparameter *frame-width* 992)
(defparameter *frame-height* 448)
(defparameter *asteroid-size* 4)
(defparameter *margin* 8)

(defun calculate-distance-scale (bbox)
  (let* ((available-vertical-area (- *frame-height* (* 2 *margin*) *asteroid-size*))
         (available-horizontal-area (- *frame-width* (* 2 *margin*) *asteroid-size*))
         (vertical-scale (/ available-vertical-area (- (aoc:bbox-top bbox) (aoc:bbox-bottom bbox))))
         (horizontal-scale (/ available-horizontal-area (- (aoc:bbox-right bbox) (aoc:bbox-left bbox)))))
    (min vertical-scale horizontal-scale)))

(defun group-asteroids-by-angle (monitoring-location coords)
  (let ((result (make-hash-table)))
    (iter (for item in (list-visible-coords monitoring-location coords))
          (for angle = (mod (first item) (* 2 pi)))
          (for asteroids = (rest item))
          (setf (gethash angle result) asteroids))
    result))

(defun draw-asteroids (surface monitoring-location asteroids-by-angle asteroid-origin scale)
  (cairo:with-context-from-surface (surface)
    (cairo:translate (realpart asteroid-origin) (imagpart asteroid-origin))
    (viz:set-color :dark-primary)
    (iter (for (_ coords) in-hashtable asteroids-by-angle)
          (iter (for coord in coords)
                (cairo:arc (* scale (realpart coord)) (* scale (imagpart coord))
                           (/ *asteroid-size* 2)
                           0 (* 2 pi))
                (cairo:fill-path)))
    (viz:set-color :dark-emphasized)
    (cairo:arc (* scale (realpart monitoring-location)) (* scale (imagpart monitoring-location))
               *asteroid-size*
               0 (* 2 pi))
    (cairo:fill-path)))

(defun draw-and-destroy-asteroid (angle surface monitoring-location asteroids-by-angle asteroid-origin scale)
  (cairo:with-context-from-surface (surface)
    (cairo:translate (realpart asteroid-origin)
                     (imagpart asteroid-origin))
    (viz:set-color :red)
    (cairo:set-line-width 2)
    (cairo:move-to (+ (* scale (realpart monitoring-location)) (* 2 *asteroid-size* (cos angle)))
                   (+ (* scale (imagpart monitoring-location)) (* 2 *asteroid-size* (sin angle))))
    (let ((asteroid (pop (gethash angle asteroids-by-angle))))
      (when asteroid
        (cairo:line-to (* scale (realpart asteroid))
                       (* scale (imagpart asteroid)))
        (cairo:stroke)))))

(defun draw-frame (angle angle-delta surface monitoring-location asteroids-by-angle asteroid-origin scale)
  (cairo:with-context-from-surface (surface)
    (cairo:translate (+ (realpart asteroid-origin) (* scale (realpart monitoring-location)))
                     (+ (imagpart asteroid-origin) (* scale (imagpart monitoring-location))))
    (cairo:set-line-width 1)
    (viz:set-color :red)
    (cairo:arc 0 0
               (* 1.5 *asteroid-size*)
               0 (* 2 pi))
    (cairo:stroke)
    (cairo:set-line-width 2)
    (cairo:move-to (* *asteroid-size* (cos angle)) (* *asteroid-size* (sin angle)))
    (cairo:line-to (* 2 *asteroid-size* (cos angle)) (* 2 *asteroid-size* (sin angle)))
    (cairo:stroke))
  (iter (for angle-of-destruction
             in (remove-if (lambda (a) (not (<= angle a (+ angle angle-delta))))
                           (alexandria:hash-table-keys asteroids-by-angle)))
        (draw-and-destroy-asteroid angle-of-destruction surface monitoring-location asteroids-by-angle asteroid-origin scale))
  (draw-asteroids surface monitoring-location asteroids-by-angle asteroid-origin scale))

(defun animate-asteroid-destruction (map filename &key (frames-per-rotation 360))
  (let* ((coords (map-to-coords map))
         (bbox (aoc:find-bbox coords))
         (scale (calculate-distance-scale bbox))
         (monitoring-location (nth-value 1 (find-monitoring-location map)))
         (asteroids-by-angle (group-asteroids-by-angle monitoring-location coords))
         (asteroid-origin (complex (- (/ *frame-width* 2)
                                      (* scale (/ (- (aoc:bbox-right bbox)
                                                     (aoc:bbox-left bbox))
                                                  2)))
                                   (- (/ *frame-height* 2)
                                      (* scale (/ (- (aoc:bbox-top bbox)
                                                     (aoc:bbox-bottom bbox))
                                                  2))))))
    (viz:with-video (video (viz:create-video filename *frame-width* *frame-height*))
      (iter (while (plusp (hash-table-count asteroids-by-angle)))
            (with angle-delta = (/ (* 2 pi) frames-per-rotation))
            (for i from 0)
            (for angle = (mod (- (* i angle-delta) (/ pi 2)) (* 2 pi)))
            (let ((surface (viz:create-video-surface video)))
              (viz:clear-surface surface :dark-background)
              (draw-frame angle angle-delta surface monitoring-location asteroids-by-angle asteroid-origin scale)
              (viz:write-video-surface video surface))
            (iter (for k in (alexandria:hash-table-keys asteroids-by-angle))
                  (when (null (gethash k asteroids-by-angle))
                    (remhash k asteroids-by-angle)))))))
