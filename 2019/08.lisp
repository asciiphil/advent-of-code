(in-package :aoc-2019-08)

; Part 2: "ZBJAB"
(aoc:define-day 2806 nil)


;;; Input


(defparameter *example-1* "123456789012")
(defparameter *example-1-dimensions* '(2 3))
(defparameter *example-2* "0222112222120000")
(defparameter *example-2-dimensions* '(2 2))

(defparameter *input* (aoc:input))
(defparameter *dimensions* '(6 25))


;;; Part 1

(defun parse-image (dimensions data)
  "Indexing on returned image is (layer row column)."
  (let* ((2d-cell-count (apply #'* dimensions))
         (layer-count (/ (length data) 2d-cell-count))
         (image (make-array (cons layer-count dimensions)
                            :element-type '(integer 0 9)
                            :initial-element 0))
         (flattened-image (make-array (array-total-size image)
                                      :element-type '(integer 0 9)
                                      :displaced-to image)))
    (iter (for c in-string data)
          (for i from 0)
          (for color = (- (char-code c) (char-code #\0)))
          (setf (aref flattened-image i) color))
    image))

(defun count-colors-in-layer (image layer color)
  (iter outer
        (for row from 0 below (array-dimension image 1))
        (iter (for col from 0 below (array-dimension image 2))
              (in outer
                  (counting (= color (aref image layer row col)))))))

(defun find-layer-with-fewest-zeros (image)
  (iter (for layer from 0 below (array-dimension image 0))
        (for zeros = (count-colors-in-layer image layer 0))
        (finding layer minimizing zeros)))

(defun get-answer-1 (&optional (data *input*) (dimensions *dimensions*))
  (let* ((image (parse-image dimensions data))
         (zero-layer (find-layer-with-fewest-zeros image)))
    (* (count-colors-in-layer image zero-layer 1)
       (count-colors-in-layer image zero-layer 2))))


;;; Part 2

(defun render-image (image)
  (let ((result (make-array (cdr (array-dimensions image))
                            :element-type '(integer 0 2))))
    (iter (for row from 0 below (array-dimension result 0))
          (iter (for col from 0 below (array-dimension result 1))
                (for color = (iter (for layer from 0 below (array-dimension image 0))
                                   (for source-color = (aref image layer row col))
                                   (while (= source-color 2))
                                   (finally (return source-color))))
                (setf (aref result row col) color)))
    result))

(defun print-rendered-image (rendered-image)
  (iter (for row from 0 below (array-dimension rendered-image 0))
        (iter (for col from 0 below (array-dimension rendered-image 1))
              (format t "~A" (if (zerop (aref rendered-image row col))
                                 " "
                                 "#")))
        (format t "~%"))
  (values))


;;; Visualization

(defparameter *cell-size* 32)
(defparameter *frame-width* 1280)
(defparameter *frame-height* 720)
(defparameter *frames-per-layer* 10)

(defun draw-image (image filename)
  (let ((rendered-image (render-image image)))
    (cairo:with-png-file (filename
                          :rgb24
                          *frame-width*
                          *frame-height*)
      (viz:set-color :dark-background)
      (cairo:rectangle 0 0 *frame-width* *frame-height*)
      (cairo:fill-path)

      (cairo:translate (- (/ *frame-width* 2)
                          (/ (* (array-dimension rendered-image 1) *cell-size*) 2))
                       (- (/ *frame-height* 2)
                          (/ (* (array-dimension rendered-image 0) *cell-size*) 2)))
      (cairo:scale *cell-size* *cell-size*)

      (iter (for row from 0 below (array-dimension rendered-image 0))
            (iter (for col from 0 below (array-dimension rendered-image 1))
                  (for color = (aref rendered-image row col))
                  (ecase color
                    (0 (viz:set-color :dark-highlight))
                    (1 (viz:set-color :dark-primary)))
                  (cairo:rectangle col row 1 1)
                  (cairo:fill-path))))))

(defun animate-layer (video base-surface image layer)
  "Generates the frames for the given layer.

  *Side effect*: When finished, modifies BASE-SURFACE to add the given layer."
  (let ((working-surface (viz:create-video-surface video))
        (layer-surface (cairo:create-image-surface :argb32
                                                   (* (array-dimension image 2) *cell-size*)
                                                   (* (array-dimension image 1) *cell-size*))))
    ;; Initialize layer surface
    (cairo:with-context-from-surface (layer-surface)
      (cairo:set-source-rgba 0 0 0 0)
      (cairo:rectangle 0 0
                       (cairo:image-surface-get-width layer-surface)
                       (cairo:image-surface-get-height layer-surface))
      (cairo:fill-path)
      (cairo:scale *cell-size* *cell-size*)
      (iter (for col from 0 below (array-dimension image 2))
            (iter (for row from 0 below (array-dimension image 1))
                  (ecase (aref image layer row col)
                    (0 (viz:set-color :dark-highlight))
                    (1 (viz:set-color :dark-primary))
                    (2 (next-iteration)))
                  (cairo:rectangle col row 1 1)
                  (cairo:fill-path))))
    ;; Animate in the frames.
    (iter (for frame from 1 below *frames-per-layer*)
          (with layer-x = (- (/ (cairo:image-surface-get-width working-surface) 2)
                             (/ (cairo:image-surface-get-width layer-surface) 2)))
          (with start-y = (- (cairo:image-surface-get-height layer-surface)))
          (with end-y = (- (/ (cairo:image-surface-get-height working-surface) 2)
                           (/ (cairo:image-surface-get-height layer-surface) 2)))
          (for layer-y = (+ start-y
                            (* (- end-y start-y)
                               (/ frame *frames-per-layer*))))
          (cairo:with-context-from-surface (working-surface)
            (cairo:set-source-surface base-surface 0 0)
            (cairo:paint)
            (cairo:set-source-surface layer-surface layer-x layer-y)
            (cairo:paint))
          (viz:write-video-surface video working-surface))
    ;; Update the base surface
    (cairo:with-context-from-surface (base-surface)
      (cairo:set-source-surface layer-surface
                                (- (/ (cairo:image-surface-get-width working-surface) 2)
                                   (/ (cairo:image-surface-get-width layer-surface) 2))
                                (- (/ (cairo:image-surface-get-height working-surface) 2)
                                   (/ (cairo:image-surface-get-height layer-surface) 2)))
      (cairo:paint)
      (viz:write-video-surface video base-surface))))

(defun animate-image (image filename)
  (viz:with-video (video (viz:create-video filename *frame-width* *frame-height*))
    (iter (with base-surface = (viz:create-video-surface video))
          (if-first-time
           (cairo:with-context-from-surface (base-surface)
             (viz:set-color :dark-background)
             (cairo:rectangle 0 0 *frame-width* *frame-height*)
             (cairo:fill-path)))
          (for layer from (1- (array-dimension image 0)) downto 0)
          (animate-layer video base-surface image layer)
          (when (<= 90 layer)
            (iter (repeat 5)
                  (viz:write-video-surface video base-surface)))
          (finally (iter (repeat (* 15 25))
                         (viz:write-video-surface video base-surface))))))
