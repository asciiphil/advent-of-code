(in-package :aoc-2019-23)

(aoc:define-day 21897 16424)


;;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))


;;;; Part 1

(defstruct packet
  source
  destination
  x
  y)

(defcoroutine make-packet (value)
  (let (address destination x y)
    (loop (setf address value)
          (yield nil)
          (setf destination value)
          (yield nil)
          (setf x value)
          (yield nil)
          (setf y value)
          (yield (make-packet :source address
                              :destination destination
                              :x x
                              :y y)))))

(defun start-thread (address output-queue idle-queue)
  (let ((input-queue (lparallel.queue:make-queue))
        (pending-input address)
        (packet-coroutine (make-coroutine 'make-packet))
        (last-idle-status nil))
    (funcall packet-coroutine address)
    (labels ((update-idle-status (value)
               (when (not (eql value last-idle-status))
                 (setf last-idle-status value)
                 (lparallel.queue:push-queue (cons address value) idle-queue)))
             (input ()
               (cond
                 (pending-input
                  (prog1
                      pending-input
                    (setf pending-input nil)))
                 ((lparallel.queue:queue-empty-p input-queue)
                  (update-idle-status t)
                  (sleep 0.001)
                  -1)
                 (t
                  (let ((packet (lparallel.queue:pop-queue input-queue)))
                    (assert (= address (packet-destination packet)))
                    (update-idle-status nil)
                    (setf pending-input (packet-y packet))
                    (packet-x packet)))))
             (output (value)
               (update-idle-status nil)
               (let ((packet (funcall packet-coroutine value)))
                 (when packet
                   (lparallel.queue:push-queue packet output-queue)
                   (funcall packet-coroutine address)))))
      (values (bt:make-thread (lambda () (intcode:run *input* :input-fn #'input :output-fn #'output))
                              :name (format nil "NIC ~A" address))
              input-queue))))

(defun start-switch (&optional (nic-count 50))
  (let ((output-queue (lparallel.queue:make-queue))
        (idle-queue (lparallel.queue:make-queue))
        (threads nil)
        (nic-queues (make-array nic-count)))
    (iter (for address from 0 below nic-count)
          (for (values thread nic-queue) = (start-thread address output-queue idle-queue))
          (setf (svref nic-queues address) nic-queue)
          (push thread threads))
    (values output-queue idle-queue nic-queues threads)))

(defun run-switch (output-queue idle-queue nic-queues threads &key with-nat)
  (let* ((nat-packet)
         (nic-idle (make-array (length nic-queues)))
         (seen-y (make-hash-table)))
    (flet ((dispatch-packets ()
             (iter (for packet = (lparallel.queue:try-pop-queue output-queue :timeout 0.01))
                   (while packet)
                   (if (= 255 (packet-destination packet))
                       (setf nat-packet packet)
                       (lparallel.queue:push-queue packet (svref nic-queues (packet-destination packet))))))
           (track-idle ()
             (iter (for idle-cons = (lparallel.queue:try-pop-queue idle-queue :timeout 0.01))
                   (while idle-cons)
                   (for (address . idle) = idle-cons)
                   (setf (svref nic-idle address) idle)))
           (all-idle-p ()
             (and (iter (for idle in-vector nic-idle)
                        (always idle))
                  (iter (for queue in-vector nic-queues)
                        (always (lparallel.queue:queue-empty-p queue)))))
           (inject-nat-packet ()
             (lparallel.queue:push-queue (make-packet :source 255
                                                      :destination 0
                                                      :x (packet-x nat-packet)
                                                      :y (packet-y nat-packet))
                                         output-queue)))
     (unwind-protect
          (iter (dispatch-packets)
                (track-idle)
                (when (and nat-packet (not with-nat))
                  (return nat-packet))
                (when (and (lparallel.queue:queue-empty-p output-queue)
                           (lparallel.queue:queue-empty-p idle-queue)
                           (all-idle-p))
                  (when (< 1 (incf (gethash (packet-y nat-packet) seen-y 0)))
                    (return nat-packet))
                  (inject-nat-packet)))
       (iter (for thread in threads)
             (when (bt:thread-alive-p thread)
               (bt:destroy-thread thread)))))))

(defun get-answer-1 ()
  (packet-y (multiple-value-call #'run-switch (start-switch 50))))


;;;; Part 2

(defun get-answer-2 ()
  (multiple-value-bind (output-queue idle-queue nic-queues threads) (start-switch 50)
    (packet-y (run-switch output-queue idle-queue nic-queues threads :with-nat t))))
