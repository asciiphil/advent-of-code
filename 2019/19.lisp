(in-package :aoc-2019-19)

(aoc:define-day 154 9791328)


;;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))



;;;; Part 1

(defun test-position (program x y)
  (car (intcode:run program :input-list (list x y))))

(defun test-tractor-beam (program width height)
  (let ((result (make-array (list height width)
                            :element-type '(integer 0 1)
                            :initial-element 0)))
    (iter (for y from 0 below height)
          (iter (for x from 0 below width)
                (setf (aref result y x)
                      (test-position program x y))))
    result))

(defun print-beam (positions)
  (iter (for y from 0 below (array-dimension positions 0))
        (iter (for x from 0 below (array-dimension positions 1))
              (format t "~[.~;#~]" (aref positions y x)))
        (format t "~%")))

(defun get-answer-1 ()
  (reduce #'+ (aoc:flatten-array (test-tractor-beam *input* 50 50))))


;;;; Part 2

(defun square-fits-p (program width height ll-x ll-y)
  (and (= 1 (test-position program ll-x ll-y))
       (= 1 (test-position program (1- (+ ll-x width)) (1+ (- ll-y height))))))

(defun find-first-active-x (program y &optional (base-x 0))
  (iter (for x from base-x)
        (finding x such-that (= 1 (test-position program x y)))))

(defun find-square-fit (program width height &optional x y)
  (cond
    ((not (and x y))
     (let* ((start-y (max 7 height))
            (start-x (find-first-active-x program start-y)))
       (find-square-fit program width height start-x start-y)))
    ((square-fits-p program width height x y)
     (values x (1+ (- y height))))
    (t
     (find-square-fit program
                      width height
                      (find-first-active-x program (1+ y) x)
                      (1+ y)))))

(defun get-answer-2 ()
  (multiple-value-bind (x y) (find-square-fit *input* 100 100)
    (+ (* x 10000) y)))
