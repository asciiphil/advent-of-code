(in-package :aoc-2019-15)

(aoc:define-day 248 382)


;;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))


;;;; Part 1

(defconstant +north+ 1)
(defconstant +south+ 2)
(defconstant +west+ 3)
(defconstant +east+ 4)

(defconstant +movement-result-wall+ 0)
(defconstant +movement-result-traversable+ 1)
(defconstant +movement-result-oxygen+ 2)

(defparameter +cardinal-vectors+ (list #(0 1) #(0 -1) #(1 0) #(-1 0)))

(defun direction-vector (direction)
  (ecase direction
    (#.+north+ #( 0 -1))
    (#.+south+ #( 0  1))
    (#.+east+  #( 1  0))
    (#.+west+  #(-1  0))))

(defun vector-direction (vector)
  (alexandria:eswitch (vector :test #'point:=)
    (#( 0 -1) +north+)
    (#( 0  1) +south+)
    (#( 1  0) +east+)
    (#(-1  0) +west+)))

(defvar *rpc-object* nil
  "Dynamic variable holding the value of an active rpc object.")

(defun move (direction)
  (intcode:rpc-call *rpc-object* 'move direction))

(defun set-landscape (map position landscape)
  (setf (gethash position map) landscape)
  (when (or (eq landscape :traversable)
            (eq landscape :oxygen))
    (iter (for vector in +cardinal-vectors+)
          (for adjacent-position = (point:+ position vector))
          (when (not (nth-value 1 (gethash adjacent-position map)))
            (setf (gethash adjacent-position map) :unknown))))
  landscape)

(defun init-map ()
  (let ((map (make-hash-table :test 'equalp)))
    (setf (gethash #(0 0) map) :traversable)
    map))

(defun landscape-char (landscape)
  (ecase landscape
    ((nil) " ")
    (:wall "#")
    (:traversable ".")
    (:unknown "?")
    (:oxygen "O")))

(defun print-map (map)
  (multiple-value-bind (min-point max-point) (point:bbox (alexandria:hash-table-keys map))
    (iter (for row from (point:elt min-point 1) to (point:elt max-point 1))
          (iter (for col from (point:elt min-point 0) to (point:elt max-point 0))
                (format t "~A" (landscape-char (gethash (point:make-point col row) map))))
          (format t "~%"))))

(defun explore-map-step-random (map position &optional full-map)
  (let* ((vector (alexandria:random-elt +cardinal-vectors+))
         (new-position (point:+ position vector))
         (move-result (move (vector-direction vector))))
    (when (and full-map (= move-result +movement-result-oxygen+))
      (setf *oxygen-positions* (list new-position)))
    (setf (gethash new-position map)
          (ecase move-result
            (#.+movement-result-wall+ :wall)
            (#.+movement-result-traversable+ :traversable)
            (#.+movement-result-oxygen+ :oxygen)))
    (if (= move-result +movement-result-wall+)
        position
        new-position)))

(defun explore-map-random (map &optional video full-map)
  (iter (with last-frame = -1)
        (for i from 1 to 10000000)
        (for log-frame = (truncate (* (log i 2) 100)))
        (for position first #(0 0) then (explore-map-step-random map position full-map))
        (when (and video (< last-frame log-frame))
          (setf last-frame log-frame)
          (for surface = (viz:create-video-surface video))
          (draw-map map surface :drone-position position :full-map full-map :step-count i)
          (viz:write-video-surface video surface))))

(defun explore-map-step (map position &optional video full-map)
  (iter (for vector in +cardinal-vectors+)
        (for new-position = (point:+ position vector))
        (when (nth-value 1 (gethash new-position map))
          (next-iteration))
        (for move-result = (move (vector-direction vector)))
        (when (and full-map *oxygen-positions*)
          (setf *oxygen-positions* (fill-oxygen-step full-map *oxygen-positions*)))
        (when (and full-map (= move-result +movement-result-oxygen+))
          (setf *oxygen-positions* (list new-position)))
        (setf (gethash new-position map)
              (ecase move-result
                (#.+movement-result-wall+ :wall)
                (#.+movement-result-traversable+ :traversable)
                (#.+movement-result-oxygen+ :oxygen)))
        (when video
          (for surface = (viz:create-video-surface video))
          (draw-map map
                    surface
                    :drone-position (if (= move-result +movement-result-wall+)
                                        position
                                        new-position)
                    :full-map full-map)
          (viz:write-video-surface video surface))
        (unless (= move-result +movement-result-wall+)
          (explore-map-step map new-position video full-map)
          (move (vector-direction (point:- vector)))
          (when (and full-map *oxygen-positions*)
            (setf *oxygen-positions* (fill-oxygen-step full-map *oxygen-positions*)))
          (when video
            (for return-surface = (viz:create-video-surface video))
            (draw-map map return-surface :drone-position position :full-map full-map)
            (viz:write-video-surface video return-surface)))))

(defun explore-map (map &optional video full-map)
  (explore-map-step map #(0 0) video full-map))

(defun oxygen-position (map)
  (iter (for (position landscape) in-hashtable map)
        (finding position such-that (eq landscape :oxygen))))

(defun make-dijkstra-options (map &key end allow-unknown)
  (assert (or end allow-unknown))
  (assert (not (and end allow-unknown)))
  (lambda (position)
    (iter (for vector in +cardinal-vectors+)
          (for new-position = (point:+ position vector))
          (for landscape = (gethash new-position map))
          (when (or (eq :traversable landscape)
                    (eq :oxygen landscape)
                    (and end
                         (point:= new-position end))
                    (and allow-unknown
                         (eq :unknown landscape)))
            (collecting (list 1 new-position))))))

(defun make-dijkstra-heuristic (end)
  (lambda (position)
    (point:manhattan-distance position end)))

(defun path-to (map start end)
  (aoc:shortest-path start
                     (make-dijkstra-options map :end end)
                     :end end
                     :heuristic (make-dijkstra-heuristic end)))

(defun get-answer-1 ()
  (intcode:with-rpc (*rpc-object* *input* '((move nil 1 1)))
    (let ((map (init-map)))
      (explore-map map)
      (1- (length (path-to map #(0 0) (oxygen-position map)))))))


;;;; Part 2

(defvar *oxygen-positions*)

(defun fill-oxygen-step (map last-oxygen-positions)
  (iter outer
        (for position in last-oxygen-positions)
        (iter (for vector in +cardinal-vectors+)
              (for new-position = (point:+ position vector))
              (for landscape = (gethash new-position map))
              (when (eq landscape :traversable)
                (set-landscape map new-position :oxygen)
                (in outer (collecting new-position))))))

(defun fill-oxygen (map &optional video)
  (iter (for oxygen-positions first (list (oxygen-position map)) then (fill-oxygen-step map oxygen-positions))
        (when video
          (for surface = (viz:create-video-surface video))
          (draw-map map surface :full-map map)
          (viz:write-video-surface video surface))
        (until (endp oxygen-positions))
        (for steps from 0)
        (finally (return steps))))

(defun get-answer-2 ()
  (intcode:with-rpc (*rpc-object* *input* '((move nil 1 1)))
    (let ((map (init-map)))
      (explore-map map)
      (fill-oxygen map))))


;;;; Visualization

(defun fill-walls (map)
  "Fills in every missing map tile with wall."
  (multiple-value-bind (min-point max-point) (point:bbox (alexandria:hash-table-keys map))
    (iter (for row from (point:elt min-point 1) to (point:elt max-point 1))
          (iter (for col from (point:elt min-point 0) to (point:elt max-point 0))
                (for position = (point:make-point col row))
                (when (not (nth-value 1 (gethash position map)))
                  (setf (gethash position map) :wall))))))

(defparameter *frame-width* 640)
(defparameter *frame-height* 480)

(defun landscape-color (landscape)
  (ecase landscape
    ((nil) :light-secondary)
    (:traversable :light-background)
    (:wall :light-primary)
    (:oxygen :cyan)
    (:unknown :red)
    (:drone :green)))

(defun trans-matrix-for-map (map)
  (multiple-value-bind (min-point max-point) (point:bbox (alexandria:hash-table-keys map))
    (let* ((bbox-size (point:- max-point min-point))
           (map-cell-width (1+ (point:elt bbox-size 0)))
           (map-cell-height (1+ (point:elt bbox-size 1)))
           (trial-pixel-cell-width (truncate *frame-width* map-cell-width))
           (trial-pixel-cell-height (truncate *frame-height* map-cell-height))
           (pixel-cell-size (min trial-pixel-cell-width trial-pixel-cell-height))
           (x-margin (truncate (- *frame-width* (* map-cell-width pixel-cell-size)) 2))
           (y-margin (truncate (- *frame-height* (* map-cell-height pixel-cell-size)) 2))
           (x-offset (* pixel-cell-size (- (point:elt min-point 0))))
           (y-offset (* pixel-cell-size (- (point:elt min-point 1)))))
      (cairo:make-trans-matrix :xx (float pixel-cell-size 1.0d0)
                               :yy (float pixel-cell-size 1.0d0)
                               :x0 (float (+ x-margin x-offset) 1.0d0)
                               :y0 (float (+ y-margin y-offset) 1.0d0)))))

(defun draw-map (map surface &key drone-position full-map step-count)
  (let ((reference-map (or full-map map)))
    (cairo:with-context-from-surface (surface)
      (viz:clear-surface surface (landscape-color nil))
      (when step-count
        (viz:set-color :red)
        (viz:draw-text (format nil "Steps: ~A" step-count)
                       16 16
                       :vertical :top))
      (cairo:set-trans-matrix (trans-matrix-for-map reference-map))
      (iter (for (position landscape) in-hashtable reference-map)
            (for alpha = (if (and full-map
                                  (not (nth-value 1 (gethash position map))))
                             (if (eq landscape :oxygen)
                                 0.3
                                 0.1)
                             1.0))
            (viz:set-color (landscape-color landscape) :alpha alpha)
            (cairo:rectangle (point:elt position 0) (point:elt position 1) 1 1)
            (cairo:fill-path)
            (when (eq (gethash position map) :unknown)
              (viz:set-color (landscape-color :unknown) :alpha 0.3)
              (cairo:rectangle (point:elt position 0) (point:elt position 1) 1 1)
              (cairo:fill-path)))
      (when drone-position
        (viz:set-color (landscape-color :drone))
        (cairo:arc (+ 1/2 (point:elt drone-position 0))
                   (+ 1/2 (point:elt drone-position 1))
                   1/3
                   0
                   (* 2 pi))
        (cairo:fill-path)))))

(defun write-map (map filename)
  (cairo:with-png-file (filename
                        :rgb24
                        *frame-width*
                        *frame-height*
                        :surface surface)
    (draw-map map surface)))

(defun animate-exploration (filename &key (explore-map #'explore-map))
  (viz:with-video (video (viz:create-video filename *frame-width* *frame-height* :fps 30))
    (let ((full-map (init-map))
          (map (init-map))
          (*oxygen-positions* nil))
      (intcode:with-rpc (*rpc-object* *input* '((move nil 1 1)))
        (explore-map full-map)
        (fill-walls full-map))
      (intcode:with-rpc (*rpc-object* *input* '((move nil 1 1)))
        (funcall explore-map map video full-map))
      (iter (while *oxygen-positions*)
            (setf *oxygen-positions* (fill-oxygen-step full-map *oxygen-positions*))
            (for surface = (viz:create-video-surface video))
            (draw-map full-map surface)
            (viz:write-video-surface video surface))
      (let ((surface (viz:create-video-surface video)))
        (draw-map full-map surface)
        (dotimes (i 150)
          (viz:write-video-surface video surface))))))
