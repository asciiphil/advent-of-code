(in-package :aoc-2019-11)

; "LPZKLGHR"
(aoc:define-day 2064 nil)


;;;; Input

(defparameter *test-output* '(1 0 0 0 1 0 1 0 0 1 1 0 1 0))
(defparameter *input* (intcode:load-program (aoc:input)))


;;;; Part 1

(defclass state ()
  ((position :initform 0)
   (direction :initform #C(0 -1))
   (hull-colors :initform (make-hash-table))))

(defun make-state (&optional initial-color)
  (let ((state (make-instance 'state)))
    (when initial-color
      (with-slots (position hull-colors) state
        (setf (gethash position hull-colors) initial-color)))
    state))

(defun make-input-fn (state)
  (with-slots (position hull-colors) state
    (lambda ()
      (gethash position hull-colors 0))))

(defun make-output-fn (state)
  (with-slots (position direction hull-colors) state
    (flet ((paint-panel (color)
             (setf (gethash position hull-colors) color))
           (turn (new-direction)
             (setf direction (* direction
                                (ecase new-direction
                                  (0 #C(0 -1))
                                  (1 #C(0  1)))))
             (incf position direction)))
     (let ((waiting-for :color))
       (lambda (value)
         (ecase waiting-for
           (:color
            (paint-panel value)
            (setf waiting-for :direction))
           (:direction
            (turn value)
            (setf waiting-for :color))))))))

(defun test-output (&optional (output-sequence *test-output*))
  (let* ((state (make-state))
         (output-fn (make-output-fn state)))
    (iter (for n in output-sequence)
          (funcall output-fn n))
    state))

(defun run-program (program &optional initial-color)
  (let ((state (make-state initial-color)))
    (intcode:run program
                 :input-fn (make-input-fn state)
                 :output-fn (make-output-fn state))
    state))

(defun get-answer-1 ()
  (with-slots (hull-colors) (run-program *input*)
    (hash-table-count hull-colors)))


;;;; Part 2

(defun print-hull (state)
  (with-slots (hull-colors) state
    (let ((bbox (aoc:find-bbox (alexandria:hash-table-keys hull-colors))))
      (iter (for row from (aoc:bbox-bottom bbox) to (aoc:bbox-top bbox))
            (iter (for col from (aoc:bbox-left bbox) to (aoc:bbox-right bbox))
                  (princ (if (= 1 (gethash (complex col row) hull-colors 0))
                             #\U2588
                             #\ )))
            (format t "~%"))))
  (values))

(defun get-answer-2 ()
  "Prints the hull rather than returning an answer."
  (print-hull (run-program *input* 1))
  (values))


;;;; Visualization

(defparameter *alternate-output*
  '(0 1
    
    1 1 1 0 0 0 1 1 1 1 0 0 1 0 0 1 0 1 0 0 1 0 1 1 1 1 0 0 0 0 1 1 1 1 0 0 0 0 0 1 1 1 1 0 0 0
    1 1 1 1 0 0 1 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 1 0 1 1 1 1 0 0 0 0 1 1 0 1 1 0
    0 0 0 1 1 1 1 0 0 0 0 1 0 1 0 0 0 0 0 1 1 1 1 0 0 0 0 1 0 1 1 0 0 0 1 1 1 1 0 0 1 0 0 1 0 1
    0 0 1 0 1 1 1 1 0 0 0 0 1 1 0 1 1 0 0 0 0 1 1 1 1 0 0 0 1 1 1 1 0 0 0 0 1 1 0 1 0 0 0 1 0 1
    
    0 0 0 1 0 1 0 0 1 0 0 1 0 1 1 0 1 0 1 1 0 1 0 0 1 0 0 1 1 1 0 0 0 0 1 1 1 1 1 0 0 0 0 1 1 1
    1 0 1 0 0 1 0 1 1 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1
    0 1 0 0 0 0 0 1 1 1 1 0 0 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0 0 1 1 1 1 0 0 0 0 1 0 1 0 0 1 0
    1 1 0 1 0 0 0 0 0 1 0 1 1 0 1 0 0 1 1 1 1 0 0 0 0 1 0 1 1 0 0 0 1 1 1 1 0 0 1 0 1 1 0 0 0 0
    
    1 1 1 0 0 0 0 1 1 1 0 0 1 0 0 1 0 1 0 0 1 0 1 1 0 1 1 0 1 0 0 1 0 1 1 0 0 0 0 1 1 1 1 0 1 0
    0 1 0 1 1 0 0 0 1 1 0 1 0 0 0 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0 1 0 1 1 0 1 1 0 1 0 0 1 1 1 0 0
    0 0 0 1 0 1 0 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0 0 0 0 0 1 1 1 1 0 0 0 0 1 0 1 0 0 1 0 1 1 0 1
    0 0 1 0 1 1 0 1 0 0 0 0 1 1 0 1 1 0 0 0 0 1 1 1 1 0 0 0 0 1 0 1 0 0 0 0 0 1 0 1 0 0))

(defparameter *frame-width* 1280)
(defparameter *frame-height* 720)
(defparameter *pixels-per-frame* 24)

(defstruct drawing-param
  bbox
  matrix
  frames)

(defun state-bbox (state &key with-next-position)
  (let ((tiles (cons (slot-value state 'position)
                     (alexandria:hash-table-keys (slot-value state 'hull-colors)))))
    (when with-next-position
      (push (+ (slot-value state 'position) (slot-value state 'direction)) tiles))
    (aoc:find-bbox tiles)))

(defun trans-matrix-for-bbox (bbox)
  (let* ((bbox-width (1+ (- (aoc:bbox-right bbox) (aoc:bbox-left bbox))))
         (bbox-height (1+ (- (aoc:bbox-top bbox) (aoc:bbox-bottom bbox))))
         (trial-cell-width (truncate *frame-width* (+ 2 bbox-width)))
         (trial-cell-height (truncate *frame-height* (+ 2 bbox-height)))
         (cell-size (min trial-cell-width trial-cell-height))
         (x0 (floor (- (/ *frame-width* 2) (* cell-size (+ (/ bbox-width 2) (aoc:bbox-left bbox))))))
         (y0 (floor (- (/ *frame-height* 2) (* cell-size (+ (/ bbox-height 2) (aoc:bbox-bottom bbox)))))))
    ;; `make-trans-matrix' requires doubles as parameters, so we have to
    ;; cast our integers appropriately before passing them.
    (cairo:make-trans-matrix :xx (float cell-size 1.0d0)
                             :yy (float cell-size 1.0d0)
                             :x0 (float x0 1.0d0)
                             :y0 (float y0 1.0d0))))

(defun make-param-for-state (state &key with-next-position)
  (let* ((bbox (state-bbox state :with-next-position with-next-position))
         (matrix (trans-matrix-for-bbox bbox))
         (frames (max 1 (truncate (cairo:trans-matrix-xx matrix) *pixels-per-frame*))))
    (make-drawing-param :bbox bbox :matrix matrix :frames frames)))

(defun list-transition-params (start end steps)
  (let ((start-matrix (drawing-param-matrix start))
        (end-matrix (drawing-param-matrix end)))
    (let ((start-xx (cairo:trans-matrix-xx start-matrix))
          (start-yy (cairo:trans-matrix-yy start-matrix))
          (start-x0 (cairo:trans-matrix-x0 start-matrix))
          (start-y0 (cairo:trans-matrix-y0 start-matrix))
          (end-xx (cairo:trans-matrix-xx end-matrix))
          (end-yy (cairo:trans-matrix-yy end-matrix))
          (end-x0 (cairo:trans-matrix-x0 end-matrix))
          (end-y0 (cairo:trans-matrix-y0 end-matrix)))
      (iter (for step from 1 to steps)
            (collecting
              (make-drawing-param
               :bbox (drawing-param-bbox end)
               :matrix (cairo:make-trans-matrix
                        :xx (alexandria:lerp (/ step steps) start-xx end-xx)
                        :yy (alexandria:lerp (/ step steps) start-yy end-yy)
                        :x0 (alexandria:lerp (/ step steps) start-x0 end-x0)
                        :y0 (alexandria:lerp (/ step steps) start-y0 end-y0))
               :frames (drawing-param-frames end)))))))

(defun draw-paint (state surface param &key transition)
  (viz:clear-surface surface :dark-background)
  (let ((padding (if transition
                     (/ 1/2 (cairo:trans-matrix-xx (drawing-param-matrix param)))
                     0)))
    (with-slots (hull-colors) state
      (cairo:with-context-from-surface (surface)
        (cairo:set-trans-matrix (drawing-param-matrix param))
        (viz:set-color :dark-primary)
        (iter (for (cell-pos color) in-hashtable hull-colors)
              (when (= 1 color)
                (cairo:rectangle (- (realpart cell-pos) padding)
                                 (- (imagpart cell-pos) padding)
                                 (+ 1 (* 2 padding))
                                 (+ 1 (* 2 padding)))
                (cairo:fill-path)))))))

(defun draw-robot (state surface param &key (translation 0) (rotation 0))
  (with-slots (position direction) state
    (cairo:with-context-from-surface (surface)
      (cairo:set-trans-matrix (drawing-param-matrix param))
      (viz:set-color :green)
      (cairo:translate (+ 1/2
                          (realpart position)
                          (* translation (realpart direction)))
                       (+ 1/2
                          (imagpart position)
                          (* translation (imagpart direction))))
      (cairo:rotate (+ rotation (atan (imagpart direction) (realpart direction))))
      (cairo:move-to 1/2 0)
      (cairo:line-to -1/2 1/2)
      (cairo:line-to -1/2 -1/2)
      (cairo:close-path)
      (cairo:fill-path))))

(defun draw-tile (surface param position color alpha)
    (cairo:with-context-from-surface (surface)
      (cairo:set-trans-matrix (drawing-param-matrix param))
      (viz:set-color color :alpha alpha)
      (cairo:rectangle (realpart position) (imagpart position) 1 1)
      (cairo:fill-path)))

(defun paint-tile (state video param color)
  (with-slots (position hull-colors) state
    (when (/= (gethash position hull-colors 0) color)
      (let ((base-surface (viz:create-video-surface video)))
        (draw-paint state base-surface param)
        (iter (for i from 1 to (drawing-param-frames param))
              (for alpha = (/ i (drawing-param-frames param)))
              (for working-surface = (viz:clone-surface base-surface))
              (draw-tile working-surface
                         param
                         position
                         (ecase color
                           (0 :dark-background)
                           (1 :dark-primary))
                         alpha)
              (draw-robot state working-surface param)
              (viz:write-video-surface video working-surface))))))

(defun rotate-robot (state video param rotation)
  (let ((base-surface (viz:create-video-surface video)))
    (draw-paint state base-surface param)
    (iter (for i from 1 to (drawing-param-frames param))
          (with angle-delta = (/ rotation (drawing-param-frames param)))
          (for angle = (* i angle-delta))
          (for working-surface = (viz:clone-surface base-surface))
          (draw-robot state working-surface param :rotation angle)
          (viz:write-video-surface video working-surface))))

(defun move-robot (state video param &key new-param)
  (with-slots (position direction) state
    (let ((new-param-real (or new-param param)))
      (iter (for transition-param in (list-transition-params param new-param-real
                                                             (drawing-param-frames new-param-real)))
            (for i from 1)
            (for translation = (/ i (drawing-param-frames transition-param)))
            (for working-surface = (viz:create-video-surface video))
            (draw-paint state working-surface transition-param :transition new-param)
            (draw-robot state working-surface transition-param :translation translation)
            (viz:write-video-surface video working-surface)))))

(defun make-drawing-output-fn (state video)
  (with-slots (position direction hull-colors) state
    (let* ((param (make-param-for-state state)))
      (flet ((paint-panel (color)
               (paint-tile state video param color)
               (setf (gethash position hull-colors) color))
             (turn (new-direction)
               (rotate-robot state video param (ecase new-direction
                                                 (0 (- (/ pi 2)))
                                                 (1 (/ pi 2))))
               (setf direction (* direction
                                  (ecase new-direction
                                    (0 #C(0 -1))
                                    (1 #C(0  1)))))
               (let ((new-bbox (state-bbox state :with-next-position t)))
                 (if (not (equalp new-bbox (drawing-param-bbox param)))
                     (let ((new-param (make-param-for-state state :with-next-position t)))
                       (move-robot state video param :new-param new-param)
                       (setf param new-param))
                     (move-robot state video param)))
               (incf position direction)))
        (let ((waiting-for :color))
          (lambda (value)
            (ecase waiting-for
              (:color
               (paint-panel value)
               (setf waiting-for :direction))
              (:direction
               (turn value)
               (setf waiting-for :color)))))))))

(defun draw-program (program filename &optional initial-color)
  (let ((state (make-state initial-color)))
    (viz:with-video (video (viz:create-video filename *frame-width* *frame-height*))
      (intcode:run program
                   :input-fn (make-input-fn state)
                   :output-fn (make-drawing-output-fn state video))
      (let ((surface (viz:create-video-surface video)))
        (draw-paint state surface (make-param-for-state state))
        (iter (repeat 125)
              (viz:write-video-surface video surface))))))

(defun draw-test-data (test-data filename &optional initial-color)
  (viz:with-video (video (viz:create-video filename *frame-width* *frame-height*))
    (let* ((state (make-state initial-color))
           (output-fn (make-drawing-output-fn state video)))
      (iter (for value in test-data)
            (funcall output-fn value))
      (let ((surface (viz:create-video-surface video)))
        (draw-paint state surface (make-param-for-state state))
        (iter (repeat 125)
              (viz:write-video-surface video surface))))))
