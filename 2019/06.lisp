(in-package :aoc-2019-06)

(aoc:define-day 261306 382)


;;; Input

(defparameter *example-1* '("COM)B"
                            "B)C"
                            "C)D"
                            "D)E"
                            "E)F"
                            "B)G"
                            "G)H"
                            "D)I"
                            "E)J"
                            "J)K"
                            "K)L"))
(defparameter *example-2* '("COM)B"
                            "B)C"
                            "C)D"
                            "D)E"
                            "E)F"
                            "B)G"
                            "G)H"
                            "D)I"
                            "E)J"
                            "J)K"
                            "K)L"
                            "K)YOU"
                            "I)SAN"))
(defparameter *input* (aoc:input))


;;; Part 1

(defun build-graph (orbits)
  (let ((graph (make-instance 'graph:digraph)))
    (iter (for orbit in orbits)
          (for (source target) = (ppcre:split "\\)" orbit))
          (graph:add-edge graph (list (intern source) (intern target))))
    graph))

(defun count-orbits (orbits)
  (let ((graph (build-graph orbits)))
    (labels ((count-r (node)
               (let ((neighbors (graph:neighbors graph node)))
                 (if (endp neighbors)
                     (values 0 1)
                     (iter (for n in neighbors)
                           (for (values edges nodes) = (count-r n))
                           (summing (+ nodes edges) into my-edges)
                           (summing nodes into my-nodes)
                           (finally (return (values my-edges (1+ my-nodes)))))))))
      (count-r 'COM))))

(aoc:given 1
  (= 42 (count-orbits *example-1*)))

(defun get-answer-1 ()
  (count-orbits *input*))


;;; Part 2

(defun count-transfers (orbits)
  (- (nth-value 1 (graph:shortest-path (graph:graph-of (build-graph orbits))
                                       'YOU 'SAN))
     2))

(aoc:given 2
  (= 4 (count-transfers *example-2*)))

(defun get-answer-2 ()
  (count-transfers *input*))


;;; Visualization

(defun furthest-node (digraph node)
  (let ((neighbors (graph:neighbors digraph node)))
    (if (endp neighbors)
        (values node 0)
        (iter (for n in neighbors)
              (for (values distant-node distance) = (furthest-node digraph n))
              (finding distant-node maximizing distance into most-distant-node)
              (maximizing distance into longest-distance)
              (finally (return (values most-distant-node (1+ longest-distance))))))))

(defun orbit-radius (digraph node)
  "At what radius from NODE do its children orbit?"
  (let ((neighbors (graph:neighbors digraph node)))
    (if (endp neighbors)
        1/2
        (apply #'max (mapcar (lambda (n) (* 2 (1+ (orbit-radius digraph n)))) neighbors)))))

(defparameter *image-width* 1024)
(defparameter *history-alpha* 0.15)

(defun draw-orbit (graph node scale distance selected-nodes)
  (let ((radius (orbit-radius graph node))
        (neighbors (graph:neighbors graph node)))
    (when (or (endp selected-nodes)
              (member node selected-nodes))
     (viz:set-color (case node
                      (SAN :red)
                      (YOU :green)
                      (COM :dark-emphasized)
                      (t :dark-primary)))
     (let ((obj-radius (case node
                         (SAN (/ 2 scale))
                         (YOU (/ 2 scale))
                         (COM (/ 4 scale))
                         (t (/ 1 scale)))))
       (cairo:arc 0 0 obj-radius 0 (* 2 pi)))
     (cairo:fill-path))
    (unless (endp neighbors)
      (iter (for n in neighbors)
            (for phi from 0 by (/ (* 2 pi) (length neighbors)))
            (cairo:save)
            (cairo:rotate (/ distance radius))
            (cairo:translate radius 0)
            (draw-orbit graph n scale distance selected-nodes)
            (cairo:restore)))))

(defun draw-orbits (orbits surface distance &key (reuse-surface-p nil))
  "DISTANCE gives how far the planet has traveled around its orbit."
  (let* ((graph (build-graph orbits))
         (scale (/ *image-width* (* 4 (orbit-radius graph 'COM)))))
    (cairo:with-context-from-surface (surface)
      (unless reuse-surface-p
        (viz:set-color :dark-background)
        (cairo:rectangle 0 0 *image-width* *image-width*)
        (cairo:fill-path))

      (cairo:translate (/ *image-width* 2) (/ *image-width* 2))
      (cairo:scale scale scale)
      (cairo:set-line-width (/ scale))

      (draw-orbit graph 'COM scale distance nil)
      (draw-orbit graph 'COM scale distance '(COM YOU))
      (draw-orbit graph 'COM scale distance '(SAN)))))

(defun animate-orbits (orbits filename &key (fps 30) (duration 12) (rotations 1))
  (setf cl-progress-bar:*progress-bar-enabled* t)
  (viz:with-video (video (viz:create-video filename *image-width* *image-width*
                                           :fps fps))
    (cl-progress-bar:with-progress-bar ((1+ (* duration fps)) "Generating frames")
      (iter (with outer-circumference = (* 2 pi
                                           (orbit-radius (build-graph orbits) 'COM)))
            (for distance from 0
                          to (* rotations outer-circumference)
                          by (/ outer-circumference (/ (* duration fps) rotations)))
            (with surface = (viz:create-video-surface video))
            (if-first-time
             (cairo:with-context-from-surface (surface)
               (viz:set-color :dark-background)
               (cairo:rectangle 0 0 *image-width* *image-width*)
               (cairo:fill-path))
             (cairo:with-context-from-surface (surface)
               (viz:set-color :dark-background :alpha *history-alpha*)
               (cairo:rectangle 0 0 *image-width* *image-width*)
               (cairo:fill-path)))
            (draw-orbits orbits surface distance :reuse-surface-p t)
            (viz:write-video-surface video surface)
            (cl-progress-bar:update 1)))))
