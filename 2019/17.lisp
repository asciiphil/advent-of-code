(in-package :aoc-2019-17)

(aoc:define-day 3192 684691)


;;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))



;;;; Part 1

(defconstant +scaffold-code+ 35)

(defparameter +cardinal-vectors+ (list #(0 1) #(0 -1) #(1 0) #(-1 0)))

(defun print-view (view)
  (iter (for char in view)
        (princ (code-char char)))
  (values))

(defun view-to-grid (view)
  (let ((grid (make-hash-table :test 'equalp)))
    (iter (with row = 0)
          (with col = 0)
          (for char in view)
          (if (= char 10)
              (setf row (1+ row)
                    col 0)
              (psetf (gethash (point:make-point col row) grid) char
                     col (1+ col))))
    grid))

(defun intersection-p (grid point)
  (let ((reference-value (gethash point grid)))
    (and (= reference-value +scaffold-code+)
         (iter (for vector in +cardinal-vectors+)
               (for this-value = (gethash (point:+ point vector) grid -1))
               (always (= reference-value this-value))))))

(defun list-intersections (grid)
  (multiple-value-bind (min-point max-point) (point:bbox (alexandria:hash-table-keys grid))
    (iter outer
          (for row from (point:elt min-point 1) to (point:elt max-point 1))
          (iter (for col from (point:elt min-point 0) to (point:elt max-point 0))
                (for point = (point:make-point col row))
                (when (intersection-p grid point)
                  (in outer (collecting point)))))))

(defun print-grid (grid)
  (multiple-value-bind (min-point max-point) (point:bbox (alexandria:hash-table-keys grid))
    (iter (for row from (point:elt min-point 1) to (point:elt max-point 1))
          (iter (for col from (point:elt min-point 0) to (point:elt max-point 0))
                (for point = (point:make-point col row))
                (if (intersection-p grid point)
                    (princ #\O)
                    (princ (code-char (gethash point grid)))))
          (format t "~%"))))

(defun alignment-parameter (point)
  (* (point:elt point 0) (point:elt point 1)))

(defun get-answer-1 ()
  (reduce #'+
          (mapcar #'alignment-parameter
                  (list-intersections (view-to-grid (intcode:run *input*))))))


;;;; Part 2

;;; L,4,R,8,L,6,L,10, L,6,R,8,R,10,L,6,L,6,
;;; L,4,R,8,L,6,L,10, L,6,R,8,R,10,L,6,L,6, L,4,L,4,L,10,
;;;                                         L,4,L,4,L,10,
;;;                   L,6,R,8,R,10,L,6,L,6,
;;; L,4,R,8,L,6,L,10, L,6,R,8,R,10,L,6,L,6, L,4,L,4,L,10
;;; 
;;; A = L,4,R,8,L,6,L,10
;;; B = L,6,R,8,R,10,L,6,L,6
;;; C = L,4,L,4,L,10
;;; 
;;; A,B,A,B,C,C,B,A,B,C

(defparameter *movement-A* "L,4,R,8,L,6,L,10")
(defparameter *movement-B* "L,6,R,8,R,10,L,6,L,6")
(defparameter *movement-C* "L,4,L,4,L,10")
(defparameter *main-movement* "A,B,A,B,C,C,B,A,B,C")

(defun input-list (video-feed-p)
  (let ((input-string (format nil "~{~A~%~}~A~%"
                              (list *main-movement*
                                    *movement-A*
                                    *movement-B*
                                    *movement-C*)
                              (if video-feed-p
                                  "y"
                                  "n"))))
    (iter (for char in-string input-string)
          (collecting (char-code char)))))

(defun get-answer-2 ()
  (let ((program (copy-seq *input*)))
    (setf (svref program 0) 2)
    (car (last (intcode:run program :input-list (input-list nil))))))
