(in-package :aoc-2019-12)

(aoc:define-day 7179 428576638953552)


;;;; Input

(defclass moon ()
  ((position
    :initarg :position
    :reader moon-position)
   (velocity
    :initarg :velocity
    :reader moon-velocity)))

(defmethod print-object ((moon moon) stream)
  (with-slots (position velocity) moon
    (format stream "#<MOON P:~A V:~A>" position velocity)))

(defun moon= (moon-a moon-b)
  (and (point:= (moon-position moon-a) (moon-position moon-b))
       (point:= (moon-velocity moon-a) (moon-velocity moon-b))))

(defun moons= (moon-list-a moon-list-b)
  (iter (for moon-a in moon-list-a)
        (for moon-b in moon-list-b)
        (always (moon= moon-a moon-b))))

;;; This is so we can throw moon objects into hash tables.
(defun sxhash-moon (moon)
  (sxhash (list (moon-position moon) (moon-velocity moon))))
(sb-ext:define-hash-table-test moon= sxhash-moon)
(defun sxhash-moons (moons)
  (sxhash (mapcar #'sxhash-moon moons)))
(sb-ext:define-hash-table-test moons= sxhash-moons)

(defun make-moon (position &optional velocity)
  (make-instance 'moon :position position :velocity (or velocity (point:make-point 0 0 0))))

(defun parse-input (strings)
  (iter (for string in strings)
        (collecting (make-moon (apply #'point:make-point (aoc:extract-ints string))))))

(defparameter *example-1a* (parse-input '("<x=-1, y=0, z=2>"
                                          "<x=2, y=-10, z=-7>"
                                          "<x=4, y=-8, z=8>"
                                          "<x=3, y=5, z=-1>")))
(defparameter *example-2a* (parse-input '("<x=-8, y=-10, z=0>"
                                          "<x=5, y=5, z=10>"
                                          "<x=2, y=-7, z=3>"
                                          "<x=9, y=-8, z=-3>")))
(defparameter *input* (parse-input (aoc:input)))


;;;; Part 1

(defun apply-gravity (moons)
  (labels ((gravity-pair (a b)
             (cond
               ((< a b)  1)
               ((= a b)  0)
               ((> a b) -1))))
    (iter (for moon in moons)
          (for velocity-delta =
               (iter (for other-moon in moons)
                     (reducing (point:map-coordinates #'gravity-pair
                                                      (list
                                                       (moon-position moon)
                                                       (moon-position other-moon)))
                               by #'point:+)))
          (collecting (make-moon (moon-position moon)
                                 (point:+ (moon-velocity moon)
                                          velocity-delta))))))

(defun apply-velocity (moons)
  (iter (for moon in moons)
        (collecting (make-moon (point:+ (moon-position moon)
                                        (moon-velocity moon))
                               (moon-velocity moon)))))

(defun simulate-step (moons)
  (apply-velocity (apply-gravity moons)))

(defun simulate (moons steps)
  (iterate (for ms first moons then (simulate-step ms))
           (repeat steps)
           (finally (return ms))))

(defun energy (point)
  (reduce (lambda (a b) (+ (abs a) (abs b))) (point:list-coordinates point)))

(defun potential-energy (moon)
  (energy (moon-position moon)))

(defun kinetic-energy (moon)
  (energy (moon-velocity moon)))

(defun total-energy (moon)
  (* (potential-energy moon) (kinetic-energy moon)))

(defun system-total-energy (moons)
  (reduce (lambda (sum moon) (+ sum (total-energy moon))) moons :initial-value 0))

(aoc:given 1
  (= 179 (system-total-energy (simulate *example-1a* 10))))

(defun get-answer-1 ()
  (system-total-energy (simulate *input* 1000)))


;;;; Part 2

(defun slice-coordinate (index moon)
  (make-moon (point:make-point (point:elt (moon-position moon) index))
             (point:make-point (point:elt (moon-velocity moon) index))))

(defun find-cycle (moons)
  "Returns two integers: the cycle start index, and the cycle length"
  (iter (for current-moons first moons then (simulate-step current-moons))
        (for step from 0)
        (until (and (plusp step) (moons= moons current-moons)))
        (finally (return (values 0 step)))))

(defun get-answer-2 (&optional (moons *input*))
  (iter (for i from 0 to 2)
        (for (values cycle-start cycle-length) = (find-cycle (mapcar (alexandria:curry #'slice-coordinate i) moons)))
        (assert (zerop cycle-start))
        (collecting cycle-length into lengths)
        (finally (return (apply #'lcm lengths)))))

(aoc:given 2
  (= 2772 (get-answer-2 *example-1a*))
  (= 4686774924 (get-answer-2 *example-2a*)))
