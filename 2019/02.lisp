(in-package :aoc-2019-02)

(aoc:define-day 2692315 9507)


;;; Input

(defparameter *input* (intcode:load-program (aoc:input)))


;;; Part 1

;; Names taken from part 2.
(defun fix-program (noun verb)
  (let ((program (copy-seq *input*)))
    (setf (svref program 1) noun)
    (setf (svref program 2) verb)
    program))

(defun get-answer-1 ()
  (aref (intcode:run (fix-program 12 2) :return-memory t)
        0))


;;; Part 2

(defun find-noun-and-verb (target-output)
  (iter outer
        (for noun from 0 to 99)
        (iter (for verb from 0 to 99)
              (for program = (fix-program noun verb))
              (when (= target-output
                       (aref (intcode:run program :return-memory t) 0))
                (return-from outer (values noun verb))))))

(defun get-answer-2 ()
  (multiple-value-bind (noun verb)
      (find-noun-and-verb 19690720)
    (+ (* 100 noun)
       verb)))
