(in-package :visualization)


;;;; Colors (Solarized)

(defparameter *color-base03* (multiple-value-list (dufy:lab-to-xyz 15 -12 -12)))
(defparameter *color-base02* (multiple-value-list (dufy:lab-to-xyz 20 -12 -12)))
(defparameter *color-base01* (multiple-value-list (dufy:lab-to-xyz 45 -07 -07)))
(defparameter *color-base00* (multiple-value-list (dufy:lab-to-xyz 50 -07 -07)))
(defparameter *color-base0*  (multiple-value-list (dufy:lab-to-xyz 60 -06 -03)))
(defparameter *color-base1*  (multiple-value-list (dufy:lab-to-xyz 65 -05 -02)))
(defparameter *color-base2*  (multiple-value-list (dufy:lab-to-xyz 92 -00  10)))
(defparameter *color-base3*  (multiple-value-list (dufy:lab-to-xyz 97  00  10)))
(defparameter *color-dark-background* *color-base03*)
(defparameter *color-dark-highlight*  *color-base02*)
(defparameter *color-dark-secondary*  *color-base01*)
(defparameter *color-dark-primary*    *color-base0*)
(defparameter *color-dark-emphasized* *color-base1*)
(defparameter *color-light-background* *color-base3*)
(defparameter *color-light-highlight*  *color-base2*)
(defparameter *color-light-secondary*  *color-base1*)
(defparameter *color-light-primary*    *color-base00*)
(defparameter *color-light-emphasized* *color-base01*)

(defparameter *color-yellow*  (multiple-value-list (dufy:lab-to-xyz 60  10  65)))
(defparameter *color-orange*  (multiple-value-list (dufy:lab-to-xyz 50  50  55)))
(defparameter *color-red*     (multiple-value-list (dufy:lab-to-xyz 50  64  45)))
(defparameter *color-magenta* (multiple-value-list (dufy:lab-to-xyz 50  65 -05)))
(defparameter *color-violet*  (multiple-value-list (dufy:lab-to-xyz 50  15 -45)))
(defparameter *color-blue*    (multiple-value-list (dufy:lab-to-xyz 55 -10 -45)))
(defparameter *color-cyan*    (multiple-value-list (dufy:lab-to-xyz 60 -35 -05)))
(defparameter *color-green*   (multiple-value-list (dufy:lab-to-xyz 60 -20  65)))

(defun color (keyword)
  (symbol-value (find-symbol (format nil "*COLOR-~A*" keyword)
                             (find-package :viz))))

(defun set-color (keyword &key (alpha 1))
  (multiple-value-bind (r g b)
      (apply #'dufy:xyz-to-rgb (color keyword))
    (cairo:set-source-rgba r g b alpha)))

(defun color-hex (keyword)
  (format nil "#~{~2,'0X~}" (multiple-value-list (apply #'dufy:xyz-to-qrgb (color keyword)))))


;;;; Color Ranges (ColorBrewer2)

(defparameter *color-range-GnBu* (list (multiple-value-list (dufy:qrgb-to-xyz 247 252 240))
                                       (multiple-value-list (dufy:qrgb-to-xyz   8  64 129))))

(defun color-range (keyword-or-list)
  (if (symbolp keyword-or-list)
      (symbol-value (find-symbol (format nil "*COLOR-RANGE-~A*" keyword-or-list)
                                 (find-package :viz)))
      (mapcar #'color keyword-or-list)))

(defun make-color-func (keyword min max &optional (scale :linear))
  "Returns a function of one argument.  The function will return a color
  adjusted linearly between MIN and MAX according to the color scheme
  given by KEYWORD."
  (destructuring-bind (start end) (color-range keyword)
    (multiple-value-bind (start-l start-a start-b) (apply #'dufy:xyz-to-lab start)
      (multiple-value-bind (end-l end-a end-b) (apply #'dufy:xyz-to-lab end)
        (let ((l-range (- end-l start-l))
              (a-range (- end-a start-a))
              (b-range (- end-b start-b)))
          (labels ((apply-scale (value range)
                     (ecase scale
                       (:linear
                        (* range (/ (- value min) (- max min))))
                       (:logarithmic
                        (* range (log (- value (1- min)) (- max min)))))))
            (lambda (value)
              (multiple-value-list
               (multiple-value-call #'dufy:xyz-to-rgb
                 (dufy:lab-to-xyz (coerce (+ start-l (apply-scale value l-range)) 'double-float)
                                  (coerce (+ start-a (apply-scale value a-range)) 'double-float)
                                  (coerce (+ start-b (apply-scale value b-range)) 'double-float)))))))))))


;;;; Color Distributions

;;; Algorithm for making the set of distributed colors is from Annex 2 of:
;;;
;;; Glasbey, C., van der Heijden, G., Toh, A., and Gray, V.F.K. (2007), Colour displays for categorical images.
;;; Color Res. Appl., 32: 304-309. doi:10.1002/col.20327
;;;
;;; https://strathprints.strath.ac.uk/30312/1/colorpaper_2006.pdf
;;;
;;; Functions related to this paper will have the prefix "glasbey-"

(defparameter *glasbey-iterations* 100)

(defun glasbey-new-color-rule-1 (luminance)
  (list luminance (- (random 199) 99) (- (random 199) 99)))

(defun glasbey-new-color-rule-2 (old-color)
  (destructuring-bind (l a b) old-color
    (list l (+ a (- (random 5) 2)) (+ b (- (random 5) 2)))))

(defun glasbey-random-new-color (old-color iteration)
  (if (< (float (/ iteration *glasbey-iterations*)) (random 1.0))
      (glasbey-new-color-rule-1 (first old-color))
      (glasbey-new-color-rule-2 old-color)))

(defun glasbey-minimum-distance (color other-colors)
  (iter (with (l a b) = color)
        (for other-color in other-colors)
        (when (equal color other-color)
          (next-iteration))
        (for (ol oa ob) = other-color)
        (minimizing (sqrt (+ (expt (- ol l) 2)
                             (expt (- oa a) 2)
                             (expt (- ob b) 2))))))

(defun glasbey-maybe-replace-color (old-color old-min-distance colors iteration temperature)
  (let* ((new-color (glasbey-random-new-color old-color iteration))
         (new-min-distance (glasbey-minimum-distance new-color colors))
         (new-color-probability (if (<= old-min-distance new-min-distance)
                                    1.0
                                    (min 1.0 (exp (/ (- new-min-distance old-min-distance)
                                                     temperature))))))
    (if (< (random 1.0) new-color-probability)
        new-color
        old-color)))

(defun glasbey-random-color-list (count luminance)
  (if (zerop count)
      nil
      (cons (glasbey-new-color-rule-1 luminance)
            (glasbey-random-color-list (1- count)  luminance))))

(defun glasbey-find-minimum-color-distance (colors)
  (iter (for color in colors)
        (for distance = (glasbey-minimum-distance color colors))
        (finding (list color distance) minimizing distance)))

(defun glasbey-anneal (count luminance)
  (iter (for iteration from 1 to *glasbey-iterations*)
        (for temperature first 10.0 then (* temperature 0.9))
        (for colors first (glasbey-random-color-list count luminance)
                    then (cons new-color color-subset))
        (for (worst-color worst-distance) = (glasbey-find-minimum-color-distance colors))
        (for color-subset = (remove worst-color colors))
        (for new-color = (glasbey-maybe-replace-color
                          worst-color worst-distance color-subset iteration temperature))
        (finally (return colors))))

(defun make-category-colors (count &key (luminance 50))
  "Returns a list of COUNT colors that are all reasonably distinct from each other..

  Each color is a list of three floats between 0 and 1 in the sRGB color
  space.  (They're suitable to be passed to `cairo:set_source_rgb' and
  company.)"
  (iter (for (lstar astar bstar) in (glasbey-anneal count luminance))
        (for (values r g b) = (multiple-value-call #'dufy:xyz-to-rgb (dufy:lab-to-xyz lstar astar bstar)))
        (collecting (list (alexandria:clamp r 0.0 1.0)
                          (alexandria:clamp g 0.0 1.0)
                          (alexandria:clamp b 0.0 1.0)))))

;;;; Utilities

(defun clear-surface (surface color &optional (alpha 1))
  (cairo:with-context-from-surface (surface)
    (set-color color)
    (cairo:rectangle 0 0
                     (cairo:image-surface-get-width surface)
                     (cairo:image-surface-get-height surface))
    (cairo:paint-with-alpha alpha)))

(defun clone-surface (surface)
  (let ((new-surface (cairo:create-image-surface
                      (cairo:image-surface-get-format surface)
                      (cairo:image-surface-get-width surface)
                      (cairo:image-surface-get-height surface))))
    (cairo:with-context-from-surface (new-surface)
      (cairo:set-source-surface surface 0 0)
      (cairo:rectangle 0 0
                       (cairo:image-surface-get-width surface)
                       (cairo:image-surface-get-height surface))
      (cairo:set-operator :source)
      (cairo:fill-path))
    new-surface))

(defun trans-matrix-for-points (points canvas-width canvas-height &key (margin 0))
  "POINTS is a collection of POINT objects.  Returns a Cairo transform
  matrix to make unit squares for each point fill the area, with MARGIN
  space around them."
  (multiple-value-bind (min-point max-point) (point:bbox points)
    (let* ((available-width (- canvas-width (* 2 margin)))
           (available-height (- canvas-height (* 2 margin)))
           (bbox-span (point:+ (point:- max-point min-point)
                               (point:make-point 1 1)))
           (trial-cell-width (/ available-width (+ 2 (point:elt bbox-span 0))))
           (trial-cell-height (/ available-height (+ 2 (point:elt bbox-span 1))))
           (cell-size (min trial-cell-width trial-cell-height))
           (x-margin (floor (- (/ available-width 2) (* cell-size (/ (point:elt bbox-span 0) 2)))))
           (y-margin (floor (- (/ available-height 2) (* cell-size (/ (point:elt bbox-span 1) 2)))))
           (x-offset (* cell-size (- (point:elt min-point 0))))
           (y-offset (* cell-size (- (point:elt min-point 1)))))
      ;; `make-trans-matrix' requires doubles as parameters, so we have to
      ;; cast our integers appropriately before passing them.
      (cairo:make-trans-matrix :xx (float cell-size 1.0d0)
                               :yy (float cell-size 1.0d0)
                               :x0 (float (+ margin x-margin x-offset) 1.0d0)
                               :y0 (float (+ margin y-margin y-offset) 1.0d0)))))


;;; Text Drawing

(defun text-extents (object size)
  (cairo:with-surface (surface (cairo:create-image-surface :rgb24 1 1))
    (cairo:with-context-from-surface (surface)
      (cairo:set-font-size size)
      (cairo:text-extents (format nil "~A" object)))))

(defun text-bbox (object &key (text-size 10))
  "Returns two values: width and height."
  (multiple-value-bind (x-bearing y-bearing width height) (text-extents object text-size)
    (declare (ignore x-bearing y-bearing))
    (values width height)))

(defun draw-text (object x y &key (size 10) (horizontal :left) (vertical :baseline))
  (multiple-value-bind (x-bearing y-bearing width height) (text-extents object size)
    (let ((real-x (ecase horizontal
                    (:left (- x x-bearing))
                    (:center (- x (/ width 2) x-bearing))
                    (:right (- x width x-bearing))))
          (real-y (ecase vertical
                    (:top (- y y-bearing))
                    (:middle (- y (/ height 2) y-bearing))
                    (:baseline y)
                    (:bottom (- y height y-bearing)))))
      (cairo:move-to real-x real-y)
      (cairo:set-font-size size)
      (cairo:show-text (format nil "~A" object)))))


;;; Video

(defstruct video
  process
  width
  height)

(defmacro with-video ((video-name video) &body body)
  `(let ((,video-name ,video))
     (unwind-protect
          (progn ,@body)
       (finish-video ,video-name))))

(defun create-video (filename width height &key (fps 25))
  "Begin the process of creating a video.  Returns a video object to be
  passed to other video functions.

  To make a video, call `create-video' with your preferred parameters and
  keep the returned video object.  Next, call `create-video-surface' to
  get a Cairo surface compatible with the video.  Once you've drawn on the
  surface to your liking, pass it (and the video object) to
  `write-video-surface'.  That writes the current state of the surface as
  the next frame of the video.  When you're done, call `finish-video' to
  finish the process.  The video file might exist before calling
  `finish-video', but it won't be usable until `finish-video' returns.

  For your convenience, you can use the `with-video' macro, which
  automatically calls `finish-video' when it's done.

  The created video will be H.264 video in an MPEG-4 container with no
  audio stream."
  (declare (type (integer 0) width height))
  (let* ((ffmpeg-args (list "-loglevel" "error"
                            "-nostats"
                            "-nostdin"
                            
                            "-f" "rawvideo"
                            "-s" (format nil "~Dx~D" width height)
                            "-pix_fmt" "bgra"
                            "-r" (format nil "~D" fps)
                            "-i" "-"
                            
                            "-an"
                            
                            "-f" "mp4"
                            "-c:v" "libx264"
                            "-preset" "veryslow"
                            "-crf" "17"
                            "-tune" "animation"
                            "-pix_fmt" "yuv420p"
                            "-profile:v" "main"
                            "-level" "3.1"
                            
                            "-y"
                            filename))
         (ffmpeg-process (external-program:start "ffmpeg" ffmpeg-args
                                                 :input :stream :output t :error :output)))
    (make-video :process ffmpeg-process :width width :height height)))

(defun create-video-surface (video)
  "Returns a Cairo surface that can be used as a frame for VIDEO."
  (cairo:create-image-surface :argb32 (video-width video) (video-height video)))

(defun write-video-surface (video surface)
  "Takes the provided surface and writes it as the next frame in VIDEO.

  Only pass surfaces created by `create-video-surface' for this video!
  Other surfaces might not be compatible with the video."
  (when (not (eq :argb32 (cairo:image-surface-get-format surface)))
    (error "Incorrect surface format for this video."))
  (when (or (/= (video-width video) (cairo:image-surface-get-width surface))
            (/= (video-height video) (cairo:image-surface-get-height surface)))
    (error "Incorrect surface dimensions for this video."))
  (let ((video-fd (sb-sys:fd-stream-fd (external-program:process-input-stream (video-process video))))
        (data-pointer (cairo:image-surface-get-data surface :pointer-only t))
        (data-size (* (video-width video) (video-height video) 4)))
    (labels ((write-data (written-so-far)
               (when (< written-so-far data-size)
                 (let ((newly-written
                        (sb-posix:write video-fd
                                        (cffi:inc-pointer data-pointer written-so-far)
                                        (- data-size written-so-far))))
                   (write-data (+ written-so-far newly-written))))))
      (write-data 0))))

(defun finish-video (video)
  "Finishes the process of creating the video.

  Do not use the supplied video object again after calling this function."
  (close (external-program:process-input-stream (video-process video))))
