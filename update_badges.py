#!/usr/bin/env python3

import datetime
import json
import pathlib
import sys

import requests
import toml


AOCRC_FILE = pathlib.Path.home().joinpath('.aocrc')
BADGES_FILE = pathlib.Path(__file__).parent.joinpath('badges.json')
LEADERBOARD_TPL = 'https://adventofcode.com/leaderboard/private/view/{userid}.json'


if not AOCRC_FILE.exists():
    print('No config file at {}'.format(AOCRC_FILE), file=sys.stderr)
    exit(1)

config = toml.load(AOCRC_FILE)

full_url = LEADERBOARD_TPL.format(**config)
cookies = {'session': config['session']}
r = requests.get(full_url, cookies=cookies)
leaderboard = r.json()

badges_dict = {
    'stars': 0,
    'days_completed': 0,
}

## day
today = datetime.date.today()
if today.month < 12:
    if today.year == int(leaderboard['event']):
        # It's before December, but the website has been updated for this
        # year's event.
        badges_dict['day'] = 0
    else:
        # Still on day 25 of last year.
        badges_dict['day'] = 25
elif today.day > 25:
    badges_dict['day'] = 25
else:
    badges_dict['day'] = today.day
badges_dict['year'] = int(leaderboard['event'])

## stars
badges_dict['stars'] = leaderboard['members'][str(config['userid'])]['stars']

## days_completed
for day, levels in leaderboard['members'][str(config['userid'])]['completion_day_level'].items():
    if len(levels) >= 2:
        badges_dict['days_completed'] += 1

with open(BADGES_FILE, 'w') as output:
    json.dump(badges_dict, output, sort_keys=True, indent=2)
