(defpackage advent-of-code-system (:use :cl :asdf))
(in-package advent-of-code-system)

(defsystem :advent-of-code
  :author "Phil! Gold <phil_g@pobox.com>"
  :depends-on (:alexandria
               :array-operations
               :bordeaux-threads
               :cl-cairo2
               :cl-coroutine
               :cl-cpus
               :cl-fad
               :cl-ppcre
               :cl-progress-bar
               :cl-speedy-queue
               :drakma
               :dufy
               :external-program
               :fiveam
               :fset
               :functional-priority-queues
               :graph
               :iterate
               :lparallel
               :lquery
               :md5
               :parseq
               :pp-toml
               :printv
               :sb-gmp
               :series
               :trivial-download
               :yason)
  :serial t
  :components #.(append '((:file "packages")
                          (:file "point")
                          (:file "queue")
                          (:file "advent-of-code")
                          (:file "clist")
                          (:file "visualization")
                          (:file "colorcet")
                          (:file "2016/assembunny")
                          (:file "2018/elfcode")
                          (:file "2019/intcode"))
                        (loop for year in '(2015 2016 2017 2018 2019 2020 2021 2022 2023)  ; YEAR-MARKER - do not edit this line.
                              append (loop for n from 1 to 25
                                           collect (list :file
                                                         (format nil "~D/~2,'0D" year n))))))
