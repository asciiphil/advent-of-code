(in-package :aoc-2017-03)

(aoc:define-day 552 330785)


;;; Input

(defparameter *starting-square* (parse-integer (aoc:input)))


;;; Part 1

;;; For this spiral:
;;;
;;;     5 4 3
;;;     6 1 2
;;;     7 8 9
;;;
;;; Square 1 is in ring 0.
;;; Squares 2–9 are in ring 1.
;;; Squares 2 and 3 are in segment 0 of ring 1.
;;; Squares 4 and 5 are in segment 1 of ring 1.
;;; Squares 6 and 7 are in segment 2 of ring 1.
;;; Squares 8 and 9 are in segment 3 of ring 1.
;;;
;;; For coordinates, square 1 is at #<0 0>.  x increases to the right.  y
;;; increases up.

(defun square-ring (square)
  "Returns the ring number (zero-indexed) the square is in."
  (floor (/ (ceiling (sqrt square)) 2)))

(defun square-ring-segment (square)
  "Returns two values: the ring segment the square is in, and the steps from
  the beginning of that segment to get to the square."
  (let* ((ring (square-ring square))
         (side-length (* 2 ring))
         (start-of-ring (1+ (expt (1+ (* 2 (1- ring)))
                                  2)))
         (steps-along-ring (- square start-of-ring)))
    (floor steps-along-ring side-length)))

(defun segment-start (ring segment)
  "Returns the coordinates of the start of the segment."
  (let ((x (ecase segment
             (0 ring)
             (1 (1- ring))
             (2 (- ring))
             (3 (1+ (- ring)))))
        (y (ecase segment
             (0 (1+ (- ring)))
             (1 ring)
             (2 (1- ring))
             (3 (- ring)))))
    (point:make-point x y)))

(defun square-coords (square)
  (if (= 1 square)
      #<0 0>
      (multiple-value-bind (segment segment-steps)
          (square-ring-segment square)
        (let ((ring (square-ring square)))
          (point:+ (segment-start ring segment)
                   (ecase segment
                     (0 (point:make-point 0 segment-steps))
                     (1 (point:make-point (- segment-steps) 0))
                     (2 (point:make-point 0 (- segment-steps)))
                     (3 (point:make-point segment-steps 0))))))))

(defun get-answer-1 (&optional (starting-square *starting-square*))
  (point:manhattan-distance #<0 0> (square-coords starting-square)))


;;; Part 2

(defparameter *initial-value-map*
  (fset:with (fset:empty-map) #<0 0> 1))

(defun calculate-value (values square)
  (fset:reduce (lambda (result neighbor)
                 (let ((value (fset:lookup values neighbor)))
                   (if value
                       (+ value result)
                       result)))
               (point:neighbor-set (square-coords square) :diagonals t)
               :initial-value 0))

(defun add-square (values square)
  "Returns two values: the new VALUES map and the value added for SQUARE."
  (let ((coord (square-coords square))
        (new-value (calculate-value values square)))
    (values (fset:with values coord new-value)
            new-value)))

(defun find-value-above (threshold values square)
  "Returns the first value calculated above THRESHOLD, starting by adding SQUARE to VALUES."
  (multiple-value-bind (new-values added-value)
      (add-square values square)
    (if (< threshold added-value)
        added-value
        (find-value-above threshold new-values (1+ square)))))

(defun get-answer-2 (&optional (threshold *starting-square*))
  (find-value-above threshold *initial-value-map* 2))
