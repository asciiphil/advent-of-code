(in-package :aoc-2017-11)

(aoc:define-day 743 1493)


;;; Parsing

(defun parse-path (path-string)
  (mapcar (lambda (str)
            (intern (string-upcase str)))
          (ppcre:split "," path-string)))


;;; Input

(defparameter *path* (parse-path (aoc:input)))

;;; Part 1

;; https://www.redblobgames.com/grids/hexagons/
;; Cube coordinates; flat top; coords q, r, s
(defparameter +neighbor-vectors+
  (fset:map ('n  #< 0 -1  1>)
            ('ne #< 1 -1  0>)
            ('se #< 1  0 -1>)
            ('s  #< 0  1 -1>)
            ('sw #<-1  1  0>)
            ('nw #<-1  0  1>)))

(defun hex-manhattan-distance (point1 point2)
  (/ (point:manhattan-distance point1 point2)
     2))

(defun follow-path (path position)
  (if (endp path)
      (values position (hex-manhattan-distance #<0 0 0> position))
      (multiple-value-bind (end-position max-distance)
          (follow-path (cdr path)
                       (point:+ (fset:lookup +neighbor-vectors+ (car path))
                                position))
        (values end-position
                (max (hex-manhattan-distance #<0 0 0> position)
                     max-distance)))))

(defun get-answer-1 (&optional (path *path*))
  (let ((start #<0 0 0>))
    (hex-manhattan-distance (follow-path path start)
                            start)))


;;; Part 2

(defun get-answer-2 (&optional (path *path*))
  (nth-value 1 (follow-path path #<0 0 0>)))
