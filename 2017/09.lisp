(in-package :aoc-2017-09)

(aoc:define-day 9662 4903)


;;; Parsing

;;; This is where most of the magic happens.

(parseq:defrule group ()
    (and "{" (? (aoc:comma-list group-or-garbage)) "}")
  (:choose 1))

(parseq:defrule group-or-garbage ()
    (or group garbage))

(parseq:defrule garbage ()
    (and "<" (* garbage-character) ">")
  (:choose 1)
  (:string))

(parseq:defrule garbage-character ()
    (or canceled-character char)
  (:string)
  (:not (c) (string= c ">")))

(parseq:defrule canceled-character ()
    (and "!" char)
  (:constant ""))


;;; Input

;; Weirdly, passing 'group to `aoc:input's :parse parameter failes to
;; parse, but this works.
(defparameter *stream* (parseq:parseq 'group (aoc:input)))


;;; Part 1

(defun group-score (group &optional (depth 1))
  (cond
    ((stringp group)
     0)
    ((endp group)
     depth)
    (t
     (reduce (lambda (score child)
               (+ score (group-score child (1+ depth))))
             group
             :initial-value depth))))

(defun get-answer-1 (&optional (stream *stream*))
  (group-score stream))


;;; Part 2

(defun count-garbage-chars (group)
  (if (stringp group)
      (length group)
      (reduce #'+ (mapcar #'count-garbage-chars group))))

(defun get-answer-2 (&optional (group *stream*))
  (count-garbage-chars group))
