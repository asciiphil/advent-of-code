(in-package :aoc-2017-19)

(aoc:define-day "YOHREPXWN" 16734)


;;; Parsing

(defun char-to-symbol (char)
  (cond
    ((char= #\+ char)
     :turn)
    ((or (char= #\- char)
         (char= #\| char))
     :straight)
    (t char)))

(defun parse-line (line row)
  (reduce (lambda (result col)
            (if (char= #\  (schar line col))
                result
                (fset:with result
                           (point:make-point col row)
                           (char-to-symbol (schar line col)))))
          (alexandria:iota (length line))
          :initial-value (fset:empty-map)))

(defun parse-lines (lines)
  (reduce #'fset:map-union
          (mapcar #'parse-line lines (alexandria:iota (length lines)))))


;;; Input

(defparameter *cells* (parse-lines (aoc:input)))
(defparameter *example* (parse-lines (aoc:input :file "examples/19.txt")))


;;; Part 1

(define-condition visited-character (condition)
  ((char :initarg :char :reader visited-character)))

(define-condition path-end (error)
  ((position :initarg :position :reader path-position)
   (vector :initarg :vector :reader path-vector)))

(defun find-start-cell (cells)
  (fset:find-if (lambda (cell) (zerop (point:y cell)))
                (fset:domain cells)))

;; ASSUMPTION: Turn cells are adjacent only to connected cells.
;; In other words, this never happens:
;;
;;       |
;;     -+|
;;      ||
;;
(defun next-position (cells position vector)
  (let ((cell (fset:lookup cells position)))
    (if (null cell)
        (signal 'path-end :position position :vector vector)
        (cond
          ((eq cell :turn)
           (let ((next-cells (fset:less (fset:intersection (point:neighbor-set position)
                                                           (fset:domain cells))
                                        (point:- position vector))))
             (assert (= 1 (fset:size next-cells)))
             (values (fset:arb next-cells)
                     (point:- (fset:arb next-cells) position))))
          ((eq cell :straight)
           (values (point:+ position vector)
                   vector))
          (t
           (signal 'visited-character :char cell)
           (values (point:+ position vector)
                   vector))))))

(defun travel-path (cells)
  (let ((steps -1))
    (labels ((travel-r (position vector)
               (incf steps)
               (multiple-value-call #'travel-r (next-position cells position vector))))
      (let ((letters))
        (handler-case
            (handler-bind ((visited-character (lambda (c) (push (visited-character c) letters))))
              (travel-r (find-start-cell cells) #<0 1>))
          (path-end (e) (values (reverse letters) steps (path-position e) (path-vector e))))))))

(defun get-answer-1 (&optional (cells *cells*))
  (format nil "~{~A~}" (travel-path cells)))

(aoc:given 1
  (string= "ABCDEF" (get-answer-1 *example*)))


;;; Part 2

(defun get-answer-2 (&optional (cells *cells*))
  (nth-value 1 (travel-path cells)))

(aoc:given 2
  (= 38 (get-answer-2 *example*)))
