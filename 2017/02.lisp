(in-package :aoc-2017-02)

(aoc:define-day 47623 312)


;;; Parsing

(defun parse-line (line)
  (mapcar #'parse-integer (ppcre:split "\\s+" line)))


;;; Input

(defparameter *spreadsheet* (mapcar #'parse-line (aoc:input)))


;;; Part 1

(defun row-checksum (row)
  (- (apply #'max row)
     (apply #'min row)))

(defun get-answer-1 (&optional (spreadsheet *spreadsheet*))
  (apply #'+ (mapcar #'row-checksum spreadsheet)))


;;; Part 2

(defun evenly-divisible-p (a b)
  (if (< a b)
      (evenly-divisible-p b a)
      (zerop (mod a b))))

(defun find-division (row)
  (flet ((test-pair (pair)
           (destructuring-bind (a b) pair
             (when (evenly-divisible-p a b)
               (return-from find-division
                 (/ (max a b) (min a b)))))))
    (aoc:visit-subsets #'test-pair row 2)))

(defun get-answer-2 (&optional (spreadsheet *spreadsheet*))
  (apply #'+ (mapcar #'find-division spreadsheet)))
