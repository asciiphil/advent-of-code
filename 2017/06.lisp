(in-package :aoc-2017-06)

(aoc:define-day 7864 1695)


;;; Input

(defparameter *memory-banks* (fset:convert 'fset:seq (aoc:extract-ints (aoc:input))))
(defparameter *example-memory-banks* (fset:seq 0 2 7 0))


;;; Part 1

(defun seq-greatest (seq)
  "Returns two values: the index and the value of the greatest value in the
  sequence."
  (declare (type fset:seq seq))
  (let ((greatest-value (fset:lookup seq 0))
        (greatest-index 0))
    (fset:do-seq (v seq :index i)
      (when (eq :less (fset:compare greatest-value v))
        (setf greatest-value v
              greatest-index i)))
    (values greatest-index greatest-value)))

(defun most-full-bank (banks)
  "Returns the index number of the memory bank with the most blocks."
  (declare (type fset:seq banks))
  (seq-greatest banks))

(defun reallocate-memory (banks)
  "Reallocates memory from the most-full bank and returns the new memory
  banks."
  (declare (type fset:seq banks))
  (labels ((reallocate-r (new-banks index quantity stepdown-index remaining)
             "STEPDOWN-INDEX is the last index of the higher quantity.  After
             processing that index, the quantity is reduced by one."
             (if (zerop remaining)
                 new-banks
                 (reallocate-r (fset:with new-banks index (+ quantity (fset:lookup new-banks index)))
                               (mod (1+ index) (fset:size new-banks))
                               (if (= index stepdown-index)
                                   (1- quantity)
                                   quantity)
                               stepdown-index
                               (1- remaining)))))
    (multiple-value-bind (full-index full-quantity) (most-full-bank banks)
      (reallocate-r (fset:with banks full-index 0)
                    (mod (1+ full-index) (fset:size banks))
                    (ceiling full-quantity (fset:size banks))
                    (mod (+ full-index (mod full-quantity (fset:size banks)))
                         (fset:size banks))
                    (fset:size banks)))))

(defun find-reallocation-loop (banks &optional (seen-states (fset:empty-set)) (steps 0))
  (if (fset:lookup seen-states banks)
      (values steps banks)
      (find-reallocation-loop (reallocate-memory banks)
                              (fset:with seen-states banks)
                              (1+ steps))))

(defun get-answer-1 (&optional (banks *memory-banks*))
  (find-reallocation-loop banks))

(aoc:given 1
  (= 5 (get-answer-1 *example-memory-banks*)))


;;; Part 2

(defun find-reallocation-loop-length (banks)
  (find-reallocation-loop (nth-value 1 (find-reallocation-loop banks))))

(defun get-answer-2 (&optional (banks *memory-banks*))
  (find-reallocation-loop-length banks))

(aoc:given 2
  (= 4 (get-answer-2 *example-memory-banks*)))
