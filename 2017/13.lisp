(in-package :aoc-2017-13)

(aoc:define-day 2264 3875838)


;;; Input
;;; List of tuples; each tuple: (depth range)

(defparameter *scanners* (mapcar #'aoc:extract-ints (aoc:input)))
(defparameter *example*
  '((0 3)
    (1 2)
    (4 4)
    (6 4)))


;;; Part 1

(defun scanner-at-time (range time)
  (let* ((cycle (- (* 2 range) 2))
         (pos (mod time cycle)))
    (if (< pos range)
        pos
        (- cycle pos))))

(defun collision-p (range time)
  (zerop (scanner-at-time range time)))

(defun find-collisions (scanners start-time)
  (fset:filter (lambda (scanner)
                 (destructuring-bind (depth range) scanner
                   (collision-p range (+ start-time depth))))
               scanners))

(defun get-answer-1 (&optional (scanners *scanners*))
  (reduce #'+
          (mapcar (alexandria:curry #'apply #'*)
                  (find-collisions scanners 0))))


;;; Part 2

(defun find-start-time (scanners &optional (trial-time 0))
  (if (endp (find-collisions scanners trial-time))
      trial-time
      (find-start-time scanners (1+ trial-time))))

(defun get-answer-2 (&optional (scanners *scanners*))
  (find-start-time scanners))
