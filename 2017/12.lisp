(in-package :aoc-2017-12)

(aoc:define-day 130 189)


;;; Parsing

(parseq:defrule pipe ()
    (and aoc:integer-string " <-> " (aoc:comma-list aoc:integer-string))
  (:choose 0 2)
  (:lambda (source destinations)
    (fset:reduce (lambda (result destination)
                   (aoc:map-set-with result destination source))
                 destinations
                 :initial-value (fset:with-default (fset:map (source (fset:convert 'fset:set destinations)))
                                  (fset:empty-set)))))

(defun merge-pipes (pipe-sets)
  (fset:reduce (lambda (ps1 ps2)
                 (fset:map-union ps1 ps2 (lambda (s1 s2) (fset:union s1 s2))))
               pipe-sets))

;;; Input

(defparameter *pipes* (merge-pipes (aoc:input :parse-line 'pipe)))
(defparameter *example*
  (merge-pipes
   (mapcar (alexandria:curry #'parseq:parseq 'pipe)
           '("0 <-> 2"
             "1 <-> 1"
             "2 <-> 0, 3, 4"
             "3 <-> 2, 4"
             "4 <-> 2, 3, 6"
             "5 <-> 6"
             "6 <-> 4, 5"))))


;;; Part 1

(defun get-connected-set (pipes program &optional (visited-programs (fset:empty-set)))
  (fset:reduce (lambda (new-visited new-program)
                 (if (fset:lookup new-visited new-program)
                     new-visited
                     (get-connected-set pipes new-program new-visited)))
               (fset:lookup pipes program)
               :initial-value (fset:with visited-programs program)))

(defun get-answer-1 (&optional (pipes *pipes*))
  (fset:size (get-connected-set pipes 0)))


;;; Part 2

(defun get-connected-groups (pipes &optional (found-groups (fset:empty-set)))
  (let ((unexplored-programs (fset:set-difference (fset:domain pipes)
                                                  (fset:reduce #'fset:union found-groups :initial-value (fset:empty-set)))))
    (if (zerop (fset:size unexplored-programs))
        found-groups
        (get-connected-groups pipes
                              (fset:with found-groups
                                         (get-connected-set pipes (fset:arb unexplored-programs)))))))

(defun get-answer-2 (&optional (pipes *pipes*))
  (fset:size (get-connected-groups pipes)))
