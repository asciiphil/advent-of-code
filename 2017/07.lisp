(in-package :aoc-2017-07)

(aoc:define-day "qibuqqg" 1079)


;;; Data Structures

(defstruct program
  name
  weight
  children)


;;; Parsing

(parseq:defrule program ()
    (and name " (" aoc:integer-string ")" (? (and " -> " (aoc:comma-list name))))
  (:choose 0 2 4)
  (:lambda (name weight children)
    (make-program :name name
                  :weight weight
                  :children (fset:convert 'fset:set (second children)))))

(parseq:defrule name ()
    (+ (char "a-z"))
  (:string)
  (:function #'string-upcase)
  (:function #'intern))


;;; Input

(defparameter *programs* (aoc:input :parse-line 'program))
(defparameter *example-programs* (aoc:input :parse-line 'program :file "examples/07.txt"))


;;; Part 1

(defun find-root (programs)
  (let* ((all-programs (fset:convert 'fset:set (mapcar #'program-name programs)))
         (child-programs (fset:reduce (lambda (result program)
                                        (fset:union result (program-children program)))
                                      programs
                                      :initial-value (fset:empty-set)))
         (root-programs (fset:set-difference all-programs child-programs)))
    (assert (= 1 (fset:size root-programs)))
    (fset:arb root-programs)))

(defun get-answer-1 (&optional (programs *programs*))
  (string-downcase (symbol-name (find-root programs))))

(aoc:given 1
  (string= "tknk" (get-answer-1 *example-programs*)))


;;; Part 2

(defun get-program (programs name)
  (find name programs :key #'program-name))

(defun full-program-weight (programs program-name)
  "Gives the weight of a program and all of its children."
  (let ((program (get-program programs program-name)))
    (+ (program-weight program)
       (fset:reduce (lambda (child-sum child-name)
                      (+ child-sum (full-program-weight programs child-name)))
                    (program-children program)
                    :initial-value 0))))

(defun find-unbalanced-child (programs program-name)
  "Returns two values: the name of the unbalanced child program, and the weight it should be.
  If children are balanced, returns NIL and the weight of each child."
  (let* ((program (get-program programs program-name))
         (child-weights (fset:reduce (lambda (weights child-name)
                                       (aoc:map-set-with weights
                                                         (full-program-weight programs child-name)
                                                         child-name))
                                     (program-children program)
                                     :initial-value (fset:empty-map (fset:empty-set)))))
    (ecase (fset:size child-weights)
      (1  ; No differing child
       (values nil (fset:arb child-weights)))
      (2  ; There's a difference
       (let ((unbalanced-name (fset:arb (nth-value 1 (fset:find-if (lambda (weight) (= 1 (fset:size (fset:lookup child-weights weight))))
                                                                   child-weights))))
             (balanced-weight (fset:find-if (lambda (weight) (< 1 (fset:size (fset:lookup child-weights weight))))
                                            child-weights)))
         (values unbalanced-name balanced-weight))))))

(defun find-unbalanced-program (programs program-name &optional (target-weight 0))
  "Returns two values: the name of the unbalanced program, and the weight it should be."
  (let ((program (get-program programs program-name)))
    (multiple-value-bind (unbalanced-child balanced-weight)
        (find-unbalanced-child programs program-name)
      (if unbalanced-child
          (find-unbalanced-program programs unbalanced-child balanced-weight)
          (values program-name
                  (- target-weight
                     (* (fset:size (program-children program))
                        balanced-weight)))))))

(defun get-answer-2 (&optional (programs *programs*))
  (nth-value 1 (find-unbalanced-program programs (find-root programs))))

(aoc:given 2
  (= 60 (get-answer-2 *example-programs*)))
