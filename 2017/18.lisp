(in-package :aoc-2017-18)

(aoc:define-day 3188 7112)


;;; Data Structures

(defstruct context
  program
  (ip 0)
  (registers (fset:empty-map 0))
  (input (fset:seq))
  (output (fset:seq))
  terminated)

(defun update-context (context &key ip registers input output terminated)
  (make-context :program (context-program context)
                :ip (or ip (context-ip context))
                :registers (or registers (context-registers context))
                :input (or input (context-input context))
                :output (or output (context-output context))
                :terminated (or terminated (context-terminated context))))


;;; Conditions

(define-condition part-1-rcv ()
  ((value :initarg :value :reader rcv-value)))

(define-condition empty-queue ()
  ((context :initarg :context :reader rcv-context))
  (:documentation "Signaled when attempting to RCV from an empty input queue."))


;;; Parsing

(parseq:defrule instruction ()
    (and opcode " " parameter (? (and " " parameter)))
  (:choose 0 2 3)
  (:lambda (op p1 p2)
    (if p2
        (list op p1 (second p2))
        (list op p1))))

(parseq:defrule opcode ()
    (rep 3 (char "a-z"))
  (:string)
  (:lambda (str)
    (intern (string-upcase str))))

(parseq:defrule parameter ()
    (or aoc:integer-string register))

(parseq:defrule register ()
    char
  (:lambda (c) (intern (string-upcase (format nil "~A" c)))))


;;; Input

(defparameter *instructions* (aoc:input :parse-line 'instruction))
(defparameter *example*
  (mapcar (alexandria:curry #'parseq:parseq 'instruction)
          '("set a 1"
            "add a 2"
            "mul a a"
            "mod a 5"
            "snd a"
            "set a 0"
            "rcv a"
            "jgz a -1"
            "set a 1"
            "jgz a -2")))


;;; Part 1
;;;
;;; The registers are a map, with a special key:
;;;   SND - the last sound frequency played
;;;
;;; Every OP-* function returns two values: the new set of registers, and
;;; the delta change in the instruction pointer.  (Every instruction is
;;; effectively followed by a GOTO.)

(defparameter *program-count* 1)

(defun get-register (registers key)
  (if (symbolp key)
      (fset:lookup registers key)
      key))

(defun opcode-function (opcode)
  (symbol-function (intern (format nil "OP-~A" opcode))))

(defun op-simple (fn params context)
  (destructuring-bind (x y) params
    (with-slots (ip registers) context
      (update-context context
                      :ip (1+ ip)
                      :registers (fset:with registers x
                                            (funcall fn
                                                     (get-register registers x)
                                                     (get-register registers y)))))))

(defun op-add (params context)
  (op-simple #'+ params context))

(defun op-mul (params context)
  (op-simple #'* params context))

(defun op-mod (params context)
  (op-simple #'mod params context))

(defun op-set (params context)
  (destructuring-bind (x y) params
    (with-slots (ip registers) context
      (update-context context
                      :ip (1+ ip)
                      :registers (fset:with registers x
                                            (get-register registers y))))))

(defun op-jgz (params context)
  (destructuring-bind (x y) params
    (with-slots (ip registers) context
      (if (plusp (get-register registers x))
          (update-context context
                          :ip (+ ip (get-register registers y)))
          (update-context context
                          :ip (1+ ip))))))

(defun op-snd (params context)
  (destructuring-bind (x) params
    (with-slots (ip registers output) context
      (if (= 1 *program-count*)
          (update-context context
                          :ip (1+ ip)
                          :registers (fset:with registers 'snd (get-register registers x)))
          (update-context context
                          :ip (1+ ip)
                          :output (fset:with-last output (get-register registers x)))))))

(defun op-rcv (params context)
  (destructuring-bind (x) params
    (with-slots (ip registers input) context
      (if (= 1 *program-count*)
          (if (plusp (get-register registers x))
              (signal 'part-1-rcv :value (get-register registers 'snd))
              (update-context context
                              :ip (1+ ip)))
          (if (plusp (fset:size input))
              (update-context context
                              :ip (1+ ip)
                              :registers (fset:with registers x (fset:first input))
                              :input (fset:less-first input))
              (signal 'empty-queue :context context))))))

(defun step-program (context)
  (with-slots (program ip) context
    (if (or (< ip 0)
            (<= (fset:size program) ip))
        (assert nil nil "NYI: program termination")
        (destructuring-bind (opcode &rest params) (fset:lookup program ip)
          (funcall (opcode-function opcode) params context)))))

(defun run-program (context)
  (do ((new-context context (step-program new-context)))
      (nil)))

(defun get-answer-1 (&optional (program *instructions*))
  (handler-case
      (run-program (make-context :program program))
    (part-1-rcv (rcv) (rcv-value rcv))))


;;; Part 2

(defparameter *pgm-1-snd-count* 0)

(defparameter *example2*
  (mapcar (alexandria:curry #'parseq:parseq 'instruction)
          '("snd 1"
            "snd 2"
            "snd p"
            "rcv a"
            "rcv b"
            "rcv c"
            "rcv d")))

(defun make-context-with-id (program id)
  (make-context :program program
                :registers (fset:with (fset:empty-map 0)
                                      'p id)))

(defun initialize-parallel-programs (program)
  "Returns the two programs' contexts as a list."
  (list (make-context-with-id program 0)
        (make-context-with-id program 1)))

(defun step-parallel-programs (programs)
  "Runs each program until it needs input from the other one."
  (let ((*program-count* 2))
    (fset:image (lambda (program)
                  (handler-case
                      (run-program program)
                    (empty-queue (rcv) (rcv-context rcv))))
                programs)))

(defun swap-parallel-queues (programs)
  (destructuring-bind (p1 p2) programs
    (with-slots ((input1 input) (output1 output)) p1
      (with-slots ((input2 input) (output2 output)) p2
        (let ((need-to-terminate (and (fset:empty? output1)
                                      (fset:empty? output2))))
          (incf *pgm-1-snd-count* (fset:size output2))
          (list (update-context p1
                                :input (fset:concat input1 output2)
                                :output (fset:empty-seq)
                                :terminated need-to-terminate)
                (update-context p2
                                :input (fset:concat input2 output1)
                                :output (fset:empty-seq)
                                :terminated need-to-terminate)))))))

(defun run-parallel-programs (program)
  (do ((contexts (initialize-parallel-programs program)
                 (swap-parallel-queues (step-parallel-programs contexts))))
      ((or (context-terminated (first contexts))
           (context-terminated (second contexts)))
       contexts)))

(defun get-answer-2 (&optional (program *instructions*))
  (let ((*pgm-1-snd-count* 0))
    (run-parallel-programs program)
    *pgm-1-snd-count*))
