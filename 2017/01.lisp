(in-package :aoc-2017-01)

(aoc:define-day 1343 1274)


;;; Input

(defparameter *digits* (map 'vector (lambda (c) (parse-integer (string c))) (aoc:input)))


;;; Part 1

(defun digit-matches-p (digits index)
  (= (svref digits index) (svref digits (mod (1+ index) (length digits)))))

(defun get-answer-1 (&key (digits *digits*) (match-fn #'digit-matches-p))
  (series:collect-sum
   (series:map-fn 'number
                  (alexandria:curry #'svref digits)
                  (series:choose-if (alexandria:curry match-fn digits)
                                    (series:scan-range :below (length digits))))))


;;; Part 2

(defun halfway-digit-matches-p (digits index)
  (= (svref digits index) (svref digits (mod (+ index (/ (length digits) 2)) (length digits)))))

(defun get-answer-2 ()
  (get-answer-1 :match-fn #'halfway-digit-matches-p))
