(in-package :aoc-2017-21)

(aoc:define-day nil nil)


;;; Parsing

(defun parse-rules (lines)
  (reduce #'fset:map-union (mapcar (alexandria:curry #'parseq:parseq 'rule) lines)))

(parseq:defrule rule ()
    (and pattern " => " pattern)
  (:choose 0 2)
  (:lambda (source target) (fset:map (source target))))

(defun parse-pattern-lines (lines)
  (fset:domain (aoc:parse-grid-to-map lines :key (alexandria:curry #'char= #\#))))

(defun parse-pattern (pattern)
  (parse-pattern-lines (ppcre:split "/" pattern)))

(parseq:defrule pattern ()
    (+ (char ".#/"))
  (:string)
  (:function #'parse-pattern))


;;; Input

(defparameter *rules* (parse-rules (aoc:input)))
(defparameter *example*
  (parse-rules '("../.# => ##./#../..."
                 ".#./..#/### => #..#/..../..../#..#")))


;;; Part 1

(defparameter *start-pattern*
  (fset:set        #<1 0>
                          #<2 1>
            #<0 2> #<1 2> #<2 2>))

(defparameter +rotate-2+
  (fset:map (#<0 0> #<1 0>)
            (#<1 0> #<1 1>)
            (#<1 1> #<0 1>)
            (#<0 1> #<0 0>)))

(defun rotate-pattern-2 (pattern)
  (fset:image (alexandria:curry #'fset:lookup +rotate-2+) pattern))

(defparameter +rotate-3+
  (fset:map (#<0 0> #<2 0>)
            (#<1 0> #<2 1>)
            (#<2 0> #<2 2>)
            (#<0 1> #<1 0>)
            (#<1 1> #<1 1>)
            (#<2 1> #<1 2>)
            (#<0 2> #<0 0>)
            (#<1 2> #<0 1>)
            (#<2 2> #<0 2>)))

(defun rotate-pattern-3 (pattern)
  (fset:image (alexandria:curry #'fset:lookup +rotate-3+) pattern))

(defun rotate-pattern (pattern size)
  (ecase size
    (2 (rotate-pattern-2 pattern))
    (3 (rotate-pattern-3 pattern))))

(defparameter +flip-2+
  (fset:map (#<0 0> #<1 0>)
            (#<1 0> #<0 0>)
            (#<0 1> #<1 1>)
            (#<1 1> #<0 1>)))

(defun flip-pattern-2 (pattern)
  (fset:image (alexandria:curry #'fset:lookup +flip-2+) pattern))

(defparameter +flip-3+
  (fset:map (#<0 0> #<2 0>)
            (#<1 0> #<1 0>)
            (#<2 0> #<0 0>)
            (#<0 1> #<2 1>)
            (#<1 1> #<1 1>)
            (#<2 1> #<0 1>)
            (#<0 2> #<2 2>)
            (#<1 2> #<1 2>)
            (#<2 2> #<0 2>)))

(defun flip-pattern-3 (pattern)
  (fset:image (alexandria:curry #'fset:lookup +flip-3+) pattern))

(defun flip-pattern (pattern size)
  (ecase size
    (2 (flip-pattern-2 pattern))
    (3 (flip-pattern-3 pattern))))

(defun match-pattern (rules pattern size)
  (labels ((find-match (cur-pattern rotated flipped)
             (or (fset:lookup rules cur-pattern)
                 (if (= 3 rotated)
                     (progn
                       (assert (not flipped))
                       (find-match (flip-pattern cur-pattern size) 0 t))
                     (find-match (rotate-pattern cur-pattern size) (1+ rotated) flipped)))))
    (find-match pattern 0 nil)))

(defun extract-pattern (full-pattern base size)
  (let ((max-point (point:+ base (point:make-point (1- size) (1- size)))))
    (fset:image (lambda (p) (point:- p base))
                (fset:filter (lambda (p) (point:<= base p max-point)) full-pattern))))

(defun replace-pattern (pattern base)
  (fset:image (alexandria:curry #'point:+ base) pattern))

(defun match-patterns (rules full-pattern size)
  (let ((chunk-size (if (evenp size)
                        2
                        3))
        (next-chunk (if (evenp size)
                        3
                        4))
        (result (fset:empty-set)))
    (dotimes (x (/ size chunk-size))
      (dotimes (y (/ size chunk-size))
        (let ((base-point (point:make-point (* x chunk-size)
                                            (* y chunk-size)))
              (next-base (point:make-point (* x next-chunk)
                                           (* y next-chunk))))
          (setf result (fset:union result (replace-pattern (match-pattern rules
                                                                          (extract-pattern full-pattern
                                                                                           base-point
                                                                                           chunk-size)
                                                                          chunk-size)
                                                           next-base))))))
    (values result
            (if (evenp size)
                (* 3 (/ size 2))
                (* 4 (/ size 3))))))

(defun expand-pattern (rules full-pattern size &optional (count 1))
  (if (zerop count)
      (values full-pattern size)
      (multiple-value-bind (next-pattern next-size)
          (match-patterns rules full-pattern size)
        (expand-pattern rules next-pattern next-size (1- count)))))

(defun get-answer-1 (&optional (rules *rules*) (count 5))
  (fset:size (expand-pattern rules *start-pattern* 3 count)))

(aoc:given 1
  (= 12 (get-answer-1 *example* 2)))
