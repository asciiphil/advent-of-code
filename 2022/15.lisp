(in-package :aoc-2022-15)

;;; Part 2 answer is 11583882601918, but it takes too long to find it currently.
(aoc:define-day 4985193 nil)


;;; Parsing

(parseq:defrule sensor ()
    (and "Sensor at x=" aoc:integer-string ", y=" aoc:integer-string
         ": closest beacon is at x=" aoc:integer-string ", y=" aoc:integer-string)
  (:choose 1 3 5 7)
  (:lambda (sx sy bx by)
    (cons (point:make-point sx sy) (point:make-point bx by))))


;;; Input

(defparameter *example* (fset:convert 'fset:map (aoc:input :parse-line 'sensor :file "examples/15.txt")))
(defparameter *sensors* (fset:convert 'fset:map (aoc:input :parse-line 'sensor)))


;;; Part 1

(defun sensor-bbox (sensor beacon)
  (let ((beacon-distance (point:manhattan-distance sensor beacon)))
    (values (point:make-point (- (point:x sensor) beacon-distance) (- (point:y sensor) beacon-distance))
            (point:make-point (+ (point:x sensor) beacon-distance) (+ (point:y sensor) beacon-distance)))))

(defun sensors-at-y (sensors y)
  (fset:filter (lambda (sensor beacon)
                 (multiple-value-bind (min-point max-point) (sensor-bbox sensor beacon)
                   (<= (point:y min-point) y (point:y max-point))))
               sensors))

(defun beacons-at-y (sensors y)
  (fset:filter (lambda (beacon)
                 (= y (point:y beacon)))
               (fset:range sensors)))

(defun sensor-line-overlap (sensor beacon y)
  "Returns a cons of two x values.  These represent a closed interval
  along the horizontal line with the given y value."
  (let* ((beacon-distance (point:manhattan-distance sensor beacon))
         (dx (- beacon-distance (abs (- y (point:y sensor))))))
    (cons (- (point:x sensor) dx) (+ (point:x sensor) dx))))

(defun sensor-line-overlaps (sensors y)
  "Returns overlap pairs (see `sensor-line-overlap') sorted by their first
  value."
  (sort (fset:reduce (lambda (result sensor beacon)
                       (cons (sensor-line-overlap sensor beacon y)
                             result))
                     sensors
                     :initial-value nil)
        #'<=
        :key #'car))

(defun line-overlaps-intersect-p (overlap1 overlap2)
  (destructuring-bind (l1 . h1) overlap1
    (destructuring-bind (l2 . h2) overlap2
      (and (<= l2 h1)
           (<= l1 h2)))))

(defun combine-line-overlap (overlap1 overlap2)
  (assert (line-overlaps-intersect-p overlap1 overlap2))
  (destructuring-bind (l1 . h1) overlap1
    (destructuring-bind (l2 . h2) overlap2
      (cons (min l1 l2) (max h1 h2)))))

(defun merge-line-overlap (existing-overlaps new-overlap)
  (reduce (lambda (result overlap)
            (if (line-overlaps-intersect-p (car result) overlap)
                (cons (combine-line-overlap (car result) overlap)
                      (cdr result))
                (cons (car result) (cons overlap (cdr result)))))
          existing-overlaps
          :initial-value (list new-overlap)))

(defun merge-all-overlaps (line-overlaps)
  (reduce #'merge-line-overlap
          (cdr line-overlaps)
          :initial-value (list (car line-overlaps))))

(defun overlap-size (overlap)
  (1+ (- (cdr overlap) (car overlap))))

(defun count-covered-positions (overlaps)
  (reduce #'+ (mapcar #'overlap-size overlaps)))

(defun get-answer-1 (&optional (sensors *sensors*) (y 2000000))
  (- (count-covered-positions (merge-all-overlaps (sensor-line-overlaps (sensors-at-y sensors y) y)))
     (fset:size (beacons-at-y sensors y))))


;;; Part 2

;; TODO: Improve this algorithm.  Searching the y-coordinate space
;; linearly takes several minutes on my system.
(defun find-gap (sensors)
  (labels ((find-r (y)
             (assert (<= y 4000000))
             (when (zerop (mod y 100000))
               (format t "~A~%" y))
             (let ((overlap (merge-all-overlaps (sensor-line-overlaps (sensors-at-y sensors y) y))))
               (if (not (endp (cdr overlap)))
                   (values y overlap)
                   (find-r (1+ y))))))
    (find-r 0)))

(defun get-answer-2 (&optional (sensors *sensors*))
  (multiple-value-bind (y overlap) (find-gap sensors)
    (assert (= 2 (length overlap)))
    (destructuring-bind ((x0 . x1) (x2 . x3)) (sort overlap #'< :key #'car)
      (declare (ignore x0 x3))
      (assert (= 2 (- x2 x1)))
      (let ((x (1- x2)))
        (+ (* x 4000000)
           y)))))
