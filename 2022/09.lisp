(in-package :aoc-2022-09)

(aoc:define-day 6067 2471)


;;; Data Structures

(defstruct (snake (:constructor make-snake))
  segments
  history)

(defun init-snake (segment-count)
  (make-snake
   :segments (make-list segment-count :initial-element #<0 0>)
   :history (make-list segment-count :initial-element (fset:seq #<0 0>))))


;;; Parsing

(parseq:defrule motion ()
    (and direction " " aoc:integer-string)
  (:choose 0 2))

(defparameter +direction-vectors+
  (fset:map (#\U #<0 -1>)
            (#\L #<-1 0>)
            (#\R #<1 0>)
            (#\D #<0 1>)))

(parseq:defrule direction ()
    (char "ULDR")
  (:lambda (c) (fset:lookup +direction-vectors+ c)))


;;; Input

(defparameter *example* (aoc:input :parse-line 'motion :file "examples/09.txt"))
(defparameter *example2* (aoc:input :parse-line 'motion :file "examples/09-longer.txt"))
(defparameter *motions* (aoc:input :parse-line 'motion))


;;; Part 1

(defun move-snake (snake movement)
  (destructuring-bind (vector count) movement
    (labels ((move-r (snake remaining-count)
               (if (zerop remaining-count)
                   snake
                   (move-r (move-head snake vector)
                           (1- remaining-count)))))
      (move-r snake count))))

(defun move-head (snake vector)
  (labels ((move-tail (segments history leader)
             (if (endp segments)
                 (values nil nil)
                 (let ((new-pos (move-segment leader (car segments))))
                   (multiple-value-bind (lower-segments lower-history)
                       (move-tail (cdr segments) (cdr history) new-pos)
                     (values (cons new-pos lower-segments)
                             (cons (fset:with-last (car history) new-pos)
                                   lower-history)))))))
    (with-slots (segments history) snake
      (let ((new-head (point:+ (car segments) vector)))
        (multiple-value-bind (new-segments new-history)
            (move-tail (cdr segments) (cdr history) new-head)
          (make-snake :segments (cons new-head new-segments)
                      :history (cons (fset:with-last (car history) new-head)
                                     new-history)))))))

(defun move-segment (leader follower)
  "LEADER should be the *new* position of the leading segment; FOLLOWER
  should be the *old* (or current) possition of the following segment.
  Will return the new location of the following segment."
  (let ((vector (point:- leader follower)))
    (if (or (< 1 (abs (point:x vector)))
            (< 1 (abs (point:y vector))))
        (point:+ follower (clamp-tail-vector vector))
        follower)))

(defun clamp-tail-vector (vector)
  (point:map-coordinates #'signum (list vector)))

(defun follow-motions (motions segments)
  (reduce #'move-snake motions :initial-value (init-snake segments)))

(defun get-answer-1 (&optional (motions *motions*) (segments 2))
  (let ((tail-history (car (last (snake-history (follow-motions motions segments))))))
    (fset:size (fset:convert 'fset:set tail-history))))


;;; Part 2

(defun get-answer-2 (&optional (motions *motions*))
  (get-answer-1 motions 10))


;;; Visualization

(defun pad-history-for-animation (history)
  (let ((added-steps (1- (length history))))
    (labels ((add-steps (positions with-fn fixed-position count)
               (if (zerop count)
                   positions
                   (add-steps (funcall with-fn positions fixed-position)
                              with-fn
                              fixed-position
                              (1- count))))
             (pad-history (history-list leading-count)
               (if (endp history-list)
                   nil
                   (let* ((positions (car history-list)))
                     (cons (add-steps (add-steps positions
                                                 #'fset:with-first
                                                 (fset:first positions)
                                                 leading-count)
                                      #'fset:with-last
                                      (fset:last positions)
                                      (- added-steps leading-count))
                           (pad-history (cdr history-list) (1+ leading-count)))))))
      (pad-history history 0))))

(defun draw-video (motions filename &key (canvas-width 1920) (canvas-height 1080) (canvas-margin 16) (fps 30) (move-ms 1000) (segments 10) (outro-sec 15))
  (let* ((steps (pad-history-for-animation (snake-history (follow-motions motions segments))))
         (all-points (fset:reduce (lambda (r s)
                                    (fset:union r (fset:convert 'fset:set s)))
                                  steps
                                  :initial-value (fset:empty-set)))
         (trans-matrix (viz:trans-matrix-for-points all-points
                                                    canvas-width
                                                    canvas-height
                                                    :margin canvas-margin)))
    (viz:with-video (video (viz:create-video filename canvas-width canvas-height :fps fps))
      (dotimes (i (1- (fset:size (first steps))))
        (draw-move video trans-matrix steps (1+ i) (round (* fps (/ move-ms 1000))))
        (sb-ext:gc))
      (draw-frame video trans-matrix steps (1- (fset:size (first steps))) 1
                  :repeat (* fps outro-sec)))))

(defun interpolate-segment-position (index distance positions)
  (point:+ (point:* (point:- (fset:lookup positions index)
                             (fset:lookup positions (1- index)))
                    distance)
           (fset:lookup positions (1- index))))

(defun interpolate-segment-positions (steps step-index distance)
  (fset:image (alexandria:curry #'interpolate-segment-position step-index distance)
              steps))

(defun segment-path-subsets (steps step-index distance)
  (fset:image (lambda (positions)
                (fset:with-last (fset:subseq positions 0 step-index)
                                (interpolate-segment-position step-index distance positions)))
              steps))

(defun draw-move (video trans-matrix steps step-index frames)
  (dotimes (i frames)
    (draw-frame video trans-matrix steps step-index (/ i frames))))

(defun draw-frame (video trans-matrix steps step-index distance &key (repeat 1))
  (let ((surface (viz:create-video-surface video)))
    (viz:clear-surface surface :dark-background)
    (cairo:with-context-from-surface (surface)
      (cairo:set-trans-matrix trans-matrix)
      (cairo:translate 0.5 0.5)
      (draw-segment-paths (segment-path-subsets steps step-index distance))
      (draw-segments (interpolate-segment-positions steps step-index distance)))
    (dotimes (i repeat)
      (viz:write-video-surface video surface))))

(defun draw-segment-paths (steps)
  "Assumes a Cairo context has already been set up."
  (flet ((draw-segment (positions color)
           (viz:set-color color)
           (cairo:move-to (point:x (fset:first positions))
                          (point:y (fset:first positions)))
           (fset:do-seq (pos (fset:less-first positions))
             (cairo:line-to (point:x pos) (point:y pos)))
           (cairo:stroke)))
    (cairo:set-line-width 1)
    (cairo:set-line-cap :round)
    (cairo:set-line-join :round)
    (dolist (positions steps)
      (draw-segment positions :dark-highlight))
    (draw-segment (nth 1 steps) :dark-secondary)
    (when (not (endp (cddr steps)))
      (draw-segment (fset:last steps) :blue))))

(defun draw-segments (positions)
  "Assumes a Cairo context has already been set up."
  (viz:set-color :dark-primary)
  (dolist (pos (rest positions))
    (cairo:arc (point:x pos) (point:y pos) 0.5 0 (* 2 pi))
    (cairo:fill-path))
  (viz:set-color :dark-emphasized)
  (cairo:arc (point:x (first positions)) (point:y (first positions))
             0.5 0 (* 2 pi))
  (cairo:fill-path)
  (viz:set-color :orange)
  (cairo:set-line-width 0.1)
  (cairo:set-line-cap :round)
  (cairo:set-line-join :round)
  (cairo:move-to (point:x (first positions))
                 (point:y (first positions)))
  (dolist (pos (cdr positions))
    (cairo:line-to (point:x pos) (point:y pos)))
  (cairo:stroke))
