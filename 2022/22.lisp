(in-package :aoc-2022-22)

(aoc:define-day 75388 182170)


;;; Parsing

(parseq:defrule input ()
    (and (+ map-line) #\newline path)
  (:choose 0 2)
  (:lambda (map-lines path)
    (list (parse-map map-lines) path)))

(parseq:defrule map-line ()
    (and (+ (char " .#")) (? #\newline))
  (:choose 0))

(parseq:defrule path ()
    (and (+ (or aoc:integer-string path-turn)) (? #\newline))
  (:choose 0))

(parseq:defrule path-turn ()
    (char "LR")
  (:lambda (c)
    (ecase c
      (#\L :left)
      (#\R :right))))

(defun parse-map (lines)
  (let* ((max-width (reduce #'max (mapcar #'length lines)))
         (map (make-array (list (length lines) max-width) :initial-element nil)))
    (let ((row 0))
      (dolist (line lines)
        (let ((col 0))
          (dolist (char line)
            (setf (aref map row col)
                  (ecase char
                    (#\  nil)
                    (#\. :tile)
                    (#\# :wall)))
            (incf col)))
        (incf row)))
    map))


;;; Input

(defparameter *example-input* (aoc:input :parse 'input :file "examples/22.txt"))
(defparameter *example-map* (first *example-input*))
(defparameter *example-path* (second *example-input*))
(defparameter *input* (aoc:input :parse 'input))
(defparameter *map* (first *input*))
(defparameter *path* (second *input*))


;;; Part 1

(defparameter *cube* nil)

(defstruct state position orientation)

(defun init-state (map)
  (gmap:gmap :or
             (lambda (col)
               (when (eq (aref map 0 col) :tile)
                 (make-state :position (point:make-point col 0)
                             :orientation #<1 0>)))
             (:index 0)))

(defun update-state (state &key position orientation)
  (with-slots ((old-position position) (old-orientation orientation)) state
    (make-state :position (or position old-position)
                :orientation (or orientation old-orientation))))

;; Because y increases down, we have to swap roation directions.
(defun state-turn (state direction)
  (update-state state :orientation (point:turn (state-orientation state)
                                               (ecase direction
                                                 (:left :right)
                                                 (:right :left)))))

(defun step-forward (map position vector)
  (let ((new-position (point:mod (point:+ position vector)
                                 (point:make-point (array-dimension map 1)
                                                   (array-dimension map 0)))))
    (if (and (symbolp (point:aref map new-position))
             (point:aref map new-position))
        new-position
        (step-forward map new-position vector))))

(defun step-path-forward (map steps state)
  (let ((next-position (step-forward map (state-position state) (state-orientation state))))
    (cond
      ((or (zerop steps)
           (eq :wall (point:aref map next-position)))
       state)
      (t
       (step-path-forward map (1- steps) (update-state state :position next-position))))))

(defun step-path (map state step)
  (if (symbolp step)
      (state-turn state step)
      (step-path-forward map step state)))

(defun follow-path (map path)
  (reduce (alexandria:curry #'step-path map)
          path
          :initial-value (init-state map)))

(defun final-password (state)
  (with-slots (position orientation) state
    (+ (* 1000 (1+ (point:y position)))
       (* 4 (1+ (point:x position)))
       (cond
         ((point:= #< 1  0> orientation) 0)
         ((point:= #< 0  1> orientation) 1)
         ((point:= #<-1  0> orientation) 2)
         ((point:= #< 0 -1> orientation) 3)
         (t (assert nil))))))

(defun get-answer-1 (&optional (map *map*) (path *path*))
  (final-password (follow-path map path)))


;;; Part 2

(defparameter *sector-size* 50)

(defstruct sector-jump
  new-sector
  new-vector
  translation)

;;; #<0 1> (#<0 n> #<n n>) to:
;;; #< 0  1> (#<0 0> #<n 0>)  x1=x0,   y1=0
;;; #< 0 -1> (#<n n> #<0 n>)  x1=n-x0, y1=n
;;; #< 1  0> (#<0 n> #<0 0>)  x1=0,    y1=n-x0
;;; #<-1  0> (#<n 0> #<n n>)  x1=n,    y1=x0
;;; 
;;; #<-1 0> (#<0 0> #<0 n>) to:
;;; #< 0  1> (#<0 0> #<n 0>)  x1=y0,   y1=0
;;; #< 0 -1> (#<n n> #<0 n>)  x1=n-y0, y1=n
;;; #< 1  0> (#<0 n> #<0 0>)  x1=0,    y1=n-y0
;;; #<-1  0> (#<n 0> #<n n>)  x1=n,    y1=y0
;;; 
;;; #<1 0> (#<n 0> #<n n>) to:
;;; #< 0  1> (#<n 0> #<0 0>)  x1=n-y0, y1=0
;;; #< 0 -1> (#<0 n> #<n n>)  x1=y0,   y1=n
;;; #< 1  0> (#<0 0> #<0 n>)  x1=0,    y1=y0
;;; #<-1  0> (#<n n> #<n 0>)  x1=n,    y1=n-y0
;;; 
;;; #<0 -1> (#<0 0> #<n 0>) to:
;;; #< 0  1> (#<n 0> #<0 0>)  x1=n-x0, y1=0
;;; #< 0 -1> (#<0 n> #<n n>)  x1=x0,   y1=n
;;; #<-1  0> (#<n n> #<n 0>)  x1=n,    y1=n-x0
;;; #< 1  0> (#<0 0> #<0 n>)  x1=0,    y1=x0

(defun translation-fn (source-vector target-vector n)
  "N is the highest index on the side of a vector."
  (declare (type (point:point 2) source-vector target-vector)
           (type (integer 1) n))
  (assert (and (or (zerop (point:x source-vector))
                   (zerop (point:y source-vector)))
               (not (and (zerop (point:x source-vector))
                         (zerop (point:y source-vector))))))
  (assert (and (or (zerop (point:x target-vector))
                   (zerop (point:y target-vector)))
               (not (and (zerop (point:x target-vector))
                         (zerop (point:y target-vector))))))
  (let ((coord-fn (if (zerop (point:y source-vector))
                      #'point:y
                      #'point:x)))
    (if (or (point:= source-vector #<0 1>)
            (point:= source-vector #<-1 0>))
        (cond
          ((point:= target-vector #< 0  1>)
           (lambda (old-point)
             (point:make-point (funcall coord-fn old-point)
                               0)))
          ((point:= target-vector #< 0 -1>)
           (lambda (old-point)
             (point:make-point (- n (funcall coord-fn old-point))
                               n)))
          ((point:= target-vector #< 1  0>)
           (lambda (old-point)
             (point:make-point 0
                               (- n (funcall coord-fn old-point)))))
          ((point:= target-vector #<-1  0>)
           (lambda (old-point)
             (point:make-point n
                               (funcall coord-fn old-point))))
          (t (assert nil)))
        (cond
          ((point:= target-vector #< 0  1>)
           (lambda (old-point)
             (point:make-point (- n (funcall coord-fn old-point))
                               0)))
          ((point:= target-vector #< 0 -1>)
           (lambda (old-point)
             (point:make-point (funcall coord-fn old-point)
                               n)))
          ((point:= target-vector #< 1  0>)
           (lambda (old-point)
             (point:make-point 0
                               (funcall coord-fn old-point))))
          ((point:= target-vector #<-1  0>)
           (lambda (old-point)
             (point:make-point n
                               (- n (funcall coord-fn old-point)))))
          (t (assert nil))))))

(defun make-jump-cell (n source-sector source-vector target-sector target-vector)
  (cons (cons source-sector source-vector)
        (make-sector-jump :new-sector target-sector
                          :new-vector target-vector
                          :translation (translation-fn source-vector target-vector n))))

(defun make-jump-cells (n source-sector source-vector target-sector target-vector)
  "Creates both forward and reverse jumps."
  (list (make-jump-cell n source-sector source-vector target-sector target-vector)
        (make-jump-cell n
                        target-sector (point:* target-vector -1)
                        source-sector (point:* source-vector -1))))

(defparameter *example-jumps*
  (fset:convert 'fset:map
                (concatenate 'list
                             (make-jump-cells 3 #<0 1> #<0 -1> #<2 0> #<0 1>)
                             (make-jump-cells 3 #<0 1> #<-1 0> #<3 2> #<0 -1>)
                             (make-jump-cells 3 #<0 1> #<0 1> #<2 2> #<0 -1>)
                             (make-jump-cells 3 #<1 1> #<0 -1> #<2 0> #<1 0>)
                             (make-jump-cells 3 #<1 1> #<0 1> #<2 2> #<1 0>)
                             (make-jump-cells 3 #<2 0> #<1 0> #<3 2> #<-1 0>)
                             (make-jump-cells 3 #<2 1> #<1 0> #<3 2> #<0 1>))))

(defparameter *jumps*
  (fset:convert 'fset:map
                (concatenate 'list
                             (make-jump-cells 49 #<0 2> #<0 -1> #<1 1> #<1 0>)
                             (make-jump-cells 49 #<0 2> #<-1 0> #<1 0> #<1 0>)
                             (make-jump-cells 49 #<0 3> #<-1 0> #<1 0> #<0 1>)
                             (make-jump-cells 49 #<0 3> #<1 0> #<1 2> #<0 -1>)
                             (make-jump-cells 49 #<0 3> #<0 1> #<2 0> #<0 1>)
                             (make-jump-cells 49 #<1 1> #<1 0> #<2 0> #<0 -1>)
                             (make-jump-cells 49 #<1 2> #<1 0> #<2 0> #<-1 0>))))

(defun cell-sector (sector-size position)
  (point:make-point (floor (point:x position) sector-size)
                    (floor (point:y position) sector-size)))


;; HACK HACK Copy and paste of Part 1 functions.

(defun step-forward-cube (sector-size jumps position vector)
  "Returns two values: new position and new vector."
  (let ((sector (cell-sector sector-size position))
        (new-position (point:+ position vector)))
    (if (point:= sector (cell-sector sector-size new-position))
        (values new-position vector)
        (let ((jump (fset:lookup jumps (cons sector vector))))
          (if jump
              (values (point:+ (point:* (sector-jump-new-sector jump) sector-size)
                               (funcall (sector-jump-translation jump)
                                        (point:mod position (point:make-point sector-size sector-size))))
                      (sector-jump-new-vector jump))
              (values new-position vector))))))

(defun step-path-forward-cube (map sector-size jumps steps state)
  (multiple-value-bind (next-position next-orientation)
      (step-forward-cube sector-size jumps (state-position state) (state-orientation state))
    (when (or (minusp (point:x next-position))
              (minusp (point:y next-position)))
      (:printv "s-p-f-c" steps state next-position next-orientation))
    (cond
      ((or (zerop steps)
           (eq :wall (point:aref map next-position)))
       state)
      (t
       (assert (eq :tile (point:aref map next-position)))
       (step-path-forward-cube map sector-size jumps (1- steps)
                               (update-state state
                                             :position next-position
                                             :orientation next-orientation))))))
    

(defun step-path-cube (map sector-size jumps state step)
  (if (symbolp step)
      (state-turn state step)
      (step-path-forward-cube map sector-size jumps step state)))

(defun follow-path-cube (map sector-size jumps path)
  (reduce (alexandria:curry #'step-path-cube map sector-size jumps)
          path
          :initial-value (init-state map)))

(defun get-answer-2 ()
  (final-password (follow-path-cube *map* 50 *jumps* *path*)))


;;; Visualization

(defun print-map (map)
  (dotimes (y (array-dimension map 0))
    (dotimes (x (array-dimension map 1))
      (princ (case (aref map y x)
               (:tile #\.)
               (:wall #\#)
               (t #\ ))))
    (princ #\newline))
  (values))

(defun rotation-for-orientation (orientation)
  (cond
    ((point:= #<1 0> orientation) 0)
    ((point:= #<0 1> orientation) 90)
    ((point:= #<-1 0> orientation) 180)
    ((point:= #<0 -1> orientation) -90)
    (t (assert nil))))

(defun draw-map (map)
  (aoc:draw-array-svg-grid
   map
   :key (lambda (value)
          (case value
            (:wall #\#)
            (:tile '(:color :dark-highlight :character #\.))))))
