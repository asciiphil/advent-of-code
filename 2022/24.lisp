(in-package :aoc-2022-24)

;; Part 1 is 271, but it takes three and a half minutes.
;; Part 2 is 813, but it takes two hours!
(aoc:define-day nil nil)


;;; Data Structures

(defstruct state
  expedition
  (steps 0)
  walls
  blizzards)

(defun update-state (state &key expedition steps walls blizzards)
  (make-state :expedition (or expedition (state-expedition state))
              :steps (or steps (state-steps state))
              :walls (or walls (state-walls state))
              :blizzards (or blizzards (state-blizzards state))))

(defun move-expedition (state new-position new-blizzards)
  (update-state state
                :expedition new-position
                :steps (1+ (state-steps state))
                :blizzards new-blizzards))

(defmethod fset:compare ((s1 state) (s2 state))
  (with-slots ((s1-pos expedition) (s1-steps steps) walls) s1
    (with-slots ((s2-pos expedition) (s2-steps steps)) s2
      (let ((cycle (blizzards-cycle walls)))
        (cond
          ((< (mod s1-steps cycle) (mod s2-steps cycle))
           :less)
          ((< (mod s2-steps cycle) (mod s1-steps cycle))
           :greater)
          (t  ; Equal number of steps
           (cond
             ((point:total< s1-pos s2-pos)
              :less)
             ((point:total< s2-pos s1-pos)
              :greater)
             (t
              :equal))))))))


;;; Parsing

(defun expedition-start (walls)
  (gmap:gmap :or
             (lambda (col)
               (let ((point (point:make-point col 0)))
                 (and (not (fset:lookup walls point))
                      point)))
             (:index 0)))

(defun expedition-end (walls)
  (let ((last-row (point:y (nth-value 1 (point:bbox walls)))))
    (gmap:gmap :or
               (lambda (col)
                 (let ((point (point:make-point col last-row)))
                   (and (not (fset:lookup walls point))
                        point)))
               (:index 0))))

(defun parse-input (lines)
  (iter
    (with blizzards = (fset:empty-map))
    (with walls = (fset:empty-set))
    (for line in lines)
    (for row from 0)
    (iter
      (for char in-string line)
      (for col from 0)
      (for point = (point:make-point col row))
      (ecase char
        (#\.)
        (#\# (fset:adjoinf walls point))
        (#\^ (fset:adjoinf blizzards point (fset:set #< 0 -1>)))
        (#\v (fset:adjoinf blizzards point (fset:set #< 0  1>)))
        (#\< (fset:adjoinf blizzards point (fset:set #<-1  0>)))
        (#\> (fset:adjoinf blizzards point (fset:set #< 1  0>)))))
    (finally (return (make-state :expedition (expedition-start walls)
                                 :walls walls
                                 :blizzards blizzards)))))

(defun blizzard-char (vector)
  (cond
    ((point:= #< 0 -1> vector) #\^)
    ((point:= #< 0  1> vector) #\v)
    ((point:= #<-1  0> vector) #\<)
    ((point:= #< 1  0> vector) #\>)
    (t (assert nil))))

(defun state-char (state point)
  (with-slots (expedition walls blizzards) state
    (cond
      ((point:= point expedition)
       #\E)
      ((fset:lookup walls point)
       #\#)
      ((fset:lookup blizzards point)
       (let ((blizzard-dirs (fset:lookup blizzards point)))
         (if (< 1 (fset:size blizzard-dirs))
             (fset:size blizzard-dirs)
             (blizzard-char (fset:arb blizzard-dirs)))))
      (t
       #\.))))

(defun print-state (state)
  (with-slots (steps walls) state
    (format t "Steps: ~A~%" steps)
    (multiple-value-bind (min-point max-point) (point:bbox walls)
      (dotimes (row (1+ (point:y (point:- max-point min-point))))
        (dotimes (col (1+ (point:x (point:- max-point min-point))))
          (princ (state-char state (point:make-point col row))))
        (princ #\newline)))))


;;; Input

(defparameter *small-example* (parse-input (aoc:input :file "examples/24-small.txt")))
(defparameter *example* (parse-input (aoc:input :file "examples/24.txt")))
(defparameter *initial-state* (parse-input (aoc:input)))


;;; Part 1

(defparameter +moves+ (list #<-1 0> #<0 -1> #<0 0> #<0 1> #<1 0>))

(defparameter *blizzards-at-time* (vector))

(defun wrap-position (walls point)
  (let ((mod-point (point:+ #<1 1> (nth-value 1 (point:bbox walls)))))
    (point:mod point mod-point)))

(defun move-blizzard (walls old-position vector)
  (let ((new-point (point:+ old-position vector)))
    (if (fset:lookup walls new-point)
        (wrap-position walls
                       (point:+ (point:* vector 2)
                                new-point))
        new-point)))

(defun step-blizzards (walls blizzards)
  (fset:reduce (lambda (new-blizzards point blizzards-at-point)
                 (fset:reduce (lambda (inner-new-blizzards vector)
                                (let ((new-point (move-blizzard walls point vector)))
                                  (fset:with inner-new-blizzards
                                             new-point
                                             (fset:with (fset:lookup inner-new-blizzards new-point)
                                                        vector))))
                              blizzards-at-point
                              :initial-value new-blizzards))
               blizzards
               :initial-value (fset:empty-map (fset:empty-set))))

(defun blizzards-cycle (walls)
  (let ((max-point (nth-value 1 (point:bbox walls))))
    (lcm (1- (point:x max-point))
         (1- (point:y max-point)))))

(defun init-blizzards-cache (state)
  (let* ((cycle (blizzards-cycle (state-walls state)))
         (result (make-array cycle :initial-element nil)))
    (setf (svref result (state-steps state))
          (state-blizzards state))
    result))

(defun get-blizzards-at-time (walls steps)
  (if (<= (length *blizzards-at-time*) steps)
      (get-blizzards-at-time walls (mod steps (length *blizzards-at-time*)))
      (or (svref *blizzards-at-time* steps)
          (setf (svref *blizzards-at-time* (:printv :ts steps))
                (step-blizzards walls (get-blizzards-at-time walls (1- steps)))))))

(defun state-option-fn (state)
  (with-slots (expedition walls steps) state
    (let ((new-blizzards (get-blizzards-at-time walls (1+ steps))))
      (mapcar (lambda (new-position)
                (list 1 (move-expedition state new-position new-blizzards)))
              (remove-if (lambda (new-point)
                           (or (not (point:<= #<0 0> new-point))
                               (nth-value 1 (fset:lookup walls new-point))
                               (nth-value 1 (fset:lookup new-blizzards new-point))))
                         (mapcar (alexandria:curry #'point:+ expedition) +moves+))))))

(defun state-equal (s1 s2)
  (eq :equal (fset:compare s1 s2)))

(defun find-expedition-path (state end-pos)
  (multiple-value-bind (path cost)
      (aoc:shortest-path state #'state-option-fn
                         :finishedp (lambda (new-state)
                                      (point:= end-pos (state-expedition new-state)))
                         :heuristic (lambda (new-state)
                                      (point:manhattan-distance end-pos (state-expedition new-state))))
    (values (mapcar #'state-expedition path)
            cost
            (car (last path)))))

(defun get-answer-1 (&optional (state *initial-state*))
  (let ((*blizzards-at-time* (init-blizzards-cache state))
        (end-pos (expedition-end (state-walls state))))
    (nth-value 1 (find-expedition-path state end-pos))))


;;; Part 2

(defun get-answer-2 (&optional (state *initial-state*))
  (let ((*blizzards-at-time* (init-blizzards-cache state))
        (start-pos (state-expedition state))
        (end-pos (expedition-end (state-walls state))))
    (multiple-value-bind (path-out cost-out state-out)
        (find-expedition-path state end-pos)
      (declare (ignore path-out))
      (multiple-value-bind (path-back cost-back state-back)
          (find-expedition-path state-out start-pos)
        (declare (ignore path-back))
        (multiple-value-bind (path-out-2 cost-out-2 state-out-2)
            (find-expedition-path state-back end-pos)
          (declare (ignore path-out-2 state-out-2))
          (+ (:printv cost-out) (:printv cost-back) (:printv cost-out-2)))))))


;;; Visualization

(defun draw-state (state)
  (with-slots (expedition steps walls blizzards) state
    (multiple-value-bind (min-point max-point) (point:bbox walls)
      (let ((span (point:+ #<1 1> (point:- max-point min-point))))
        (aoc:draw-svg-grid
         (point:x span)
         (point:y span)
         (lambda (point)
           (cond
             ((point:= point expedition)
              '(:shape :circle
                :color :green))
             ((fset:lookup walls point)
              '(:shape :square
                :color :dark-primary))
             ((nth-value 1 (fset:lookup blizzards point))
              (let ((bset (fset:lookup blizzards point)))
                (if (= 1 (fset:size bset))
                    (list :shape :triangle
                          :color :dark-secondary
                          :rotation (blizzard-rotation (fset:arb bset)))
                    (list :shape :circle
                          :color :dark-secondary
                          :character (fset:size bset)
                          :font-color :dark-highlight)))))))))))

(defun blizzard-rotation (vector)
  (cond
    ((point:= #< 0 -1> vector) -90)
    ((point:= #< 0  1> vector)  90)
    ((point:= #<-1  0> vector) 180)
    ((point:= #< 1  0> vector)   0)
    (t (assert nil))))
