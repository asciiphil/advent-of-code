(in-package :aoc-2022-19)

;; Part 1 answer is 960, but it takes about a minute.
(aoc:define-day nil 2040)


;;; Data Structures

(defstruct blueprint id recipes)


;;; Parsing

(parseq:defrule blueprint ()
    (and "Blueprint " aoc:integer-string ":" (+ (and " " robot)))
  (:choose 1 3)
  (:lambda (id ingredients)
    (make-blueprint :id id
                    :recipes (reduce #'fset:map-union
                                     (mapcar #'second ingredients)
                                     :initial-value (fset:empty-map)))))

(parseq:defrule robot ()
    (and "Each " material " robot costs " ingredients ".")
  (:choose 1 3)
  (:lambda (robot-type ingredients)
    (fset:map (robot-type ingredients))))

(parseq:defrule ingredients ()
    (and material-quantity (* (and " and " material-quantity)))
  (:lambda (first-material other-materials)
    (fset:reduce #'fset:union (mapcar #'second other-materials)
                 :initial-value first-material)))

(parseq:defrule material-quantity ()
    (and aoc:integer-string " " material)
  (:choose 0 2)
  (:lambda (quantity material) (fset:with (fset:empty-bag) material quantity)))

(parseq:defrule material ()
    (or "ore" "clay" "obsidian" "geode")
  (:function #'string-upcase)
  (:function #'intern))


;;; Input

(defparameter *example* (aoc:input :parse-line 'blueprint :file "examples/19.txt"))
(defparameter *blueprints* (aoc:input :parse-line 'blueprint))


;;; Part 1

(defparameter *ranked-materials* '(geode obsidian clay ore))
(defparameter *keep-state-count* 1024
  "The number of intermediate states to keep for each minute.")

(defstruct state
  (minutes 0 :type (integer 0))         ; Minutes elapsed
  (materials (fset:empty-bag) :type fset:bag)
  (robots (fset:bag 'ore) :type fset:bag)
  (steps (fset:empty-seq) :type fset:seq))

(defmethod fset:compare ((s1 state) (s2 state))
  (fset:compare-slots s1 s2 'minutes 'materials 'robots))

(defun update-state (state &key minutes materials robots steps)
  (make-state :minutes (or minutes (state-minutes state))
              :materials (or materials (state-materials state))
              :robots (or robots (state-robots state))
              :steps (or steps (state-steps state))))

(defun materials< (m1 m2)
  (eq :less
      (fset:reduce (lambda (result material)
                     (if (eq result :equal)
                         (let ((c1 (fset:multiplicity m1 material))
                               (c2 (fset:multiplicity m2 material)))
                           (cond
                             ((< c1 c2) :less)
                             ((= c1 c2) :equal)
                             ((> c1 c2) :greater)))
                         result))
                   *ranked-materials*
                   :initial-value :equal)))

(defun min-materials-produced (total-minutes state)
  (with-slots (minutes materials robots) state
    (fset:reduce (lambda (result material)
                   (fset:with result material
                              (+ (fset:multiplicity materials material)
                                 (* (- total-minutes minutes)
                                    (fset:multiplicity robots material)))))
                 *ranked-materials*
                 :initial-value (fset:empty-bag))))

(defun state-robots-< (total-minutes s1 s2)
  (materials< (min-materials-produced total-minutes s1)
              (min-materials-produced total-minutes s2)))

(defun state-materials-< (s1 s2)
  (materials< (state-materials s1) (state-materials s2)))

(defun can-build-robot-p (state ingredients)
  (fset:subbag? ingredients (state-materials state)))

(defun build-robot (recipes state to-build)
  (with-slots (minutes materials robots steps) state
    (update-state state
                  :minutes (1+ minutes)
                  :materials (fset:bag-sum
                              (if to-build
                                  (fset:bag-difference materials (fset:lookup recipes to-build))
                                  materials)
                              robots)
                  :robots (if to-build
                              (fset:with robots to-build)
                              robots)
                  :steps (fset:with-last steps to-build))))

(defun state-step-minute (recipes state)
  (fset:reduce (lambda (new-states robot ingredients)
                 (if (can-build-robot-p state ingredients)
                     (fset:with new-states (build-robot recipes state robot))
                     new-states))
               recipes
               :initial-value (fset:set (build-robot recipes state nil))))

(defun step-minute (recipes states)
  (fset:reduce (lambda (new-states state)
                 (fset:union new-states (state-step-minute recipes state)))
               states
               :initial-value (fset:empty-set)))

(defun prune-states (total-minutes states)
  (let ((sorted-states (fset:reverse
                        (fset:sort (fset:convert 'fset:seq states)
                                   (alexandria:curry #'state-robots-< total-minutes)))))
    (fset:subseq sorted-states 0 *keep-state-count*)))

(defun get-final-state (states)
  (let ((sorted-states (fset:sort (fset:convert 'fset:seq states)
                                  #'state-materials-<)))
    (fset:last sorted-states)))

(defun find-best-state (recipes total-minutes states)
  (if (<= total-minutes (state-minutes (aoc:arb-element states)))
      (get-final-state states)
      (find-best-state recipes total-minutes
                       (prune-states total-minutes (step-minute recipes states)))))

(defun try-blueprint (minutes blueprint)
  (find-best-state (blueprint-recipes blueprint)
                   minutes
                   (fset:set (make-state))))

;; Each blueprint's calculation is independent of the others, so we'll
;; examine them in parallel.
(defun get-answer-1 (&optional (blueprints *blueprints*) (minutes 24))
  (gmap:gmap :sum
             (lambda (blueprint best-state)
               (* (blueprint-id blueprint)
                  (fset:multiplicity (state-materials best-state) 'geode)))
             (:list blueprints)
             (:list (lparallel:pmap 'list
                                    (alexandria:curry #'try-blueprint minutes)
                                    blueprints))))

(aoc:given 1
  (= 33 (get-answer-1 *example*)))


;;; Part 2

(defun get-answer-2 (&optional (blueprints *blueprints*) (minutes 32))
  (fset:reduce #'*
               (lparallel:pmap 'list
                               (lambda (blueprint)
                                 (fset:multiplicity (state-materials (try-blueprint minutes blueprint))
                                                    'geode))
                               (subseq blueprints 0 3))))
