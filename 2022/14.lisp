(in-package :aoc-2022-14)

(aoc:define-day 692 31706)


;;; Parsing

(parseq:defrule path ()
    (and point (* (and " -> " point)))
  (:lambda (point other-points)
    (cons point (mapcar #'second other-points))))

(parseq:defrule point ()
    (and aoc:integer-string "," aoc:integer-string)
  (:choose 0 2)
  (:function #'point:make-point))


;;; Input

(defparameter *rock-paths* (aoc:input :parse-line 'path))


;;; Part 1

(defun all-rocks (rock-paths)
  (reduce (lambda (rocks path)
            (rocks-in-path path rocks))
          rock-paths
          :initial-value (fset:empty-set)))

(defun rocks-in-path (rock-path &optional (other-rocks (fset:empty-set)))
  (labels ((rocks-r (starts ends points)
             (if (endp ends)
                 points
                 (rocks-r (cdr starts)
                          (cdr ends)
                          (add-rock-segment points (car starts) (car ends))))))
    (rocks-r rock-path (cdr rock-path) (fset:with other-rocks (first rock-path)))))

(defun add-rock-segment (other-points start end)
  (let ((step (point:map-coordinates #'signum (list (point:- end start)))))
    (labels ((segment-r (current-start points)
               (if (point:= current-start end)
                   (fset:with points current-start)
                   (segment-r (point:+ current-start step)
                              (fset:with points current-start)))))
      (segment-r start other-points))))


(defun init-obstacles (rocks)
  (let ((max-point (nth-value 1 (point:bbox rocks))))
    (let ((obstacles (make-array (list (+ 2 (point:y max-point)) 1000))))
      (fset:do-set (point rocks)
        (setf (point:aref obstacles point) 1))
      obstacles)))

(defun print-obstacles (obstacles)
  ;; We don't just call `aoc:[print-array-braille` directly because we
  ;; want to limit the horizontal span of the output to only the area that
  ;; has stuff in it.
  (let ((min-x (array-dimension obstacles 1))
        (max-x 0))
    (dotimes (y (array-dimension obstacles 0))
      (dotimes (x (array-dimension obstacles 1))
        (when (plusp (aref obstacles y x))
          (when (< x min-x)
            (setf min-x x))
          (when (< max-x x)
            (setf max-x x)))))
    (aoc:print-braille (1+ (- max-x min-x))
                       (array-dimension obstacles 0)
                       (lambda (point)
                         (plusp (aref obstacles (point:y point) (+ min-x (point:x point))))))))

(defun print-obstacles (obstacles &optional rocks)
  ;; We don't just call `aoc:draw-array-svg-grid` directly because we
  ;; want to limit the horizontal span of the output to only the area that
  ;; has stuff in it.
  (let ((min-x (array-dimension obstacles 1))
        (max-x 0))
    (dotimes (y (array-dimension obstacles 0))
      (dotimes (x (array-dimension obstacles 1))
        (when (plusp (aref obstacles y x))
          (when (< x min-x)
            (setf min-x x))
          (when (< max-x x)
            (setf max-x x)))))
    (aoc:draw-svg-grid (1+ (- max-x min-x))
                       (array-dimension obstacles 0)
                       (lambda (img-point)
                         (let ((point (point:make-point (+ min-x (point:x img-point)) (point:y img-point))))
                           (when (plusp (point:aref obstacles point))
                             (if rocks
                                 (if (fset:lookup rocks point)
                                     #\#
                                     '(:color :yellow :shape :circle :character #\o))
                                 t)))))))


(defparameter +sand-start+ #<500 0>)
(defparameter +sand-moves+ (list #<0 1> #<-1 1> #<1 1>))

(defun sand-step (freefall obstacles position)
  "Returns the new position of the sand particle.

  NOTE: It's possible for the particle to not move."
  (or (find-if (lambda (new-pos)
                 (if (<= (array-dimension obstacles 0) (point:y new-pos))
                     freefall
                     (zerop (point:aref obstacles new-pos))))
               (mapcar (alexandria:curry #'point:+ position) +sand-moves+))
      position))

(defun add-sand (freefall obstacles)
  "FREEFALL should be true if freefall is allowed and false otherwise.

  Returns two values: First, the new values of obstacles with the sand in
  its final location.  Second, true if the sand came to rest and false if
  it fell out of frame."
  (labels ((add-r (position)
             (let ((new-pos (sand-step freefall obstacles position)))
               (cond
                 ((point:= position new-pos)
                  (setf (point:aref obstacles position) 1)
                  (values obstacles t))
                 ((<= (array-dimension obstacles 0) (point:y position))
                  (assert freefall)
                  (values obstacles nil))
                 (t
                  (add-r new-pos))))))
    (add-r +sand-start+)))

(defun place-all-sand (rocks &key freefall)
  "Returns a set of points representing all sand positions."
  (labels ((place-r (obstacles)
             (multiple-value-bind (new-obstacles sand-stopped)
                 (add-sand freefall obstacles)
               (if (and sand-stopped
                        (zerop (point:aref new-obstacles +sand-start+)))
                   (place-r new-obstacles)
                   new-obstacles))))
    (place-r (init-obstacles rocks))))


(defun get-answer-1 (&key (rock-paths *rock-paths*) print (freefall t))
  (let* ((rocks (all-rocks rock-paths))
         (obstacles (place-all-sand rocks :freefall freefall)))
    (when print
      (print-obstacles obstacles rocks))
    (- (count 1 (aoc:flatten-array obstacles))
       (fset:size rocks))))


;;; Part 2

(defun flood-fill-obstacles (obstacles position)
  (when (and (< (point:y position) (array-dimension obstacles 0))
             (zerop (point:aref obstacles position)))
    (setf (point:aref obstacles position) 1)
    (dolist (vector +sand-moves+ obstacles)
      (flood-fill-obstacles obstacles (point:+ position vector))))
  obstacles)

(defun get-answer-2 (&key (rock-paths *rock-paths*) print)
  (let* ((rocks (all-rocks rock-paths))
         (obstacles (flood-fill-obstacles (init-obstacles rocks)
                                          #<500 0>)))
    (when print
      (print-obstacles obstacles rocks))
    (- (count 1 (aoc:flatten-array obstacles))
       (fset:size rocks))))
