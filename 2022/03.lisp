(in-package :aoc-2022-03)

(aoc:define-day 8233 2821)


;;; Parsing

(parseq:defrule compartment ()
    (+ item)
  (:lambda (&rest items) (fset:convert 'fset:set items)))

(parseq:defrule item ()
    (or lc-item uc-item))

(parseq:defrule lc-item ()
    (char "a-z")
  (:lambda (c) (1+ (- (char-code c) (char-code #\a)))))

(parseq:defrule uc-item ()
    (char "A-Z")
  (:lambda (c) (+ 27 (- (char-code c) (char-code #\A)))))

(defun parse-rucksack (items)
  (let ((compartment-size (/ (length items) 2)))
    (list (parseq:parseq 'compartment (subseq items 0 compartment-size))
          (parseq:parseq 'compartment (subseq items compartment-size)))))


;;; Input

(defparameter *rucksacks* (mapcar #'parse-rucksack (aoc:input)))


;;; Part 1

(defun find-common-item (rucksack)
  (destructuring-bind (compartment1 compartment2) rucksack
    (let ((intersection (fset:intersection compartment1 compartment2)))
      (assert (= 1 (fset:size intersection)))
      (fset:arb intersection))))

(defun get-answer-1 (&optional (rucksacks *rucksacks*))
  (reduce #'+ (mapcar #'find-common-item rucksacks)))


;;; Part 2

(defun find-badge (rucksacks)
  (let ((badge-set
          (reduce #'fset:intersection
                  (mapcar (lambda (compartments) (apply #'fset:union compartments))
                          rucksacks))))
    (assert (= 1 (fset:size badge-set)))
    (fset:arb badge-set)))

(defun map-groups (function rucksacks)
  (if (endp rucksacks)
      nil
      (cons (funcall function (subseq rucksacks 0 3))
            (map-groups function (subseq rucksacks 3)))))

(defun get-answer-2 (&optional (rucksacks *rucksacks*))
  (reduce #'+ (map-groups #'find-badge rucksacks)))
