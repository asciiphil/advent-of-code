(in-package :aoc-2022-08)

(aoc:define-day 1870 517440)


;;; Parsing

(parseq:defrule tree-grid ()
    (+ tree-row)
  (:lambda (&rest grid)
    (make-array (list (length grid) (length (first grid)))
                :element-type '(integer 0 9)
                :initial-contents grid)))

(parseq:defrule tree-row ()
    (and (+ tree) (? #\newline))
  (:choose 0))

(parseq:defrule tree ()
    digit
  (:function #'digit-char-p))


;;; Input

(defparameter *example* (aoc:input :file "examples/08.txt" :parse 'tree-grid))
(defparameter *trees* (aoc:input :parse 'tree-grid))


;;; Part 1

(defun visible-trees (trees)
  "Returns a set of coordinates giving the trees that are visible from
  outside the grid."
  (labels ((examine-line (x0 y0 delta visible)
             (let ((start (point:make-point
                           (cond
                             ((minusp (point:x delta)) (1- (array-dimension trees 1)))
                             ((zerop  (point:x delta)) x0)
                             ((plusp  (point:x delta)) 0))
                           (cond
                             ((minusp (point:y delta)) (1- (array-dimension trees 0)))
                             ((zerop  (point:y delta)) y0)
                             ((plusp  (point:y delta)) 0)))))
               (collect-viewable-trees trees
                                       delta
                                       (point:+ start delta)
                                       (let ((max-height (point:aref trees start)))
                                         (lambda (pos)
                                           (and (< max-height (point:aref trees pos))
                                                (setf max-height (point:aref trees pos)))))
                                       (fset:with visible start)))))
    (let ((visible (fset:empty-set)))
      (dotimes (y (array-dimension trees 0))
        (setf visible (examine-line 0 y #<1 0> visible))
        (setf visible (examine-line 0 y #<-1 0> visible)))
      (dotimes (x (array-dimension trees 1))
        (setf visible (examine-line x 0 #<0 1> visible))
        (setf visible (examine-line x 0 #<0 -1> visible)))
      visible)))

(defun collect-viewable-trees (trees delta start-pos viewable-fn viewable-trees)
  (let ((min-bound #<-1 -1>)
        (max-bound (apply #'point:make-point (reverse (array-dimensions trees)))))
    (if (point:< min-bound start-pos max-bound)
        (collect-viewable-trees trees
                                delta
                                (point:+ start-pos delta)
                                viewable-fn
                                (if (funcall viewable-fn start-pos)
                                    (fset:with viewable-trees start-pos)
                                    viewable-trees))
        viewable-trees)))

(defun get-answer-1 (&optional (trees *trees*))
  (fset:size (visible-trees trees)))


;;; Part 2

(defun trees-visible-from-treehouse (trees treehouse-pos)
  (let ((treehouse-height (point:aref trees treehouse-pos)))
    (reduce (lambda (visible delta)
              (collect-viewable-trees trees
                                      delta
                                      (point:+ treehouse-pos delta)
                                      (let ((view-blocked nil))
                                        (lambda (pos)
                                          (and (not view-blocked)
                                               (or (< (point:aref trees pos) treehouse-height)
                                                   (setf view-blocked t)))))
                                      visible))
            (list #<-1 0> #<0 -1> #<1 0> #<0 1>)
            :initial-value (fset:empty-set))))

(defun treehouse-view-area (trees treehouse-pos)
  "Returns two values forming a bounding box for the treehouse's viewing area."
  (point:bbox (trees-visible-from-treehouse trees treehouse-pos)))

(defun treehouse-scenic-score (trees treehouse-pos)
  (multiple-value-bind (min-point max-point) (treehouse-view-area trees treehouse-pos)
    (* (- (point:x treehouse-pos) (point:x min-point))
       (- (point:y treehouse-pos) (point:y min-point))
       (- (point:x max-point) (point:x treehouse-pos))
       (- (point:y max-point) (point:y treehouse-pos)))))

(defun find-max-scenic-score (trees)
  (labels ((find-r (best-pos best-score pos)
             (cond
               ((<= (array-dimension trees 1) (point:x pos))
                (find-r best-pos best-score (point:make-point 0 (1+ (point:y pos)))))
               ((<= (array-dimension trees 0) (point:y pos))
                (values best-pos best-score))
               (t
                (let ((score (treehouse-scenic-score trees pos))
                      (next-pos (point:+ pos #<1 0>)))
                  (if (< best-score score)
                      (find-r pos score next-pos)
                      (find-r best-pos best-score next-pos)))))))
    (find-r nil 0 #<0 0>)))

(defun get-answer-2 (&optional (trees *trees*))
  (nth-value 1 (find-max-scenic-score trees)))
