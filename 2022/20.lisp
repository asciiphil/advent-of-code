(in-package :aoc-2022-20)

(aoc:define-day 872 5382459262696)


;;; Input

(defparameter *example* (vector 1 2 -3 3 -2 0 4))
(defparameter *numbers* (coerce (aoc:input :parse-line 'aoc:integer-string)
                                'vector))

;;; Part 1

(defun init-pointers (numbers)
  (let ((forward-pointers (make-array (length numbers)))
        (reverse-pointers (make-array (length numbers))))
    (dotimes (i (length numbers))
      (setf (svref forward-pointers i) (mod (1+ i) (length forward-pointers))
            (svref reverse-pointers i) (mod (1- i) (length reverse-pointers))))
    (values forward-pointers reverse-pointers)))

(defun follow-pointers-direction (pointers step index count)
  (if (zerop count)
      index
      (follow-pointers-direction pointers step (svref pointers index) (+ step count))))

(defun follow-pointers (forward reverse index count)
  (cond
    ((minusp count)
     (follow-pointers-direction reverse 1 index
                                (1- (- (1+ (mod (1- (- count)) (1- (length reverse))))))))
    ((zerop count)
     index)
    ((plusp count)
     (follow-pointers-direction forward -1 index
                                (1+ (mod (1- count) (1- (length forward))))))
    (t
     (assert nil))))

(defun move-number! (numbers forward reverse index)
  (if (zerop (svref numbers index))
      (values forward reverse)
      (let ((last-index (svref reverse index))
            (next-index (svref forward index)))
        ;; Remove from list
        (psetf (svref forward last-index) next-index
               (svref reverse next-index) last-index)
        (let ((destination-index (follow-pointers forward reverse index (svref numbers index))))
          (psetf (svref forward destination-index) index
                 (svref reverse index) destination-index
                 (svref forward index) (svref forward destination-index)
                 (svref reverse (svref forward destination-index)) index))
        (values forward reverse))))

(defun mix! (numbers forward reverse)
  (dotimes (i (length numbers))
    (move-number! numbers forward reverse i))
  (values forward reverse))

(defun apply-mixing (numbers forward)
  (let ((result (make-array (length numbers))))
    (do ((i (position 0 numbers)
            (svref forward i))
         (count 0 (1+ count)))
        ((<= (length numbers) count))
      (setf (svref result count) (svref numbers i)))
    result))

(defun get-answer-1 (&optional (numbers *numbers*))
  (multiple-value-bind (forward reverse) (init-pointers numbers)
    (mix! numbers forward reverse)
    (let ((mixed-numbers (apply-mixing numbers forward)))
      (+ (svref mixed-numbers (mod 1000 (length mixed-numbers)))
         (svref mixed-numbers (mod 2000 (length mixed-numbers)))
         (svref mixed-numbers (mod 3000 (length mixed-numbers)))))))


;;; Part 2

(defun get-answer-2 (&optional (numbers *numbers*))
  (let ((big-numbers (make-array (length numbers))))
    (dotimes (i (length big-numbers))
      (setf (svref big-numbers i)
            (* 811589153 (svref numbers i))))
    (multiple-value-bind (forward reverse) (init-pointers big-numbers)
      (dotimes (i 10)
        (mix! big-numbers forward reverse))
      (let ((mixed-numbers (apply-mixing big-numbers forward)))
        (+ (svref mixed-numbers (mod 1000 (length mixed-numbers)))
           (svref mixed-numbers (mod 2000 (length mixed-numbers)))
           (svref mixed-numbers (mod 3000 (length mixed-numbers))))))))
