(in-package :aoc-2022-05)

(aoc:define-day "VPCDMSLWJ" "TPWCGNCCG")


;;; Data structures

(defun print-stacks (stacks)
  (let ((max-height (fset:reduce #'max (fset:image #'fset:size stacks))))
    (aoc:draw-svg-grid
     (1- (fset:size stacks))
     (1+ max-height)
     (lambda (point)
       (let ((col (point:x point)))
         (if (= (point:y point) max-height)
             (list :color :dark-background
                   :font-color :dark-primary
                   :character (1+ col))
             (fset:lookup (fset:lookup stacks (1+ col))
                          (- max-height (point:y point) 1)))))
     :cell-size 16)))

(defstruct move qty from to)


;;; Parsing

(parseq:defrule input ()
    (and stacks #\newline (+ move))
  (:choose 0 2))

(defun make-stacks (rows labels)
  (reduce (lambda (result row)
            (reduce (lambda (inner-result pair)
                      (destructuring-bind (label crate) pair
                        (if crate
                            (fset:with inner-result label
                                       (fset:with-first (fset:lookup inner-result label)
                                         crate))
                            inner-result)))
                    (mapcar #'list labels row)
                    :initial-value result))
          rows
          :initial-value (reduce (lambda (result label)
                                   (fset:with result label (fset:empty-seq)))
                                 labels
                                 :initial-value (fset:empty-seq))))

(parseq:defrule stacks ()
    (and (+ stack-row) stack-labels)
  (:function #'make-stacks))

(parseq:defrule stack-labels ()
    (and (* " ") aoc:integer-string (* (and (+ " ") aoc:integer-string)) (* " ") (? #\newline))
  (:lambda (space1 first-num other-nums space2 space3)
    (declare (ignore space1 space2 space3))
    (cons first-num (mapcar #'second other-nums))))

(parseq:defrule stack-row ()
    (and stack-slot (* (and " " stack-slot)) (* " ") (? #\newline))
  (:lambda (first-slot other-slots space1 space2)
    (declare (ignore space1 space2))
    (cons first-slot (mapcar #'second other-slots))))

(parseq:defrule stack-slot ()
    (or crate empty-crate))

(parseq:defrule crate ()
    (and "[" char "]")
  (:choose 1))

(parseq:defrule empty-crate ()
    "   "
  (:constant nil))

(parseq:defrule move ()
    (and "move " aoc:integer-string " from " aoc:integer-string " to " aoc:integer-string (? #\newline))
  (:choose 1 3 5)
  (:lambda (qty from to)
    (make-move :qty qty :from from :to to)))


;;; Input

(defparameter *example-input* (aoc:input :file "examples/05.txt" :parse 'input))
(defparameter *example-stacks* (first *example-input*))
(defparameter *example-moves* (second *example-input*))
(defparameter *input* (aoc:input :parse 'input))
(defparameter *stacks* (first *input*))
(defparameter *moves* (second *input*))


;;; Part 1

(defun move-stack (stacks move)
  (with-slots (qty from to) move
    (if (zerop qty)
        stacks
        (let* ((old-from (fset:lookup stacks from))
               (old-to (fset:lookup stacks to))
               (new-from (fset:less-last old-from))
               (new-to (fset:with-last old-to (fset:last old-from)))
               (new-stacks (fset:with (fset:with stacks from new-from)
                                      to new-to)))
          (move-stack new-stacks (make-move :qty (1- qty) :from from :to to))))))

(defun make-all-moves (stacks moves &optional (move-fn #'move-stack))
  (reduce move-fn moves :initial-value stacks))

(defun get-answer-1 (&optional (stacks *stacks*) (moves *moves*))
  (let ((final-stacks (make-all-moves stacks moves)))
    (fset:convert 'string (fset:image #'fset:last (fset:filter #'identity final-stacks)))))


;;; Part 2

(defun move-stack-9001 (stacks move)
  (with-slots (qty from to) move
    (let* ((old-from (fset:lookup stacks from))
           (old-to (fset:lookup stacks to))
           (breakpoint (- (fset:size old-from) qty))
           (new-from (fset:subseq old-from 0 breakpoint))
           (new-to (fset:concat old-to (fset:subseq old-from breakpoint)))
           (new-stacks (fset:with (fset:with stacks from new-from)
                                  to new-to)))
      new-stacks)))

(defun get-answer-2 (&optional (stacks *stacks*) (moves *moves*))
  (let ((final-stacks (make-all-moves stacks moves #'move-stack-9001)))
    (fset:convert 'string (fset:image #'fset:last (fset:filter #'identity final-stacks)))))
