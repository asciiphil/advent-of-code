(in-package :aoc-2022-10)

;; Part 2 answer is "EFUGLPAP", but no automated results on that until I
;; implement OCR.
(aoc:define-day 15020 nil)


;;; Data Structures

(defstruct comms
  instructions
  (ip 0)
  (x 1)
  (cycles 0)
  (instruction-cycles 0)
  (pixels (make-array '(6 40) :element-type 'bit :initial-element 0)))

(defparameter +instruction-cycles+
  (fset:map ('addx 2)
            ('noop 1)))


;;; Parsing

(parseq:defrule instruction ()
    (or addx noop))

(parseq:defrule addx ()
    (and "addx " aoc:integer-string)
  (:choose 1)
  (:lambda (value) (list 'addx value)))

(parseq:defrule noop ()
    "noop"
  (:constant '(noop)))


;;; Input

(defparameter *example*
  (coerce (aoc:input :parse-line 'instruction
                     :file "examples/10.txt")
          'vector))
(defparameter *instructions*
  (coerce (aoc:input :parse-line 'instruction)
          'vector))


;;; Part 1

(defun tick (comms)
  (declare (type comms comms))
  (with-slots (instructions ip x cycles instruction-cycles pixels)
      comms
    (assert (< ip (length instructions)))
    (when (<= (abs (- x (mod cycles 40)))
              1)
      (setf (aref pixels (truncate cycles 40) (mod cycles 40))
            1))
    (incf cycles)
    (incf instruction-cycles)
    (when (<= (fset:lookup +instruction-cycles+
                           (first (svref instructions ip)))
              instruction-cycles)
      (when (eq 'addx (first (svref instructions ip)))
        (incf x (second (svref instructions ip))))
      (incf ip)
      (setf instruction-cycles 0)))
  comms)

(defun run-program (instructions &optional breakpoints)
  (let ((comms (make-comms :instructions instructions))
        (bp-vals nil))
    (do ()
        ((<= (length instructions) (comms-ip comms)))
      (when (and breakpoints
                 (= (1+ (comms-cycles comms)) (car breakpoints)))
        (push (comms-x comms) bp-vals)
        (pop breakpoints))
      (tick comms))
    (values comms (reverse bp-vals))))

(defun get-answer-1 (&optional (instructions *instructions*))
  (let ((bps '(20 60 100 140 180 220)))
    (multiple-value-bind (cpu bp-vals) (run-program instructions bps)
      (declare (ignore cpu))
      (gmap:gmap :sum
                 #'*
                 (:list bps)
                 (:list bp-vals)))))


;;; Part 2

(defun print-crt (comms)
  (aoc:draw-array-svg-grid (comms-pixels comms) :key #'plusp))

(defun get-answer-2 (&optional (instructions *instructions*))
  (print-crt (run-program instructions)))
