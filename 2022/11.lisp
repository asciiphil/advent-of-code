(in-package :aoc-2022-11)

(aoc:define-day 54036 13237873355)


;;; Data Structures

(defstruct monkey
  number
  items
  operation
  test
  modulus
  (inspection-count 0))

(defmethod print-object ((monkey monkey) stream)
  (print-unreadable-object (monkey stream :type t)
    (with-slots (number items inspection-count) monkey
      (format stream "~A: ~A (~A)" number items inspection-count))))

(defun monkey-update (monkey &key items inspection-count)
  (with-slots (number (old-items items) operation test modulus (old-inspection-count inspection-count))
      monkey
    (make-monkey :number number
                 :items (or items old-items)
                 :operation operation
                 :test test
                 :modulus modulus
                 :inspection-count (or inspection-count old-inspection-count))))

(defun monkey-add-item (monkey item)
  (monkey-update monkey :items (fset:with-last (monkey-items monkey) item)))

(defun monkey-clear-items (monkey)
  "Assumes all items were inspected, so increases inspection count."
  (monkey-update monkey
                 :items (fset:empty-seq)
                 :inspection-count (+ (monkey-inspection-count monkey)
                                      (fset:size (monkey-items monkey)))))


;;; Parsing

(parseq:defrule monkeys ()
    (+ monkey)
  (:lambda (&rest monkeys)
    (fset:convert 'fset:seq monkeys)))

(parseq:defrule monkey ()
    (and monkey-number items operation test (? #\newline))
  (:lambda (number items operation test _)
    (declare (ignore _))
    (make-monkey :number number
                 :items (fset:convert 'fset:seq items)
                 :operation operation
                 :modulus (first test)
                 :test (second test))))

(parseq:defrule monkey-number ()
    (and "Monkey " aoc:integer-string ":" (? #\newline))
  (:choose 1))

(parseq:defrule items ()
    (and "  Starting items: " (aoc:comma-list aoc:integer-string) (? #\newline))
  (:choose 1))

(parseq:defrule operation ()
    (and "  Operation: new = " operand " " operator " " operand (? #\newline))
  (:choose 1 3 5)
  (:lambda (op1 oper op2)
    (lambda (old)
      (funcall oper
               (if (symbolp op1)
                   old
                   op1)
               (if (symbolp op2)
                   old
                   op2)))))

(parseq:defrule operand ()
    (or varname aoc:integer-string))

(parseq:defrule varname ()
    (+ (char "a-z"))
  (:string)
  (:function #'string-upcase)
  (:function #'intern))

(parseq:defrule operator ()
    (char "-+/*")
  (:string)
  (:function #'intern)
  (:function #'symbol-function))

(parseq:defrule test ()
    (and test-divisor (+ throw))
  (:lambda (divisor throws)
    (list
     divisor
     (lambda (item)
       (let ((divisible (zerop (mod item divisor))))
         (second (find divisible throws :key #'first)))))))

(parseq:defrule test-divisor ()
    (and "  Test: divisible by " aoc:integer-string (? #\newline))
  (:choose 1))

(parseq:defrule throw ()
    (and "    If " true-or-false ": throw to monkey " aoc:integer-string (? #\newline))
  (:choose 1 3))

(parseq:defrule true-or-false ()
    (or "true" "false")
  (:lambda (tf) (string= tf "true")))


;;; Input

(defparameter *example* (aoc:input :parse 'monkeys :file "examples/11.txt"))
(defparameter *monkeys* (aoc:input :parse 'monkeys))


;;; Part 1

(defparameter *relief-divisor* 3)

(defun monkey-round (monkeys &optional (count 1))
  (declare ((integer 0) count))
  (labels ((round-r (new-monkeys index)
             (if (<= (fset:size monkeys) index)
                 new-monkeys
                 (round-r (monkey-turn new-monkeys index)
                          (1+ index)))))
    (if (zerop count)
        monkeys
        (monkey-round (round-r monkeys 0)
                      (1- count)))))

(defun monkey-turn (monkeys index)
  "Has the monkey at position INDEX inspect all of its items"
  (let ((monkey (fset:lookup monkeys index)))
    (fset:with (fset:reduce (alexandria:curry #'monkey-inspect monkey)
                            (monkey-items monkey)
                            :initial-value monkeys)
               index
               (monkey-clear-items monkey))))

(defun monkey-inspect (monkey monkeys item)
  (with-slots (operation test) monkey
    (let* ((worry-lcm (fset:reduce #'lcm monkeys :key #'monkey-modulus))
           (worry-level (mod (truncate (funcall operation item)
                                       *relief-divisor*)
                             worry-lcm))
           (target-monkey (funcall test worry-level)))
      (fset:with monkeys target-monkey
                 (monkey-add-item (fset:lookup monkeys target-monkey) worry-level)))))

(defun monkey-business (monkeys)
  (let ((monkeys-by-inspections (fset:sort monkeys #'>= :key #'monkey-inspection-count)))
    (* (monkey-inspection-count (fset:lookup monkeys-by-inspections 0))
       (monkey-inspection-count (fset:lookup monkeys-by-inspections 1)))))

(defun get-answer-1 (&optional (monkeys *monkeys*))
  (monkey-business (monkey-round monkeys 20)))


;;; Part 2

(defun get-answer-2 (&optional (monkeys *monkeys*))
  (let ((*relief-divisor* 1))
    (monkey-business (monkey-round monkeys 10000))))
