(in-package :aoc-2022-02)

(aoc:define-day 9651 nil)


;;; Parsing

(defparameter *move-map*
  (fset:map (#\A :rock)
            (#\B :paper)
            (#\C :scissors)
            (#\X :rock)
            (#\Y :paper)
            (#\Z :scissors)))

(defparameter *outcome-map*
  (fset:map (#\X :lose)
            (#\Y :tie)
            (#\Z :win)))

(parseq:defrule round-by-move ()
    (and move " " move)
  (:choose 0 2))

(parseq:defrule move ()
    (char "ABCXYZ")
  (:lambda (char)
    (fset:lookup *move-map* char)))

(parseq:defrule round-by-outcome ()
    (and move " " outcome)
  (:choose 0 2))

(parseq:defrule outcome ()
    (char "XYZ")
  (:lambda (char)
    (fset:lookup *outcome-map* char)))


;;; Input

(defparameter *rounds-by-move* (aoc:input :parse-line 'round-by-move))
(defparameter *rounds-by-outcome* (aoc:input :parse-line 'round-by-outcome))


;;; Part 1

(defparameter *wins-against*
  (fset:map (:rock :scissors)
            (:scissors :paper)
            (:paper :rock)))

(defparameter *loses-against*
  (fset:image (lambda (a b) (values b a)) *wins-against*))

(defparameter *scores*
  (fset:map (:rock 1)
            (:paper 2)
            (:scissors 3)
            (:lose 0)
            (:tie 3)
            (:win 6)))

(defun play-round (your-move opponent-move)
  (cond
    ((eq your-move opponent-move)
     :tie)
    ((eq (fset:lookup *wins-against* your-move)
         opponent-move)
     :win)
    (t
     :lose)))

(defun score-round (your-move opponent-move)
  (+ (fset:lookup *scores* your-move)
     (fset:lookup *scores* (play-round your-move opponent-move))))

(defun score-rounds (rounds &optional (score-fn #'score-round))
  (reduce #'+ (mapcar (lambda (moves)
                        (destructuring-bind (opponent-move your-move) moves
                          (funcall score-fn your-move opponent-move)))
                      rounds)))

(defun get-answer-1 (&optional (rounds *rounds-by-move*))
  (score-rounds rounds))


;;; Part 2

(defun get-move (outcome opponent-move)
  (ecase outcome
    (:tie opponent-move)
    (:lose (fset:lookup *wins-against* opponent-move))
    (:win (fset:lookup *loses-against* opponent-move))))

(defun score-round-by-outcome (outcome opponent-move)
  (score-round (get-move outcome opponent-move)
               opponent-move))

(defun get-answer-2 (&optional (rounds *rounds-by-outcome*))
  (score-rounds rounds #'score-round-by-outcome))
