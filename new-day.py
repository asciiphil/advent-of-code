#!/usr/bin/env python3

import datetime
import json
import pathlib


BADGES_FILE = pathlib.Path(__file__).parent.joinpath('badges.json')
# AoC releases at midnight, EST.  It's always EST, since EDT is well over
# by the time AoC starts.  So we can fudge things by hardcoding the UTC
# offset.
AOC_TIMEZONE = datetime.timezone(datetime.timedelta(hours=-5))

with open(BADGES_FILE) as badges_file:
    badges_dict = json.load(badges_file)

## day
today = datetime.datetime.now(tz=AOC_TIMEZONE)
if today.month < 12:
    # Only update the file in December.
    exit(0)
elif today.day > 25:
    badges_dict['day'] = 25
else:
    badges_dict['day'] = today.day

with open(BADGES_FILE, 'w') as output:
    json.dump(badges_dict, output, sort_keys=True, indent=2)
